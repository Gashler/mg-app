<?php

class Post extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
        //
    ];

    // Don't forget to fill this array
    protected $table = 'posts';
    protected $fillable = [
        'image',
        'title',
        'name',
        'description',
        'body',
        'publish_date',
        'public',
        'disabled',
        'user_id'
    ];

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    public function getFormattedDateAttribute() {
        if ($this->publish_date > 0) $date = $this->publish_date;
        else $date = $this->created_at;
        return date('M d Y', strtotime($date));
    }

    protected $appends = [
        'model',
        'formatted_date'
    ];

    /****************************
    * Relationships
    *****************************/

    public function categories()
    {
        return $this->belongsToMany(PostCategory::class, 'category_post_pivot');
    }

}
