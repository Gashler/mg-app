<?php

class Stat extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
        // 'title' => 'required'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($stat) { }); // returning false will cancel the operation
        static::created(function($stat) { });
        static::updating(function($stat) { }); // returning false will cancel the operation
        static::updated(function($stat) { });
        static::saving(function($stat) { });  // returning false will cancel the operation
        static::saved(function($stat) { });
        static::deleting(function($stat) { }); // returning false will cancel the operation
        static::deleted(function($stat) { });
    }

    // Don't forget to fill this array
    protected $table = 'stats';
    protected $fillable = [
        'date',
        'f_orgasm',
        'sex',
        'm_orgasm',
        'quality_time',
        'romantic_date'
    ];

    protected $appends = [
        'model'
    ];

    protected $with = [

    ];

    /****************************
     * Relationships
     ****************************/



    /****************************
     * Attributes
     ****************************/

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    /****************************
     * Methods
     ****************************/
}
