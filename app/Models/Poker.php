<?php

class Poker extends Eloquent
{

    // Add your validation rules here
    public static $rules = [

    ];

    public static $authorized = [
        'data'
    ];

    // Don't forget to fill this array
    protected $table = 'poker';
    protected $fillable = [
        'f_data',
        'm_data'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $appends = [
        'model'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'f_data' => 'array',
        'm_data' => 'array'
    ];

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

}
