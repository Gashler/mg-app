<?php

class State extends Eloquent {

    // Add your validation rules here
    public static $rules = [
        'abbr' => 'required|alpha|digits:2',
        'name' => 'required|alpha'
    ];

    // Don't forget to fill this array
    protected $fillable = ['abbr','name'];

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    public function getNewRecordAttribute() {
        return (strtotime($this->created_at) >= (time() - Config::get('site.new_time_frame') ))?true:false;
    }

    protected $appends = [
        'model',
        'new_record'
    ];

}
