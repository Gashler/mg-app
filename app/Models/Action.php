<?php

class Action extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
        // 'title' => 'required'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($action) { }); // returning false will cancel the operation
        static::created(function($action) { });
        static::updating(function($action) { }); // returning false will cancel the operation
        static::updated(function($action) { });
        static::saving(function($action) { });  // returning false will cancel the operation
        static::saved(function($action) { });
        static::deleting(function($action) { }); // returning false will cancel the operation
        static::deleted(function($action) { });
    }

    protected $table = 'actions';

    // Don't forget to fill this array
    protected $fillable = [
        'category_id',
        'description',
        'rating_id'
    ];

    protected $appends = [
        'new_record',
        'rating',
        'model'
    ];

    /****************************
     * Relationships
     ****************************/

     public function uvents()
     {
         return $this->belongsToMany('Uvent');
     }

    /****************************
     * Attributes
     ****************************/

    public function getNewRecordAttribute() {
        return (strtotime($this->created_at) >= (time() - Cache::get('settings.new_time_frame') ))?true:false;
    }

    public function getRatingAttribute() {
        if ($this->level == 1) return 'G';
        if ($this->level == 2) return 'PG';
        if ($this->level == 3) return 'PG-13';
        if ($this->level == 4) return 'R';
        if ($this->level == 5) return 'NC-17';
    }

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    /****************************
     * Methods
     ****************************/

}
