<?php

class Game extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
    // 'title' => 'required'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($game) { }); // returning false will cancel the operation
        static::created(function($game) { });
        static::updating(function($game) { }); // returning false will cancel the operation
        static::updated(function($game) { });
        static::saving(function($game) { });  // returning false will cancel the operation
        static::saved(function($game) { });
        static::deleting(function($game) { }); // returning false will cancel the operation
        static::deleted(function($game) { });
    }

    // Don't forget to fill this array
    protected $table = 'games';
    protected $fillable = [
        'player_id',
        'user_id',
        'name',
        'description',
        'game_id',
        'private',
        'public',
        'width',
    ];

    protected $appends = [
        'model',
        'new_record'
    ];

    /****************************
     * Relationships
     ****************************/

    public function characters() {
        return $this->hasMany('Character')->where('type', 'npc');
    }

    public function maps() {
        return $this->morphMany('Map', 'morph');
    }

    public function logics() {
        return $this->hasMany('Logic');
    }

    public function narrations() {
        return $this->hasMany('Narration');
    }

    public function medias() {
        return $this->belongsToMany('Media');
    }

    public function objects() {
        return $this->hasMany('Object');
    }

    public function players() {
        return $this->hasMany('Player');
    }

    public function timers() {
        return $this->hasMany('Timer');
    }

    public function user() {
        return $this->belongsTo('User');
    }

    public function uvents() {
        return $this->morphMany('Uvent', 'morph');
    }

    /****************************
     * Attributes
     ****************************/

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    public function getNewRecordAttribute() {
        return (strtotime($this->created_at) >= (time() - Cache::get('settings.new_time_frame') ))?true:false;
    }

    /****************************
     * Methods
     ****************************/

}
