<?php

class Dialog extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
    // 'title' => 'required'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($dialog) { }); // returning false will cancel the operation
        static::created(function($dialog) { });
        static::updating(function($dialog) { }); // returning false will cancel the operation
        static::updated(function($dialog) { });
        static::saving(function($dialog) { });  // returning false will cancel the operation
        static::saved(function($dialog) { });
        static::deleting(function($dialog) { }); // returning false will cancel the operation
        static::deleted(function($dialog) { });
    }
    // Don't forget to fill this array
    protected $table = 'dialogs';
    protected $fillable = [
        'body',
        'character_id',
        'effect_id',
        'group_id',
        'mid',
        'order',
    ];

    protected $appends = [
        'body_short',
        'model'
    ];

    /****************************
     * Relationships
     ****************************/

    public function effects() {
        return $this->morphMany('Effect', 'subject');
    }

    /****************************
     * Attributes
     ****************************/

    public function getBodyShortAttribute() {
        $str = limitWords($this->body, 9);
        return '(' . $this->id . ') ' . $str;
    }

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    /****************************
     * Methods
     ****************************/

}
