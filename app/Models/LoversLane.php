<?php

class LoversLane extends Eloquent
{

    // Add your validation rules here
    public static $rules = [

    ];

    // Don't forget to fill this array
    protected $table = 'lovers_lane';
    protected $fillable = [
        'account_id',
        'multiple_devices',
        'data'
    ];

    public static $authorized = [
        'multiple_devices',
        'data'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    protected $appends = [
        'model'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'data' => 'array'
    ];

    public function getModelAttribute()
    {
        return get_class($this);
    }

}
