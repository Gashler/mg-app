<?php

class Level extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
    // 'title' => 'required'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($level) { }); // returning false will cancel the operation
        static::created(function($level) { });
        static::updating(function($level) { }); // returning false will cancel the operation
        static::updated(function($level) { });
        static::saving(function($level) { });  // returning false will cancel the operation
        static::saved(function($level) { });
        static::deleting(function($level) { }); // returning false will cancel the operation
        static::deleted(function($level) { });
    }
    // Don't forget to fill this array
    protected $table = 'levels';
    protected $fillable = array('name');

    protected $appends = [
        'model',
        'new_record'
    ];

    /****************************
     * Relationships
     ****************************/



    /****************************
     * Attributes
     ****************************/

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    public function getNewRecordAttribute() {
        return (strtotime($this->created_at) >= (time() - Cache::get('settings.new_time_frame') ))?true:false;
    }

    /****************************
     * Methods
     ****************************/

}
