<?php

class Media extends Eloquent
{

 // Don't forget to fill this array
  protected $table = 'medias';

    protected $fillable = array('type','url','user_id','title','description','reps','disabled','height','width','size','extension');

    protected $appends = [
        'new_record',
        'owner',
        'xxs',
        'xs',
        'sm',
        'md',
        'lg',
        'xl',
        'original',
        'largest',
        'model'
    ];

    public function tags()
    {
        return $this->morphMany('TagModel', 'taggable');
    }

    public function getOwnerAttribute() {
        $user = User::find($this->user_id);
        if(isset($user)) return $user->name;
    }

    public function getNewRecordAttribute() {
        return (strtotime($this->created_at) >= (time() - Cache::get('settings.new_time_frame') ))?true:false;
    }

    public function getXxsAttribute() {
        if ($this->width >= 50) return '/uploads/' . $this->url . '-xxs.' . $this->extension;
        else return '/img/default-xxs.png';
    }

    public function getXsAttribute() {
        if ($this->width >= 100) return '/uploads/' . $this->url . '-xs.' . $this->extension;
        else return '/img/default-xs.png';
    }

    public function getSmAttribute() {
        if ($this->width >= 200) return '/uploads/' . $this->url . '-sm.' . $this->extension;
        else return '/img/default-sm.png';
    }

    public function getMdAttribute() {
        if ($this->width >= 400) return '/uploads/' . $this->url . '-md.' . $this->extension;
        else return '/img/default-md.png';
    }

    public function getXlAttribute() {
        if ($this->width >= 750) return '/uploads/' . $this->url . '-lg.' . $this->extension;
        else return '/img/default-xl.png';
    }

    public function getLgAttribute() {
        if ($this->width >= 1200) return '/uploads/' . $this->url . '-xl.' . $this->extension;
        else return '/img/default-lg.png';
    }

    public function getLargestAttribute() {
        if($this->xl != '/img/default-xl.png' || $this->width == 0) return $this->xl;
        if($this->lg != '/img/default-lg.png') return $this->lg;
        if($this->md != '/img/default-md.png') return $this->md;
        if($this->sm != '/img/default-sm.png') return $this->sm;
        if($this->xs != '/img/default-xs.png') return $this->xs;
        if($this->xxs != '/img/default-xxs.png') return $this->xxs;
        if($this->width <= 1200) return '/uploads/' . $this->url . '.' . $this->extension;
    }

    public function getOriginalAttribute() {
        if($this->url == '') return $this->xl;
        return '/uploads/' . $this->url . '.' . $this->extension;
    }

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

}
