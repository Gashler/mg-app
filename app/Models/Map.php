<?php

class Map extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
    // 'title' => 'required'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($map) { }); // returning false will cancel the operation
        static::created(function($map) { });
        static::updating(function($map) { }); // returning false will cancel the operation
        static::updated(function($map) { });
        static::saving(function($map) { });  // returning false will cancel the operation
        static::saved(function($map) { });
        static::deleting(function($map) { }); // returning false will cancel the operation
        static::deleted(function($map) { });
    }
    // Don't forget to fill this array
    protected $table = 'maps';
    protected $fillable = [
        'name',
        'morph_type',
        'morph_id'
    ];

    protected $appends = [
        'model',
        'new_record'
    ];

    /****************************
    * Relationships
     ****************************/

    public function rows() {
        return $this->hasMany('Row')->orderBy('order')->orderBy('id');
    }

    /****************************
     * Attributes
     ****************************/

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    public function getNewRecordAttribute() {
        return (strtotime($this->created_at) >= (time() - Cache::get('settings.new_time_frame') ))?true:false;
    }

    /****************************
     * Methods
     ****************************/

}
