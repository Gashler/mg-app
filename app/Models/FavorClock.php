<?php

class FavorClock extends Eloquent
{
    // Add your validation rules here
    public static $rules = [
        // 'title' => 'required'
    ];

    protected $table = 'favor_clocks';

    // Don't forget to fill this array
    protected $fillable = [
        'active',
        'increasing',
        'ower',
        'server',
        'time_owed'
    ];

    protected $appends = [
        'time'
    ];

    /****************************
     * Relationships
     ****************************/


    /****************************
     * Attributes
     ****************************/

     public function getTimeAttribute()
     {
         $timeSinceChange = 0;
         if ($this->active) {
            $timeSinceChange = (time() - strtotime($this->updated_at));
         }
         if ($this->increasing) {
             $this->time_owed += $timeSinceChange;
         } else {
             if ($this->time_owed - $timeSinceChange > 0) {
                 $this->time_owed -= $timeSinceChange;
            } else {
                $this->time_owed += $timeSinceChange;
                if ($this->server == 'm') {
                    $ower = 'f';
                } else {
                    $ower = 'm';
                }
                $this->update([
                    'increasing' => true,
                    'ower' => $ower
                ]);
            }
         }
         return $this->time_owed;
     }

    /****************************
     * Methods
     ****************************/

}
