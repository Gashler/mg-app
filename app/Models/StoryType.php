<?php

class StoryType extends Eloquent {

    // Add your validation rules here
    public static $rules = [
    // 'title' => 'required'
    ];

    // Don't forget to fill this array
    protected $table = 'story_types';
    protected $fillable = array('name');
    protected $appends = [
        'model'
    ];

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

}
