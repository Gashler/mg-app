<?php

class Verb extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
    // 'title' => 'required'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($verb) { }); // returning false will cancel the operation
        static::created(function($verb) { });
        static::updating(function($verb) { }); // returning false will cancel the operation
        static::updated(function($verb) { });
        static::saving(function($verb) { });  // returning false will cancel the operation
        static::saved(function($verb) { });
        static::deleting(function($verb) { }); // returning false will cancel the operation
        static::deleted(function($verb) { });
    }
    // Don't forget to fill this array
    protected $table = 'verbs';
    protected $fillable = array('name');

    protected $appends = [
        'model',
        'new_record'
    ];

    /****************************
     * Relationships
     ****************************/



    /****************************
     * Attributes
     ****************************/

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    public function getNewRecordAttribute() {
        return (strtotime($this->created_at) >= (time() - Cache::get('settings.new_time_frame') ))?true:false;
    }

    /****************************
     * Methods
     ****************************/
}
