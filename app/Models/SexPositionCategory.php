<?php

class SexPositionCategory extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
        // 'title' => 'required'
    ];

    protected $table = 'sex_position_categories';

    // Don't forget to fill this array
    protected $fillable = [
        'name'
    ];

    protected $appends = [
        'model'
    ];

    /****************************
     * Relationships
     ****************************/

     public function sexPositions()
     {
         return $this->belongsToMany(SexPosition::class, 'sexposition_category_pivot');
     }

    /****************************
     * Attributes
     ****************************/

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    /****************************
     * Methods
     ****************************/

}
