<?php

class Appointment extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
    // 'title' => 'required'
    ];
    public static $authorized = [
        'date_end',
        'date_start',
        'description',
        'in_charge',
        'name',
        'recurring',
        'turn',
        'sun',
        'mon',
        'tue',
        'wed',
        'thu',
        'fri',
        'sat',
        'time_end',
        'time_start',
        'recurring_interval'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($appointment) { }); // returning false will cancel the operation
        static::created(function($appointment) { });
        static::updating(function($appointment) { }); // returning false will cancel the operation
        static::updated(function($appointment) { });
        static::saving(function($appointment) { });  // returning false will cancel the operation
        static::saved(function($appointment) { });
        static::deleting(function($appointment) { }); // returning false will cancel the operation
        static::deleted(function($appointment) { });
    }

    // Don't forget to fill this array
    protected $table = 'appointments';
    protected $fillable = [
        'account_id',
        'date_end',
        'date_start',
        'description',
        'in_charge',
        'name',
        'recurring',
        'start',
        'turn',
        'sun',
        'mon',
        'tue',
        'wed',
        'thu',
        'fri',
        'sat',
        'time_end',
        'time_start',
        'recurring_interval'
    ];
    protected $appends = [
        'date_start_js',
        'model',
        'start',
        'end'
    ];

    /****************************
     * Relationships
     ****************************/

    public function actions()
    {
        return $this->belongsToMany('Action');
    }

    public function objects()
    {
        return $this->morphMany('Object', 'morph');
    }

    /****************************
     * Attributes
     ****************************/

    public function getDateStartJsAttribute()
    {
        return date('Y,m,d', strtotime($this->date_start));
    }

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    public function getStartAttribute()
    {
        return [
            'hour' => date('h', strtotime($this->time_start)),
            'minute' => date('i', strtotime($this->time_start)),
            'am' => date('a', strtotime($this->time_start))
        ];
    }

    public function getEndAttribute()
    {
        return [
            'hour' => date('h', strtotime($this->time_end)),
            'minute' => date('i', strtotime($this->time_end)),
            'am' => date('a', strtotime($this->time_end))
        ];
    }

    /****************************
     * Methods
     ****************************/

}
