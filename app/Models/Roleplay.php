<?php

class Roleplay extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
        'title' => 'required'
    ];

     // Don't forget to fill this array
    protected $table = 'roleplays';
    protected $fillable = array(
        'title',
        'his_name',
        'her_name',
        'story_type',
        'location',
        'his_personality',
        'her_personality',
        'his_role',
        'her_role',
        'first_line_sayer',
        'first_line',
        'description',
        'user_id',
        'public',
        'disabled'
    );
    protected $appends = [
        'model'
    ];

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

}
