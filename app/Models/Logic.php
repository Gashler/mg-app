<?php

class Logic extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
    // 'title' => 'required'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($logic) { }); // returning false will cancel the operation
        static::created(function($logic) { });
        static::updating(function($logic) { }); // returning false will cancel the operation
        static::updated(function($logic) { });
        static::saving(function($logic) { });  // returning false will cancel the operation
        static::saved(function($logic) { });
        static::deleting(function($logic) { }); // returning false will cancel the operation
        static::deleted(function($logic) { });
    }
    // Don't forget to fill this array
    protected $table = 'logics';
    protected $fillable = [
        'name',
        'morph_type',
        'morph_id'
    ];

    protected $appends = [
        'description',
        'model'
    ];

    /****************************
     * Relationships
     ****************************/

    public function triggers() {
        return $this->hasMany('Trigger');
    }

    public function effects() {
        return $this->hasMany('Effect');
    }

    /****************************
     * Attributes
     ****************************/

    public function getDescriptionAttribute() {
        if($this->name !== null && $this->name !== '') {
            return $this->name;
        }
        elseif(count($this->effects) > 0) {
            return $this->effects[0]->name;
        }
        else {
            return 'New Logic';
        }
    }

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    /****************************
     * Methods
     ****************************/

}
