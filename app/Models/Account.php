<?php

use Illuminate\Database\Eloquent\Model;

// use Laravel\Cashier\Billable;

class Account extends Eloquent
{
    // use Billable;

    protected $table = 'accounts';

    public static $rules = [
        //
    ];

    // Don't forget to fill this array
    protected $fillable = [
        'trial_ends_at',
        'wp_user_id',
        'wp_subscribed',
        'bypass_subscription',
        'stripe_id',
        'card_brand',
        'card_last_four'
    ];

    // custom attributes
    protected $appends = [
        'subscribed',
        'on_trial'
    ];

    ##############################################################################################
    # Relationships
    ##############################################################################################

    public function entries()
    {
        return $this->hasMany(Entry::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function poker()
    {
        return $this->hasOne(Poker::class);
    }

    public function loversLane()
    {
        return $this->hasOne(LoversLane::class);
    }

    public function oauthLogins()
    {
        return $this->hasMany(OauthLogin::class);
    }

    public function favorClock()
    {
        return $this->hasOne(FavorClock::class);
    }

    ##############################################################################################
    # Custom Attributes
    ##############################################################################################

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    public function getSubscribedAttribute()
    {
        // if (
        //     date('Y-m-d h:i:s') < $this->trial_ends_at
        //     || $this->subscribed('main')
        //     || $this->wp_subscribed
        //     || $this->bypass_subscription
        // ) {
        //     return true;
        // }
        // return false;
        return true;
    }

    public function getOnTrialAttribute()
    {
        if (date('Y-m-d h:i:s') < $this->trial_ends_at) {
            return true;
        }
        // if ($this->onTrial('main')) {
        //     return true;
        // }
        return false;
    }

    /**
     * Hack as of 2020-07-13, because checks throughout the system check for a stripe_id as a way of
     * verifying that the user has a subscription
     *
     * @return void
     */
    public function getStripeIdAttribute()
    {
        return true;
    }
}
