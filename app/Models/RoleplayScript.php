<?php

class RoleplayScript extends Eloquent
{
    // Add your validation rules here
    public static $rules = [
      // 'title' => 'required'
    ];

    // Don't forget to fill this array
    protected $table = 'roleplay_scripts';
    protected $fillable = [
        'name',
        'description',
        'body',
        'user_id',
        'public',
        'icon',
        'disabled'
    ];

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }
}
