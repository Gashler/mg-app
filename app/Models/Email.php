<?php

class Email extends Eloquent
{
    protected $table = 'emails';
    protected $fillable = [
        'template_id',
        'key',
        'subject',
        'body',
    ];
    protected $with = [
        'template'
    ];

    /**
    * Relationships
    */

    public function template()
    {
        return $this->belongsTo(EmailTemplate::class);
    }

    public function campaigns()
    {
        return $this->belongsToMany(Campaign::class);
    }
}
