<?php

class Rating extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
      // 'title' => 'required'
    ];

     // Don't forget to fill this array
    protected $table = 'ratings';
    protected $fillable = [
        'name',
        'description'
    ];

    protected $appends = [
        'model'
    ];

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

}
