<?php

class Role extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
      // 'title' => 'required'
    ];

     // Don't forget to fill this array
    protected $table = 'roles';
    protected $fillable = array('name','disabled');
    protected $appends = [
        'model'
    ];

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

}
