<?php

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Eloquent implements AuthenticatableContract, CanResetPasswordContract {

    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';
    // public $timestamps = false;

    public static $rules = [
        'email' => 'email|unique:users'
    ];

    // Don't forget to fill this array
    protected $fillable = [
        'disabled',
        'first_name',
        'last_name',
        'logins',
        'sponsor_id',
        'spouse_id',
        'email',
        'campaign_emails_received',
        'key',
        'login',
        'password',
        'gender',
        'dob',
        'public_id',
        'received_campaign_email_at',
        'role_id',
        'money',
        'created_at',
        'updated_at',
        'account_id',
        'unsubscribed',
        'verified',
        'wp_user_id',
        'need_password'
    ];

    public static $authorized = [
        'first_name',
        'last_name',
        'email',
        'password',
        'dob',
        'need_password',
        'verified'
    ];

    // custom attributes
    protected $appends = [
        'accusative',
        'name',
        'husband',
        'model',
        'new_record',
        'nominative',
        'possessive',
        'role_name',
        'wife',
        'spouse_type'
    ];

    protected $with = [
        'account'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'campaign_emails_received' => 'array'
    ];

    ##############################################################################################
    # Relationships
    ##############################################################################################

    public function account()
    {
        return $this->belongsTo(Account::class);
    }

    public function activeGames() {
        return $this->hasMany('Game')->where('account_id', $this->account_id)->orWhere('player_id', $this->user_id);
    }

    public function addresses()
    {
        return $this->hasMany('Address', 'addressable_id', 'id');
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public function campaigns()
    {
        return $this->belongsToMany(Campaign::class);
    }

    public function games()
    {
        return $this->hasMany('Game')->where('user_id', $this->id)->orWhere('account_id', $this->account_id);
    }

    public function sponsor() {
        return $this->belongsTo('User');
    }

    public function spouse() {
        return $this->belongsTo('User', 'spouse_id', 'id');
    }

    public function appointments() {
        return $this->hasMany('Appointment', 'account_id', 'account_id');
    }

    public function stats() {
        return $this->hasMany('Stat', 'account_id', 'account_id');
    }

    public function services() {
        return $this->hasMany(Service::class);
    }

    public function purchases() {
        return $this->belongsToMany(Service::class, 'purchases_pivot');
    }

    public function role() {
        return $this->belongsTo('Role');
    }

    public function roleplays() {
        return $this->hasMany('Roleplay', 'user_id');
    }

    public function oauthLogins()
    {
        return $this->hasMany(OauthLogin::class);
    }

    public function goal()
    {
        return $this->hasOne(Goal::class);
    }

    ##############################################################################################
    # Custom Attributes
    ##############################################################################################

    public function setPasswordAttribute($password)
    {
        $this->attributes["password"] = Hash::make($password);
    }

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    public function getNominativeAttribute() {
        if($this->gender == 'm') return 'he';
        else return 'she';
    }

    public function getAccusativeAttribute() {
        if($this->gender == 'm') return 'him';
        else return 'her';
    }

    public function getNameAttribute() {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function getPossessiveAttribute() {
        if($this->gender == 'm') return 'his';
        else return 'her';
    }

    public function getNewRecordAttribute() {
        return (strtotime($this->created_at) >= (time() - Config::get('site.new_time_frame') ))?true:false;
    }

    public function getRoleNameAttribute() {
        if (isset($this->role->name))
        {
            return $this->role->name;
        }
        return false;
    }

    public function getHusbandAttribute() {
        if (auth()->check()) {
            if (auth()->user()->gender == 'm') {
                return auth()->user()->first_name;
            }
            return auth()->user()->spouse->first_name;
        }
    }

    public function getWifeAttribute() {
        if (auth()->check()) {
            if (auth()->user()->gender == 'f') {
                return auth()->user()->first_name;
            }
            if (!auth()->user()->hasRole(['Admin'])) {
                return auth()->user()->spouse->first_name;
            }
        }
    }

    public function getSpouseTypeAttribute() {
        if ($this->gender == 'm') {
            return 'Husband';
        }
        return 'Wife';
    }

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password', 'remember_token');

    ##############################################################################################
    # Password reminder methods
    ##############################################################################################

    public function getRememberToken()
    {
        return $this->remember_token;
    }

    public function setRememberToken($value)
    {
        $this->remember_token = $value;
    }

    public function getRememberTokenName()
    {
        return 'remember_token';
    }

    ##############################################################################################
    # role and permissions
    ##############################################################################################

    public function hasRole($key) {
        if (!is_array($key)) return false;
        foreach ($key as $role){
            if ($this->role->name === $role){
                return true;
            }
        }
        return false;
    }

}
