<?php

class Name extends Eloquent
{
    protected $table = 'names';
    protected $fillable = [
        'name',
        'gender',
        'locale'
    ];
    protected $appends = [
        'model'
    ];
    
    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }
}
