<?php

class Character extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
    // 'title' => 'required'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($character) { }); // returning false will cancel the operation
        static::created(function($character) { });
        static::updating(function($character) { }); // returning false will cancel the operation
        static::updated(function($character) { });
        static::saving(function($character) { });  // returning false will cancel the operation
        static::saved(function($character) { });
        static::deleting(function($character) { }); // returning false will cancel the operation
        static::deleted(function($character) { });
    }

    // Don't forget to fill this array
    protected $table = 'characters';
    protected $fillable = [
        'description',
        'first_name',
        'game_id',
        'gender',
        'hidden',
        'icon',
        'last_name',
        'mid',
        'place_id',
        'player_id',
        'type',
        'user_id',
    ];
    protected $appends = [
        'model',
        'name',
        'new_record',
    ];

    /****************************
     * Relationships
     ****************************/

    public function container() {
        return $this->morphTo('Object', 'morph');
    }

    public function narrations() {
        return $this->morphMany('Narration', 'morph');
    }

    public function logics() {
        return $this->morphMany('Logic', 'morph');
    }

    public function medias() {
        return $this->belongsToMany('Media');
    }

    public function objects() {
        return $this->morphMany('Object', 'morph');
    }

    public function place() {
        return $this->belongsTo('Place');
    }

    public function customProperties() {
        return $this->morphMany('Property', 'morph');
    }

    public function defaultProperties() {
        return $this->morphMany('Property', 'morph')->whereNull('morph_id');
    }

    public function timers() {
        return $this->morphMany('Timer', 'morph');
    }

    /****************************
     * Attributes
     ****************************/

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    public function getNewRecordAttribute() {
        return (strtotime($this->created_at) >= (time() - Cache::get('settings.new_time_frame') ))?true:false;
    }

    public function getNameAttribute() {
        return $this->first_name . ' ' . $this->last_name;
    }

    /****************************
     * Methods
     ****************************/

}
