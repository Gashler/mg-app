<?php

class Goal extends Eloquent
{

    // Add your validation rules here
    public $rules = [
        // 'title' => 'required'
    ];

    // Don't forget to fill this array
    protected $table = 'goals';
    protected $fillable = [
        'sex',
        'orgasm',
        'quality_time',
        'romantic_date'
    ];

    public static $authorized = [
        'sex',
        'orgasm',
        'quality_time',
        'romantic_date'
    ];

    protected $appends = [
        'model'
    ];

    protected $with = [

    ];

    /****************************
     * Relationships
     ****************************/



    /****************************
     * Attributes
     ****************************/

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    /****************************
     * Methods
     ****************************/
}
