<?php

class QuestionCategory extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
        // 'title' => 'required'
    ];

    protected $table = 'question_categories';

    // Don't forget to fill this array
    protected $fillable = [
        'name'
    ];

    protected $appends = [
        'model'
    ];

    /****************************
     * Relationships
     ****************************/

     public function questions()
     {
         return $this->hasMany(Question::class);
     }

    /****************************
     * Attributes
     ****************************/

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    /****************************
     * Methods
     ****************************/

}
