<?php

class Trigger extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
    // 'title' => 'required'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($trigger) { }); // returning false will cancel the operation
        static::created(function($trigger) { });
        static::updating(function($trigger) { }); // returning false will cancel the operation
        static::updated(function($trigger) { });
        static::saving(function($trigger) { });  // returning false will cancel the operation
        static::saved(function($trigger) { });
        static::deleting(function($trigger) { }); // returning false will cancel the operation
        static::deleted(function($trigger) { });
    }
    // Don't forget to fill this array
    protected $table = 'triggers';
    protected $fillable = [
        'act_type',
        'distance',
        'logic_id',
        'operator_1',
        'operator_2',
        'scope',
        'subject_id',
        'subject_type',
        'type',
        'value','type',
        'verb'
    ];

    protected $appends = [
        'model',
        'name'
    ];

    /****************************
     * Relationships
     ****************************/



    /****************************
     * Attributes
     ****************************/

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    public function getNameAttribute() {
        $txt = "If ";
        if($this->type == 'act') {
            if($this->act_type == 'give') {
                $txt .= "given ";
            }
            else {
                "acted upon by a ";
            }
            $txt .= "unit of a type that is ";
        }
        if($this->type == 'proximity') {
            $txt .= "within a proximity that is ";
        }
        if($this->type == 'verb') {
            $txt .= "acted upon by a verb that is ";
        }
        $txt .= $this->filterOperator($this->operator_1) . " to ";
        if($this->type == 'proximity') {
            $txt .= $this->distance . " space";
            if($this->distance > 1) {
                $txt .= "s";
            }
            $txt .= " of ";
        }
        if($this->type == 'act' || $this->type == 'proximity') {
            if($this->scope == 'any') {
                $txt .= 'any ';
            }
            if($this->scope == 'current') {
                $txt .= 'the current ';
            }
            $txt .= '"' . $this->subject_type . '"';
        }
        if($this->type == 'verb') {
            $txt .= '"' . strtolower($this->verb) . '"';
        }
        if($this->type !== 'verb' && $this->scope == 'specific') {
            $txt .= " with an ID that is " . $this->filterOperator($this->operator_2) . " " . $this->subject_id;
        }
        return $txt;
    }

    /****************************
     * Methods
     ****************************/

     public function filterOperator($operator) {
         if($operator == '==') return '=';
         if($operator == '!=') return '&ne;';
     }


}
