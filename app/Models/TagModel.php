<?php

class TagModel extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
    // 'name' => 'required'
    ];

    // Don't forget to fill this array
    protected $table = 'tags';
    protected $fillable = array('name','taggable_id','service_category_id','disabled');

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    public function getNewRecordAttribute() {
        return (strtotime($this->created_at) >= (time() - Config::get('site.new_time_frame') ))?true:false;
    }

    protected $appends = [
        'model',
        'new_record'
    ];

}
