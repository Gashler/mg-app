<?php

class PropertyOption extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
        // 'title' => 'required'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($propertyOption) { }); // returning false will cancel the operation
        static::created(function($propertyOption) { });
        static::updating(function($propertyOption) { }); // returning false will cancel the operation
        static::updated(function($propertyOption) { });
        static::saving(function($propertyOption) { });  // returning false will cancel the operation
        static::saved(function($propertyOption) { });
        static::deleting(function($propertyOption) { }); // returning false will cancel the operation
        static::deleted(function($propertyOption) { });
    }
    // Don't forget to fill this array
    protected $table = 'propertyOptions';
    protected $fillable = [
        'description',
        'name',
        'property_id',
        'value',
    ];

    protected $appends = [
        'model'
    ];

    /****************************
     * Relationships
     ****************************/



    /****************************
     * Attributes
     ****************************/

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    /****************************
     * Methods
     ****************************/

}
