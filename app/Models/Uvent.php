<?php

class Uvent extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
    // 'title' => 'required'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($uvent) { }); // returning false will cancel the operation
        static::created(function($uvent) { });
        static::updating(function($uvent) { }); // returning false will cancel the operation
        static::updated(function($uvent) { });
        static::saving(function($uvent) { });  // returning false will cancel the operation
        static::saved(function($uvent) { });
        static::deleting(function($uvent) { }); // returning false will cancel the operation
        static::deleted(function($uvent) { });
    }
    // Don't forget to fill this array
    protected $table = 'uvents';
    protected $fillable = [
        'result_type',
        'result_id',
        'date_start',
        'date_end',
        'name',
        'description',
        'turn',
        'morph_type',
        'morph_id'
    ];
    protected $appends = [
        'model',
        'new_record',
    ];

    public function actions()
    {
        return $this->belongsToMany('Action');
    }

    // public function characters()
    // {
    // 	return $this->belongsToMany('Character');
    // }

    public function effects()
    {
        return $this->morphMany('Effect', 'morph');
    }

    public function medias()
    {
        return $this->belongsToMany('Media');
    }

    public function options()
    {
        return $this->morphMany('Option', 'morph');
    }

    public function objects()
    {
        return $this->morphMany('Object', 'morph');
    }

    public function places()
    {
        return $this->belongsToMany('Place');
    }

    public function timer()
    {
        return $this->morphOne('Timer', 'morph');
    }

    /****************************
     * Relationships
     ****************************/



    /****************************
     * Attributes
     ****************************/

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    public function getNewRecordAttribute() {
        return (strtotime($this->created_at) >= (time() - Cache::get('settings.new_time_frame') ))?true:false;
    }

    /****************************
     * Methods
     ****************************/

}
