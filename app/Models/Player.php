<?php

class Player extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
        // 'title' => 'required'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($player) { }); // returning false will cancel the operation
        static::created(function($player) { });
        static::updating(function($player) { }); // returning false will cancel the operation
        static::updated(function($player) { });
        static::saving(function($player) { });  // returning false will cancel the operation
        static::saved(function($player) { });
        static::deleting(function($player) { }); // returning false will cancel the operation
        static::deleted(function($player) { });
    }

    // Don't forget to fill this array
    protected $table = 'players';
    protected $fillable = [
        'character_id',
        'game_id',
        'user_id'
    ];
    protected $appends = [
        // 'character',
        'game_id_attribute',
        'model'
    ];
    protected $with = [
        'user'
    ];

    /****************************
     * Relationships
     ****************************/

    public function characters() {
        return $this->hasMany('Character', 'player_id', 'id');
    }

    public function user() {
        return $this->belongsTo('User');
    }

    /****************************
     * Attributes
     ****************************/

    public function getCharacterAttribute() {
        if (!isset($this->character_id)) {
            return $this->characters->first();
        }
        else {
            foreach($this->characters as $character) {
                if($character->id == $this->character_id) {
                    return $character;
                }
            }
        }
    }

    public function getGameIdAttributeAttribute() {
        return $this->game_id;
    }

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    /****************************
     * Methods
     ****************************/

}
