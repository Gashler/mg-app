<?php

class Service extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
        //
    ];
    public static $authorized = [
        'name',
        'description',
        'price'
    ];

    // Don't forget to fill this array
    protected $table = 'services';
    protected $fillable = [
        'description',
        'name',
        'performer',
        'price'
    ];
    protected $appends = [
        'model'
    ];

    /******************
     * Relationships
     ******************/

    /******************
     * Attributes
     ******************/

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

}
