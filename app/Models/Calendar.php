<?php

class Calendar extends Eloquent
{

 // Add your validation rules here
 public static $rules = [
  // 'title' => 'required'
 ];

 // Don't forget to fill this array
 protected $table = 'calendars';
    protected $fillable = array('name','description','date','public','subscribers','members','editors','admins','disabled');
    $protected $appends = [
        'model'
    ];

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

}
