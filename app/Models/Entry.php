<?php

class Entry extends Eloquent
{

    // Add your validation rules here
    public static $rules = [

    ];

    // Don't forget to fill this array
    protected $table = 'entries';

    protected $fillable = [
        'title',
        'body',
        'public',
        'disabled'
    ];

    public static $authorized = [
        'title',
        'body',
        'publish_date'
    ];

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    protected $appends = [
        'model',
        'formatted_date'
    ];

    public function getFormattedDateAttribute()
    {
        return date('D, M jS Y', strtotime($this->created_at));
    }
}
