<?php

class Row extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
    // 'title' => 'required'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($row) { }); // returning false will cancel the operation
        static::created(function($row) { });
        static::updating(function($row) { }); // returning false will cancel the operation
        static::updated(function($row) { });
        static::saving(function($row) { });  // returning false will cancel the operation
        static::saved(function($row) { });
        static::deleting(function($row) { }); // returning false will cancel the operation
        static::deleted(function($row) { });
    }
    // Don't forget to fill this array
    protected $table = 'rows';
    protected $fillable = [
        'map_id',
        'order'
    ];
    protected $appends = [
        'model'
    ];

    /****************************
     * Relationships
     ****************************/

    public function cols() {
        return $this->hasMany('Col')->orderBy('order')->orderBy('id');
    }

    /****************************
     * Attributes
     ****************************/

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    /****************************
     * Methods
     ****************************/

}
