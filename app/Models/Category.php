<?php

class Category extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
      // 'title' => 'required'
    ];

     // Don't forget to fill this array
    protected $table = 'categories';
    protected $fillable = [
        'name',
        'description',
        'rate'
    ];

    protected $appends = [
        'model'
    ];

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

}
