<?php

class Setting extends Eloquent {

    // Add your validation rules here
    public static $rules = [
        // 'title' => 'required'
    ];

    // Don't forget to fill this array
    protected $fillable = [
        'key',
        'value',
        'name',
        'description',
        'type'
    ];

    protected $appends = [
        'formatted_value',
        'days',
        'hours',
        'minutes',
        'model'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($siteconfig) { }); // returning false will cancel the operation
        static::created(function($siteconfig) {
            Cache::forget('settings');
        });
        static::updating(function($siteconfig) { }); // returning false will cancel the operation
        static::updated(function($siteconfig) {
            Cache::forget('settings');
        });
        static::saving(function($siteconfig) { });  // returning false will cancel the operation
        static::saved(function($siteconfig) {
            Cache::forget('settings');
        });
        static::deleting(function($siteconfig) { }); // returning false will cancel the operation
        static::deleted(function($siteconfig) { });
    }

    /************************************
    * Relationships
    *************************************/

    public function tags() {
        return $this->morphMany('TagModel', 'taggable');
    }

    /************************************
    * Attributes
    *************************************/

    public function getFormattedValueAttribute() {
        if($this->type == 'Time') {
            return calculateTime($this->value);
        }
        elseif($this->type == 'Color') {
            return '<div class="inline-block" style="vertical-align:middle; width:20px; height:20px; border-radius:2px; background-color:' . $this->value . '"></div> ' . $this->value;
        }
        else return $this->value;
    }

    public function getDaysAttribute() {
        if($this->type == 'Time') {
            return calculateTimeUnits($this->value, 'day');
        }
        else return '';
    }

    public function getHoursAttribute() {
        if($this->type == 'Time') {
            return calculateTimeUnits($this->value, 'hour');
        }
        else return '';
    }

    public function getMinutesAttribute() {
        if($this->type == 'Time') {
            return calculateTimeUnits($this->value, 'minute');
        }
        else return '';
    }

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

}
