<?php

class Question extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
        // 'title' => 'required'
    ];

    protected $table = 'questions';

    // Don't forget to fill this array
    protected $fillable = [
        'description',
        'asker',
        'question_category_id'
    ];

    protected $appends = [
        'model'
    ];

    /****************************
     * Relationships
     ****************************/

     public function categories()
     {
         return $this->belongsTo(QuestionCategory::class);
     }

    /****************************
     * Attributes
     ****************************/

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    /****************************
     * Methods
     ****************************/

}
