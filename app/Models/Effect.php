<?php

class Effect extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
    // 'title' => 'required'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($effect) { }); // returning false will cancel the operation
        static::created(function($effect) { });
        static::updating(function($effect) { }); // returning false will cancel the operation
        static::updated(function($effect) { });
        static::saving(function($effect) { });  // returning false will cancel the operation
        static::saved(function($effect) { });
        static::deleting(function($effect) { }); // returning false will cancel the operation
        static::deleted(function($effect) { });
    }
    // Don't forget to fill this array
    protected $table = 'effects';
    protected $fillable = [
        'destination_type',
        'destination_id',
        'logic_id',
        'max',
        'min',
        'negative',
        'operator',
        'property_id',
        'property_index',
        'propertyOption_id',
        'qty',
        'rand',
        'scope',
        'subject_id',
        'subject_type',
        'type',
        'value'
    ];

    protected $appends = [
        'model',
        'name'
    ];

    protected $with = [
        'dialogs',
        'property',
        'propertyOption'
    ];

    /****************************
     * Relationships
     ****************************/

    public function dialogs() {
        return $this->hasMany('Dialog');
    }

    public function property() {
        return $this->belongsTo('Property', 'property_id');
    }

    public function propertyOption() {
        return $this->belongsTo('PropertyOption', 'propertyOption_id');
    }

    /****************************
     * Attributes
     ****************************/

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    public function getNameAttribute()
    {
        $txt = '';
        $subject_type = lcfirst(str_replace('App\Models\\', '', $this->subject_type));
        if($this->subject_type == 'dialog') {
            if(count($this->dialogs) > 0) {
                return 'Dialog: "' . $this->dialogs[0]->body_short . '"';
            }
            else {
                return 'Dialog';
            }
        }
        if($this->type == 'add') {
            $txt .= 'Add ';
        }
        if($this->scope == 'all') {
            $txt .= $this->scope;
        }
        elseif($this->scope == 'qty') {
            $txt .= $this->qty . ' ';
        }
        elseif($this->scope == 'specific' || $this->scope == 'current') {
            $txt .= 'Set ';
            if($this->scope == 'specific') {
                $txt .= $subject_type . ' ' . $this->subject_id;
            }
            elseif($this->scope == 'current') {
                $txt .= ' the current ' . $subject_type;
            }
            $txt .= "'s property \"" . lcfirst($this->property->name) . '" to "' . lcfirst($this->propertyOption->name) . '"';
        }
        if($this->rand) {
            $txt .= 'a random value between ' . $this->min . ' and ' . $this->max;
        }
        if($this->type == 'add' || $this->type == 'remove') {
            $txt .= ' within ';
        }
        if($this->destination_type == 'current_place') {
            $txt .= 'the current place';
        }
        if($this->destination_type == 'place') {
            $txt .= 'the place with ID ' . $this->destination_id;
        }
        if($this->destination_type == 'current_player') {
            $txt .= "the current player's inventory";
        }
        if($this->destination_type == 'character' || $this->destination_type == 'player' ) {
            $txt .= "the " . $this->destination_type . "'s inventory with ID " . $this->destination_id;
        }
        return $txt;
    }

    /****************************
     * Methods
     ****************************/

    static function randOptions() {
        return [
            [
                'name' => 'The value',
                'value' => 0
            ],
            [
                'name' => 'A random value',
                'value' => 1
            ]
        ];
    }

}
