<?php

class Bodypart extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
    // 'title' => 'required'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($bodypart) { }); // returning false will cancel the operation
        static::created(function($bodypart) { });
        static::updating(function($bodypart) { }); // returning false will cancel the operation
        static::updated(function($bodypart) { });
        static::saving(function($bodypart) { });  // returning false will cancel the operation
        static::saved(function($bodypart) { });
        static::deleting(function($bodypart) { }); // returning false will cancel the operation
        static::deleted(function($bodypart) { });
    }
    // Don't forget to fill this array
    protected $table = 'bodyparts';
    protected $fillable = array('name','gender');

    protected $appends = [
        'model',
        'new_record'
    ];

    /****************************
     * Relationships
     ****************************/



    /****************************
     * Attributes
     ****************************/

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    public function getNewRecordAttribute() {
        return (strtotime($this->created_at) >= (time() - Cache::get('settings.new_time_frame') ))?true:false;
    }

    /****************************
     * Methods
     ****************************/

}
