<?php

class Option extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
    // 'title' => 'required'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($option) { }); // returning false will cancel the operation
        static::created(function($option) { });
        static::updating(function($option) { }); // returning false will cancel the operation
        static::updated(function($option) { });
        static::saving(function($option) { });  // returning false will cancel the operation
        static::saved(function($option) { });
        static::deleting(function($option) { }); // returning false will cancel the operation
        static::deleted(function($option) { });
    }
    // Don't forget to fill this array
    protected $table = 'options';
    protected $fillable = array('morph_id','result_id','order','name','morph_type','result_type','description');

    protected $appends = [
        'model',
        'new_record'
    ];

    /****************************
     * Relationships
     ****************************/

    public function effects() {
        return $this->morphMany('Effect', 'morph');
    }

    /****************************
     * Attributes
     ****************************/

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    public function getNewRecordAttribute() {
        return (strtotime($this->created_at) >= (time() - Cache::get('settings.new_time_frame') ))?true:false;
    }

    /****************************
     * Methods
     ****************************/

}
