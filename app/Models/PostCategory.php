<?php

class PostCategory extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
        //
    ];

    // Don't forget to fill this array
    protected $table = 'post_categories';
    protected $fillable = [
        'description',
        'name'
    ];

    public function getModelAttribute()
    {
        return snake_case(get_class($this));
    }

    protected $appends = [
        'model'
    ];

    /****************************
    * Relationships
    *****************************/

    public function posts()
    {
        return $this->belongsToMany(Post::class, 'category_post_pivot');
    }
}
