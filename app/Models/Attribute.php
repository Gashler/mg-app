<?php

class Attribute extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
    // 'title' => 'required'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($attribute) { }); // returning false will cancel the operation
        static::created(function($attribute) { });
        static::updating(function($attribute) { }); // returning false will cancel the operation
        static::updated(function($attribute) { });
        static::saving(function($attribute) { });  // returning false will cancel the operation
        static::saved(function($attribute) { });
        static::deleting(function($attribute) { }); // returning false will cancel the operation
        static::deleted(function($attribute) { });
    }
    // Don't forget to fill this array
    protected $table = 'attributes';
    protected $fillable = array('name','attributeable_type','value','attributeable_id','default');

    protected $appends = [
        'model',
        'new_record'
    ];

    /****************************
     * Relationships
     ****************************/



    /****************************
     * Attributes
     ****************************/

    public function getNewRecordAttribute() {
        return (strtotime($this->created_at) >= (time() - Cache::get('settings.new_time_frame') ))?true:false;
    }

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    /****************************
     * Methods
     ****************************/

}
