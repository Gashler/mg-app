<?php

class Narration extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
    // 'title' => 'required'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($narration) { }); // returning false will cancel the operation
        static::created(function($narration) { });
        static::updating(function($narration) { }); // returning false will cancel the operation
        static::updated(function($narration) { });
        static::saving(function($narration) { });  // returning false will cancel the operation
        static::saved(function($narration) { });
        static::deleting(function($narration) { }); // returning false will cancel the operation
        static::deleted(function($narration) { });
    }
    // Don't forget to fill this array
    protected $table = 'narrations';
    protected $fillable = [
        'body',
        'game_id',
        'morph_id',
        'morph_type'
    ];

    protected $appends = [
        'model',
        'new_record'
    ];

    /****************************
     * Relationships
     ****************************/



    /****************************
     * Attributes
     ****************************/

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    public function getNewRecordAttribute() {
        return (strtotime($this->created_at) >= (time() - Cache::get('settings.new_time_frame') ))?true:false;
    }

    /****************************
     * Methods
     ****************************/

}
