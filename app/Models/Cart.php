<?php

class Cart extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
    // 'title' => 'required'
    ];

    // Don't forget to fill this array
    protected $table = 'carts';
    protected $fillable = array('service_id','disabled');

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    public function getNewRecordAttribute() {
        return (strtotime($this->created_at) >= (time() - Config::get('site.new_time_frame') ))?true:false;
    }

    protected $appends = [
        'model',
        'new_record'
    ];


}
