<?php

class Answer extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
        // 'title' => 'required'
    ];

    protected $table = 'answers';

    // Don't forget to fill this array
    protected $fillable = [
        'body',
        'question_id',
        'user_id'
    ];

    protected $appends = [
        'loading',
        'model'
    ];

    protected $with = [
        'question'
    ];

    /****************************
     * Relationships
     ****************************/

     public function question()
     {
         return $this->belongsTo(Question::class);
     }

     public function user()
     {
         return $this->belongsTo(User::class);
     }

    /****************************
     * Attributes
     ****************************/

    public function getLoadingAttribute()
    {
        return false;
    }

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    /****************************
     * Methods
     ****************************/

}
