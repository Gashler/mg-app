<?php

class Object extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
        // 'title' => 'required'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($object) { }); // returning false will cancel the operation
        static::created(function($object) { });
        static::updating(function($object) { }); // returning false will cancel the operation
        static::updated(function($object) { });
        static::saving(function($object) { });  // returning false will cancel the operation
        static::saved(function($object) { });
        static::deleting(function($object) { }); // returning false will cancel the operation
        static::deleted(function($object) { });
    }
    // Don't forget to fill this array
    protected $table = 'objects';
    protected $fillable = [
        'description',
        'game_id',
        'hidden',
        'mid',
        'morph_id',
        'morph_type',
        'name',
        'object_id',
        'takeable',
        'type',
    ];

    protected $appends = [
        'model'
    ];

    /****************************
    * Relationships
    ****************************/

    public function container() {
        return $this->morphTo('Object', 'morph');
    }

    public function customProperties() {
        return $this->morphMany('Property', 'morph');
    }

    public function defaultProperties() {
        return $this->morphMany('Property', 'morph')->whereNull('morph_id');
    }

    public function logics() {
        return $this->morphMany('Logic', 'morph');
    }

    public function medias() {
        return $this->belongsToMany('Media');
    }

    public function morph() {
        return $this->morphTo();
    }

    public function objects() {
        return $this->morphMany('Object', 'morph');
    }

    public function place()
    {
        return $this->belongsTo('Place');
    }

    public function timers()
    {
        return $this->morphMany('Timer', 'morph');
    }

    /****************************
    * Attributes
    ****************************/

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    /****************************
    * Methods
    ****************************/

}
