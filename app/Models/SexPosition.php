<?php

class SexPosition extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
        // 'title' => 'required'
    ];

    protected $table = 'sex_positions';

    // Don't forget to fill this array
    protected $fillable = [
        'name',
        'key',
        'description',
        'rating',
        'tall'
    ];

    protected $appends = [
        'model',
        'stars_width'
    ];

    /****************************
     * Relationships
     ****************************/

     public function categories()
     {
         return $this->belongsToMany(SexPositionCategory::class, 'sexposition_category_pivot');
     }

    /****************************
     * Attributes
     ****************************/

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    public function getStarsWidthAttribute()
    {
        return $this->rating * 25;
    }

    /****************************
     * Methods
     ****************************/

}
