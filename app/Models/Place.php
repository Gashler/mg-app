<?php

class Place extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
        // 'title' => 'required'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($place) { }); // returning false will cancel the operation
        static::created(function($place) { });
        static::updating(function($place) { }); // returning false will cancel the operation
        static::updated(function($place) { });
        static::saving(function($place) { });  // returning false will cancel the operation
        static::saved(function($place) { });
        static::deleting(function($place) { }); // returning false will cancel the operation
        static::deleted(function($place) { });
    }
    // Don't forget to fill this array
    protected $table = 'places';
    protected $fillable = [
        'block_top',
        'block_right',
        'block_bottom',
        'block_left',
        'col_id',
        'current',
        'description',
        'down_id',
        'down_type',
        'left_id',
        'left_type',
        'mid',
        'name',
        'right_id',
        'right_type',
        'start',
        'up_type',
        'up_id',
    ];

    protected $appends = [
        'model',
        'title'
    ];

    /****************************
    * Relationships
    ****************************/

    public function characters() {
        return $this->hasMany('Character');
    }

    public function logics() {
        return $this->morphMany('Logic', 'morph');
    }

    public function medias() {
        return $this->belongsToMany('Media');
    }

    public function objects() {
        return $this->morphMany('Object', 'morph');
    }

    public function timers()
    {
        return $this->morphMany('Timer', 'morph');
    }

    public function uvents()
    {
        return $this->belongsToMany('Uvent');
    }

    /****************************
    * Attributes
    ****************************/

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    public function getTitleAttribute()
    {
        if(isset($this->name)) {
            return $this->name;
        }
        else {
            return 'place ' . $this->id;
        }
    }

    /****************************
    * Methods
    ****************************/

}
