<?php

class StripeLog extends Eloquent
{
    // Add your validation rules here
    public static $rules = [
        //
    ];
    protected $table = 'stripe';
    protected $fillable = ['data'];
}
