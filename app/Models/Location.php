<?php

class Location extends Eloquent {

    // Add your validation rules here
    public static $rules = [
    // 'title' => 'required'
    ];

    // Don't forget to fill this array
    protected $table = 'locations';
    protected $fillable = array('name');
    protected $appends = [
        'model'
    ];

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

}
