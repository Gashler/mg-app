<?php

class Modification extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
    // 'title' => 'required'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($modification) { }); // returning false will cancel the operation
        static::created(function($modification) { });
        static::updating(function($modification) { }); // returning false will cancel the operation
        static::updated(function($modification) { });
        static::saving(function($modification) { });  // returning false will cancel the operation
        static::saved(function($modification) { });
        static::deleting(function($modification) { }); // returning false will cancel the operation
        static::deleted(function($modification) { });
    }
    // Don't forget to fill this array
    protected $table = 'modifications';
    protected $fillable = array('modifiable_type','col','game_id','account_id','modifiable_id','value');

    protected $appends = [
        'model',
        'new_record'
    ];

    /****************************
     * Relationships
     ****************************/



    /****************************
     * Attributes
     ****************************/

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    public function getNewRecordAttribute() {
        return (strtotime($this->created_at) >= (time() - Cache::get('settings.new_time_frame') ))?true:false;
    }

    /****************************
     * Methods
     ****************************/

}
