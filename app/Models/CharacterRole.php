<?php

class CharacterRole extends Eloquent {

    // Add your validation rules here
    public static $rules = [
    // 'title' => 'required'
    ];

    // Don't forget to fill this array
    protected $table = 'character_roles';
    protected $fillable = array(
        'name',
        'gender'
    );
    protected $appends = [
        'model'
    ];

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

}
