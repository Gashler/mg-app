<?php

class Timer extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
    // 'title' => 'required'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($timer) { }); // returning false will cancel the operation
        static::created(function($timer) { });
        static::updating(function($timer) { }); // returning false will cancel the operation
        static::updated(function($timer) { });
        static::saving(function($timer) { });  // returning false will cancel the operation
        static::saved(function($timer) { });
        static::deleting(function($timer) { }); // returning false will cancel the operation
        static::deleted(function($timer) { });
    }
    // Don't forget to fill this array
    protected $table = 'timers';
    protected $fillable = [
        'direction',
        'morph_type',
        'result_type',
        'min',
        'set_max',
        'max',
        'value',
        'morph_id',
        'result_id',
        'active',
        'game_id'
    ];

    protected $appends = [
        'model',
        'new_record'
    ];

    /****************************
     * Relationships
     ****************************/



    /****************************
     * Attributes
     ****************************/

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    public function getNewRecordAttribute() {
        return (strtotime($this->created_at) >= (time() - Cache::get('settings.new_time_frame') ))?true:false;
    }

    /****************************
     * Methods
     ****************************/

}
