<?php

class Col extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
        // 'title' => 'required'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($col) { }); // returning false will cancel the operation
        static::created(function($col) { });
        static::updating(function($col) { }); // returning false will cancel the operation
        static::updated(function($col) { });
        static::saving(function($col) { });  // returning false will cancel the operation
        static::saved(function($col) { });
        static::deleting(function($col) { }); // returning false will cancel the operation
        static::deleted(function($col) { });
    }
    // Don't forget to fill this array
    protected $table = 'cols';
    protected $fillable = [
        'row_id',
        'order'
    ];

    protected $appends = [
        'model'
    ];

    /****************************
    * Relationships
    ****************************/

    public function place() {
        return $this->hasOne('Place');
    }

    /****************************
    * Attributes
    ****************************/

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    /****************************
    * Methods
    ****************************/

}
