<?php

class Property extends Eloquent
{

    // Add your validation rules here
    public static $rules = [
    // 'title' => 'required'
    ];

    public static function boot() {
        parent::boot();
        static::creating(function($property) { }); // returning false will cancel the operation
        static::created(function($property) { });
        static::updating(function($property) { }); // returning false will cancel the operation
        static::updated(function($property) { });
        static::saving(function($property) { });  // returning false will cancel the operation
        static::saved(function($property) { });
        static::deleting(function($property) { }); // returning false will cancel the operation
        static::deleted(function($property) { });
    }
    // Don't forget to fill this array
    protected $table = 'properties';
    protected $fillable = [
        'column',
        'description',
        'key',
        'morph_type',
        'morph_id',
        'name',
        'value'
    ];

    protected $appends = [
        'model'
    ];

    /****************************
     * Relationships
     ****************************/

    public function options() {
        return $this->hasMany('PropertyOption');
    }

    /****************************
     * Attributes
     ****************************/

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    /****************************
     * Methods
     ****************************/

}
