<?php

class Orderline extends Eloquent {

    // Add your validation rules here
    public static $rules = [
        // 'title' => 'required'
    ];

    // Don't forget to fill this array
    protected $fillable = [];

    public function order() {
        return $this->belongsTo('Order');
    }

    public function getModelAttribute()
    {
        return strtolower(get_class($this));
    }

    public function getNewRecordAttribute() {
        return (strtotime($this->created_at) >= (time() - Config::get('site.new_time_frame') ))?true:false;
    }

    protected $appends = [
        'model',
        'new_record'
    ];

}
