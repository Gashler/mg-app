<?php

namespace App\Console\Commands;

use Log;
use \Campaign;
use \User;
use App\Services\MailService;
use Illuminate\Console\Command;

class SendEmails extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:send_emails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(MailService $mailService)
    {
        parent::__construct();
        $this->mailService = $mailService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // get active campaign
        $campaigns = Campaign::with([
            'emails',
            'users'
        ])->where('disabled', 0)
        ->where(function($query) {
            $query->whereNull('start_at')
                ->orWhere('start_at', '<=', date('Y-m-d h:i:s'));
        })->where(function($query) {
            $query->whereNull('end_at')
                ->orWhere('end_at', '>=', date('Y-m-d h:i:s'));
        })->get();

        // order emails by intervals
        foreach ($campaigns as $campaign_index => $campaign) {
            foreach ($campaign->emails as $email_index => $email) {
                $sort = $email->pivot->interval;
                if (isset($email->pivot->interval_type)) {
                    if ($email->pivot->interval_type == 'hours') {
                        $sort /= 24;
                    }
                    if ($email->pivot->interval_type == 'minutes') {
                        $sort /= 1440;
                    }
                }
                $campaigns[$campaign_index]->emails[$email_index]->pivot->sort = $sort;
            }
            $emails = $campaigns[$campaign_index]->emails;
            unset($campaigns[$campaign_index]->emails);
            $campaigns[$campaign_index]->emails = array_values(array_sort($emails, function($value) {
                return $value->pivot->sort;
            }));
        }

        // loop through campaign's users and send emails if conditions are met
        foreach ($campaigns as $campaign) {
            foreach ($campaign->users as $user) {

                $userReceivedThisCycle = false;

                // determine time since last campaign email was received
                if (isset($user->received_campaign_email_at)) {
                    $timeSinceLastCampaignEmail = time() - strtotime($user->received_campaign_email_at);
                }

                // determine how long the user has been subscribed to this campaign
                $daysSinceSubscribing = round((time() - strtotime($user->pivot->created_at)) * 86400);
                $hoursSinceSubscribing = round((time() - strtotime($user->pivot->created_at)) * 3600);

                // loop through campaign emails
                foreach ($campaign->emails as $email) {

                    // determine interval type
                    $timeSinceSubscribing = $daysSinceSubscribing;
                    if ($email->pivot->interval_type) {
                        if ($email->pivot->interval_type == 'hours') {
                            $timeSinceSubscribing = $hoursSinceSubscribing;
                        }
                    }

                    // determine if user should receive email
                    if (
                        ($user->verified || $campaign->name == 'incomplete-registration')
                        && !$user->unsubscribed // user hasn't unsubscribed
                        && !$userReceivedThisCycle // user hasn't already received an email during this cycle
                        && (!$user->campaign_emails_received || !in_array($email->id, $user->campaign_emails_received)) // user hasn't already received this particular email
                        && (!isset($timeSinceLastCampaignEmail) || $email->pivot->interval_type == 'hours' || $timeSinceLastCampaignEmail >= 86100) // this is the first email a user has received, the interval is hour-based, or it's been at least 23:55 since last campaign email
                        && $email->pivot->interval <= $timeSinceSubscribing
                    ) {
                        // attempt to send the email
                        if ($userReceivedThisCycle = $this->mailService->send($email->key, $user)) {

                            // if the email was successfully sent
                            $campaign_emails_received = $user->campaign_emails_received;
                            $campaign_emails_received[] = $email->id;
                            $user->received_campaign_email_at = date('Y-m-d h:i:s');
                            $user->update([
                                'campaign_emails_received' => $campaign_emails_received,
                                'received_campaign_email_at' => $user->received_campaign_email_at
                            ]);
                        }
                    }
                }
            }
        }
        Log::info('Campaign emails sent');
    }
}
