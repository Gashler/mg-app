<?php

namespace App\Listeners;

use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Auth\Events\Login;

class UserLoggedIn
{
    /**
    * Create the event listener.
    *
    * @return void
    */
    public function __construct()
    {

    }

    /**
    * Handle the event.
    *
    * @param  Login  $event
    * @return void
    */
    public function handle(Login $event)
    {
        $logins = auth()->user()->logins + 1;
        auth()->user()->update(['logins' =>  $logins]);
    }
}
