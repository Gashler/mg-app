<?php

namespace App\Services;

use \Carbon\Carbon;
use \Log;
use \Account;
use \Campaign;
use App\Services\MailService;

class SubscriptionService
{
    public function __construct(
        MailService $mailService
    ) {
        \Stripe\Stripe::setApiKey(env('STRIPE_SECRET'));
        $this->mailService = $mailService;
    }

    /**
    * Create a Subscription
    *
    * @param array $params
    * @return Response
    */
    public function create($params)
    {
        // return $params;
        try {

            // for ongoing charges
            if ($params['object'] == 'plan') {
                auth()->user()->account->newSubscription('main', $params['plan_id'])->create($params['stripeToken'], [
                    'email' => auth()->user()->email,
                    'metadata' => [
                        'husband_name' => auth()->user()->husband,
                        'wife_name' => auth()->user()->wife,
                        'account_id' => auth()->user()->account_id
                    ]
                ]);

                // email admin
                $plans = $this->plans($params['plan_id']);
                $user = \User::where('email', 'marriedgames@gmail.com')->first();
                $this->mailService->send('new-subscription', $user, [
                    'user' => auth()->user(),
                    'plan' => $plans[0]
                ]);

            // for one-time fees for lifetime subscriptions
            } elseif ($params['object'] == 'product') {
                $plans = $this->plans($params['plan_id']);
                $product = $plans[0];
                $response = \Stripe\Charge::create([
                    "amount" => $product->skus->data[0]->price,
                    "currency" => "usd",
                    "source" => $params['stripeToken'],
                    "description" => $product->name,
                    "metadata" => [
                        'husband' => auth()->user()->first_name,
                        'wife' => auth()->user()->spouse->first_name,
                        'account_id' => auth()->user()->account_id
                    ]
                ]);
                session()->put('response', $response);
                auth()->user()->account->update([
                    'bypass_subscription' => 1,
                    'trial_ends_at' => null,
                    'stripe_id' => $response->id,
                    'card_brand' => $response->source->brand,
                    'card_last_four' => $response->source->last4
                ]);
            }

            // update account's "trial_ends_at" property (while normally superfluous, this ensures that people signing up for extended trials don't get locked out)
            $trial_ends_at = Carbon::now()->addDays(config('site.free_trial_days'));
            auth()->user()->account()->update([
                'trial_ends_at' => $trial_ends_at
            ]);

            // detach user from new user email campaigns
            $campaigns = [
                'new-users',
                '24-hour-trial',
                'incomplete-registration'
            ];
            foreach ($campaigns as $campaignKey) {
                $campaign = Campaign::where('key', $campaignKey)->first();
                if (
                    auth()->user()->campaigns()->find($campaign->id)
                    || auth()->user()->spouse->campaigns()->find($campaign->id)
                ) {
                    auth()->user()->campaigns()->detach($campaign->id);
                    auth()->user()->spouse->campaigns()->detach($campaign->id);
                }
            }

            // add user to new customers email campaign
            $campaign = Campaign::where('key', 'new-customers')->first();
            if (
                !$campaign->disabled
                && (!isset($campaign->start_at) || $campaign->start_at < date('Y-m-d h:i:s'))
                && (!isset($campaign->end_at) || $campaign->end_at > date('Y-m-d h:i:s'))
            ) {
                if (!auth()->user()->campaigns()->find($campaign->id)) {
                    auth()->user()->campaigns()->save($campaign);
                }
                if (!auth()->user()->spouse->campaigns()->find($campaign->id)) {
                    auth()->user()->spouse->campaigns()->save($campaign);
                }
            }

            return [
                'user' => \User::find(auth()->user()->id),
                'message' => [
                    'type' => 'success',
                    'body' => 'Success! Your subscription has been created.'
                ]
            ];
        } catch (Exception $e) {
            return [
                'message' => [
                    'type' => 'danger',
                    'body' => $e->getMessage()
                ]
            ];
        }
    }

    /**
    * Return a Subscription
    *
    * @return Response
    */
    public function get()
    {
        if ($stripe_id = auth()->user()->account->stripe_id) {
            $customer = \Stripe\Customer::retrieve($stripe_id);
            return [
                'error' => false,
                'data' => $customer
            ];
        } else {
            return [
                'error' => true,
                'message' => "You don't currently have a subscription."
            ];
        }
    }

    /**
    * Update a Stripe object property
    *
    * @return Response
    */
    public function update($params)
    {
        $key = $params['key'];
        $class = '\Stripe\\'.ucfirst($params['object']);
        $object = $class::retrieve($params['object_id']);
        $object->$key = $params['value'];
        $object->save();
        return [
            'error' => false,
            'message' => "Your change has been saved."
        ];
    }

    /**
    * Cancel or Activate a Subscription
    *
    * @param array $params
    * @return Response
    */
    public function toggle($params)
    {
        $method = $params['method'];
        auth()->user()->account->subscription('main')->$method();
        if ($method == 'cancel') {
            $word = 'canceled';
        } else {
            $word = 'resumed';
        }
        return [
            'error' => false,
            'message' => "Automatic payments have been $word.",
            'customer' => \Stripe\Customer::retrieve(auth()->user()->account->stripe_id)
        ];
    }

    /**
    * Add a payment method
    *
    * @param array $params
    * @return Response
    */
    public function addPaymentMethod($params)
    {
        $stripe_id = auth()->user()->account->stripe_id;
        $customer = \Stripe\Customer::retrieve($stripe_id);
        $source = $customer->sources->create(array("source" => $params['stripeToken']));
        $customer->default_source = $source->id;
        $customer->save();
        return [
            'error' => false,
            'message' => "Payment method added",
            'data' => \Stripe\Customer::retrieve($stripe_id)
        ];
    }

    /**
    * Change a subscription plan
    *
    * @param array $params
    * @return Response
    */
    public function changePlan($params)
    {
        auth()->user()->account->subscription('main')->swap($params['plan']);
        return [
            'error' => false,
            'message' => "Your subscription plan has been changed.",
            'data' => \Stripe\Customer::retrieve(auth()->user()->account->stripe_id)
        ];
    }

    /**
    * Return a list of all billing plans
    *
    * @param array $params
    * @return Response
    */
    public function plans($id = null)
    {
        $products = \Stripe\Product::all();
        $plans = \Stripe\Plan::all();
        $plans = array_merge($products->data, $plans->data);
        foreach ($plans as $index => $plan) {
            if (!isset($id)) {
                $whitelist = [
                    '3NTamqpuXT9W2GKtKUAs',
                    'soPLZ9oIXbNEOU6d2uKp'
                ];
                if (!in_array($plan->id, $whitelist)) {
                    unset($plans[$index]);
                }
            } else {
                if ($plan->id !== $id) {
                    unset($plans[$index]);
                }
            }
        }

        // order by price
        return $plans = array_values(array_sort($plans, function($object) {
            return $object->amount;
        }));
    }
}
