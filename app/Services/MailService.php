<?php

namespace App\Services;

use Mail;
use Email;
use App\Mail\DefaultMailer;
use App\Services\CurlService;

class MailService extends CurlService
{
    // send email via curl
    public function request($endpoint, $method = 'GET', $body = null)
    {
        $url = config('services.sparkpost.url') . "/" . $endpoint;
        $headers = [
            "x-auth-token: api-key " . config("services.sparkpost.key"),
            "content-type: application/json"
        ];
        $response = $this->curl($method, $url, $headers, $body);

        // if there's a curl error
        if ($response['error']) {
            return $response;
        }

        // handle service errors
        $response['response'] = json_decode($response['response']);
        if (isset($response['response']->httpStatus) && $response['response']->httpStatus !== 200) {
            return [
                'error' => true,
                'response' => $response['response']
            ];
        }
        return [
            'error' => false,
            'response' => $response['response']
        ];
    }

    // send an email
    public function send($emailKey, $user = null, $object = null)
    {
        if (!isset($user)) {
            $user = auth()->user();
        }
        $email = Email::where('key', $emailKey)->orWhere('id', $emailKey)->first();
        $email->subject = evalString($email->subject, $user, $object);
        $email->body = evalString($email->body, $user, $object);
        try {
            Mail::to($user)->send(new DefaultMailer($email, $user, $object));
            return ['error' => false];
        } catch (\Exception $e) {
            return [
                'error' => true,
                'message' => $e->getMessage()
            ];
        }
    }
}
