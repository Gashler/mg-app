<?php

namespace App\Services;

use Log;

class CurlService
{
    public function curl(
        $method = 'GET',
        string $url,
        array $headers = null,
        array $body = null
    ) {
        // prepare data
        $method = strtoupper($method);
        if ($method == 'POST') {
            $params = json_encode($body);
        }
        if ($method == 'GET' && isset($body)) {
            $params = null;
            $url .= '?';
            foreach ($body as $key => $value) {
                $url .= $key . "=" . $value . "&";
            }
        }

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => $method,
            CURLOPT_POSTFIELDS => $params,
            CURLOPT_HTTPHEADER => $headers
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return [
                'error' => true,
                'message' => "cURL Error #:" . $err
            ];
        } else {
            return [
                'error' => false,
                'response' => json_decode($response)
            ];
        }
    }
}
