<?php

namespace App\Services;

use Log;

use App\Services\CurlService;

class GetresponseService extends CurlService
{
    public function request($endpoint, $method = 'GET', $body = null)
    {
        $url = config('services.getresponse.url') . "/" . $endpoint;
        $headers = [
            "x-auth-token: api-key " . config("services.getresponse.key"),
            "content-type: application/json"
        ];
        $response = $this->curl($method, $url, $headers, $body);

        // if there's a curl error
        if ($response['error']) {
            return $response;
        }

        // handle Getresponse errors
        $response['response'] = json_decode($response['response']);
        if (isset($response['response']->httpStatus) && $response['response']->httpStatus !== 200) {
            return [
                'error' => true,
                'response' => $response['response']
            ];
        }
        return [
            'error' => false,
            'response' => $response['response']
        ];
    }

    // add a contact
    public function subscribe($user)
    {
        if ($user['gender'] == 'm') {
            $gender = 'Male';
        } else {
            $gender = 'Female';
        }
        $response = $this->request('contacts', 'POST', [
            "name" => $user['first_name'],
            "email" => $user['email'],
            "dayOfCycle" => 1,
            "campaign" => [
                "campaignId" => config("services.getresponse.campaignId")
            ],
            "customFieldValues" => [
                [
                    "customFieldId" => "FGWeO",
                    "value" => [$gender]
                ]
            ]
        ]);

        // if contact already exists, move on without error
        if ($response['error']) {
            if ($response['response']->code == 1008) {
                return [
                    'error' => false,
                    'response' => $response['response']
                ];
            }
        }

        return $response;
    }

    // get custom fields
    public function customFields()
    {
        return $this->request('custom-fields');
    }

    // search contacts
    public function searchContacts($params)
    {
        foreach ($params as $key => $value) {
            $body['query[' . $key . ']'] = $value;
        }
        return $this->request('contacts', 'GET', $body);
    }
}
