<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

##############################################################################################
# Session Control
##############################################################################################
Route::resource('sessions', 'SessionController');
Route::post('wp-login', 'SessionController@wp');
Route::get('select-user', 'SessionController@select');
Route::get('switch-user/{gender}', 'SessionController@switchUser');
Route::match(['GET', 'POST'], 'login', array('as' => 'login', 'uses' => 'SessionController@create'));
Route::get('logout', array('as' => 'logout', 'uses' => 'SessionController@destroy'));
Route::get('sign-up/{code}', array('as' => 'sign-up', 'uses' => 'UserController@create'));
Route::get('password/forgot', 'SessionController@forgotPassword');
Route::post('password/reset', 'SessionController@resetPassword');
Route::get('password/confirm-reset', 'SessionController@confirmResetPassword');
Route::get('oauth', 'OAuthController@login');
Route::match(['GET', 'POST'], 'login-as-user/{id?}', 'SessionController@loginAsUser');

// register
Route::get('register', 'RegisterController@create');
Route::resource('Register', 'RegisterController');
Route::get('register/data', 'RegisterController@data');
Route::get('register/send-again', 'RegisterController@sendAgain');
Route::get('subscription-plans/{id?}', 'SubscriptionController@plans');
Route::get('subscriptions/create', 'SubscriptionController@create');
Route::get('verify', 'RegisterController@verify');

// Stripe
Route::get('stripe/webhook', 'StripeLogController@webhook');

##############################################################################################
# Public Routes
##############################################################################################

// blasts
Route::get('send_text/{phoneId}', 'SmsMessagesController@create');
Route::resource('send_text', 'SmsMessagesController');
Route::get('send_mail/{personId}', 'MailMessagesController@create');
Route::resource('send_mail/', 'MailMessagesController');
Route::get('blast_email', ['as' => 'blast_email', 'uses' => 'BlastController@CreateMail']);
Route::post('blast_email', ['uses' => 'BlastController@StoreMail']);
Route::get('blast_sms', ['as' => 'blast_sms', 'uses' => 'BlastController@CreateSms']);
Route::post('blast_sms', ['uses' => 'BlastController@StoreSms']);

// contact form
Route::post('send-contact-form', ['as' => 'send-contact-form', 'uses' => 'ContactController@send']);

// pages
Route::resource('pages', 'PageController');
Route::post('pages/disable', 'PageController@disable');
Route::post('pages/enable', 'PageController@enable');
Route::post('pages/delete', 'PageController@delete');

//timezone
Route::post('set-timezone', 'TimezoneController@setTimezone');

Route::get('constants', 'Controller@constants');

// home
Route::get('/', 'PagesController@home');

// subscriptions
Route::get('unsubscribe', 'EmailSubscriptionController@unsubscribe');
Route::post('subscription/update/{email}', 'EmailSubscriptionController@update');

##############################################################################################
// Protected Routes
##############################################################################################
Route::group(array('middleware' => 'auth'), function () {
    // App::bind('TriggerRepositoryInterface','EloquentTriggerRepository');

    // dashboard
    // Route::get('/', 'PagesController@home');

    Route::get('settings', ['as' => 'settings', 'uses' => 'DashboardController@settings']);
    Route::get('dice', function () {
        return view('dice/index');
    });

    // answers
    Route::get('answers/index/{person}', 'AnswerController@index');
    Route::get('answers/show/{id}', 'AnswerController@show');
    Route::get('answers/random', 'AnswerController@random');
    Route::post('answers', 'AnswerController@store');
    Route::delete('answers/delete/{id}', 'AnswerController@delete');

    Route::resource('Account', 'AccountController');

    Route::get('Action/all', 'ActionController@getAll');
    Route::get('Action/get/{id}', 'ActionController@getGet');
    Route::get('Action/random', 'ActionController@getRandom');
    Route::post('Action/assign', 'ActionController@postAssign');
    Route::get('Action/index', 'ActionController@getIndex');
    Route::get('Action/create', 'ActionController@getCreate');
    Route::post('Action/store', 'ActionController@postStore');
    Route::get('Action/{id}', 'ActionController@show');
    Route::get('Action/{id}/edit', 'ActionController@edit');

    Route::put('Action/update/{id}', 'ActionController@putUpdate');
    Route::post('Action/destroy/{id}', 'ActionController@postDestroy');
    Route::resource('Alert', 'AlertController');

    Route::get('Appointment/all', 'AppointmentController@getAll');
    Route::get('Appointment/get/{id}', 'AppointmentController@getGet');
    Route::get('Appointment/upcoming', 'AppointmentController@getUpcoming');
    Route::post('Appointment/store', 'AppointmentController@postStore');
    Route::get('Appointment/{id}', 'AppointmentController@show');
    Route::get('Appointment/{id}/edit', 'AppointmentController@edit');

    Route::get('Attribute/all', 'AttributeController@getAll');
    Route::get('Attribute/get/{id}', 'AttributeController@getGet');
    Route::get('Attribute/index', 'AttributeController@getIndex');
    Route::get('Attribute/create', 'AttributeController@getCreate');
    Route::post('Attribute/store', 'AttributeController@postStore');
    Route::get('Attribute/{id}', 'AttributeController@show');
    Route::get('Attribute/{id}/edit', 'AttributeController@edit');
    Route::put('Attribute/update/{id}', 'AttributeController@putUpdate');
    Route::post('Attribute/destroy/{id}', 'AttributeController@postDestroy');

    Route::patch('Base', 'Controller@patchIndex');
    Route::post('Base/attach', 'Controller@postAttach');
    Route::post('Base/delete/{model}', 'Controller@postDelete');
    Route::post('Base/detach', 'Controller@postDetach');
    Route::get('Base/constants', 'Controller@constants');

    Route::get('Bodypart/all', 'BodypartController@getAll');
    Route::get('Bodypart/get/{id}', 'BodypartController@getGet');
    Route::get('Bodypart/index', 'BodypartController@getIndex');
    Route::get('Bodypart/create', 'BodypartController@getCreate');
    Route::post('Bodypart/store', 'BodypartController@postStore');
    Route::get('Bodypart/{id}', 'BodypartController@show');
    Route::get('Bodypart/{id}/edit', 'BodypartController@edit');
    Route::put('Bodypart/update/{id}', 'BodypartController@putUpdate');
    Route::post('Bodypart/destroy/{id}', 'BodypartController@postDestroy');

    Route::get('Character/all', 'CharacterController@getAll');
    Route::get('Character/get/{id}', 'CharacterController@getGet');
    Route::post('Character/generate', 'CharacterController@postGenerate');
    Route::get('Character/detach/{model}/{model_id}/{character_id}', 'CharacterController@detach');
    Route::get('Character/index', 'CharacterController@getIndex');
    Route::get('Character/create', 'CharacterController@getCreate');
    Route::post('Character/store', 'CharacterController@postStore');
    Route::get('Character/{id}', 'CharacterController@show');
    Route::get('Character/{id}/edit', 'CharacterController@edit');
    Route::put('Character/update/{id}', 'CharacterController@putUpdate');

    Route::get('Dialog/all', 'DialogController@getAll');
    Route::get('Dialog/get/{id}', 'DialogController@getGet');
    Route::post('Dialog/store', 'DialogController@postStore');
    Route::put('Dialog/update/{id}', 'DialogController@putUpdate');

    Route::get('Effect/all', 'EffectController@getAll');
    Route::get('Effect/get/{id}', 'EffectController@getGet');
    Route::post('Effect/store', 'EffectController@postStore');
    Route::get('Effect/{id}', 'EffectController@show');
    Route::get('Effect/{id}/edit', 'EffectController@edit');
    Route::put('Effect/update/{id}', 'EffectController@putUpdate');
    Route::post('Effect/destroy/{id}', 'EffectController@postDestroy');

    Route::resource('Entry', 'EntryController');
    Route::get('entries/create', 'EntryController@create');

    Route::get('ForeplayGenerator', 'ForeplayGeneratorController@getIndex');

    Route::get('FavorClock', 'FavorClockController@getIndex');
    Route::put('FavorClock', 'FavorClockController@putIndex');

    Route::get('Helper/pluralize/{string}', 'HelperController@getPluralize');

    Route::get('Game/all', 'GameController@getAll');
    Route::get('Game/get/{id, $basic = false}', 'GameController@getGet');
    Route::get('Game/getedit/{id}', 'GameController@getGetedit');
    Route::get('Game/create', 'GameController@getCreate');
    Route::get('Game/play/{id}', 'GameController@getPlay');
    Route::get('Game/show/{id}', 'GameController@getShow');
    Route::post('Game/duplicate/{ids = false, $instance = false}', 'GameController@postDuplicate');
    Route::get('Game/end/{id, $restart = false}', 'GameController@getEnd');
    Route::get('Game/{id}/edit', 'GameController@edit');

    Route::put('Game/update/{id}', 'GameController@putUpdate');
    Route::post('Game/destroy/{id}', 'GameController@postDestroy');

    Route::get('Logic/all', 'LogicController@getAll');
    Route::get('Logic/get/{id}', 'LogicController@getGet');
    Route::get('Logic/index', 'LogicController@getIndex');
    Route::get('Logic/create', 'LogicController@getCreate');
    Route::post('Logic/store', 'LogicController@postStore');
    Route::put('Logic/update/{id}', 'LogicController@putUpdate');
    Route::post('Logic/destroy/{id}', 'LogicController@postDestroy');

    Route::get('lovers-lane/game', 'LoversLaneController@getGame');
    Route::patch('LoversLane/index', 'LoversLaneController@patchIndex');
    Route::get('loversLane/game/{id}', 'LoversLaneController@getGame');
    Route::patch('LoversLane', 'LoversLaneController@patchIndex');
    Route::get('games/lovers-lane', 'LoversLaneController@getIndex');

    Route::get('Place/all', 'PlaceController@getAll');
    Route::get('Place/get/{id}', 'PlaceController@getGet');
    Route::post('Place/store', 'PlaceController@postStore');
    Route::get('Place/{id}', 'PlaceController@show');
    Route::get('Place/{id}/edit', 'PlaceController@edit');

    Route::put('Place/update/{id}', 'PlaceController@putUpdate');
    Route::post('Place/destroy/{id}', 'PlaceController@postDestroy');

    Route::resource('Question', 'QuestionController');
    Route::get('questions/random', 'QuestionController@random');
    Route::get('questions/random-with-categories', 'QuestionController@randomWithCategories');

    Route::get('Map/all', 'MapController@getAll');
    Route::get('Map/get/{id}', 'MapController@getGet');
    Route::get('Map/index', 'MapController@getIndex');
    Route::get('Map/create', 'MapController@getCreate');
    Route::post('Map/store', 'MapController@postStore');
    Route::get('Map/{id}', 'MapController@show');
    Route::get('Map/{id}/edit', 'MapController@edit');
    Route::put('Map/update/{id}', 'MapController@putUpdate');
    Route::post('Map/destroy/{id}', 'MapController@postDestroy');

    Route::post('Media/all', 'MediaController@postAll');
    Route::get('Media/index', 'MediaController@getIndex');
    Route::get('Media/user{id}', 'MediaController@user');
    Route::get('Media/create', 'MediaController@getCreate');
    Route::post('Media/store', 'MediaController@postStore');
    Route::post('Media/{id}', 'MediaController@show');
    Route::post('Media/showAJAX/{id}', 'MediaController@showAJAX');
    Route::post('Media/{id}/edit', 'MediaController@edit');
    Route::put('Media/update/{id}', 'MediaController@putUpdate');
    Route::post('Media/destroy/{id}', 'MediaController@postDestroy');
    Route::get('Media/destroyAJAX/{id}', 'MediaController@DestroyAJAX');
    Route::get('Media/detach/{model}/{model_id}/{media_id}', 'MediaController@detach');

    Route::get('Modification/all/{id}', 'ModificationController@getAll');
    Route::get('Modification/get/{id}', 'ModificationController@getGet');
    Route::post('Modification/store/{id}', 'ModificationController@postStore');
    Route::put('Modification/update/{id}', 'ModificationController@putUpdate');

    Route::get('Narration/all/{id}', 'NarrationController@getAll');
    Route::get('Narration/get/{id}', 'NarrationController@getGet');
    Route::post('Narration/store/{id}', 'NarrationController@postStore');
    Route::put('Narration/update/{id}', 'NarrationController@putUpdate');

    Route::get('Option/all/{id}', 'OptionController@getAll');
    Route::get('Option/get/{id}', 'OptionController@getGet');
    Route::get('Option/index/{id}', 'OptionController@getIndex');
    Route::get('Option/create/{id}', 'OptionController@getCreate');
    Route::post('Option/store/{id}', 'OptionController@postStore');
    Route::get('Option/{id}', 'OptionController@show');
    Route::get('Option/{id}/edit', 'OptionController@edit');
    Route::put('Option/update/{id}', 'OptionController@putUpdate');
    Route::post('Option/destroy/{id}', 'OptionController@postDestroy');

    Route::get('Object/all/{id}', 'ObjectController@getAll');
    Route::get('Object/get/{id}', 'ObjectController@getGet');
    Route::get('Object/index/{id}', 'ObjectController@getIndex');
    Route::get('Object/create/{id}', 'ObjectController@getCreate');
    Route::post('Object/store/{id}', 'ObjectController@postStore');
    Route::get('Object/{id}', 'ObjectController@show');
    Route::get('Object/{id}/edit', 'ObjectController@edit');
    Route::put('Object/update/{id}', 'ObjectController@putUpdate');
    Route::post('Object/destroy/{id}', 'ObjectController@postDestroy');

    Route::get('Player/all', 'PlayerController@getAllPlayers');
    Route::get('Player/get/{id}', 'PlayerController@getGet');
    Route::post('Player/store/{id}', 'PlayerController@postStore');
    Route::put('Player/update/{id}', 'PlayerController@putUpdate');

    Route::get('Roleplay/generate', 'RoleplayController@getGenerate');
    Route::get('Roleplay/all', 'RoleplayController@getAllRoleplays');
    Route::get('Roleplay/index/{id}', 'RoleplayController@getIndex');
    Route::get('Roleplay/user/{id}', 'RoleplayController@byUser');
    Route::get('Roleplay/create/{id}', 'RoleplayController@getCreate');
    Route::post('Roleplay/store/{id}', 'RoleplayController@postStore');
    Route::get('Roleplay/{id}', 'RoleplayController@show');
    Route::get('Roleplay/{id}/edit', 'RoleplayController@edit');
    Route::put('Roleplay/update/{id}', 'RoleplayController@putUpdate');
    Route::post('Roleplay/destroy/{id}', 'RoleplayController@postDestroy');
    Route::resource('RoleplayOutline', 'RoleplayOutlineController');
    Route::resource('RoleplayScript', 'RoleplayScriptController');

    // roleplays
    Route::post('roleplays/disable', 'RoleplayController@disable');
    Route::post('roleplays/enable', 'RoleplayController@enable');
    Route::post('roleplays/delete', 'RoleplayController@delete');
    Route::get('roleplay-generator', 'RoleplayController@quick');
    Route::get('roleplays/user/{user_id}', 'RoleplayController@byUser');

    Route::post('Row/index', 'RowController@postIndex');
    Route::delete('Row/index/{$id}', 'RowController@deleteIndex');

    Route::resource('Service', 'ServiceController');
    Route::get('services/create', 'ServiceController@create');
    Route::get('services/add/{id}', 'ServiceController@add');
    Route::get('services/purchase/{id}', 'ServiceController@purchase');
    Route::get('services/{person?}', 'ServiceController@indexForUsers');
    Route::get('services/purchases/{person?}', 'ServiceController@purchases');
    Route::get('services/complete/{id}', 'ServiceController@complete');
    Route::resource('SexPosition', 'SexPositionController');
    Route::get('SexPosition/random', 'SexPositionController@random');

    Route::get('Stat/month/', 'StatController@getMonth');
    Route::get('Stat/month/{month}', 'StatController@getMonth');
    Route::post('Stat/update/{id}', 'StatController@postUpdate');
    Route::post('Stat/user-stats', 'StatController@getUserStats');
    Route::get('Stat/user-stats-and-calendar', 'StatController@getUserStatsAndCalendar');

    Route::get('users/show/{id}', 'UserController@show');

    // posts
    Route::get('posts', 'PostController@index');
    Route::get('posts/show/{id}', 'PostController@show');
    Route::get('posts/random-set', 'PostController@randomSet');

    // subscriptions
    Route::resource('Subscription', 'SubscriptionController');
    Route::post('Subscription/toggle', 'SubscriptionController@toggle');
    Route::post('Subscription/addPaymentMethod', 'SubscriptionController@addPaymentMethod');
    Route::post('Subscription/changePlan', 'SubscriptionController@changePlan');
    Route::put('Subscription/update/{account_id}', 'SubscriptionController@update');
    Route::get('subscription-plan', 'SubscriptionController@plan');

    Route::get('Trigger/all/{id}', 'TriggerController@getAll');
    Route::get('Trigger/get/{id}', 'TriggerController@getGet');
    Route::get('Trigger/index/{id}', 'TriggerController@getIndex');
    Route::get('Trigger/create/{id}', 'TriggerController@getCreate');
    Route::post('Trigger/store/{id}', 'TriggerController@postStore');
    Route::get('Trigger/{id}', 'TriggerController@show');
    Route::get('Trigger/{id}/edit', 'TriggerController@edit');
    Route::put('Trigger/update/{id}', 'TriggerController@putUpdate');
    Route::post('Trigger/destroy/{id}', 'TriggerController@postDestroy');

    Route::get('Timer/all', 'TimerController@getAll');
    Route::get('Timer/get/{$id}', 'TimerController@getGet');
    Route::get('Timer/index', 'TimerController@getIndex');
    Route::get('Timer/create', 'TimerController@getCreate');
    Route::post('Timer/store', 'TimerController@postStore');
    Route::get('Timer/{id}', 'TimerController@show');
    Route::get('Timer/{id}/edit', 'TimerController@edit');
    Route::put('Timer/update/{$id}', 'TimerController@putUpdate');
    Route::post('Timer/destroy/{$id}', 'TimerController@postDestroy');
    Route::get('Timer/all/{id}', 'TimerController@getAll');
    Route::get('Timer/get/{id}', 'TimerController@getGet');
    Route::get('Timer/index/{id}', 'TimerController@getIndex');
    Route::get('Timer/create/{id}', 'TimerController@getCreate');
    Route::post('Timer/store/{id}', 'TimerController@postStore');
    Route::get('Timer/{id}', 'TimerController@show');
    Route::get('Timer/{id}/edit', 'TimerController@edit');
    Route::put('Timer/update/{id}', 'TimerController@putUpdate');
    Route::post('Timer/destroy/{id}', 'TimerController@postDestroy');

    Route::get('User/{id}', 'UserController@show');

    Route::get('Uvent/all/{id}', 'UventController@getAll');
    Route::get('Uvent/get/{id}', 'UventController@getGet');
    Route::get('Uvent/upcoming/{id}', 'UventController@getUpcoming');
    Route::post('Uvent/store/{id}', 'UventController@postStore');
    Route::get('Uvent/{id}', 'UventController@show');
    Route::get('Uvent/{id}/edit', 'UventController@edit');
    Route::put('Uvent/update/{id}', 'UventController@putUpdate');
    Route::post('Uvent/destroy/{id}', 'UventController@postDestroy');

    Route::get('Verb/all/{id}', 'VerbController@getAll');
    Route::get('Verb/get/{id}', 'VerbController@getGet');
    Route::get('Verb/index/{id}', 'VerbController@getIndex');
    Route::get('Verb/create/{id}', 'VerbController@getCreate');
    Route::post('Verb/store/{id}', 'VerbController@postStore');
    Route::get('Verb/{id}', 'VerbController@show');
    Route::get('Verb/{id}/edit', 'VerbController@edit');
    Route::put('Verb/update/{id}', 'VerbController@putUpdate');
    Route::post('Verb/destroy/{id}', 'VerbController@postDestroy');

    // users
    Route::post('users/email', 'BlastController@createMail');
    Route::post('users/sms', 'BlastController@createSms');
    Route::get('users/{id}/privacy', 'UserController@privacy');
    Route::post('users/updateprivacy/{id}', 'UserController@updatePrivacy');

    // poker
    Route::get('poker/game', 'PokerController@getGame');
    Route::patch('poker', 'PokerController@patchIndex');
    Route::delete('poker', 'PokerController@deleteIndex');
    Route::get('games/strip-poker', 'PokerController@getIndex');

    // roleplayoutlines
    Route::post('roleplayoutlines/disable', 'RoleplayoutlineController@disable');
    Route::post('roleplayoutlines/enable', 'RoleplayoutlineController@enable');
    Route::post('roleplayoutlines/delete', 'RoleplayoutlineController@delete');
    Route::get('roleplayoutlines/user/{user_id}', 'RoleplayoutlineController@byUser');
});

##############################################################################################
# Admin Routes
##############################################################################################
Route::group(array('middleware' => 'admin'), function () {
    Route::resource('CompanyStat', 'CompanyStatController');
});

// subscriptions
Route::post('stripe/webhook', '\Laravel\Cashier\Http\Controllers\WebhookController@handleWebhook');

##############################################################################################
# Testing
##############################################################################################

Route::resource('test', 'TestController');
