<?php

use App\Repositories\QuestionRepository;

class AnswerController extends Controller {

    public function __construct(QuestionRepository $questionRepo)
    {
        $this->questionRepo = $questionRepo;
    }

    // return all answers
    public function index($person)
    {
        $params = request()->all();
        if ($person == 'My') {
            $user = auth()->user();
        } else {
            $user = auth()->user()->spouse;
        }
        $answers = $user->answers()->whereHas('question', function($query) use ($params) {
            $query->where('question_category_id', $params['question_category_id']);
        })->get();
        if (count($answers) == 0) {
            if ($person == 'My') {
                $answers = [$this->random($params)];
            }
        } else {
            foreach ($answers as $index => $answer) {
                $answers[$index]->question = $this->questionRepo->format($answer->question, true);
            }
        }
        return [
            'answers' => $answers,
            'categories' => QuestionCategory::all()
        ];
    }

    // random question/answer
    public function random()
    {
        $params = request()->all();
        $answer = new Answer;
        $answer->body = null;
        $answer->question = $this->questionRepo->random($params);
        return $answer;
    }

    // return the specified answer
    public function show($id)
    {
        return Answer::find($id);
    }

    // store or update an answer
    public function store()
    {
        $data = request()->all();
        if (isset($data['id'])) {
            $answer = Answer::find($data['id'])->update([
                'body' => $data['body']
            ]);
        } else {
            return Answer::create([
                'body' => $data['body'],
                'question_id' => $data['question']['id'],
                'user_id' => auth()->user()->id
            ]);
        }
    }

    // delete
    public function delete($id)
    {
        if (auth()->user()->answers()->find($id)) {
            Answer::destroy($id);
        }
    }
}
