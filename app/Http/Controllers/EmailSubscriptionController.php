<?php

use App\Services\MailService;

class EmailSubscriptionController extends Controller
{
    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
    }

    /**
     * Unsubscribe an email address
     */
    public function unsubscribe()
    {
        $params = request()->all();
        if ($user = User::where([
            'email' => $params['email']
        ])->first()) {
            $user->update([
                'unsubscribed' => true
            ]);
            return "You have been successfully unsubscribed from " . env('COMPANY_NAME');
        }
        return 'Subscriber not found.';
    }

    /**
     * Update a user's subscription
     */
    public function update()
    {
        $data = request()->all();

        // get user
        if (!$user = User::where('email', $data['email'])->first()) {
            return response([
                'errors' => ["Can't find user"]
            ], 404);
        }

        // detach user from lists
        $campaign = Campaign::where('key', 'new-users')->first();
        if ($user->campaigns()->find($campaign->id)) {
            $user->campaigns()->detach($campaign->id);
        }

        // add user to lists
        $campaign = Campaign::where('key', 'new-customers')->first();
        if (
            !$campaign->disabled
            && (!$campaign->start_at || $campaign_start_at < date('Y-m-d h:i:s'))
            && (!$campaign->end_at || $campaign_end_at > date('Y-m-d h:i:s'))
        ) {
            if (!$user->campaigns()->find($campaign->id)) {
                $user->campaigns()->save($campaign);
            }
        }
    }
}
