<?php

class ColController extends Controller {

    // all cols
    public function getAll() {
        return Col::all();
    }

    // get col
    public function getGet($id) {
        return Col::find($id);
    }

    /**
     * Display a listing of cols
     */
    public function getIndex()
    {
        $cols = Col::all();

        return View::make('col.index', compact('cols'));
    }

    /**
     * Show the form for creating a new col
     */
    public function getCreate()
    {
        return View::make('col.create');
    }

    /**
     * Store a newly created col in storage.
     */
    public function postStore()
    {
        $validator = Validator::make($data = Input::all(), Col::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        Col::create($data);

        return Redirect::route('cols.index')->with('message', 'Col created.');
    }

    /**
     * Display the specified col.
     */
    public function show($id)
    {
        $col = Col::findOrFail($id);

        return View::make('col.show', compact('col'));
    }

    /**
     * Show the form for editing the specified col.
     */
    public function edit($id)
    {
        $col = Col::find($id);

        return View::make('col.edit', compact('col'));
    }

    /**
     * Update the specified col in storage.
     */
    public function putUpdate($id)
    {
        $col = Col::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Col::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $col->update($data);
    }

    /**
     * Remove the specified col from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postDestroy($id)
    {
        Col::destroy($id);

        return Redirect::route('cols.index')->with('message', 'Col deleted.');
    }

}
