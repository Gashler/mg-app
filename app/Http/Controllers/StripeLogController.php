<?php

class StripeController extends Controller {

    /**
     * Webhook for Stripe
     */
    public function webhook()
    {
        \Stripe\Stripe::setApiKey(config('services.stripe.key'));
        $data = request()->all();
        StripeLog::create($data);
        return response(null, 200);
    }
}
