<?php

use App\Repositories\StatRepository;

class StatController extends Controller {

    public function __construct(StatRepository $statRepo) {
        $this->statRepo = $statRepo;
    }

    /**
     * Get Month
     */
    public function getMonth($date = null)
    {
        if (!$date) {
            $date = date('Y-m');
        }
        return $this->statRepo->month($date);
    }

    /**
     * Create or update stats
     */
    public function postUpdate()
    {
        $data = Request::all();
        $data['account_id'] = Auth::user()->account_id;
        $stat = Stat::firstOrNew(['id' => Request::get('id')]);
        unset($data['model']);
        foreach ($data as $key => $value) {
            $stat->$key = $value;
        }
        $stat->save();
        return $stat;
    }

    /**
    * Get User stats and calendar data
    */
    public function getUserStatsAndCalendar()
    {
        return [
            'stats' => $this->statRepo->stats(),
            'year' => $this->statRepo->year()
        ];
    }

    /**
    * Get User Stats verbose
    */
    public function getUserStats()
    {
        return $this->statRepo->stats();
    }
}
