<?php

class NarrationController extends Controller {

    // all narrations
    public function getAll() {
        return Narration::all();
    }

    // get narration
    public function getGet($id) {
        return Narration::find($id);
    }

    // store narration
    public function postStore() {
        $validator = Validator::make($data = Input::all(), Narration::$rules);
        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        return Narration::create($data);
    }

    // update narration
    public function putUpdate($id) {
        $narration = Narration::findOrFail($id);
        $validator = Validator::make($data = Input::all(), Narration::$rules);
        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $narration->update($data);
        return Model::find($narration->id);
    }

}
