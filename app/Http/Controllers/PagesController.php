<?php

class PagesController extends Controller {

    // home
    public function home()
    {
        // if not logged in, redirect to home page
        if (!auth()->check()) {
            return view('pages.home');
        }

        // logged in, redirect to dashboard
        return view('layouts.default');
    }
}
