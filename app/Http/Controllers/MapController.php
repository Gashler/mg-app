<?php

class MapController extends Controller
{

    // all maps
    public function getAll()
    {
        return Map::all();
    }

    // get map
    public function getGet($id)
    {
        return Map::find($id);
    }

    /**
     * Display a listing of maps
     */
    public function getIndex()
    {
        $maps = Map::all();

        return View::make('map.index', compact('maps'));
    }

    /**
     * Show the form for creating a new map
     */
    public function getCreate()
    {
        return View::make('map.create');
    }

    /**
     * Store a newly created map in storage.
     */
    public function postStore()
    {
        $validator = Validator::make($data = Input::all(), Map::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        Map::create($data);

        return Redirect::route('maps.index')->with('message', 'Map created.');
    }

    /**
     * Display the specified map.
     */
    public function show($id)
    {
        $map = Map::findOrFail($id);

        return View::make('map.show', compact('map'));
    }

    /**
     * Show the form for editing the specified map.
     */
    public function edit($id)
    {
        $map = Map::find($id);

        return View::make('map.edit', compact('map'));
    }

    /**
     * Update the specified map in storage.
     */
    public function putUpdate($id)
    {
        $map = Map::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Map::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $map->update($data);

        return Redirect::route('maps.show', $id)->with('message', 'Map updated.');
    }

    /**
     * Remove the specified map from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postDestroy($id)
    {
        Map::destroy($id);

        return Redirect::route('maps.index')->with('message', 'Map deleted.');
    }
}
