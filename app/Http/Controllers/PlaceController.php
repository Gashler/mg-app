<?php

class PlaceController extends Controller
{

    // all places
    public function getAll()
    {
        return Place::all();
    }

    // get place
    public function getGet($id)
    {
        return Place::find($id);
    }

    /**
     * Store a newly created place in storage.
     */
    public function postStore()
    {
        $validator = Validator::make($data = Input::all(), Place::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $place = Place::create($data);
        return Place::with('characters', 'objects', 'timers')->find($place->id);
    }

    /**
     * Display the specified place.
     */
    public function show($id)
    {
        $place = Place::findOrFail($id);

        return View::make('place.show', compact('place'));
    }

    /**
     * Show the form for editing the specified place.
     */
    public function edit($id)
    {
        $place = Place::find($id);

        return View::make('place.edit', compact('place'));
    }

    /**
     * Update the specified place in storage.
     */
    public function putUpdate($id)
    {
        $place = Place::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Place::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $place->update($data);
        return [
            'success' => 1,
        ];
    }

    /**
     * Remove the specified place from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postDestroy($id)
    {
        Place::destroy($id);

        return Redirect::route('place.index')->with('message', 'Place deleted.');
    }
}
