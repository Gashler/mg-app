<?php

class AppointmentController extends Controller {

    // all appointments
    public function getAll() {
        return Appointment::all();
    }

    // get appointment
    public function getGet($id) {
        return Appointment::with(
            'actions'
        )->find($id);
    }

    // upcoming events
    public function getUpcoming() {
        return $events = Appointment::where('date_start', '<', time())->orderBy('date_start', 'desc')->get()->take(5);
    }

    /**
     * Store a newly created appointment in storage.
     */
    public function postStore()
    {
        $validator = Validator::make($data = Input::all(), Appointment::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $appointment = Appointment::create($data);
        return Appointment::with('actions')->find($appointment->id);
    }

    /**
     * Display the specified appointment.
     */
    public function show($id)
    {
        $appointment = Appointment::findOrFail($id);

        return View::make('appointment.show', compact('appointment'));
    }

    /**
     * Show the form for editing the specified appointment.
     */
    public function edit($id)
    {
        $appointment = Appointment::with('actions','options')->find($id);

        return View::make('appointment.edit', compact('appointment'));
    }

    /**
     * Update the specified appointment in storage.
     */
    public function putUpdate($id)
    {
        $appointment = Appointment::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Appointment::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $appointment->update($data);

        // return Redirect::route('appointment.show', $id)->with('message', 'Appointment updated.');
    }

    /**
     * Remove the specified appointment from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postDestroy($id)
    {
        Appointment::destroy($id);
        // return Redirect::route('appointment.index')->with('message', 'Appointment deleted.');
    }

}
