<?php

class AttributeController extends Controller {

    // all attributes
    public function getAll() {
        return Attribute::all();
    }

    // get attribute
    public function getGet($id) {
        return Attribute::find($id);
    }

    /**
     * Display a listing of attributes
     */
    public function getIndex()
    {
        $attributes = Attribute::all();

        return View::make('attribute.index', compact('attributes'));
    }

    /**
     * Show the form for creating a new attribute
     */
    public function getCreate()
    {
        return View::make('attribute.create');
    }

    /**
     * Store a newly created attribute in storage.
     */
    public function postStore()
    {
        $validator = Validator::make($data = Input::all(), Attribute::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        Attribute::create($data);

        return Redirect::route('attribute.index')->with('message', 'Attribute created.');
    }

    /**
     * Display the specified attribute.
     */
    public function show($id)
    {
        $attribute = Attribute::findOrFail($id);

        return View::make('attribute.show', compact('attribute'));
    }

    /**
     * Show the form for editing the specified attribute.
     */
    public function edit($id)
    {
        $attribute = Attribute::find($id);

        return View::make('attribute.edit', compact('attribute'));
    }

    /**
     * Update the specified attribute in storage.
     */
    public function putUpdate($id)
    {
        $attribute = Attribute::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Attribute::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $attribute->update($data);

        return Redirect::route('attribute.show', $id)->with('message', 'Attribute updated.');
    }

    /**
     * Remove the specified attribute from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postDestroy($id)
    {
        Attribute::destroy($id);

        return Redirect::route('attribute.index')->with('message', 'Attribute deleted.');
    }

}
