<?php

use App\Repositories\AuthRepository;
use App\Repositories\UserRepository;
use App\Services\MailService;

class SessionController extends Controller {

    public function __construct(
        AuthRepository $authRepo,
        MailService $mailService,
        UserRepository $userRepo
    ) {
        $this->authRepo = $authRepo;
        $this->userRepo = $userRepo;
        $this->mailService = $mailService;
    }

    /**
     * Show the form for creating a new session
     */
    public function create()
    {
        if(auth()->check())
        {
            return Redirect::to('/');
        }
        $errors[] = Input::get('message');
        return view('sessions.create', compact('errors'));
    }

    /**
     * Create a new session (log a user in)
     * POST /sessions
     *
     * @return Response
     */
    public function store($user = null)
    {
        return $this->authRepo->store($user);
    }

    /**
     * After being redirected from authenticating with Wordpress
     * POST /sessions
     *
     * @return Response
     */
    public function wp()
    {
        $data = request()->all();
        if (DB::table('tokens')->where('key', 'wp')->where('value', $data['token'])) {
            return $this->store();
        } else {
            return "Token mismatch error.";
        }
    }

    // log in as user
    public function loginAsUser($user_id = null)
    {
        if (!isset($user_id)) {
            $user_id = request('user_id');
        }
        if (auth()->user()->hasRole(['Admin']) || session('loggedOutAsAdmin')) {
            session()->put('loggedOutAsAdmin', true);
            auth()->loginUsingId($user_id);
            return redirect('/');
        } else {
            return 'Unauthorized';
        }
    }

    /**
     * Switch user
     * POST /sessions
     *
     * @return Response
     */
    public function switchUser($gender)
    {
        $user = auth()->user();
        auth()->logout();
        if ($user->gender == $gender) {
            $this->store($user);
        } else {
            $this->store($user->spouse);
        }
        return auth()->user();
    }

    /**
     * Remove the specified resource from storage.
     * DELETE /sessions/{id}
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy()
    {
        session()->flush();
        auth()->logout();
        return redirect('/login');
    }

    // forgot password
    public function forgotPassword()
    {
        return view('sessions/password-reset');
    }

    // reset password
    public function resetPassword()
    {
        if (!$user = User::where('email', request('email'))->first()) {
            $message = "Can't find a user with the email address " . request('email');
            return redirect()->back()->with('message_danger', $message);
        }
        $user->update(['key' => str_random(40)]);
        if ($success = $this->mailService->send('reset-password', $user)) {
            $message = "An email has been sent to " . request('email') . " with instructions for logging in.";
            return redirect()->back()->with('message', $message);
        };
        return redirect()->back()->with('message_danger', $response['message']);
    }

    // reset password
    public function confirmResetPassword()
    {
        if (!$user = User::where([
            'email' => request('email'),
            'key' => request('key')
        ])->first()) {
            return "We're having trouble verifying your account. Make sure you click on the link in the most recent email that was sent to you.";
        }
        $user->update(['need_password' => 1]);
        auth()->login($user);
        return redirect('/#/password');
    }
}
