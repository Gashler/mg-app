<?php

use App\Repositories\CompanyStatRepository;

class CompanyStatController extends Controller {

    public function __construct(CompanyStatRepository $companyStatRepo) {
        $this->companyStatRepo = $companyStatRepo;
    }

    /**
     * Get Month
     */
    public function index()
    {
        return $this->companyStatRepo->chartData(request()->all());
    }
}
