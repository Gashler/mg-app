<?php

use App\Repositories\PokerRepository;

class PokerController extends Controller
{
    public function __construct(PokerRepository $pokerRepo)
    {
        $this->pokerRepo = $pokerRepo;
    }

    ###########################################
    # Get the existing game or start a new game
    ###########################################
    public function getGame()
    {
        $spouseGenderPrefix = auth()->user()->spouse->gender . '_';

        // retrieve existing game
        if ($poker = auth()->user()->account->poker) {
            $poker->data = $this->pokerRepo->simplifyData($poker[$spouseGenderPrefix . 'data']);
            unset($poker->f_data, $poker->m_data);

        // or create new game
        } else {
            $poker = $this->pokerRepo->create();
        }

        // determine if both players can afford the ante
        if (auth()->user()->money <= 0 || auth()->user()->spouse->money <= 0) {
            $noAnte = true;
        } else {
            $noAnte = false;
        }

        return $poker;
    }

    #########################
    # Update the game
    #########################
    public function patchIndex()
    {
        $userGenderPrefix = auth()->user()->gender . '_';
        $spouseGenderPrefix = auth()->user()->spouse->gender . '_';

        // prepare data
        $poker = Poker::where('account_id', auth()->user()->account->id)->first();
        $updates = request()->all();
        $spouse_updated_at = $updates['meta']['spouse_updated_at'];
        if (count($updates['data']) > 0) {
            $dataset = $poker[$userGenderPrefix . 'data'];
            foreach ($poker[$userGenderPrefix . 'data'] as $oldKey => $oldValue) {
                foreach ($updates['data'] as $newKey => $newValue) {
                    if ($newKey == $oldKey) {
                        $dataset[$oldKey] = [
                            'updated_at' => microtime(true),
                            'value' => $newValue
                        ];
                        if ($newKey == $userGenderPrefix . 'money') {
                            auth()->user()->update([
                                'money' => $newValue
                            ]);
                        } elseif ($newKey == $spouseGenderPrefix . 'money') {
                            auth()->user()->spouse->update([
                                'money' => $newValue
                            ]);
                        }
                    }
                }
            }
            $dataset[$userGenderPrefix . 'updated_at'] = [
                'updated_at' => microtime(true),
                'value' => microtime(true)
            ];
            auth()->user()->account->poker->update([
                $userGenderPrefix . 'data' => $dataset
            ]);
        }

        \Log::info([
            $poker[$spouseGenderPrefix . 'data'][$spouseGenderPrefix . 'updated_at']['value'],
            $spouse_updated_at
        ]);

        if ($poker[$spouseGenderPrefix . 'data'][$spouseGenderPrefix . 'updated_at']['value'] > $spouse_updated_at) {
            return $this->pokerRepo->getSpouseUpdates($spouse_updated_at);
        }
    }

    ###########################################
    # Delete game
    ###########################################
    public function deleteIndex()
    {
        Poker::destroy(auth()->user()->account->poker->id);
    }
}
