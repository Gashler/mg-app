<?php

class MediaController extends Controller
{
    public function postAll()
    {

        // get data
        $data = Input::all();

        // prepare query
        $query = '$medias = Media::';
        if (isset($data['user_id'])) {
            $query .= 'where("user_id", ' . $data["user_id"] . ')';
        }
        if (isset($data['type'])) {
            $query .= '->where("type", "' . $data["type"] . '")';
        }
        if (isset($data['start_date'])) {
            $query .= '->where("created_at", ">=", ' . $data["start_date"] . ')';
        }
        if (isset($data['end_date'])) {
            $query .= '->where("created_at", "<=", ' . $data["end_date"] . ')';
        }
        if (isset($data['take'])) {
            $query .= '->take(' . $data["take"] . ')';
        }
        $query .= '->get();';

        // clean up query
        $query = str_replace('::->', '::', $query);

        // evaluate query
        eval($query);

        // return data
        return $medias;
    }

    /**
     * Display a listing of media
     *
     * @return Response
     */
    public function getIndex()
    {
        if (isset($_GET['filter'])) {
            $filter = $_GET['filter'];
            $filter = str_replace('-', ' ', $filter);
        }
        return View::make('media.index', compact('filter'));
    }

    /**
     * Display a listing of media belonging to a user
     *
     * @return Response
     */
    public function user($id)
    {
        $user = User::findOrFail($id);
        if (isset($_GET['filter'])) {
            $filter = $_GET['filter'];
            $filter = str_replace('-', ' ', $filter);
        }
        return View::make('media.index', compact('user', 'filter'));
    }

    /**
     * Show the form for creating a new media
     *
     * @return Response
     */
    public function getCreate()
    {
        $tags = TagModel::where('taggable_type', 'Media')->select('name')->groupBy('name')->get();
        return View::make('media.create', compact('tags'));
    }

    /**
     * Upload media
     */

    public function postStore()
    {
        $data = Input::all();
        $index = 0;
        $processed_files = [];
        // pre2($data);
        foreach ($data['files'] as $file) {

            // create path for files if it doesn't already exist
            $path = Auth::user()->account_id . date('/Y/m');
            $full_path = public_path() . '/uploads/' . Auth::user()->account_id . '/' . date('Y') . '/' . date('m');
            if (!file_exists(public_path() . '/uploads/' . Auth::user()->account_id . '/' . date('Y') . '/' . date('m'))) {
                mkdir(public_path() . '/uploads/' . Auth::user()->account_id . '/' . date('Y') . '/' . date('m'), 0755, true);
            }

            // add slashes for easier appending
            $path .= '/';
            $full_path .= '/';

            // get file extension
            $data['extension'] = strtolower($file->getClientOriginalExtension());

            // get filename
            $filename = basename($_FILES['files']["name"][$index]);
            $filename = explode('.', $filename);
            $filename = $filename[0];

            // if proposed url already exists, generate a new filename
            $url = $path . $filename;
            $existing_file = Media::where('url', $url)->get();
            if (count($existing_file) > 0) {
                $filename = Auth::user()->first_name . Auth::user()->last_name . time() . '-' . $index;
                $url = $path . $filename;
            }

            // move the uploaded file to its destination
            $uploadSuccess = $file->move($full_path, $filename . '.' . $data['extension']);

            // common media types
            $raster_image_extensions = ['jpg', 'jpeg', 'png', 'gif'];
            $image_extensions = ['svg', 'tiff', 'psd', 'ai', 'eps'];
            $video_extensions = ['avchd', 'avi', 'flv', 'mpeg', 'mpg', 'mp4', 'wmv', 'mov', 'flv', 'rm', 'vob', 'swf'];
            $audio_extensions = ['wav', 'mp3', 'wma', 'flac', 'ogg', 'ra', 'ram', 'rm', 'mid', 'aiff', 'mpa', 'm4a', 'aif', 'iff'];
            $pdf_extensions = ['pdf'];
            $document_extensions = ['doc', 'docx', 'odt', 'pages', 'rtf', 'wpd', 'wps'];
            $spreadsheet_extensions = ['gnumeric', 'gnm', 'ods', 'xls', 'xlsx', 'xlsm', 'xlsb', 'csv'];
            $text_extensions = ['txt', 'log', 'msg', 'tex'];
            $presentation_extensions = ['key', 'ppt', 'pptx', 'odp'];
            $code_extensions = ['html', 'php', 'js', 'xml', 'json', 'c', 'class', 'cpp', 'cs', 'dtd', 'fla', 'h', 'java', 'lua', 'm', 'pl', 'py', 'sh', 'sln', 'swift', 'vcxproj', 'xcodeproj'];
            $database_extensions = ['odb', 'db', 'mdb', 'accdb', 'dbf', 'pdb', 'sql'];
            $archive_extensions = ['7z', 'cbr', 'deb', 'gz', 'pkg', 'rar', 'rpm', 'sitx', 'tar.gz', 'zip', 'zipx'];

            // if media type is image
            if (in_array($data['extension'], $raster_image_extensions)) {

                // now you are able to resize the instance
                $upload_path = $full_path . $filename;
                $img = Image::make($upload_path . '.' . $data['extension']);
                $data['width'] = $img->width();
                $data['height'] = $img->height();
                $data['size'] = $img->filesize();

                // save original and xxs size
                Image::make($upload_path . '.' . $data['extension'])->save($upload_path . '.' . $data['extension'])->fit(50, 50)->save($upload_path . '-xxs.' . $data['extension']);

                // xs size
                Image::make($upload_path . '.' . $data['extension'])->fit(100, 100)->save($upload_path . '-xs.' . $data['extension']);

                // sm size
                Image::make($upload_path . '.' . $data['extension'])->fit(200, 200)->save($upload_path . '-sm.' . $data['extension']);

                // md size
                if ($data['width'] >= 400) {
                    Image::make($upload_path . '.' . $data['extension'])->resize(null, 400, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($upload_path . '-md.' . $data['extension']);
                }

                // lg size
                if ($data['width'] >= 750) {
                    Image::make($upload_path . '.' . $data['extension'])->resize(null, 600, function ($constraint) {
                        $constraint->aspectRatio();
                    })->save($upload_path . '-lg.' . $data['extension']);
                }

                // xl size
                if ($data['width'] >= 1200) {
                    Image::make($upload_path . '.' . $data['extension'])->fit(1200, 1200)->save($upload_path . '-xl.' . $data['extension']);
                }

                $data['type'] = 'Image';
            }

            // assign other media types
            elseif (in_array($data['extension'], $image_extensions)) {
                $data['type'] = 'Image media';
            } elseif (in_array($data['extension'], $video_extensions)) {
                $data['type'] = 'Video';
            } elseif (in_array($data['extension'], $audio_extensions)) {
                $data['type'] = 'Audio';
            } elseif (in_array($data['extension'], $pdf_extensions)) {
                $data['type'] = 'PDF';
            } elseif (in_array($data['extension'], $document_extensions)) {
                $data['type'] = 'Document';
            } elseif (in_array($data['extension'], $spreadsheet_extensions)) {
                $data['type'] = 'Spreadsheet';
            } elseif (in_array($data['extension'], $text_extensions)) {
                $data['type'] = 'Text';
            } elseif (in_array($data['extension'], $presentation_extensions)) {
                $data['type'] = 'Presentation';
            } elseif (in_array($data['extension'], $code_extensions)) {
                $data['type'] = 'Code';
            } elseif (in_array($data['extension'], $database_extensions)) {
                $data['type'] = 'Database';
            } elseif (in_array($data['extension'], $archive_extensions)) {
                $data['type'] = 'Archive';
            } else {
                $data['type'] = 'File';
            }

            // assign values to data array
            $user_id = Auth::user()->id;
            $file_number = $index + 1;
            if ($index < 10) {
                $file_number = '00' . $file_number;
            } elseif ($index < 100) {
                $file_number = '0' . $file_number;
            }
            $processed_files[$index]['url'] = $url;
            $processed_files[$index]['user_id'] = $user_id;
            $processed_files[$index]['type'] = $data['type'];
            $processed_files[$index]['height'] = $data['height'];
            $processed_files[$index]['width'] = $data['width'];
            $processed_files[$index]['size'] = $data['size'];
            $processed_files[$index]['extension'] = $data['extension'];
            if (isset($data['title'])) {
                if ($data['title'] == '') {
                    $processed_files[$index]['title'] = $filename . '.' . $data['extension'];
                } else {
                    $processed_files[$index]['title'] = $data['title'] . ' ' . $file_number;
                }
            }
            if (isset($data['description'])) {
                $processed_files[$index]['description'] = $data['description'];
            }
            $index++;
        }

        // store in db and redirect
        $medias = [];
        foreach ($processed_files as $processed_file) {
            $media = Media::create($processed_file);
            $medias[] = $media;

            // attach media
            if (isset($data['model']) && isset($data['id'])) {
                $model = $data['model']::find($data['id']);
                $model->medias()->save($media);
            }

            // store tags
            if (isset($data['tags'])) {
                foreach ($data['tags'] as $tag) {
                    $new_tag = TagModel::create([
                        'name' => $tag['name']
                    ]);
                    $media->tags()->save($new_tag);
                }
            }
        }

        // return media
        if (count($medias > 1)) {
            return $medias;
        } else {
            return $medias[0];
        }
    }

    /**
     * Display the specified media.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $media = Media::findOrFail($id);
        $tags = $media->tags;
        return View::make('media.show', compact('media', 'tags'));
    }

    /**
     * Display the specified media via ajax.
     *
     * @param  int  $id
     * @return Response
     */
    public function showAJAX($id)
    {
        $media = Media::findOrFail($id);
        $tags = $media->tags;
        return View::make('media.show-ajax', compact('media', 'tags'));
    }

    /**
     * Show the form for editing the specified media.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $media = Media::find($id);
        $tags = TagModel::where('taggable_type', 'Media')->select('name')->groupBy('name')->get();
        return View::make('media.edit', compact('media', 'tags'));
    }

    /**
     * Update the specified media in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function putUpdate($id)
    {
        $media = Media::findOrFail($id);

        // validation
        $rules = [
            'media' => 'sometimes|max:5000',
        ];
        $validator = Validator::make($data = Input::all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        // format checkboxes for db
        $data['reps'] = isset($data['reps']) ? 1 : 0;

        // process file
        if ($data['media'] != '') {
            include app_path() . '/helpers/processMedia.php';

            // if old file exists, delete it
            $old_file = $media->url;
            if (is_file(public_path() . '/uploads/' . $old_file)) {
                unlink(public_path() . '/uploads/' . $old_file);
            }
        }

        // if role is Superadmin, Admin, or Editor, set owner id to 0
        if (Auth::user()->hasRole(['Superadmin', 'Admin', 'Editor'])) {
            $data['user_id'] = Config::get('site.apex_user_id');
        }

        // update db
        $media->update($data);

        // store tags
        if (isset($data['tags'])) {
            foreach ($data['tags'] as $tag) {
                $new_tag = TagModel::create([
                    'name' => $tag['name']
                ]);
                $media->tags()->save($new_tag);
            }
        }

        if (Auth::user()->hasRole(['Superadmin', 'Admin', 'Editor'])) {
            $user_id = Config::get('site.apex_user_id');
        } else {
            $user_id = Auth::user()->id;
        }
        return Redirect::route('media/user', compact('user_id'))->with('message', 'Resource updated.');
    }

    /**
     * Remove the specified media from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postDestroy($id)
    {
        $media = Media::find($id);

        /***************
         * delete images
         ****************/

        // original
        $url = public_path() . '/' . $media->url;
        if (file_exists($url)) {
            unlink($url);
        }

        // other sizes
        $sizes = [
            'xl',
            'lg',
            'md',
            'sm',
            'xs',
            'xxs'
        ];
        foreach ($sizes as $size) {
            $url = public_path() . '/' . $media->$size;
            if (file_exists($url)) {
                unlink($url);
            }
        }

        // delete tags
        if (isset($media->tags)) {
            foreach ($media->tags as $tag) {
                TagModel::destroy($tag->id);
            }
        }

        // delete media
        Media::destroy($id);

        return Redirect::back()->with('message', 'Resource deleted.');
    }

    /**
     * Remove the specified media from storage through AJAX.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroyAJAX($id)
    {
        $media = Media::find($id);

        // delete images
        $files = [];
        $files[] = $media;
        deleteImages($files);

        // delete tags
        if (isset($media->tags)) {
            foreach ($media->tags as $tag) {
                TagModel::destroy($tag->id);
            }
        }

        // delete media row
        Media::destroy($id);
    }

    /**
     * Detach media
     */
    public function detach($model, $model_id, $media_id)
    {
        $model = $model::find($model_id);
        $model->media()->detach($media_id);
    }
}
