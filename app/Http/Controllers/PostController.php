<?php

class PostController extends Controller {

    // index
    public function index()
    {
        $params = request()->all();
        $categories = PostCategory::all();
        $category = new PostCategory;
        $category->id = 0;
        $category->name = "All Articles";
        $categories[] = $category;
        if (isset($params['category_id']) && $params['category_id']) {
            $category = PostCategory::with('posts')->find($params['category_id']);
            $posts = $category->posts;
            unset($category->posts);
        } else {
            $posts = Post::all();
        }
        return [
            'categories' => $categories,
            'category' => $category,
            'posts' => $posts
        ];
    }

    // index
    public function show($id)
    {
        $post = Post::with('categories')->find($id);

        // insert line breaks
        $array = explode('.', $post->body);
        foreach ($array as $index => $value) {
            if (
                $index >= 3 && $index % 3 == 0
                && strpos($array[$index + 1], 'jpg') !== 0
                && strpos($array[$index + 1], 'png') !== 0
            ) {
                $array[$index] .= '.<br><br>';
            } else {
                $array[$index] .= '.';
            }
        }
        $post->body = implode('', $array);
        return $post;
    }

    // return a random post from each category
    public function randomSet()
    {
        $categories = PostCategory::where('id', '!=', 2)->get();
        foreach ($categories as $index => $category) {
            $categories[$index]->post = $category->posts()->inRandomOrder()->first();
        }
        return $categories;
    }
}
