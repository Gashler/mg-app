<?php

class AccountController extends Controller
{
    public function show($account_id)
    {
        return Account::with('users')->find($account_id);
    }
}
