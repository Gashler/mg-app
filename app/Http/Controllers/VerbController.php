<?php

class VerbController extends Controller
{

    // all verbs
    public function getAll()
    {
        return Verb::all();
    }

    // get verb
    public function getGet($id)
    {
        return Verb::find($id);
    }

    /**
     * Display a listing of verbs
     */
    public function getIndex()
    {
        $verbs = Verb::all();

        return View::make('verb.index', compact('verbs'));
    }

    /**
     * Show the form for creating a new verb
     */
    public function getCreate()
    {
        return View::make('verb.create');
    }

    /**
     * Store a newly created verb in storage.
     */
    public function postStore()
    {
        $validator = Validator::make($data = Input::all(), Verb::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        Verb::create($data);

        return Redirect::route('verb.index')->with('message', 'Verb created.');
    }

    /**
     * Display the specified verb.
     */
    public function show($id)
    {
        $verb = Verb::findOrFail($id);

        return View::make('verb.show', compact('verb'));
    }

    /**
     * Show the form for editing the specified verb.
     */
    public function edit($id)
    {
        $verb = Verb::find($id);

        return View::make('verb.edit', compact('verb'));
    }

    /**
     * Update the specified verb in storage.
     */
    public function putUpdate($id)
    {
        $verb = Verb::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Verb::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $verb->update($data);

        return Redirect::route('verb.show', $id)->with('message', 'Verb updated.');
    }

    /**
     * Remove the specified verb from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postDestroy($id)
    {
        Verb::destroy($id);

        return Redirect::route('verb.index')->with('message', 'Verb deleted.');
    }
}
