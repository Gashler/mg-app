<?php

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Repositories\UserRepository;
use App\Services\MailService;

class RegisterController extends Controller
{
    public function __construct(
        UserRepository $userRepo,
        MailService $mailService
    ) {
        $this->userRepo = $userRepo;
        $this->mailService = $mailService;
    }

    /**
     * Return new registrant data
     */
    public function data()
    {
        if (!$users = session('users')) {
            $users = [
                'Husband' => [],
                'Wife' => []
            ];
        }
        $oauth = session('oauth');
        if (isset($oauth['users'])) {
            $your = 'Your ';
            $users = $oauth['users'];
        } else {
            $your = '';
        }
        return [
            'users' => $users,
            'oauth' => $oauth,
            'your' => $your
        ];
    }

    /**
     * Return the registration form
     */
    public function create()
    {
        if (!auth()->check()) {
            $email = Input::get('email');
            if ($user = $this->userRepo->where(['email' => $email], 1)) {
                if (isset($user->account)) {
                    return redirect('/login')->with('message_danger', "You already have an account. Please log in to continue.");
                }
            }
            $registering = true;
            return view('register.create', compact('email', 'registering'));
        }
        return redirect('/#/register');
    }

    /**
     * Store new registrants
     */
    public function store()
    {
        return $this->userRepo->createCouple(request()->all());
    }

    /**
     * Verify an email address
     */
    public function verify()
    {
        $params = request()->all();
        if ($user = $this->userRepo->where([
            'email' => $params['email'],
            'key' => $params['key']
        ], 1, ['oauthLogins'])) {
            if (!$user->verified) {
                $user->update([
                    'verified' => true,
                    'received_campaign_email_at' => date('Y-m-d h:i:s')
                ]);
                auth()->login($user);

                // subscribe new user to mailing list
                if (!auth()->user()->account->subscribed) {
                    $campaign = Campaign::where('key', 'new-customers')->first();
                    if (
                        !$campaign->disabled
                        && (!$campaign->start_at || $campaign_start_at < date('Y-m-d h:i:s'))
                        && (!$campaign->end_at || $campaign_end_at > date('Y-m-d h:i:s'))
                    ) {
                        if (!$user->campaigns()->find($campaign->id)) {
                            $user->campaigns()->save($campaign);
                        }
                    }
                }

                // if user doesn't have password or oauth login, redirect to password creation form
                if (count($user->oauthLogins) == 0 && !$user->password) {
                    $user->update([
                        'need_password' => true
                    ]);
                    return redirect('/#/add-password');
                }
                return redirect('/');
            } else {
                if (count($user->oauthLogins) == 0 && !$user->password) {
                    $user->update([
                        'need_password' => true
                    ]);
                    return redirect('/#/add-password');
                }
                return redirect('/')->with('error', "Your account has already been verified. Please log in to continue.");
            }
        }
        return "We're having trouble authenticating your account. Make sure you click on the link in the most recent email that was sent to you.";
    }

    /**
     * Send verification email again.
     */
    public function sendAgain()
    {
        if (auth()->check()) {
            $response = $this->mailService->send('subscription-confirmation', auth()->user());
            if ($response['error']) {
                return ['errors' => [$response]];
            }
            return $response;
        }
    }
}
