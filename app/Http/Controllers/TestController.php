<?php

use App\Repositories\ActionRepository;
use App\Repositories\QuestionRepository;
use App\Services\CurlService;
use App\Services\SubscriptionService;

class TestController extends Controller
{
    public function __construct(
        CurlService $curlService,
        ActionRepository $actionRepo,
        QuestionRepository $questionRepo,
        SubscriptionService $subscriptionService
    ) {
        $this->curlService = $curlService;
        $this->actionRepo = $actionRepo;
        $this->questionRepo = $questionRepo;
        $this->subscriptionService = $subscriptionService;
    }

    /**
     * Run tests
     */
    public function index()
    {
        // dd(auth()->user()->toArray());
    }
}
