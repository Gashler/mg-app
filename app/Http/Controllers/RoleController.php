<?php

class RoleController extends Controller {

    /**
     * Display a listing of roles
     *
     * @return Response
     */
    public function getIndex()
    {
        $roles = Role::all();

        return View::make('role.index', compact('roles'));
    }

    /**
     * Show the form for creating a new role
     *
     * @return Response
     */
    public function getCreate()
    {
        return View::make('role.create');
    }

    /**
     * Store a newly created role in storage.
     *
     * @return Response
     */
    public function postStore()
    {
        $validator = Validator::make($data = Input::all(), Role::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        Role::create($data);

        return Redirect::route('role.index')->with('message', 'Role created.');
    }

    /**
     * Display the specified role.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $role = Role::findOrFail($id);

        return View::make('role.show', compact('role'));
    }

    /**
     * Show the form for editing the specified role.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $role = Role::find($id);

        return View::make('role.edit', compact('role'));
    }

    /**
     * Update the specified role in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function putUpdate($id)
    {
        $role = Role::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Role::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $role->update($data);

        return Redirect::route('role.show', $id)->with('message', 'Role updated.');
    }

    /**
     * Remove the specified role from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postDestroy($id)
    {
        Role::destroy($id);

        return Redirect::route('role.index')->with('message', 'Role deleted.');
    }

}
