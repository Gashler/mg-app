<?php

class RoleplayController extends Controller
{

    // generate roleplay
    public function getGenerate()
    {
        $his_name = Name::where('gender', 'm')->orderByRaw("RAND()")->first();
        $her_name = Name::where('gender', 'f')->orderByRaw("RAND()")->first();
        $his_personality = Personality::orderByRaw("RAND()")->first();
        $her_personality = Personality::orderByRaw("RAND()")->first();
        $his_role = CharacterRole::where('gender', 'm')->orWhere('gender', '')->orderByRaw("RAND()")->first();
        $her_role = CharacterRole::where('gender', 'f')->orWhere('gender', '')->orderByRaw("RAND()")->first();
        $location = Location::orderByRaw("RAND()")->first();
        $story_type = StoryType::orderByRaw("RAND()")->first();
        $first_line = FirstLine::orderByRaw("RAND()")->first();
        $title = 'The ' . ucfirst($his_role->name) . ' and the ' . ucfirst($her_role->name);
        return $data = [
            'title' => $title,
            'his_name' => ucfirst($his_name->name),
            'her_name' => ucfirst($her_name->name),
            'his_personality' => ucfirst($his_personality->name),
            'her_personality' => ucfirst($her_personality->name),
            'his_role' => ucfirst($his_role->name),
            'her_role' => ucfirst($her_role->name),
            'location' => ucfirst($location->name),
            'story_type' => ucfirst($story_type->name),
            'first_line' => ucfirst($first_line->name),
        ];
    }

    /**
     * Data only
     */
    public function getAllRoleplays()
    {
        $roleplays = Roleplay::all();
        foreach ($roleplays as $roleplay) {
            if (strtotime($roleplay['created_at']) >= (time() - Config::get('site.new_time_frame'))) {
                $roleplay['new'] = 1;
            }
        }
        return $roleplays;
    }

    /**
     * Display a listing of roleplays
     *
     * @return Response
     */
    public function getIndex()
    {
        $title = 'All Role-plays';
        $object = 'all-roleplays';
        return View::make('roleplay.index', compact('title', 'object'));
    }

    /**
     * Display a listing of roleplays by user
     *
     * @return Response
     */
    public function byUser($id)
    {
        $title = 'My Role-plays';
        $object = 'roleplays-by-user/' . $id;
        $my = true;
        return View::make('roleplay.index', compact('title', 'object', 'my'));
    }

    /**
     * Show the form for creating a new roleplay
     *
     * @return Response
     */
    public function getCreate()
    {
        return View::make('roleplay.create');
    }

    /**
     * Role-play generator
     *
     * @return Response
     */
    public function quick()
    {
        return View::make('roleplay.generator');
    }

    /**
     * Store a newly created roleplay in storage.
     *
     * @return Response
     */
    public function postStore()
    {
        $validator = Validator::make($data = Input::all(), Roleplay::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        // assign owner
        $data['user_id'] = Auth::user()->id;

        $roleplay = Roleplay::create($data);
        if (isset($data['edit'])) {
            return Redirect::route('roleplays.edit', $roleplay->id);
        } else {
            return Redirect::to('roleplays/user/' . Auth::user()->id)->with('message', 'Roleplay saved.');
        }
    }

    /**
     * Display the specified roleplay.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $roleplay = Roleplay::findOrFail($id);

        return View::make('roleplay.show', compact('roleplay'));
    }

    /**
     * Show the form for editing the specified roleplay.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $roleplay = Roleplay::find($id);

        return View::make('roleplay.edit', compact('roleplay'));
    }

    /**
     * Update the specified roleplay in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function putUpdate($id)
    {
        $roleplay = Roleplay::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Roleplay::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $roleplay->update($data);

        return Redirect::route('roleplay.show', $id)->with('message', 'Roleplay updated.');
    }

    /**
     * Remove the specified roleplay from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postDestroy($id)
    {
        Roleplay::destroy($id);

        return Redirect::to('roleplays/user/' . Auth::user()->id)->with('message', 'Roleplay deleted.');
    }
}
