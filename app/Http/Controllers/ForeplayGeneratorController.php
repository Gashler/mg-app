<?php

use App\Repositories\ActionRepository;

class ForeplayGeneratorController extends Controller
{
    public function __construct(ActionRepository $actionRepo)
    {
        $this->actionRepo = $actionRepo;
    }

    // return foreplay generator
    public function getIndex()
    {
        $action = $this->actionRepo->random();
        return [
            'action' => $action['data'],
            'categories' => Category::all(),
            'ratings' => Rating::all()
        ];
    }
}
