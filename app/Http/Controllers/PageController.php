<?php

class PageController extends Controller {

    /**
     * Display a listing of pages
     *
     * @return Response
     */
    public function getIndex()
    {
        if (Auth::user()->hasRole(['Admin', 'Editor'])) {
            $pages = Page::all();
            return View::make('page.index', compact('pages'));
        }
    }

    /**
     * Show the form for creating a new page
     *
     * @return Response
     */
    public function getCreate()
    {
        if (Auth::user()->hasRole(['Admin', 'Editor'])) {
            return View::make('page.create');
        }
    }

    /**
     * Store a newly created page in storage.
     *
     * @return Response
     */
    public function postStore()
    {
        if (Auth::user()->hasRole(['Admin', 'Editor'])) {
            // validation
            $rules = [
                'title' => 'required',
                'short_title' => 'required',
                'url' => 'unique|required|alpha_dash',
                'url' => 'required|alpha_dash|unique:pages'
            ];
            $validator = Validator::make($data = Input::all(), Page::$rules);
            if ($validator->fails())
            {
                return Redirect::back()->withErrors($validator)->withInput();
            }

            // format checkbox values for database
            $data['public'] = isset($data['public']) ? 1 : 0;
            $data['subscribers'] = isset($data['subscribers']) ? 1 : 0;
            $data['members'] = isset($data['members']) ? 1 : 0;
            if ($data['public'] == 0 && $data['subscribers'] == 0 && $data['public'] == 0) $data['public'] = 1;
            if ($data['subscribers'] == 1 || $data['members'] == 1) $data['public'] = 0;
            $data['public_header'] = isset($data['public_header']) ? 1 : 0;
            $data['public_footer'] = isset($data['public_footer']) ? 1 : 0;
            $data['back_office_header'] = isset($data['back_office_header']) ? 1 : 0;
            $data['back_office_footer'] = isset($data['back_office_footer']) ? 1 : 0;

            $page = Page::create($data);
            return Redirect::route('pages.edit', $page->id)->with('message', 'Page created.');
        }
    }

    /**
     * Display the specified page.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($url)
    {
        $page = Page::where('url', $url)->first();
        $title = $page->title;
        if ($page->public) {
            return View::make('page.show', compact('page', 'title'));
        }
        elseif (Auth::check()) {
            if ($page->public || Auth::user()->hasRole(['Admin', 'Editor']) || (Auth::user()->hasRole(['Member']) && $page->reps) || (Auth::user()->hasRole(['Trial']) && $page->customers)) {
                return View::make('page.show', compact('page', 'title'));
            }
        }
        else return "You don't have permission to view this page.";
    }

    /**
     * Show the form for editing the specified page.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        if (Auth::user()->hasRole(['Admin', 'Editor'])) {
            $page = Page::find($id);
            if ($page['subscribers'] || $page['members']) $only_show_to = 'checked';
            else $only_show_to = '';
            return View::make('page.edit', compact('page', 'only_show_to'));
        }
    }

    /**
     * Update the specified page in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function putUpdate($id)
    {
        if (Auth::user()->hasRole(['Admin', 'Editor'])) {
            $page = Page::findOrFail($id);

            // validation
            $rules = [
                'title' => 'required',
                'short_title' => 'required',
                'url' => 'unique|required|alpha_dash',
                'url' => 'required|alpha_dash|unique:pages,url,' . $page->id
            ];
            $validator = Validator::make($data = Input::all(), Page::$rules);
            if ($validator->fails())
            {
                return Redirect::back()->withErrors($validator)->withInput();
            }

            strtolower($data['url']);

            // format checkbox values for database
            $data['public'] = isset($data['public']) ? 1 : 0;
            $data['subscribers'] = isset($data['subscribers']) ? 1 : 0;
            $data['members'] = isset($data['members']) ? 1 : 0;
            if ($data['public'] == 0 && $data['subscribers'] == 0 && $data['public'] == 0) $data['public'] = 1;
            if ($data['subscribers'] == 1 || $data['members'] == 1) $data['public'] = 0;
            $data['public_header'] = isset($data['public_header']) ? 1 : 0;
            $data['public_footer'] = isset($data['public_footer']) ? 1 : 0;
            $data['back_office_header'] = isset($data['back_office_header']) ? 1 : 0;
            $data['back_office_footer'] = isset($data['back_office_footer']) ? 1 : 0;

            $page->update($data);

            return Redirect::back()->with('message', 'Page updated.');
        }
    }

    /**
     * Remove the specified page from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postDestroy($id)
    {
        if (Auth::user()->hasRole(['Admin', 'Editor'])) {
            Page::destroy($id);

            return Redirect::route('page.index')->with('message', 'Page deleted.');
        }
    }

}
