<?php

class GameController extends Controller {

    /***************
    * Get All Games
    ****************/
    public function getAll() {
        return Game::with('user')->whereNull('player_id')->get();
    }

    /**********************
    * Get Game for Playing
    ***********************/
    public static function getGet($id, $basic = false) {

        // get game
        $game = Game::with(
            'narrations',
            'medias',
            'maps',
            'maps.rows.cols.place',
            'maps.rows.cols.place.characters',
            'maps.rows.cols.place.characters.customProperties',
            'maps.rows.cols.place.characters.defaultProperties',
            'maps.rows.cols.place.characters.defaultProperties.options',
            'maps.rows.cols.place.characters.narrations',
            'maps.rows.cols.place.characters.logics',
            'maps.rows.cols.place.characters.logics.triggers',
            'maps.rows.cols.place.characters.logics.effects',
            'maps.rows.cols.place.characters.objects',
            'maps.rows.cols.place.logics',
            'maps.rows.cols.place.medias',
            'maps.rows.cols.place.objects',
            'maps.rows.cols.place.objects.customProperties',
            'maps.rows.cols.place.objects.defaultProperties',
            'maps.rows.cols.place.objects.defaultProperties.options',
            'maps.rows.cols.place.objects.logics',
            'maps.rows.cols.place.objects.logics.triggers',
            'maps.rows.cols.place.objects.logics.effects',
            'maps.rows.cols.place.objects.objects',
            'maps.rows.cols.place.timers',
            'players',
            'players.characters',
            'uvents',
            'uvents.actions',
            // 'uvents.characters',
            // 'uvents.effects',
            'uvents.medias',
            'uvents.objects',
            'uvents.options',
            'uvents.timer'
        )->find($id);

        // all this code should be moved into a repository
        if(!$basic) {
            // build paths
            $game = buildPaths($game, 'game');

            // add additional data
            $game['defaults'] = [
                'property' => Property::with('options')->where('name', 'Hidden')->first()
            ];
            $game['verbs'] = Verb::all();
            $game['properties'] = [
                'character' => Property::with('options')->where('morph_type', 'Character')->get(),
                'object' => Property::with('options')->where('morph_type', 'Object')->get(),
                'place' => Property::with('options')->where('morph_type', 'Place')->get()
            ];
            $game['effect']['randOptions'] = Effect::randOptions();

            // generate row and col indexes for places
            foreach($game['maps'] as $map_index => $map) {
                foreach($map['rows'] as $row_index => $row) {
                    foreach($row['cols'] as $col_index => $col) {
                        if($col['place'] !== null) {
                            $game['maps'][$map_index]['rows'][$row_index]['cols'][$col_index]['place']['row_index'] = $row_index;
                            $game['maps'][$map_index]['rows'][$row_index]['cols'][$col_index]['place']['col_index'] = $col_index;
                        }
                    }
                }
            }

            // remove redundant player characters
            // foreach($game['players'] as $player_index => $player) {
            //     foreach($player['characters'] as $character_index => $character) {
            //         if($character['game_id'] !== $game['id']) {
            //             unset($game['players'][$player_index]['characters'][$character_index]);
            //         }
            //     }
            // }

        }

        // return game
        return $game;
    }

    /**********************
    * Get Game for Editing
    ***********************/
    public function getGetedit($id) {
        return [
            'actions' => Action::all(),
            'game' => $this->getGet($id),
            'verbs' => Verb::all()
        ];
    }

    /********************
     * Create a new game
     ********************/
    public function getCreate()
    {
        // create game
        $game = Game::create([
            'height' => 4,
            'name' => 'Untitled Game',
            'user_id' => Auth::user()->id,
            'width' => 4
        ]);

        // create default player (spouse)
        $player = Player::create([
            'game_id' => $game->id,
            'user_id' => Auth::user()->spouse->id
        ]);

        // create default character
        $character = Character::create([
            'first_name' => Auth::user()->spouse->first_name,
            'game_id' => $game->id,
            'gender' => Auth::user()->spouse->gender,
            'last_name' => Auth::user()->spouse->last_name,
            'player_id' => $player->id,
            'type' => 'player'
        ]);

        // create default map
        $map = Map::create([
            'name' => 'Untitled Map',
            'morph_type' => 'Game',
            'morph_id' => $game->id
        ]);
        $width = 4;
        $height = 4;
        for($r = 1; $r <= $height; $r ++) {

            // create rows
            $row = Row::create(['map_id' => $map->id]);

            // create cols
            for($c = 1; $c <= $width; $c ++) {
                $col = Col::create(['row_id' => $row->id]);

                // create default space
                if($r == 2 && $c == 2) {
                    Place::create([
                        'col_id' => $col->id,
                        'start' => 1
                    ]);
                }

            }
        }

        return $game;
    }

    /***********************
     * Play Game.
     ***********************/
    public function getPlay($id)
    {
        // check if instance of game exists
        $instance = Game::where('game_id', $id)->where('player_id', Auth::user()->id)->orWhere('player_id', Auth::user()->spouse->id)->first();

        // if neither user nor spouse have instance of game, create one
        if(!isset($instance)) {
            $instance = replicateObject('game', Game::find($id)->id, true);
        }

        // get instance
        return $this->getGet($instance->id);

    }

    /***********************
     * Show Game.
     ***********************/
    public function getShow($id)
    {
        return Game::find($id);
    }

    /************************
    * Duplicate Game
    *************************/
    public function postDuplicate($ids = false, $instance = false)
    {
        if(!$ids) $ids = Input::get('ids');
        foreach($ids as $id) {
            replicateObject('game', $id, $instance);
        }

        if(!$instance) {
            if(count($ids) > 1) $noun = 'Games';
            else $noun = 'Game';
            return Redirect::back()->with('message', $noun . ' duplicated.');
        }

    }

    /*************************
    * End Game / Restart Game
    **************************/
    public function getEnd($id, $restart = false) {
        $game = Game::find($id);
        $user = Auth::user();
        if($user->hasRole(['Admin']) || $user->user_id == $game->user->id || $user->id == $game->player_id) {
            $master_id = Game::find($id)->game_id;
            Game::destroy($id);
            return [
                'success' => 1,
                'message' => 'Saved game deleted',
                'data' => [
                    'master_id' => $master_id
                ]
            ];
        }
    }

    /***********************************************
     * Show the form for editing the specified game.
     ***********************************************/
    public function edit($id)
    {
        $actions = Action::all();
        return View::make('game.edit', compact(
            'id',
            'actions',
            'players',
            'players.objects'
        ));
    }

    /***************************************
     * Update the specified game in storage.
     ***************************************/
    public function putUpdate($id)
    {
        $game = Game::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Game::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        if(isset($data[0])) $data = $data[0];
        $game->update($data);
        return [
            'success' => 1,
            'data' => Game::find($id)
        ];
    }

    /******************************************
     * Remove the specified game from storage.
     ******************************************/
    public function postDestroy($id)
    {

        $game = Game::with(
            'characters',
            'logics',
            'medias',
            'objects',
            'players',
            'timers'
        )->find($id);

        // delete characters
        if(isset($game->characters)) {
            foreach($game->characters as $character) {

                // delete character medias
                $character = Character::with('medias')->find($character->id);
                foreach($character->medias as $media) {
                    $character->medias()->detach($media);
                }

                Character::destroy($character->id);
            }
        }

        // delete effects
        if(isset($game->effects)) {
            foreach($game->effects as $effect) {
                Effect::destroy($effect->id);
            }
        }

        // delete logics
        if(isset($game->logics)) {
            foreach($game->logics as $logic) {
                Logic::destroy($logic->id);
            }
        }

        // delete medias
        if(isset($game->medias)) {
            foreach($game->medias as $media) {
                Player::destroy($media->id);
            }
        }

        // delete objects
        if(isset($game->objects)) {
            foreach($game->objects as $object) {

                // delete object medias
                $object = Object::with('medias')->find($object->id);
                foreach($object->medias as $media) {
                    $object->medias()->detach($media);
                }

                Object::destroy($object->id);
            }
        }

        // delete places
        if(isset($game->objects)) {
            // foreach($game->places as $place) {
            //
            // 	// delete place medias
            // 	$place = Place::with('medias')->find($place->id);
            // 	foreach($place->medias as $media) {
            // 		$place->medias()->detach($media);
            // 	}
            //
            // 	Place::destroy($place->id);
            // }
        }

        // delete players
        if(isset($game->players)) {
            foreach($game->players as $player) {

                // delete player medias
                $player = Character::with('medias')->find($player->id);
                foreach($player->medias as $media) {
                    $player->medias()->detach($media);
                }

                Character::destroy($player->id);
            }
        }

        // delete timers
        if(isset($game->timers)) {
            foreach($game->timers as $timer) {
                Timer::destroy($timer->id);
            }
        }

        // delete uvents
        if(isset($game->objects)) {
            foreach($game->uvents as $uvent) {

                // delete uvent medias
                $uvent = Uvent::with('medias')->find($uvent->id);
                foreach($uvent->medias as $media) {
                    $uvent->medias()->detach($media);
                }

                Uvent::destroy($uvent->id);
            }
        }

        // delete game
        Game::destroy($id);

        return Redirect::route('game.index')->with('message', 'Game deleted.');
    }

}
