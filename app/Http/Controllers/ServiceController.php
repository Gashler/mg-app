<?php

use App\Services\MailService;

class ServiceController extends Controller {

    public function __construct(MailService $mailService)
    {
        $this->mailService = $mailService;
    }

    /**
     * Display a listing of services
     *
     * @return Response
     */
    public function index($person = null)
    {
        return Service::all();
    }

    // return a user's (or their spouse's) services
    public function indexForUsers($person)
    {
        if (isset($person)) {
            if ($person == 'My') {
                $person = auth()->user();
            } else {
                $person = auth()->user()->spouse;
            }
            return [
                'suggestions' => Service::whereNull('user_id')->get(),
                'services' => $person->services
            ];
        }
    }

    // return a user's (or their spouse's purchases)
    public function purchases($person)
    {
        if (isset($person)) {
            if ($person == 'My') {
                $person = auth()->user();
            } else {
                $person = auth()->user()->spouse;
            }
        } else {
            $person = auth()->user();
        }
        return $person->purchases;
    }

    // complete a service
    public function complete($id)
    {
        auth()->user()->purchases()->detach($id);
        return [
            'success' => true,
            'message' => [
                'type' => 'success',
                'body' => "Service completed"
            ]
        ];
    }

    // create new service
    public function create()
    {
        $service = Service::create();
        auth()->user()->services()->save($service);
        return $service;
    }

    // add an existing service
    public function add($id)
    {
        $service = Service::find($id);
        unset($service->model);
        $service = Service::create($service->toArray());
        auth()->user()->services()->save($service);
        return $service;
    }

    // purchase a service
    public function purchase($id)
    {
        $service = Service::find($id);
        if (auth()->user()->money >= $service->price) {
            $userMoney = auth()->user()->money - $service->price;
            $spouseMoney = auth()->user()->spouse->money + $service->price;
            User::find(auth()->user()->id)->update([
                'money' => $userMoney
            ]);
            User::find(auth()->user()->spouse->id)->update([
                'money' => $spouseMoney
            ]);
            auth()->user()->purchases()->save($service);

            // notify spouse
            $this->mailService->send('service-purchased', auth()->user()->spouse, $service);

            return [
                'message' => [
                    'type' => 'success',
                    'body' => "You've sent " . auth()->user()->spouse->first_name . " " . $service->price . " " . config('site.money_name_plural') . ". She has been notified that you've purchased this service from " . auth()->user()->spouse->accusative . "."
                ],
                'user_money' => $userMoney,
                'spouse_money' => $spouseMoney
            ];
        }
    }

    // return the specified service
    public function show($id)
    {
        $service = Service::find($id);
        if (
            $service->user_id == auth()->user()->id
            || $service->user_id == auth()->user()->spouse->id
        ) {
            return $service;
        }
    }

    // delete the specified service
    public function destroy($id)
    {
        Service::destroy(auth()->user()->services()->find($id)->id);
        return [
            'message' => [
                'type' => 'success',
                'body' => "Service deleted"
            ]
        ];
    }
}
