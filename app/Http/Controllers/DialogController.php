<?php

class DialogController extends Controller {

    // all dialogs
    public function getAll() {
        return Dialog::all();
    }

    // get dialog
    public function getGet($id) {
        return Dialog::find($id);
    }

    // store dialog
    public static function postStore() {
        $validator = Validator::make($data = Input::all(), Dialog::$rules);
        if($validator->fails()) return $validator;
        $dialog = Dialog::create($data);
        return $dialog;
    }

    // udpate dialog
    public function putUpdate($id) {
        $dialog = Dialog::findOrFail($id);
        $validator = Validator::make($data = Input::all(), Dialog::$rules);
        if($validator->fails()) {
            return $validator;
        }
        $dialog->update($data);
        return Dialog::find($id);
    }

}
