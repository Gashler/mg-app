<?php

class CharacterController extends Controller
{

    // all characters
    public function getAllCharacters()
    {
        return Character::all();
    }

    // get character
    public function getGet($id)
    {
        return Character::find($id);
    }

    /**************
     * get random
     ***************/
    public function postGenerate()
    {
        $data = Input::all();

        // get gender
        if (!isset($data['gender'])) {
            $rand = rand(0, 1);
            if ($rand == 0) {
                $data['gender'] = 'm';
            } else {
                $data['gender'] = 'f';
            }
        }

        // get first name
        if (!isset($data['first_name'])) {
            $data['first_name'] = Name::where('gender', $data['gender'])->first();
        }

        // randomly assign icon
        if ($data['gender'] == 'f') {
            $max = Cache::get('settings.flaticon-face-f-max');
        } else {
            $max = Cache::get('settings.flaticon-face-m-max');
        }
        $icon = 'flaticon-face-' . $data['gender'] . '-' . rand(1, $max);

        // create character
        return Character::create([
            'first_name' => $data['first_name'],
            'gender' => $data['gender'],
            'icon' => $icon,
            'type' => 'npc',
        ]);
    }

    // detach character
    public function getDetach($model, $model_id, $character_id)
    {
        $model = $model::find($model_id);
        $model->characters()->detach($character_id);
    }

    /**
     * Display a listing of characters
     */
    public function getIndex()
    {
        $characters = Character::all();

        return View::make('character.index', compact('characters'));
    }

    /**
     * Show the form for creating a new character
     */
    public function getCreate()
    {
        return View::make('character.create');
    }

    /**
     * Store a newly created character in storage.
     */
    public function postStore()
    {
        $validator = Validator::make($data = Input::all(), Character::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        // get game
        $game = Game::find($data['game_id']);

        // or generate character
        $character = $this->postGenerate();

        // attach character to game
        $game->characters()->save($character);

        // attach to place
        if (isset($data['place_id'])) {
            $place = Place::find($data['place_id']);
            $place->characters()->save($character);
        }

        // attach to player
        if (isset($data['player_id'])) {
            $player = Player::find($data['player_id']);
            $player->characters()->save($character);
        }

        return Character::with(
            'logics',
            'objects',
            'timers'
        )->find($character->id);

        // return Redirect::route('character.index')->with('message', 'Character created.');
    }

    /**
     * Display the specified character.
     */
    public function show($id)
    {
        $character = Character::findOrFail($id);

        return View::make('character.show', compact('character'));
    }

    /**
     * Show the form for editing the specified character.
     */
    public function edit($id)
    {
        $character = Character::find($id);

        return View::make('character.edit', compact('character'));
    }

    /**
     * Update the specified character in storage.
     */
    public function putUpdate($id)
    {
        $character = Character::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Character::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $character->update($data);
        return [
            'success' => 1,
            'data' => Character::find($character->id)
        ];
    }
}
