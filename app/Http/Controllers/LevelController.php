<?php

class LevelController extends Controller {

    // all levels
    public function getAll() {
        return Level::all();
    }

    // get level
    public function getGet($id) {
        return Level::find($id);
    }

    /**
     * Display a listing of levels
     */
    public function getIndex()
    {
        $levels = Level::all();

        return View::make('level.index', compact('levels'));
    }

    /**
     * Show the form for creating a new level
     */
    public function getCreate()
    {
        return View::make('level.create');
    }

    /**
     * Store a newly created level in storage.
     */
    public function postStore()
    {
        $validator = Validator::make($data = Input::all(), Level::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        Level::create($data);

        return Redirect::route('level.index')->with('message', 'Level created.');
    }

    /**
     * Display the specified level.
     */
    public function show($id)
    {
        $level = Level::findOrFail($id);

        return View::make('level.show', compact('level'));
    }

    /**
     * Show the form for editing the specified level.
     */
    public function edit($id)
    {
        $level = Level::find($id);

        return View::make('level.edit', compact('level'));
    }

    /**
     * Update the specified level in storage.
     */
    public function putUpdate($id)
    {
        $level = Level::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Level::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $level->update($data);

        return Redirect::route('level.show', $id)->with('message', 'Level updated.');
    }

    /**
     * Remove the specified level from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postDestroy($id)
    {
        Level::destroy($id);

        return Redirect::route('level.index')->with('message', 'Level deleted.');
    }

}
