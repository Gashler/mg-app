<?php

use App\Repositories\QuestionRepository;

class QuestionController extends Controller {

    public function __construct(QuestionRepository $questionRepo)
    {
        $this->questionRepo = $questionRepo;
    }

    // return all questions
    public function index()
    {
        return [
            'questions' => $this->questionRepo->format(Question::all()->toArray()),
            'categories' => QuestionCategory::all()
        ];
    }

    // return the specified question
    public function show($id)
    {
        return $this->questionRepo->format(Question::with('categories')->find($id));
    }

    // return a random question
    public function random()
    {
        $params = request()->all();
        if ($question = $this->questionRepo->random($params)) {
            return [
                'success' => true,
                'question' => $question
            ];
        }
        return [
            'success' => false
        ];
    }

    // return a random question with categories
    public function randomWithCategories()
    {
        $params = request()->all();
        $data = [];
        $data['question'] = $this->questionRepo->random($params);
        $data['categories'] = QuestionCategory::all();
        return $data;
    }
}
