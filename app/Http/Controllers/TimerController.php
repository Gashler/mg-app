<?php

class TimerController extends Controller
{

    // all timers
    public function getAll()
    {
        return Timer::all();
    }

    // get timer
    public function getGet($id)
    {
        return Timer::find($id);
    }

    /**
     * Display a listing of timers
     */
    public function getIndex()
    {
        $timers = Timer::all();

        return View::make('timer.index', compact('timers'));
    }

    /**
     * Show the form for creating a new timer
     */
    public function getCreate()
    {
        return View::make('timer.create');
    }

    /**
     * Store a newly created timer in storage.
     */
    public function postStore()
    {
        $validator = Validator::make($data = Input::all(), Timer::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        return Timer::create($data);

        return Redirect::route('timer.index')->with('message', 'Timer created.');
    }

    /**
     * Display the specified timer.
     */
    public function show($id)
    {
        $timer = Timer::findOrFail($id);

        return View::make('timer.show', compact('timer'));
    }

    /**
     * Show the form for editing the specified timer.
     */
    public function edit($id)
    {
        $timer = Timer::find($id);

        return View::make('timer.edit', compact('timer'));
    }

    /**
     * Update the specified timer in storage.
     */
    public function putUpdate($id)
    {
        $timer = Timer::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Timer::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        // determine starting value
        if ($data['direction'] == 'up') $data['svalue'] = $data['min'];
        else $data['value'] = $data['max'];

        // toggle max time
        $data['set_max'] = isset($data['set_max']) ? 1 : 0;

        $timer->update($data);

        // return Redirect::route('timer.show', $id)->with('message', 'Timer updated.');
    }

    /**
     * Remove the specified timer from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postDestroy($id)
    {
        Timer::destroy($id);

        // return Redirect::route('timer.index')->with('message', 'Timer deleted.');
    }
}
