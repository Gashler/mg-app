<?php

class ObjectController extends Controller {

    // all objects
    public function getAll() {
        return Object::all();
    }

    // get object
    public function getGet($id) {
        return Object::find($id);
    }

    /**
     * Display a listing of objects
     */
    public function getIndex()
    {
        $objects = Object::all();

        return View::make('object.index', compact('objects'));
    }

    /**
     * Show the form for creating a new object
     */
    public function getCreate()
    {
        return View::make('object.create');
    }

    /**
     * Store a newly created object in storage.
     */
    public function postStore()
    {
        $validator = Validator::make($data = Input::all(), Object::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        return Object::create($data);

        // return Redirect::route('object.index')->with('message', 'Object created.');
    }

    /**
     * Display the specified object.
     */
    public function show($id)
    {
        $object = Object::findOrFail($id);

        return View::make('object.show', compact('object'));
    }

    /**
     * Show the form for editing the specified object.
     */
    public function edit($id)
    {
        $object = Object::find($id);

        return View::make('object.edit', compact('object'));
    }

    /**
     * Update the specified object in storage.
     */
    public function putUpdate($id)
    {
        $object = Object::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Object::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        if(isset($data['takeable']) && $data['takeable'] == 'on') $data['takeable'] = 1;
        else $data['takeable'] = 0;
        if(isset($data[0])) $data = $data[0];
        $object->update($data);
        return [
            'success' => 1,
        ];
    }

    /**
     * Remove the specified object from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postDestroy($id)
    {
        Object::destroy($id);

        return Redirect::route('object.index')->with('message', 'Object deleted.');
    }

}
