<?php

class ServiceTagController extends Controller {

    /**
     * Remove the specified serviceTag from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postDestroy($id)
    {
        ServiceTagModel::destroy($id);
    }

}
