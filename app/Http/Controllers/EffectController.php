<?php

class EffectController extends Controller {

    // all effects
    public function getAll() {
        return Effect::all();
    }

    // get effect
    public function getGet($id) {
        return Effect::find($id);
    }

    /**
     * Store a newly created effect in storage.
     */
    public function postStore()
    {
        $validator = Validator::make($data = Input::all(), Effect::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $effect = Effect::create($data);
        return Effect::find($effect->id);
    }

    /**
     * Display the specified effect.
     */
    public function show($id)
    {
        $effect = Effect::findOrFail($id);

        return View::make('effect.show', compact('effect'));
    }

    /**
     * Show the form for editing the specified effect.
     */
    public function edit($id)
    {
        $effect = Effect::find($id);

        return View::make('effect.edit', compact('effect'));
    }

    /**
     * Update the specified effect in storage.
     */
    public function putUpdate($id)
    {
        $effect = Effect::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Effect::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        // prepare data
        if(!isset($data['propertyOption_id'])) {
            $data['propertyOption_id'] = 1;
        }

        $effect->update($data);
        // return [
        //     'success' => 1,
        //     'data' => Effect::find($effect->id)
        // ];
    }

    /**
     * Remove the specified effect from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postDestroy($id)
    {
        Effect::destroy($id);

        return Redirect::route('effect.index')->with('message', 'Effect deleted.');
    }

}
