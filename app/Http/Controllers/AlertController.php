<?php

use Carbon\Carbon;

class AlertController extends Controller {

    /**
     * Display a listing of Alerts
     *
     * @return Response
     */
    public function index($person = null)
    {
        $alerts = [];

        // days since registering
        $daysSinceRegistering = floor((time() - strtotime(auth()->user()->account->created_at)) / 86400);
        // $hoursSinceRegistering = floor((time() - strtotime(auth()->user()->account->created_at)) / 3600);
        $secondsTillTrialEnds = strtotime(auth()->user()->account->trial_ends_at) - time();

        // services user owes spouse
        $count = count(auth()->user()->purchases);
        if ($count > 0) {
            $s = '';
            if ($count > 1) {
                $s = 's';
            }
            $alerts[] = [
                'type' => 'success',
                'body' => auth()->user()->spouse->first_name . " owes you " . $count . " service" . $s . ".<a class='btn btn-default pull-right' href='/#/services/purchased/My'><i class='fa fa-eye'></i> View Purchased Services</a>"
            ];
        }

        // services spouse owes user
        $count = count(auth()->user()->spouse->purchases);
        if ($count > 0) {
            $s = '';
            if ($count > 1) {
                $s = 's';
            }
            $alerts[] = [
                'type' => 'danger',
                'body' => "You owe " . auth()->user()->spouse->first_name . " " . $count . " service" . $s . ".<a class='btn btn-default pull-right margin-left-4 margin-top-4' href='/#/services/purchased/" . auth()->user()->first_name . "'><i class='fa fa-eye'></i> View Owed Services</a>"
            ];
        }

        // favor clock
        $favorClock = auth()->user()->account->favorClock;
        if (isset($favorClock)) {
            if ($favorClock->time >= 300) { // 5 minutes
                if ($favorClock->ower == auth()->user()->gender) {
                    $alerts[] = [
                        'type' => 'danger',
                        'body' => "You owe " . auth()->user()->spouse->first_name . " " . secondsToTime($favorClock->time) . " of favor time.<a class='btn btn-primary pull-right margin-left-4 margin-top-4' href='/#/services/favor-clock'><i class='fa fa-check'></i> Pay " . ucFirst(auth()->user()->spouse->accusative) . " Back</a>"
                    ];
                } else {
                    $alerts[] = [
                        'type' => 'success',
                        'body' => auth()->user()->spouse->first_name . " owes you <strong>" . secondsToTime($favorClock->time) . "</strong> of favor time."
                    ];
                }
            }
        }

        if ($daysSinceRegistering >= 1 && count(auth()->user()->answers) == 0) {
            $alerts[] = [
                'type' => 'warning',
                'body' => "Answer some questions to help " . auth()->user()->spouse->first_name . " understand your sexual preferences.<a class='btn btn-primary pull-right margin-left-4' href='/#/surveys/My'><i class='fa fa-plus'></i> Take Survey</a>"
            ];
        }

        /*************************************
        * If on 24 hour trial
        **************************************/

        if (!auth()->user()->account->stripe_id && auth()->user()->account->on_trial && !auth()->user()->campaigns->find(1)) {
            $alerts[] = [
                'countdown' => $secondsTillTrialEnds,
                'type' => 'warning',
                'body' => "<strong>Get a lifetime subscription for two for only <div class='strike'>$" . config('site.default_subscription_price') . "<div class='line'></div></div> $49.99 (50% Off) when you sign up today</strong>. A single subscription will give both you and " . auth()->user()->spouse->first_name . " unlimited access. Add a payment method now to avoid an interruption in service.<a class='btn btn-primary pull-right margin-left-4 margin-top-4' href='/#/subscriptions/create'><i class='fa fa-plus'></i> Add Payment Method</a>"
            ];
        }

        /*************************************
        * If on 7 day trial trial
        **************************************/

        if (!auth()->user()->account->stripe_id && auth()->user()->account->on_trial && auth()->user()->campaigns->find(1)) {

            // days till trial ends
            $date = new Carbon(auth()->user()->account->trial_ends_at);
            $now = Carbon::now();
            $daysTillTrialEnds = ($date->diff($now)->days < 1)
                ? 'today'
                : $date->diffForHumans($now);
            $daysTillTrialEnds = str_replace(" after", "", $daysTillTrialEnds);

            if ($daysSinceRegistering == 0) {
                // $in = '';
                // if ($daysTillTrialEnds !== 'today') {
                //     $in = 'in';
                // }
                $alerts[] = [
                    'type' => 'warning',
                    'body' => "Your trial will end in <strong>" . $daysTillTrialEnds . "</strong>. A single subscription will give both you and " . auth()->user()->spouse->first_name . " full access. Add a payment method to avoid an interruption in service.<a class='btn btn-primary pull-right margin-left-4 margin-top-4' href='/#/subscriptions/create'><i class='fa fa-plus'></i> Add Payment Method</a>"
                ];
            }

            // day 2
            if ($daysSinceRegistering == 1) {
                $alerts[] = [
                    'type' => 'warning',
                    'body' => "<strong>$10 off when you sign up today!</strong> Your trial will end in <strong>" . $daysTillTrialEnds . "</strong>. A single subscription will give both you and " . auth()->user()->spouse->first_name . " full access. Add a payment method to avoid an interruption in service.<a class='btn btn-primary pull-right margin-left-4 margin-top-4' href='/#/subscriptions/create?id=3NTamqpuXT9W2GKtKUAs'><i class='fa fa-plus'></i> Claim My Discount</a>"
                ];
                $alerts[] = [
                    'type' => 'popup',
                    'header' => "Special Offer",
                    'body' => "
                        <h2 class=\"margin-top-0\">$10 Off When You Sign up Today</h2>
                        <p>
                            <strong>Your trial will end in " . $daysTillTrialEnds . "</strong>. We're confident that " . env('COMPANY_NAME') . " will help you improve your love life, and to prove it, we'll give you <strong>$10 off the normal price</strong> when you sign up today. A single subscription will give both you and " . auth()->user()->spouse->first_name . " full access.
                        </p>
                    ",
                    'footer' => "
                        <a href=\"" . env('APP_URL') . "/#/subscriptions/create?id=3NTamqpuXT9W2GKtKUAs\" class=\"btn btn-primary\"><i class=\"fa fa-plus\"></i> Claim My Discount</a>
                    "
                ];
            }

            // day 3
            if ($daysSinceRegistering == 2) {
                $alerts[] = [
                    'type' => 'warning',
                    'body' => "<strong>$20 off when you sign up today!</strong> Your trial will end in <strong>" . $daysTillTrialEnds . "</strong>. A single subscription will give both you and " . auth()->user()->spouse->first_name . " full access. Add a payment method to avoid an interruption in service.<a class='btn btn-primary pull-right margin-left-4 margin-top-4' href='/#/subscriptions/create?id=X2vCI2Xv16ZFQQNN3Cn8'><i class='fa fa-plus'></i> Claim My Discount</a>"
                ];
                $alerts[] = [
                    'type' => 'popup',
                    'header' => "Special Offer",
                    'body' => "
                        <h2 class=\"margin-top-0\">$20 Off When You Sign up Today</h2>
                        <p>
                            <strong>Your trial will end in " . $daysTillTrialEnds . "</strong>. Will a $20 discount help you make up your mind? Commit to a better love life today by making a small investment. A single subscription will give both you and " . auth()->user()->spouse->first_name . " full access.
                        </p>
                    ",
                    'footer' => "
                        <a href=\"" . env('APP_URL') . "/#/subscriptions/create?id=X2vCI2Xv16ZFQQNN3Cn8\" class=\"btn btn-primary\"><i class=\"fa fa-plus\"></i> Claim My Discount</a>
                    "
                ];
            }

            // day 4
            if ($daysSinceRegistering == 3) {
                $alerts[] = [
                    'type' => 'warning',
                    'body' => "<strong>50% off when you sign up today!</strong> Your trial will end in <strong>" . $daysTillTrialEnds . "</strong>. A single subscription will give both you and " . auth()->user()->spouse->first_name . " full access. Add a payment method to avoid an interruption in service.<a class='btn btn-primary pull-right margin-left-4 margin-top-4' href='/#/subscriptions/create?id=C2laqoq0CTJcVOUoGl8U'><i class='fa fa-plus'></i> Claim My Discount</a>"
                ];
                $alerts[] = [
                    'type' => 'popup',
                    'header' => "Final Offer!",
                    'body' => "
                        <h2 class=\"margin-top-0\">50% Off When You Sign up Today</h2>
                        <p>
                            <strong>Your trial will end in " . $daysTillTrialEnds . "</strong>. Will a 50% discount help you make up your mind? This is the lowest we can go, and you won't find a better value. Commit to a better love life today by making a small investment. A single subscription will give both you and " . auth()->user()->spouse->first_name . " full access.
                        </p>
                    ",
                    'footer' => "
                        <a href=\"" . env('APP_URL') . "/#/subscriptions/create?id=C2laqoq0CTJcVOUoGl8U\" class=\"btn btn-primary\"><i class=\"fa fa-plus\"></i> Claim My Discount</a>
                    "
                ];
            }

            // day 4
            if ($daysSinceRegistering >= 4) {
                $alerts[] = [
                    'type' => 'danger',
                    'body' => "<strong>Your trial will end soon!</strong> Claim your 50% discount or try month-to-month. A single subscription will give both you and " . auth()->user()->spouse->first_name . " full access. Add a payment method to avoid an interruption in service.<br><a class='btn btn-primary pull-right margin-left-4 margin-top-4' href='/#/subscriptions/create?id=C2laqoq0CTJcVOUoGl8U'><i class='fa fa-plus'></i> Claim My 50% Discount</a><a class='btn btn-default pull-right margin-left-4 margin-top-4 margin-right-1' href='/#/subscriptions/create?id=fiZinGUkYKgYNlW0yzEh'><i class='fa fa-plus'></i> Try Monthly Subscription</a>"
                ];
                $alerts[] = [
                    'type' => 'popup',
                    'header' => "Your trial will end soon!",
                    'body' => "
                        <h2 class=\"margin-top-0\">Which is best for you?</h2>
                        <p>
                            Try a monthly subscription or <strong>save big wth a 50% discount</strong>. Commit to a better love life today by making a small investment. A single subscription will give both you and " . auth()->user()->spouse->first_name . " full access.
                        </p>
                    ",
                    'footer' => "
                        <a href=\"" . env('APP_URL') . "/#/subscriptions/create?id=fiZinGUkYKgYNlW0yzEh\" class=\"btn btn-default margin-right-1\"><i class=\"fa fa-plus\"></i> Try Monthly Subscription</a>
                        <a href=\"" . env('APP_URL') . "/#/subscriptions/create?id=C2laqoq0CTJcVOUoGl8U\" class=\"btn btn-primary margin-bottom-2\"><i class=\"fa fa-plus\"></i> Claim My Discount</a>
                    "
                ];
            }
        }

        return $alerts;
    }
}
