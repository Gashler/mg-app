<?php

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected $layout = 'layouts.default';

    // Setup the layout used by the controller.
    protected function setupLayout()
    {
        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }

    /**
     * Update the specified record in storage.
     */
    public function patchIndex()
    {
        $data = request()->all();
        $model = '\\' . ucfirst($data['meta']['model']);
        $unit = $model::findOrFail($data['meta']['id']);

        // check authorization
        $authorized = false;
        if (auth()->user()->hasRole(['Admin'])) {
            $authorized = true;
        } elseif (
            $model == '\User' && $data['meta']['id'] == auth()->user()->id
            || $unit->user_id && $unit->user_id == auth()->user()->id
            || $unit->account_id && $unit->account_id == auth()->user()->account_id
        ) {
            $authorized = true;
        }
        if (!$authorized) {
            return [
                'errors' => ['Unauthorized']
            ];
        }

        // removed unauthorized columns for non-admins
        if (!auth()->user()->hasRole(['Admin'])) {
            if (isset($data['meta']['key'])) {
                if (!in_array($data['meta']['key'], $model::$authorized)) {
                    return [
                        'errors' => ['Unauthorized']
                    ];
                }
            } else {
                foreach ($data['value'] as $key => $value) {
                    if (!in_array($key, $model::$authorized)) {
                        unset($data['value'][$key]);
                    }
                }
            }
        }

        // validate
        if (isset($model::$rules)) {
            $validator = Validator::make([$data['value']], $model::$rules);
            if ($validator->fails()) {
                $errors = json_decode($validator->errors());
                foreach ($errors as $error) {
                    $error = str_replace('\"', '', $error);
                    $error = str_replace('[', '', $error);
                    $error = str_replace(']', '', $error);
                    $array[] = $error;
                }
                return [
                    'errors' => $array
                ];
            }
        }

        // update record
        if (isset($data['meta']['key'])) {
            $unit->update([
                $data['meta']['key'] => $data['value']
            ]);
        } else {
            $unit->update($data['value']);
        }
        if (isset($data['meta']['return']) && $data['meta']['return']) {
            return [
                'unit' => $model::find($data['meta']['id'])
            ];
        }
        return [
            'message' => ucfirst($model) . " updated."
        ];
    }


    /**
     * Update the a key-value pair of a specified record
     */
    // public function patchPatch()
    // {
    //     $data = request()->all();
    //     $model = '\\' . ucfirst($data['model']);
    //     $unit = $model::findOrFail($data['id']);
    //     $unit->update([
    //         $data['key'] => $data['value']
    //     ]);
    // }

    // attach two records
    public function postAttach()
    {
        $data = Input::all();
        $object = $data['model_path']::find($data['model_id']);
        $object->$data['method']()->attach($data['method_id']);
    }

    // update a record
    // public function putUpdate($model, $id) {
    //     $model_path = '\' . ucfirst($model);
    // 	eval('$record = \' . $model_path . '::findOrFail($id);');
    // 	$validator = Validator::make($data = Input::all(), $model_path::$rules);
    // 	if ($validator->fails()) {
    //         return $validator;
    //     }
    // 	$record->update($data);
    //     return $model_path::find($record->id);
    // }

    // delete one or more records
    public function postDelete($model)
    {
        $ids = request()->all();
        foreach ($ids as $id) {
            $model_path = '\\' . ucfirst($model);
            $authorized = false;
            if (auth()->user()->hasRole(['Admin'])) {
                $authorized = true;
            } else {
                $object = $model_path::find($id);
                if (
                    isset($object->account_id) && auth()->user()->account_id == $object->account_id
                    || isset($object->user_id) && auth()->user()->id == $object->user_id
                ) {
                    $authorized = true;
                } else {
                    $authorized = false;
                    $errors[] = ucfirst($model) . " could not be deleted";
                }
            }
            if ($authorized) {
                $model_path::destroy($id);
                return [
                    'success' => true
                ];
            } else {
                $errors[] = 'Unauthorized';
            }
        }
        return [
            'success' => false,
            'message' => [
                'type' => 'danger',
                'body' => $errors
            ]
        ];
    }

    // detach two records
    public function postDetach()
    {
        $data = Input::all();
        $object = $data['model_path']::find($data['model_id']);
        $object->$data['method']()->detach($data['method_id']);
    }

    // define constants
    public function constants()
    {
        return [
            'messages' => [],
            'settings' => Setting::all()->pluck('value', 'key'),
            'site' => config('site'),
            'token' => csrf_token(),
            'user' => auth()->user(),
            'redirect' => session('redirect')
        ];
    }
}
