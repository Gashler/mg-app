<?php

class PlayerController extends Controller
{

    // all players
    public function getAllPlayers()
    {
        return Player::all();
    }

    // get player
    public function getGet($id)
    {
        return Player::find($id);
    }

    /**
     * Store a newly created player in storage.
     */
    public function postStore()
    {
        $validator = Validator::make($data = Input::all(), Player::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        // create player
        return $player = Player::create($data);
    }

    /**
     * Update the specified player in storage.
     */
    public function putUpdate($id)
    {
        $player = Player::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Player::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $player->update($data);
        return [
            'success' => 1,
        ];
    }
}
