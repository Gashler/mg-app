<?php

class HelperController extends Controller {

    public function getPluralize($string) {
        return str_plural($string);
    }

}
