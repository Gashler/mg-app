<?php

class FavorClockController extends Controller {

    // return favor clock
    public function getIndex()
    {
        if (!$favorClock = auth()->user()->account->favorClock) {
            $favorClock = FavorClock::create();
            auth()->user()->account->favorClock()->save($favorClock);
            return $favorClock;
        }
        return FavorClock::find(auth()->user()->account->favorClock->id);
    }

    // update favor clock
    public function putIndex()
    {
        $data = request()->all();
        unset(
            $data['time'],
            $data['display_time']
        );
        auth()->user()->account->favorClock()->update($data);
        return FavorClock::find(auth()->user()->account->favorClock->id);
    }
}
