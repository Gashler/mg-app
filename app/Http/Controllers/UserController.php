<?php

use App\Repositories\UserRepository;

class UserController extends Controller {

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    /**
     * Display the specified user.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        if (
            auth()->user()->hasRole(['Admin', 'Superadmin'])
            || auth()->user()->id == $id
            || auth()->user()->spouse_id == $id
        ) {
            return User::findOrFail($id);
        }
    }
}
