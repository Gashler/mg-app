<?php

class TriggerController extends Controller
{

    // all triggers
    public function getAll()
    {
        return Trigger::all();
    }

    // get trigger
    public function getGet($id)
    {
        return Trigger::find($id);
    }

    /**
     * Display a listing of triggers
     */
    public function getIndex()
    {
        $triggers = Trigger::all();

        return View::make('trigger.index', compact('triggers'));
    }

    /**
     * Show the form for creating a new trigger
     */
    public function getCreate()
    {
        return View::make('trigger.create');
    }

    /**
     * Store a newly created trigger in storage.
     */
    public function postStore()
    {
        $validator = Validator::make($data = Input::all(), Trigger::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        return $trigger = Trigger::create($data);
    }

    /**
     * Display the specified trigger.
     */
    public function show($id)
    {
        $trigger = Trigger::findOrFail($id);

        return View::make('trigger.show', compact('trigger'));
    }

    /**
     * Show the form for editing the specified trigger.
     */
    public function edit($id)
    {
        $trigger = Trigger::find($id);

        return View::make('trigger.edit', compact('trigger'));
    }

    /**
     * Update the specified trigger in storage.
     */
    public function putUpdate($id)
    {
        $trigger = Trigger::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Trigger::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $trigger->update($data);
        return [
            'success' => 1,
            'data' => Trigger::find($trigger->id)
        ];
    }

    /**
     * Remove the specified trigger from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postDestroy($id)
    {
        Trigger::destroy($id);

        return Redirect::route('triggers.index')->with('message', 'Trigger deleted.');
    }
}
