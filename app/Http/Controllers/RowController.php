<?php

class RowController extends Controller {

    /**
     * Store a newly created row in storage.
     */
    public function postIndex()
    {
        $validator = Validator::make($data = Input::all(), Row::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        // create rows
        if(isset($data['rows'])) {
            $rows_count = $data['rows'];
            unset($data['rows']);
            if(isset($data['prepend'])) {
                $first_col = Col::where('row_id', $data['row_id'])->orderBy('order')->first();
                $data['order'] = $first_col->order - 1;
                unset($data['prepend']);
            }
            else {
                $last_col = Col::where('row_id', $data['row_id'])->orderBy('order', 'DESC')->first();
                $data['order'] = $last_col->order + 1;
            }
        }
        if(isset($data['cols'])) {
            $cols_count = $data['cols'];
            unset($data['cols']);
            if(isset($data['prepend'])) {
                $first_row = Row::where('map_id', $data['map_id'])->orderBy('order')->first();
                $data['order'] = $first_row->order - 1;
                unset($data['prepend']);
            }
            else {
                $last_row = Row::where('map_id', $data['map_id'])->orderBy('order', 'DESC')->first();
                $data['order'] = $last_row->order + 1;
            }
        }
        if(!isset($rows_count)) $row = Row::create($data);

        // create columns for existing rows
        else {
            $rows = Row::where('map_id', $data['map_id'])->get();
            $cols = [];
            foreach($rows as $row) {
                $col = Col::create([
                    'row_id' => $row->id,
                    'order' => $data['order']
                ]);
                $col->place = null;
                $cols[] = $col;
            }
            return $cols;
        }

        // create columns for new row
        if(isset($cols_count)) {
            for($i = 1; $i <= $cols_count; $i ++) {
                Col::create(['row_id' => $row->id]);
            }
        }
        return Row::with('cols','cols.place')->find($row->id);

    }

    /**
     * Remove the specified row from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function deleteIndex($id)
    {
        Row::destroy($id);

        return Redirect::route('rows.index')->with('message', 'Row deleted.');
    }

}
