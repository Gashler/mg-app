<?php

use App\Http\Requests;
use App\Services\SubscriptionService;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    public function __construct(SubscriptionService $subscriptionService)
    {
        $this->subscriptionService = $subscriptionService;
    }

    /**
    * Store a subscription
    * @return Response
    */
    public function create()
    {
        $params = request()->all();
        $params['id'] = isset($params['id']) ? $params['id'] : '';
        if (!auth()->check()) {
            if ($user = User::where([
                'email' => $params['email'],
                'key' => $params['key']
            ])->first()) {
                auth()->login($user);
            } else {
                return "User not found.";
            }
        }
        session()->put('redirect', '/#/subscriptions/create/?id=' . $params['id']);
        return redirect('/#/subscriptions/create/?id=' . $params['id']);
    }

    /**
    * Store a subscription
    * @return Response
    */
    public function store()
    {
        return $this->subscriptionService->create(request()->all());
    }

    /**
    * Return a subscription
    * @param int $account_id
    * @return Response
    */
    public function show()
    {
        return $this->subscriptionService->get();
    }

    /**
    * Return a subscription
    * @param int $user_id
    * @return Response
    */
    public function update($account_id)
    {
        if (auth()->user()->hasRole(['Admin']) || auth()->user()->account_id == $account_id) {
            return $this->subscriptionService->update(request()->all());
        } else {
            return [
                'error' => true,
                'message' => "You don't have permission to do this."
            ];
        }
    }

    /**
    * Cancel a subscription
    * @param int $account_id
    * @return Response
    */
    public function toggle()
    {
        return $this->subscriptionService->toggle(request()->all());
    }

    /**
    * Add Payment Method
    * @param int $user_id
    * @return Response
    */
    public function addPaymentMethod()
    {
        return $this->subscriptionService->addPaymentMethod(request()->all());
    }

    /**
    * Change a subscription's plan
    * @param array $params
    * @return Response
    */
    public function changePlan()
    {
        return $this->subscriptionService->changePlan(request()->all());
    }

    /**
    * Return a list of all billing plans
    * @param array $params
    * @return Response
    */
    public function plans($id = null)
    {
        return $this->subscriptionService->plans($id);
    }

    /**
    * Return a view for purchasing subscriptions
    * @param array $params
    * @return Response
    */
    public function shop()
    {
        return view('layouts.default');
    }
}
