<?php

class BodypartController extends Controller {

    // all bodyparts
    public function getAll() {
        return Bodypart::all();
    }

    // get bodypart
    public function getGet($id) {
        return Bodypart::find($id);
    }

    /**
     * Display a listing of bodyparts
     */
    public function getIndex()
    {
        $bodyparts = Bodypart::all();

        return View::make('bodypart.index', compact('bodyparts'));
    }

    /**
     * Show the form for creating a new bodypart
     */
    public function getCreate()
    {
        return View::make('bodypart.create');
    }

    /**
     * Store a newly created bodypart in storage.
     */
    public function postStore()
    {
        $validator = Validator::make($data = Input::all(), Bodypart::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        Bodypart::create($data);

        return Redirect::route('bodypart.index')->with('message', 'Bodypart created.');
    }

    /**
     * Display the specified bodypart.
     */
    public function show($id)
    {
        $bodypart = Bodypart::findOrFail($id);

        return View::make('bodypart.show', compact('bodypart'));
    }

    /**
     * Show the form for editing the specified bodypart.
     */
    public function edit($id)
    {
        $bodypart = Bodypart::find($id);

        return View::make('bodypart.edit', compact('bodypart'));
    }

    /**
     * Update the specified bodypart in storage.
     */
    public function putUpdate($id)
    {
        $bodypart = Bodypart::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Bodypart::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $bodypart->update($data);

        return Redirect::route('bodypart.show', $id)->with('message', 'Bodypart updated.');
    }

    /**
     * Remove the specified bodypart from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postDestroy($id)
    {
        Bodypart::destroy($id);

        return Redirect::route('bodypart.index')->with('message', 'Bodypart deleted.');
    }

}
