<?php

class EntryController extends Controller {

    /**
     * Display a listing of entries
     *
     * @return Response
     */
    public function index()
    {
        return auth()->user()->account->entries;
    }

    // create new entry
    public function create()
    {
        $entry = Entry::create();
        auth()->user()->account->entries()->save($entry);
        return $entry;
    }

    // return the specified entry
    public function show($id)
    {
        return auth()->user()->account->entries()->find($id);
    }

    // delete the specified entry
    public function destroy($id)
    {
        Entry::destroy(auth()->user()->account->entries()->find($id)->id);
        return [
            'message' => [
                'type' => 'success',
                'body' => "Entry deleted"
            ]
        ];
    }
}
