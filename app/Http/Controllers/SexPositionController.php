<?php

use App\Repositories\SexPositionRepository;

class SexPositionController extends Controller {

    public function __construct(SexPositionRepository $sexPositionRepo)
    {
        $this->sexPositionRepo = $sexPositionRepo;
    }

    // return all sex positions
    public function index()
    {
        return [
            'sexPositions' => SexPosition::with('categories')->get(),
            'categories' => SexPositionCategory::all()
        ];
    }

    // return the specified sex position
    public function show($id)
    {
        return SexPosition::with('categories')->find($id);
    }

    // return a random sex position
    public function random()
    {
        $params = request()->all();
        return $this->sexPositionRepo->random($params);
    }
}
