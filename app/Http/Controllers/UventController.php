<?php

class UventController extends Controller
{

    // all uvents
    public function getAll()
    {
        return Uvent::all();
    }

    // get uvent
    public function getGet($id)
    {
        return Uvent::with(
            'characters',
            'characters.items',
            'actions',
            'options'
        )->find($id);
    }

    // upcoming events
    public function getUpcoming()
    {
        return $events = Uvent::where('date_start', '<', time())->orderBy('date_start', 'desc')->get()->take(5);
    }

    /**
     * Store a newly created uvent in storage.
     */
    public function postStore()
    {
        $validator = Validator::make($data = Input::all(), Uvent::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $uvent = Uvent::create($data);
        return Uvent::with('actions', 'options', 'medias')->find($uvent->id);
        // if(Request::ajax()) {
        // return $uvent;
        // }
        // else return Redirect::route('uvent.index')->with('message', 'Uvent created.');
    }

    /**
     * Display the specified uvent.
     */
    public function show($id)
    {
        $uvent = Uvent::findOrFail($id);

        return View::make('uvent.show', compact('uvent'));
    }

    /**
     * Show the form for editing the specified uvent.
     */
    public function edit($id)
    {
        $uvent = Uvent::with('actions', 'options')->find($id);

        return View::make('uvent.edit', compact('uvent'));
    }

    /**
     * Update the specified uvent in storage.
     */
    public function putUpdate($id)
    {
        $uvent = Uvent::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Uvent::$rules);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $uvent->update($data);

        // return Redirect::route('uvent.show', $id)->with('message', 'Uvent updated.');
    }

    /**
     * Remove the specified uvent from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postDestroy($id)
    {
        Uvent::destroy($id);
        // return Redirect::route('uvent.index')->with('message', 'Uvent deleted.');
    }
}
