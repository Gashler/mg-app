<?php

class PropertyController extends Controller {

    // all properties
    public function getAll() {
        return Property::all();
    }

    // get property
    public function getGet($id) {
        return Property::find($id);
    }

    /**
     * Display a listing of properties
     */
    public function index()
    {
        $properties = Property::all();

        return View::make('property.index', compact('properties'));
    }

    /**
     * Show the form for creating a new property
     */
    public function create()
    {
        return View::make('property.create');
    }

    /**
     * Store a newly created property in storage.
     */
    public function store()
    {
        $validator = Validator::make($data = Input::all(), Property::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        Property::create($data);

        return Redirect::route('properties.index')->with('message', 'Property created.');
    }

    /**
     * Display the specified property.
     */
    public function show($id)
    {
        $property = Property::findOrFail($id);

        return View::make('property.show', compact('property'));
    }

    /**
     * Show the form for editing the specified property.
     */
    public function edit($id)
    {
        $property = Property::find($id);

        return View::make('property.edit', compact('property'));
    }

    /**
     * Update the specified property in storage.
     */
    public function update($id)
    {
        $property = Property::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Property::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $property->update($data);

        return Redirect::route('properties.show', $id)->with('message', 'Property updated.');
    }

    /**
     * Remove the specified property from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Property::destroy($id);

        return Redirect::route('properties.index')->with('message', 'Property deleted.');
    }

    /**
     * Remove properties.
     */
    public function postDelete()
    {
        foreach (Input::get('ids') as $id) {
            Property::destroy($id);
        }
        if (count(Input::get('ids')) > 1) {
            return Redirect::route('properties.index')->with('message', 'Properties deleted.');
        }
        else {
            return Redirect::back()->with('message', 'Property deleted.');
        }
    }

    /**
     * Diable properties.
     */
    public function postDisable()
    {
        foreach (Input::get('ids') as $id) {
            Property::find($id)->update(['disabled' => 1]);
        }
        if (count(Input::get('ids')) > 1) {
            return Redirect::route('properties.index')->with('message', 'Properties disabled.');
        }
        else {
            return Redirect::back()->with('message', 'Property disabled.');
        }
    }

    /**
     * Enable properties.
     */
    public function postEnable()
    {
        foreach (Input::get('ids') as $id) {
            Property::find($id)->update(['disabled' => 0]);
        }
        if (count(Input::get('ids')) > 1) {
            return Redirect::route('properties.index')->with('message', 'Properties enabled.');
        }
        else {
            return Redirect::back()->with('message', 'Property enabled.');
        }
    }

}
