<?php

class ModificationController extends Controller {

    // all modifications
    public function getAll() {
        return Modification::all();
    }

    // get modification
    public function getGet($id) {
        return Modification::find($id);
    }

    /**
     * Display a listing of modifications
     */
    public function getIndex()
    {
        $modifications = Modification::all();

        return View::make('modification.index', compact('modifications'));
    }

    /**
     * Show the form for creating a new modification
     */
    public function getCreate()
    {
        return View::make('modification.create');
    }

    /**
     * Store a newly created modification in storage.
     */
    public function postStore()
    {
        $validator = Validator::make($data = Input::all(), Modification::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        Modification::create($data);

        return Redirect::route('modification.index')->with('message', 'Modification created.');
    }

    /**
     * Display the specified modification.
     */
    public function show($id)
    {
        $modification = Modification::findOrFail($id);

        return View::make('modification.show', compact('modification'));
    }

    /**
     * Show the form for editing the specified modification.
     */
    public function edit($id)
    {
        $modification = Modification::find($id);

        return View::make('modification.edit', compact('modification'));
    }

    /**
     * Update the specified modification in storage.
     */
    public function putUpdate($id)
    {
        $modification = Modification::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Modification::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $modification->update($data);

        return Redirect::route('modification.show', $id)->with('message', 'Modification updated.');
    }

    /**
     * Remove the specified modification from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postDestroy($id)
    {
        Modification::destroy($id);

        return Redirect::route('modification.index')->with('message', 'Modification deleted.');
    }

}
