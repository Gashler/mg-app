<?php

use App\Http\Requests;
use App\Repositories\AuthRepository;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use App\Repositories\OauthLoginRepository;

class OAuthController extends Controller
{
    public function __construct(
        AuthRepository $authRepo,
        UserRepository $userRepo,
        OauthLoginRepository $oauthLoginRepo
    ) {
        $this->authRepo = $authRepo;
        $this->userRepo = $userRepo;
        $this->oauthLoginRepo = $oauthLoginRepo;
    }

    public function login(Request $request)
    {
        // store request data in session so it persists through redirects
        if (session('req') == null) {
            $req = [
                'service' => request()->get('service'),
                'register' => request()->get('register')
            ];
            session()->put('req', $req);
        } else {
            $req = session('req');
            session()->put('req.redirected', true);
            if (session('req.redirected')) {
                session()->forget('req');
            }
        }

        // get data from request
        $code = $request->get('code');

        // get service service
        $service = OAuth::consumer($req['service']);

        // if code is provided get user data and sign in
        if (!is_null($code)) {

            // This was a callback request from service, get the token
            $token = $service->requestAccessToken($code);

            // store token in session
            $accessToken = accessProtected($token, 'accessToken');
            if ($req['service'] == 'Facebook') {
                session()->put('fb_user_access_token', $accessToken);
            }

            // Send a request with it
            $oauth = json_decode($service->request(config()->get('oauth-5-laravel.uris.'.$req['service'])), true);
            if ($user = User::where(['email' => $oauth['email']])->first()) {
                $response = $this->authRepo->login(['user' => $user]);
                if ($response['success']) {
                    if (session('oauth')) {
                        $user->oauthLogins()->create([
                            'service' => session('oauth.service'),
                            'service_user_id' => session('oauth.service_user_id'),
                            'token' => session('oauth.token'),
                            'email' => session('oauth.email'),
                        ], $user);
                    }
                    return redirect('/');
                } else {
                    if ($response['error_code'] == 'no_subscription') {
                        return redirect('/#/subscriptions/create');
                    } elseif ($response['error_code'] == 'no_account') {
                        return $this->authRepo->store($response['user']);
                    }
                }
                return redirect('/');
            } else {

                // format return data
                if (isset($oauth['name']) && !isset($oauth['first_name']) && !isset($oauth['last_name'])) {
                    $names = explode(' ', $oauth['name']);
                    $oauth['first_name'] = $names[0];
                    $oauth['last_name'] = $names[1];
                }
                if (isset($oauth['given_name']) && isset($oauth['family_name'])) {
                    $oauth['first_name'] = $oauth['given_name'];
                    $oauth['last_name'] = $oauth['family_name'];
                }
                $oauth['service'] = $req['service'];
                $oauth['service_user_id'] = $oauth['id'];
                $oauth['token'] = $accessToken;

                if (isset($oauth['gender'])) {
                    $oauth['users'] = [];
                    if ($oauth['gender'] == 'male') {
                        $key = 'Husband';
                    } else {
                        $key = 'Wife';
                    }
                    $oauth['users'][$key] = [
                        'first_name' => $oauth['first_name'],
                        'last_name' => $oauth['last_name'],
                        'gender' => $oauth['gender'][0]
                    ];
                }

                // for Instagram
                if (isset($oauth['data'])) {
                    $oauth['service_user_id'] = $oauth['data']['id'];
                    $oauth['name'] = $oauth['data']['full_name'];
                    $oauth['public_id'] = $oauth['data']['username'];
                }
                session()->put('oauth', $oauth);
                if ($req['register'] && !auth()->check()) {
                    return redirect('/register');
                } else {
                    return redirect('/');
                }
            }
            // if code is not provided, ask for permission first
        } else {
            // get service authorization
            $url = $service->getAuthorizationUri();

            // return to service login url
            return redirect((string)$url);
        }
    }
}
