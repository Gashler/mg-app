<?php

class OptionController extends Controller {

    // all options
    public function getAll() {
        return Option::all();
    }

    // get option
    public function getGet($id) {
        return Option::find($id);
    }

    /**
     * Display a listing of options
     */
    public function getIndex()
    {
        $options = Option::all();

        return View::make('option.index', compact('options'));
    }

    /**
     * Show the form for creating a new option
     */
    public function getCreate()
    {
        return View::make('option.create');
    }

    /**
     * Store a newly created option in storage.
     */
    public function postStore()
    {
        $validator = Validator::make($data = Input::all(), Option::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $option = Option::create($data);
        return Option::with('effects')->find($option->id);

        // return Redirect::route('option.index')->with('message', 'Option created.');
    }

    /**
     * Display the specified option.
     */
    public function show($id)
    {
        $option = Option::findOrFail($id);

        return View::make('option.show', compact('option'));
    }

    /**
     * Show the form for editing the specified option.
     */
    public function edit($id)
    {
        $option = Option::find($id);

        return View::make('option.edit', compact('option'));
    }

    /**
     * Update the specified option in storage.
     */
    public function putUpdate($id)
    {
        $option = Option::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Option::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $option->update($data);

        // return Redirect::route('option.show', $id)->with('message', 'Option updated.');
    }

    /**
     * Remove the specified option from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postDestroy($id)
    {
        Option::destroy($id);

        return Redirect::route('option.index')->with('message', 'Option deleted.');
    }

}
