<?php

class PropertyoptionController extends Controller {

	// all propertyoptions
	public function getAll() {
		return Propertyoption::all();
	}

	// get propertyoption
	public function getGet($id) {
		return Propertyoption::find($id);
	}

	/**
	 * Display a listing of propertyoptions
	 */
	public function index()
	{
		$propertyoptions = Propertyoption::all();

		return View::make('propertyoption.index', compact('propertyoptions'));
	}

	/**
	 * Show the form for creating a new propertyoption
	 */
	public function create()
	{
		return View::make('propertyoption.create');
	}

	/**
	 * Store a newly created propertyoption in storage.
	 */
	public function store()
	{
		$validator = Validator::make($data = Input::all(), Propertyoption::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		Propertyoption::create($data);

		return Redirect::route('propertyoptions.index')->with('message', 'Propertyoption created.');
	}

	/**
	 * Display the specified propertyoption.
	 */
	public function show($id)
	{
		$propertyoption = Propertyoption::findOrFail($id);

		return View::make('propertyoption.show', compact('propertyoption'));
	}

	/**
	 * Show the form for editing the specified propertyoption.
	 */
	public function edit($id)
	{
		$propertyoption = Propertyoption::find($id);

		return View::make('propertyoption.edit', compact('propertyoption'));
	}

	/**
	 * Update the specified propertyoption in storage.
	 */
	public function update($id)
	{
		$propertyoption = Propertyoption::findOrFail($id);

		$validator = Validator::make($data = Input::all(), Propertyoption::$rules);

		if ($validator->fails())
		{
			return Redirect::back()->withErrors($validator)->withInput();
		}

		$propertyoption->update($data);

		return Redirect::route('propertyoptions.show', $id)->with('message', 'Propertyoption updated.');
	}

	/**
	 * Remove the specified propertyoption from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		Propertyoption::destroy($id);

		return Redirect::route('propertyoptions.index')->with('message', 'Propertyoption deleted.');
	}

	/**
	 * Remove propertyoptions.
	 */
	public function postDelete()
	{
		foreach (Input::get('ids') as $id) {
			Propertyoption::destroy($id);
		}
		if (count(Input::get('ids')) > 1) {
			return Redirect::route('propertyoptions.index')->with('message', 'Propertyoptions deleted.');
		}
		else {
			return Redirect::back()->with('message', 'Propertyoption deleted.');
		}
	}

	/**
	 * Diable propertyoptions.
	 */
	public function postDisable()
	{
		foreach (Input::get('ids') as $id) {
			Propertyoption::find($id)->update(['disabled' => 1]);
		}
		if (count(Input::get('ids')) > 1) {
			return Redirect::route('propertyoptions.index')->with('message', 'Propertyoptions disabled.');
		}
		else {
			return Redirect::back()->with('message', 'Propertyoption disabled.');
		}
	}

	/**
	 * Enable propertyoptions.
	 */
	public function postEnable()
	{
		foreach (Input::get('ids') as $id) {
			Propertyoption::find($id)->update(['disabled' => 0]);
		}
		if (count(Input::get('ids')) > 1) {
			return Redirect::route('propertyoptions.index')->with('message', 'Propertyoptions enabled.');
		}
		else {
			return Redirect::back()->with('message', 'Propertyoption enabled.');
		}
	}

}
