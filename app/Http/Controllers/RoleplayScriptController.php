<?php

class RoleplayScriptController extends Controller {

    /**
     * Data only
     */
    public function getAllRoleplayScripts(){
        $roleplayScripts = RoleplayScript::all();
        foreach ($roleplayScripts as $roleplayScript)
        {
            if (strtotime($roleplayScript['created_at']) >= (time() - Config::get('site.new_time_frame') ))
            {
                $roleplayScript['new'] = 1;
            }
        }
        return $roleplayScripts;
    }

    /**
     * Display a listing of roleplayScripts
     *
     * @return Response
     */
    public function index()
    {
        return RoleplayScript::select([
            'id',
            'icon',
            'description',
            'name',
            'updated_at'
        ])->get();
    }

    /**
     * Display the specified roleplayScript
     *
     * @return Response
     */
    public function show($id)
    {
        $roleplayScript = RoleplayScript::find($id);
        $roleplayScript->body = str_replace('[husband]', auth()->user()->husband, $roleplayScript->body);
        $roleplayScript->body = str_replace('[wife]', auth()->user()->wife, $roleplayScript->body);
        return $roleplayScript;
    }

    /**
     * Display a listing of roleplayScripts by user
     *
     * @return Response
     */
    public function byUser($id)
    {
        $title = 'My RoleplayScripts';
        $object = 'roleplayScripts-by-user/' . $id;
        $my = true;
        return View::make('roleplayScript.index', compact('title', 'object', 'my'));
    }

    /**
     * Show the form for creating a new roleplayScript
     *
     * @return Response
     */
    public function getCreate()
    {
        return View::make('roleplayScript.create');
    }

    /**
     * Store a newly created roleplayScript in storage.
     *
     * @return Response
     */
    public function postStore()
    {
        $validator = Validator::make($data = Input::all(), RoleplayScript::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        RoleplayScript::create($data);
        if (isset($data['edit'])) return Redirect::route('roleplayScripts.edit', $roleplayScript->id);
        else return Redirect::to('roleplayScripts/user/' . Auth::user()->id)->with('message', 'RoleplayScript saved.');
    }

    /**
     * Show the form for editing the specified roleplayScript.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $roleplayScript = RoleplayScript::find($id);

        return View::make('roleplayScript.edit', compact('roleplayScript'));
    }

    /**
     * Update the specified roleplayScript in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function putUpdate($id)
    {
        $roleplayScript = RoleplayScript::findOrFail($id);

        $validator = Validator::make($data = Input::all(), RoleplayScript::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $roleplayScript->update($data);

        return Redirect::route('roleplayScript.show', $id)->with('message', 'RoleplayScript updated.');
    }

    /**
     * Remove the specified roleplayScript from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postDestroy($id)
    {
        RoleplayScript::destroy($id);
        return Redirect::to('roleplayScripts/user/' . Auth::user()->id)->with('message', 'RoleplayScript deleted.');
    }

}
