<?php

use App\Repositories\LoversLaneRepository;

class LoversLaneController extends Controller {

    public function __construct()
    {
        $this->loversLaneRepo = New LoversLaneRepository;
    }

    ###########################################
    # Get the existing game or start a new game
    ###########################################
    public function getGame()
    {
        // retrieve existing game
        $game = LoversLane::where('account_id', auth()->user()->account_id)->first();

        // or create new game
        if(!isset($game)) {
            $game = $this->loversLaneRepo->create();
        }

        // return data
        return $game;
    }

    #########################
    # Update the game
    #########################
    public function patchIndex() {

        // update data
        $data = request()->all();
        LoversLane::where('account_id', auth()->user()->account_id)->first()->update([
            'data' => $data
        ]);

        // update money if applicable
        if (auth()->user()->gender == $data['player']['gender']) {
            $user = $data['player'];
            $spouse = $data['opponent'];
        } else {
            $spouse = $data['player'];
            $user = $data['opponent'];
        }
        if (auth()->user()->money !== $user['money']) {
            User::find(auth()->user()->id)->update([
                'money' => $user['money']
            ]);
        }
        if (auth()->user()->spouse->money !== $spouse['money']) {
            User::find(auth()->user()->spouse->id)->update([
                'money' => $spouse['money']
            ]);
        }
    }
}
