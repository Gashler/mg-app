<?php

class LogicController extends Controller {

    // all logics
    public function getAll() {
        return Logic::all();
    }

    // get logic
    public function getGet($id) {
        return Logic::find($id);
    }

    /**
     * Display a listing of logics
     */
    public function getIndex()
    {
        $logics = Logic::all();

        return View::make('logic.index', compact('logics'));
    }

    /**
     * Show the form for creating a new logic
     */
    public function getCreate()
    {
        return View::make('logic.create');
    }

    /**
     * Store a newly created logic in storage.
     */
    public function postStore()
    {
        $validator = Validator::make($data = Input::all(), Logic::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $logic = Logic::create($data);

        return Logic::with('triggers', 'effects')->find($logic->id);
    }

    /**
     * Update the specified logic in storage.
     */
    public function putUpdate($id)
    {
        $logic = Logic::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Logic::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $logic->update($data);
        return [
            'success' => 1,
        ];
    }

    /**
     * Remove the specified logic from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postDestroy($id)
    {
        Logic::destroy($id);

        return Redirect::route('logic.index')->with('message', 'Logic deleted.');
    }

}
