<?php

class AddressController extends Controller {

    /**
     * Data only
     */
    public function getAllRecords(){
        return Address::all();
    }

    /**
     * Display a listing of addresses
     *
     * @return Response
     */
    public function getIndex()
    {
        $addresses = Address::all();

        return View::make('address.index', compact('addresses'));
    }

    /**
     * Show the form for creating a new address
     *
     * @return Response
     */
    public function getCreate()
    {
        return View::make('address.create');
    }

    /**
     * Store a newly created address in storage.
     *
     * @return Response
     */
    public function postStore()
    {
        $validator = Validator::make($data = Input::all(), Address::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        Address::create($data);

        return Redirect::route('addresse.index')->with('message', 'Address created.');
    }

    /**
     * Display the specified address.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $address = Address::findOrFail($id);

        return View::make('address.show', compact('address'));
    }

    /**
     * Show the form for editing the specified address.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $address = Address::find($id);
        if (Auth::user()->hasRole(['Admin']) || $address->addressable_id == Auth::user()->id) {
            return View::make('address.edit', compact('address'));
        }
    }

    /**
     * Update the specified address in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function putUpdate($id)
    {
        $address = Address::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Address::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $address->update($data);

        return Redirect::to(Session::get('previous_page_2'))->with('message', 'Address updated.');
    }

    /**
     * Remove the specified address from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postDestroy($id)
    {
        Address::destroy($id);

        return Redirect::route('addresse.index')->with('message', 'Address deleted.');
    }

}
