<?php

use App\Repositories\ActionRepository;

class ActionController extends Controller {

    public function __construct()
    {
        $this->actionRepo = new ActionRepository;
    }

    // all actions
    public function getAll() {
        return Action::all();
    }

    // get action
    public function getGet($id) {
        return $this->actionRepo->format(Action::find($id));
    }

    // get random action
    public function getRandom() {
        return $action = $this->actionRepo->random(request()->all());
    }

    // assign action
    public function postAssign() {
        $data = Input::all();

        // get random action
        if(!isset($data['action_ids'])) {
            if(!isset($data['gender']) || $data['gender'] == '') $data['gender'] = null;
            $action = actionController::getRandom($data['gender']);
            $model = $data['model']::find($data['model_id']);
            $actions = $model->actions()->save($action);
        }

        // attach multipe actions
        else {
            $actions = [];
            foreach($data['action_ids'] as $action_id) {
                $model = $data['model']::find($data['model_id']);
                $action = Action::find($action_id);
                $actions[] = $model->actions()->save($action);
            }
        }

        // return actions
        return $actions;
    }

    /**
     * Display a listing of actions
     */
    public function getIndex()
    {
        $actions = Action::all();

        return View::make('action.index', compact('actions'));
    }

    /**
     * Show the form for creating a new action
     */
    public function getCreate()
    {
        return View::make('action.create');
    }

    /**
     * Store a newly created action in storage.
     */
    public function postStore()
    {
        $validator = Validator::make($data = Input::all(), Action::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        Action::create($data);

        return Redirect::route('action.index')->with('message', 'Action created.');
    }

    /**
     * Display the specified action.
     */
    public function show($id)
    {
        $action = Action::findOrFail($id);

        return View::make('action.show', compact('action'));
    }

    /**
     * Show the form for editing the specified action.
     */
    public function edit($id)
    {
        $action = Action::find($id);

        return View::make('action.edit', compact('action'));
    }

    /**
     * Update the specified action in storage.
     */
    public function putUpdate($id)
    {
        $action = Action::findOrFail($id);

        $validator = Validator::make($data = Input::all(), Action::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $action->update($data);

        return Redirect::route('action.show', $id)->with('message', 'Action updated.');
    }

    /**
     * Remove the specified action from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postDestroy($id)
    {
        Action::destroy($id);

        return Redirect::route('action.index')->with('message', 'Action deleted.');
    }

}
