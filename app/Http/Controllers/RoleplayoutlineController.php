<?php

class RoleplayOutlineController extends Controller {

    /**
     * Data only
     */
    public function getAllRoleplayOutlines(){
        $roleplayOutlines = RoleplayOutline::all();
        foreach ($roleplayOutlines as $roleplayOutline)
        {
            if (strtotime($roleplayOutline['created_at']) >= (time() - Config::get('site.new_time_frame') ))
            {
                $roleplayOutline['new'] = 1;
            }
        }
        return $roleplayOutlines;
    }

    /**
     * Display a listing of roleplayOutlines
     *
     * @return Response
     */
    public function index()
    {
        return RoleplayOutline::select([
            'id',
            'icon',
            'description',
            'name',
            'updated_at'
        ])->get();
    }

    /**
     * Display the specified roleplayOutline
     *
     * @return Response
     */
    public function show($id)
    {
        return RoleplayOutline::find($id);
    }

    /**
     * Display a listing of roleplayOutlines by user
     *
     * @return Response
     */
    public function byUser($id)
    {
        $title = 'My RoleplayOutlines';
        $object = 'roleplayOutlines-by-user/' . $id;
        $my = true;
        return View::make('roleplayOutline.index', compact('title', 'object', 'my'));
    }

    /**
     * Show the form for creating a new roleplayOutline
     *
     * @return Response
     */
    public function getCreate()
    {
        return View::make('roleplayOutline.create');
    }

    /**
     * Store a newly created roleplayOutline in storage.
     *
     * @return Response
     */
    public function postStore()
    {
        $validator = Validator::make($data = Input::all(), RoleplayOutline::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        RoleplayOutline::create($data);
        if (isset($data['edit'])) return Redirect::route('roleplayOutlines.edit', $roleplayOutline->id);
        else return Redirect::to('roleplayOutlines/user/' . Auth::user()->id)->with('message', 'RoleplayOutline saved.');
    }

    /**
     * Show the form for editing the specified roleplayOutline.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $roleplayOutline = RoleplayOutline::find($id);

        return View::make('roleplayOutline.edit', compact('roleplayOutline'));
    }

    /**
     * Update the specified roleplayOutline in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function putUpdate($id)
    {
        $roleplayOutline = RoleplayOutline::findOrFail($id);

        $validator = Validator::make($data = Input::all(), RoleplayOutline::$rules);

        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $roleplayOutline->update($data);

        return Redirect::route('roleplayOutline.show', $id)->with('message', 'RoleplayOutline updated.');
    }

    /**
     * Remove the specified roleplayOutline from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postDestroy($id)
    {
        RoleplayOutline::destroy($id);
        return Redirect::to('roleplayOutlines/user/' . Auth::user()->id)->with('message', 'RoleplayOutline deleted.');
    }

}
