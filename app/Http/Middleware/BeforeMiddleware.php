<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Middleware\CacheSettings;

class BeforeMiddleware
{
    public function handle($request, Closure $next)
    {
        // cache settings if not alredy cached
        CacheSettings::remember();

        return $next($request);
    }
}
