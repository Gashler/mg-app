<?php namespace App\Http\Middleware;

use Cache;
use DB;

class CacheSettings
{
    public static function remember($refresh = false)
    {
        if ($refresh || !Cache::has('settings.company_name')) {
            // Cache::forget('settings') doesn't work for some reason
            Cache::flush();
            $settings = DB::table('settings')->get();
            foreach ($settings as $setting) {
                Cache::rememberForever('settings.' . $setting->key, function () use ($setting) {
                    return $setting->value;
                });
            }
        }
    }
}
