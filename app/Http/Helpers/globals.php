<?php

##############################################################################################

function secondsToTime($seconds)
{
    $time = round($seconds / 3600); // hours
    $minutes = round(($seconds % 3600) / 60); // minutes
    if ($minutes < 10) {
        $minutes = "0" . $minutes;
    }
    $time .= ":" . $minutes;
    $seconds = ($seconds % 60); // seconds
    if ($seconds < 10) {
        $seconds = "0" . $seconds;
    }
    $time .= ":" . $seconds;
    return $time;
}

// evaluate a string
function evalString($string, $user = null, $object = null)
{
    $array = explode('{{', $string);
    foreach ($array as $index => $str) {
        if (strpos($str, '}}') !== false) {
            $array2 = explode('}}', $str);
            foreach ($array2 as $index2 => $str2) {
                if ($index2 % 2 == 0) {
                    eval('$array2[$index2] = ' . $str2 . ';');
                }
            }
            $array[$index] = implode($array2);
        }
    }
    return implode($array);
}

// replace only the first occurrence of a string
function replaceFirst($needle, $replace, $haystack)
{
    $pos = strpos($haystack, $needle);
    if ($pos !== false) {
        return substr_replace($haystack, $replace, $pos, strlen($needle));
    } else {
        return $haystack;
    }
}

// determine if a string ends with a substring
function strEndsWith($haystack, $needle)
{
    $length = strlen($needle);
    if ($length == 0) {
        return true;
    }
    return (substr($haystack, -$length) === $needle);
}

function getRandomGender()
{
    $genders = ['m', 'f'];
    return $gender = $genders[rand(0,1)];
}

// access a protected property
function accessProtected($obj, $prop) {
     $reflection = new ReflectionClass($obj);
     $property = $reflection->getProperty($prop);
     $property->setAccessible(true);
     return $property->getValue($obj);
}

// limit words in string
function limitWords($string, $word_limit) {
    $words = explode(" ", $string);
    $count = count($words);
    $str = implode(" ", array_splice($words, 0, $word_limit));
    if($count > $word_limit) {
        $str .= " ...";
    }
    return $str;
}

/*******************
 * print data
 *******************/

function pre($data) {
    echo '<pre>'; print_r($data->toArray()); echo '</pre>'; exit;
}

function pre2($data) {
    echo '<pre>'; print_r($data); echo '</pre>'; exit;
}

// format phone numbers (remove parenthases, spaces, and hyphens)
function formatPhone($phone) {
    return preg_replace('/\D+/', '', $phone);
}

// remove subdomain
function removeSubdomain($url) {
    return preg_replace('/\/\/.*\./', '//', $url);
}

// limit words
function limit_words($string, $word_limit)
{
$words = explode(" ",$string);
return implode(" ",array_splice($words,0,$word_limit));
}

// generate random string
function randomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}

// convert array to object
function arrayToObject($array, $model = null) {
    if(!isset($model)) $object = new stdClass;
    else {
        $model = ucfirst($model);
        $object = new $model;
    }
    foreach($array as $property => $value) {
        if(!is_array($value)) $object->$property = $value;
    };
    return $object;
}

/*******************************
* Replicate Relationships
********************************/
function replicateObject($grandparent_name, $id, $instance = false) {

    // replicate object
    $controller = Config::get('site.controllers_path') . ucfirst($grandparent_name) . 'Controller';
    $grandparent = $controller::getGet($id, $basic = true);
    $new_grandparent = $grandparent->replicate();
    $new_grandparent = $new_grandparent->create($new_grandparent->toArray());

    // create instance
    if($instance) {
        $data = [
            'player_id' => Auth::user()->id,
            $grandparent_name . '_id' => $grandparent->id
        ];
    }
    else $data['name'] = 'Copy of ' . $grandparent->name;
    $new_grandparent->update($data);

    // replicate relationships
    function replicateRelationships($grandparent, $grandparent_name, $new_grandparent) {

        // convert objects to arrays for easier manipulation
        if(!is_array($grandparent)) $grandparent = $grandparent->toArray();
        if(!is_array($new_grandparent)) $new_grandparent = $new_grandparent->toArray();

        // replicate relationships
        foreach($grandparent as $parent_name => $parent) {
            if(is_array($parent) && isset($parent[0])) {
                foreach($parent as $child_name => $child) {

                    // belongsTo / morphTo relationships
                    $property = str_singular($grandparent_name) . '_id';
                    if(isset($child[$property])) $continue = true;
                    else {
                        $property = 'morph_id';
                        if(isset($child[$property])) $continue = true;
                    }
                    if(isset($continue)) {
                        $model = Config::get('site.models_path') . str_singular(studly_case($parent_name));
                        $child_object = $model::find($child['id']);
                        $new_child = $child_object->replicate();
                        $data = $new_child->toArray();
                        $data[$property] = $new_grandparent['id'];
                        if(Schema::hasColumn(str_plural($parent_name), 'mid')) {
                            $data['mid'] = $child_object->id;
                        }
                        $new_child = $new_child->create($data);
                    }

                    // belongsToMany relationships
                    else {
                        // save relationship
                        $model = str_singular(studly_case($grandparent_name));
                        $new_grandparent_object = Config::get('setting.app_path') . $model::find($new_grandparent['id']);
                        $model = str_singular(studly_case($parent_name));
                        $child_object = $model::find($child['id']);
                        $new_grandparent_object->$parent_name()->save($child_object);
                    }

                    // replicate child's relationships
                    if(isset($new_child)) {
                        replicateRelationships($child, $parent_name, $new_child);
                    }
                }
            }

            // objects
            elseif(is_array($parent) && count($parent) > 0) {
                $model = Config::get('site.models_path') . str_singular(studly_case($parent_name));
                $parent_object = $model::find($parent['id']);
                $new_parent = $parent_object->replicate();
                $data = $new_parent->toArray();
                $property = str_singular($grandparent_name) . '_id';
                $data[$property] = $new_grandparent['id'];
                if(Schema::hasColumn(str_plural($parent_name), 'mid')) {
                    $data['mid'] = $parent_object->id;
                }
                $new_parent = $new_parent->create($data);

                // replicate child's relationships
                if(isset($new_parent)) {
                    replicateRelationships($parent, $parent_name, $new_parent);
                }

            }
        }
    }
    replicateRelationships($grandparent, $grandparent_name, $new_grandparent);

    // return new object
    return $new_grandparent;
}

/**************
* Build Paths
***************/
function buildPaths($grandparent, $grandparent_name, $top_level = true, $master = null) {
    if(!is_array($grandparent)) $grandparent = $grandparent->toArray();
    if(!isset($master)) {
        $master = $grandparent;
        $master['model'] = str_singular($grandparent_name);
        $master['model_path'] = Config::get('site.models_path') . ucfirst(str_singular($grandparent_name));
        $master['path'] = '$scope.' . $grandparent_name;
    }
    foreach($grandparent as $parent_name => $parent) {

        // arrays
        if(is_array($parent) && isset($parent[0])) {
            foreach($parent as $index => $child) {
                if(is_array($child)) {

                    // add on to paths for lower levels
                    if(is_array($child) && isset($grandparent['path'])) {
                        $child['model'] = str_singular($parent_name);
                        $child['model_path'] = Config::get('site.models_path') . ucfirst(str_singular($parent_name));
                        $child['parent'] = $grandparent['path'];
                        $child['path'] = $grandparent['path'] . '.' . $parent_name . '[' . $index . ']';
                        $child['path_php'] = $grandparent['path_php'] . '["' . $parent_name . '"][' . $index . ']';
                        $child['idx'] = $index;
                        eval($child['path_php'] . ' = $child;');
                    }

                    // generate paths for top levels
                    else {
                        $master[$parent_name][$index]['model'] = str_singular($parent_name);
                        $master[$parent_name][$index]['model_path'] = Config::get('site.models_path') . ucfirst(str_singular($parent_name));
                        $master[$parent_name][$index]['parent'] = '$scope.' . $grandparent_name;
                        $master[$parent_name][$index]['path'] = '$scope.' . $grandparent_name . '.' . $parent_name . '[' . $index . ']';
                        $master[$parent_name][$index]['path_php'] = '$master["' . $parent_name . '"][' . $index . ']';
                        $master[$parent_name][$index]['idx'] = $index;
                        $child = $master[$parent_name][$index];
                    }
                    $master = buildPaths($child, $parent_name, false, $master);
                }
            }
        }

        // objects
        elseif(is_array($parent) && count($parent) > 0) {

            // generate paths for top levels
            if(!isset($grandparent['path'])) {
                $path = '$scope.' . $grandparent_name . '.' . $parent_name;
                $path_php = '$master["' . ucfirst(str_singular($parent_name)) . '"]';
                $master[$parent_name]['model'] = str_singular($parent_name);
                $master[$parent_name]['model_path'] = Config::get('site.models_path') . ucfirst(str_singular($parent_name));
                $master[$parent_name]['parent'] = $grandparent_name;
                $master[$parent_name]['path'] = $path;
                $master[$parent_name]['path_php'] = $path_php;
                $master[$parent_name]['idx'] = $index;
            }

            // add on to paths for lower levels
            else {
                $parent['parent_path'] = $grandparent['path'];
                $parent['path'] = $grandparent['path'] . '.' . $parent_name;
                $parent['path_php'] = $grandparent['path_php'] . '["' . $parent_name . '"]';
                eval($parent['path_php'] . '["model"] = str_singular($parent_name);');
                eval($parent['path_php'] . '["model_path"] = "' . Config::get('site.models_path') . '" . ucfirst(str_singular($parent_name));');
                eval($parent['path_php'] . '["path"] = $parent["path"];');
                eval($parent['path_php'] . '["parent"] = $parent["parent_path"];');
                eval($parent['path_php'] . '["path"] = $parent["path"];');
                eval($parent['path_php'] . '["path_php"] = $parent["path_php"];');
            }

            // drill down further
            foreach($parent as $child_name => $child) {

                if(is_array($child) && isset($child[0]) || is_array($child) && count($child) > 0) {

                    foreach($child as $grandchild_name => $grandchild) {
                        if (!is_array($grandchild)) {
                            continue;
                        }
                        $grandchild['model'] = str_singular($child_name);
                        $grandchild['model_path'] = Config::get('site.models_path') . ucfirst(str_singular($child_name));
                        $grandchild['parent'] = $parent['path'];

                        // array
                        if(isset($child[0])) {
                            $grandchild['path'] = $parent['path'] . '.' . $child_name . '[' . $grandchild_name . ']';
                            $grandchild['path_php'] = $parent['path_php'] . '["' . $child_name . '"]' . '[' . $grandchild_name . ']';
                            $grandchild['idx'] = $grandchild_name;
                        }

                        // object
                        else {
                            $grandchild['path'] = $parent['path'] . '.' . $child_name;
                            $grandchild['path_php'] = $parent['path_php'] . '["' . $child_name . '"]' . '["' . $grandchild_name . '"]';
                        }

                        eval($grandchild['path_php'] . ' = $grandchild;');

                        $master = buildPaths($grandchild, $child_name, false, $master);
                    }
                }
            }
        }
    }
    return $master;
}
