<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;

class PostsMetaImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new \PostMeta(
            [
                'post_id' => $row[0],
                'key' => $row[0],
                'value' => $row[0]
            ]
        );
    }
}
