<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;

class PostsImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new \Post(
            [
                'ID' => $row[0],
                'user_id' => $row[1],
                'created_at' => $row[2],
                'body' => $row[3],
                'category_id' => $row[4],
                'title' => $row[5],
                'name' => $row[6],
                'updated_at' => $row[7]
            ]
        );
    }
}
