<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;

class QuestionsImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new \Question(
            [
                'description' => $row[0],
                'asker' => $row[1],
                'question_category_id' => $row[2]
            ]
        );
    }
}
