<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;

class ServicesImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new \Service([
            'name' => $row[0],
            'price' => $row[1],
            'user_id' => $row[2],
            'performer' => $row[3]
        ]);
    }
}
