<?php

namespace App\Imports;

// use App\Models\Action;
use Maatwebsite\Excel\Concerns\ToModel;

class ActionsImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new \Action(
            [
                'description' => $row[0],
                'rating_id' => $row[1],
                'performer' => $row[2],
                'category_id' => $row[3]
            ]
        );
    }
}
