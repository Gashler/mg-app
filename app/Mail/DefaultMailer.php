<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Email;
use User;

class DefaultMailer extends Mailable
{
    use Queueable, SerializesModels;

    public $email;

    public $user;

    public $object;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Email $email, User $user, $object)
    {
        $this->email = $email;
        $this->user = $user;
        $this->object = $object;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.default')
            ->subject($this->email->subject);
    }
}
