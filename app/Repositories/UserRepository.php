<?php

namespace App\Repositories;

use \Account;
use \Config;
use \Role;
use \User;
use \Log;
use \DB;

use Carbon\Carbon;
use App\Repositories\OauthLoginRepository;
use App\Repositories\CommonCrud;
use App\Services\MailService;

class UserRepository extends CommonCrud
{
    public function __construct(
        AppointmentRepository $appointmentRepo,
        OauthLoginRepository $oauthLoginRepo,
        MailService $mailService
    ) {
        $this->model = 'User';
        $this->appointmentRepo = $appointmentRepo;
        $this->oauthLoginRepo = $oauthLoginRepo;
        $this->mailService = $mailService;
    }

    /**
     * Create husband and wife users
     *
     * @param array $inputs
     * @return bool|User
     */
    public function createCouple(array $data, $bypass_emails = false)
    {
        // define variables
        $registrant = $data['registrant_key'];
        if ($registrant == 'Husband') {
            $spouse = 'Wife';
        } else {
            $spouse = 'Husband';
        }

        // check that users don't already exist
        $errors = [];
        foreach ($data['users'] as $key => $user) {
            if ($user = $this->where(['email' => $user['email']], 1)) {
                if (session('oauth') && session('oauth.service') == 'Wordpress') {
                    if (session('oauth.email') == $user->email) {
                        $existingUser = $user;
                    } else {
                        $errors[] = "The email address " . $user['email'] . " is already taken.";
                    }
                } else {
                    $errors[] = "The email address " . $user['email'] . " is already taken.";
                }
            }
        }
        if (count($errors) > 0) {
            return ['errors' => $errors];
        }

        // define variables
        $money = config('site.default_money');
        $role = Role::where('name', 'member')->first();
        $trial_ends_at = Carbon::now()->addDays(Config::get('site.free_trial_days'));

        // for wordpress users
        if (session('oauth') && session('oauth.wp_subscribed')) {
            $trial_ends_at = null;
        }

        // create account
        if (session('oauth') && session('oauth.service') == 'Wordpress') {
            $accountData = [
                'bypass_subscription' => session('bypass_subscription'),
                'wp_user_id' => session('oauth.wp_user_id'),
                'trial_ends_at' => $trial_ends_at,
                'wp_subscribed' => session('oauth.wp_subscribed')
            ];
        } else {
            $accountData = [
                'trial_ends_at' => $trial_ends_at
            ];
        }

        // Bypass all subscritpions (give everyone full access, as of 2020-07-30)
        $accountData['bypass_subscription'] = true;

        $account = Account::create($accountData);

        // add common data
        $data['users']['Husband']['account_id'] = $account->id;
        $data['users']['Wife']['account_id'] = $account->id;
        $data['users']['Husband']['gender'] = 'm';
        $data['users']['Wife']['gender'] = 'f';
        $data['users']['Husband']['money'] = $money;
        $data['users']['Husband']['role_id'] = $role->id;
        $data['users']['Wife']['money'] = $money;
        $data['users']['Wife']['role_id'] = $role->id;
        $data['users']['Husband']['key'] = str_random(40);
        $data['users']['Wife']['key'] = str_random(40);
        if (isset($data['users'][$registrant]['password'])) {
            $data['users'][$registrant]['password'] = $data['users'][$registrant]['password'];
        }

        // create users
        if (!isset($existingUser)) {
            $data['users'][$registrant] = User::create($data['users'][$registrant]);
        } else {
            $existingUser->update($data['users'][$registrant]);
            $data['users'][$registrant] = User::find($existingUser->id);
        }
        $data['users'][$spouse]['spouse_id'] = $data['users'][$registrant]->id;
        $data['users'][$spouse] = User::create($data['users'][$spouse]);
        $data['users'][$registrant]->update([
            'spouse_id' => $data['users'][$spouse]->id
        ]);

        // email functions
        if (!$bypass_emails) {

            // subscribe new user to mailing list
            foreach ($data['users'] as $user) {
                $campaign = \Campaign::where('key', 'new-users')->first();
                if (
                    isset($campaign)
                    && !$campaign->disabled
                    && (!$campaign->start_at || $campaign_start_at < date('Y-m-d h:i:s'))
                    && (!$campaign->end_at || $campaign_end_at > date('Y-m-d h:i:s'))
                ) {
                    if (!$user->campaigns()->find($campaign->id)) {
                        $user->campaigns()->save($campaign);
                    }
                }
            }

            // send confirmation emails
            if (isset($data['users'][$registrant]['email'])) {
                $response = $this->mailService->send('subscription-confirmation', $data['users'][$registrant]);
                if ($response['error']) {
                    return ['errors' => [$response]];
                }
            }
            if (isset($data['users'][$spouse]['email'])) {
                $response = $this->mailService->send('subscription-confirmation-spouse', $data['users'][$spouse]);
                if ($response['error']) {
                    return ['errors' => [$response]];
                }
            }
        }

        // create oauth logins
        if (session('oauth') && session('oauth.service') !== 'Wordpress') {
            $data['users'][$registrant]->oauthLogins()->create([
                'service' => session('oauth.service'),
                'service_user_id' => session('oauth.service_user_id'),
                'token' => session('oauth.token'),
                'email' => session('oauth.email')
            ]);
        }

        // create goals
        $data['users']['Husband']->goal()->create([
            'orgasm' => 3,
            'sex' => 6,
            'quality_time' => 1,
            'romantic_date' => 7
        ]);
        $data['users']['Wife']->goal()->create([
            'orgasm' => 6,
            'sex' => 6,
            'quality_time' => 1,
            'romantic_date' => 7
        ]);

        // log in registrant
        auth()->login($data['users'][$registrant]);

        // create default appointments
        $this->appointmentRepo->createDefaults($data['users']['Husband']);

        return $data['users'];
    }
}
