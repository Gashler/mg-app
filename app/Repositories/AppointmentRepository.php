<?php

namespace App\Repositories;

use \Appointment;

use App\Repositories\CommonCrud;

class AppointmentRepository extends CommonCrud
{

    /**
     * Create default appointments
     *
     * @param int $account_id
     * @return bool|User
     */
    public function createDefaults($user)
    {
        Appointment::create([
            'account_id' => $user->account_id,
            'date_start' => date('Y-m-d', strtotime('This Sunday')),
            'time_start' => '21:00:00',
            'time_end' => '22:30:00',
            'name' => "Sex",
            'recurring' => true,
            'sun' => true,
            'in_charge' => 'm',
            'alternate' => true,
            'description' => "All work must stop at 9:00. 9:00 - 9:30 is preparation time. Clean the bedroom, take a shower, brush teeth, plan a game or activity, light candles, put on music, select lingerie and any items, put on makeup, perfume, cologne, etc. Sex begins at 9:30, followed by a naked snuggle session. Putting on clothes or leaving bed is not permitted until 10:30."
        ]);
        Appointment::create([
            'account_id' => $user->account_id,
            'date_start' => date('Y-m-d', strtotime('This Friday')),
            'time_start' => '21:00:00',
            'time_end' => '22:15:00',
            'name' => "Massage Night",
            'recurring' => true,
            'wed' => true,
            'in_charge' => 'f',
            'alternate' => true,
            'description' => "All work must stop at 9:00. 9:00 - 9:15 is preparation time. Freshen up, light candles, put on relaxing music, and get naked. Option A: From 9:15 - 9:45, ".$user->husband." massages ".$user->wife."'s full body (or wherever she prefers). From 9:35 - 10:15, ".$user->wife." returns the favor. Option B: Either of you may request oral or manual stimulation of your genitals instead of a massage. Option C: ".$user->name." can pleasure both of you at the same time by applying lubricant to his himself and massaging ".$user->wife."'s back while either rubbing his penis between her bum cheeks or penetrating her vagina (with ".$user->wife."'s permission)."
        ]);
        Appointment::create([
            'account_id' => $user->account_id,
            'date_start' => date('Y-m-d', strtotime('This Sunday')),
            'time_start' => '20:00:00',
            'time_end' => '21:00:00',
            'name' => "Companionship Inventory",
            'recurring' => true,
            'sun' => true,
            'in_charge' => 'm',
            'alternate' => true,
            'description' => "Take an hour to sit down together and talk about your accomplishments or shortcomings from the previous week. Also discuss your upcoming plans and goals and how you can help each other meet them. Be open in talking about money, work, family, recreation, and sex. If there's any concerns that need to be addressed or bottled-up feelings that need to come out, now's the time. Be honest, yet kind and loving to each other. Acknowledge problems but also offer solutions. Include a compliment with every critique. Most importantly, remind each other what you love about them and end with a kiss."
        ]);
        Appointment::create([
            'account_id' => $user->account_id,
            'date_start' => date('Y-m-d', strtotime('This Saturday')),
            'time_start' => '17:00:00',
            'time_end' => '23:00:00',
            'name' => "Date Night",
            'recurring' => true,
            'sat' => true,
            'in_charge' => 'f',
            'alternate' => true,
            'description' => "If applicable, get babysitters. Freshen up. Dress up. Get out of the house. Include a fun recreational activity, a nice dinner, and leisurely entertainment. Talk, laugh, touch, kiss, and spend plenty of time gazing into each other's arms."
        ]);
    }
}
