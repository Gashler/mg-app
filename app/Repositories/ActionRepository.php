<?php

namespace App\Repositories;

use \Action;
use stdClass;
use App\Repositories\CommonCrud;

class ActionRepository extends CommonCrud
{
    // run query
    public function query($params = null)
    {
        if (!$action_history = session('action_history')) {
            $action_history = [];
        }
        $query = 'Action::whereNotIn("id", $action_history)->';
        if ($params) {
            foreach ($params as $key => $value) {
                if ($key == 'performer') {
                    $query .= 'where(function($q) { $q->where("performer", "' . $value . '")->orWhereNull("performer"); })->';
                } else {
                    $query .= 'where("' . $key . '", "' . $value . '")->';
                }
            }
        }
        $query .= 'orderByRaw("RAND()")->first();';
        eval('$action = ' . $query);
        return $action;
    }

    /**
     * Generate a random action
     *
     * @param string $performer
     * @return Action
     */
    public function random($params = null)
    {
        // clean data
        $address_performer = true;
        if (isset($params['address_performer'])) {
            $address_performer = $params['address_performer'];
            unset($params['address_performer']);
        }

        // run query
        $action = $this->query($params);

        // if no action is found, try changing the rating
        if (!isset($action) && isset($params['rating_id'])) {
            $ratingsTried[] = $params['rating_id'];
            $originalRatingId = $params['rating_id'];
            if ($params['rating_id'] < 5) {
                $params['rating_id'] ++;
            } else {
                $params['rating_id'] --;
            }
            while (!isset($action) && $params['rating_id'] > 0 && !in_array($params['rating_id'], $ratingsTried)) {
                $action = $this->query($params);
                $ratingsTried[] = $params['rating_id'];
                if ($params['rating_id'] < 5) {
                    $params['rating_id'] ++;
                } else {
                    $params['rating_id'] = $originalRatingId --;
                }
            }
        }

        // if still no action is found, clear action history and start over
        if (!session('tried_action_history_reset') && !isset($action)) {
            session('tried_action_history_reset', true);
            session()->forget('action_history');
            return $this->random($params);
        }

        // if still no action is found
        if (!isset($action)) {
            return [
                'success' => false,
                'message' => [
                    'type' => 'danger',
                    'body' => "There is no action that meets the specified parameters."
                ]
            ];
        }

        if (!isset($params['performer'])) {
            $params['performer'] = getRandomGender();
        }

        // add action to session history
        $action_history = session('action_history');
        $action_history[] = $action->id;
        session()->put('action_history', $action_history);

        return $this->format($action, $params['performer'], $address_performer);
    }

    /**
     * Format an action
     *
     * @param Action $action
     * @param string $subject | null
     * @return Action $action
     */
    public function format($action, $performer = null, $address_performer = true) {

        // determine subject
        if ($action->performer) {
            $performer = $action->performer;
        }
        if (isset($performer)) {
            if (auth()->user()->gender == $performer) {
                $subject = auth()->user()->spouse;
                $directObject = auth()->user();
            } else {
                $subject = auth()->user();
                $directObject = auth()->user()->spouse;
            }
        } else {
            $subject = auth()->user();
            $directObject = auth()->user()->spouse;
        }

        $description = $action->description;

        // determine
        if (isset($subject)) {
            $array = explode("[spouse's]", $description);
            if (strpos($array[0], '[spouse]') == false) {
                $subject->possessive_first = $subject->first_name . "'s";
            } else {
                $subject->possessive_first = $subject->possessive;
            }
        } else {
            $subject = new stdClass;
            $subject->first_name = 'your lover';
            $subject->possessive_first = 'your lover\'s';
            $subject->possessive = 'their';
            $subject->accusative = 'them';
            $subject->nominative = 'your lover';
        }
        $description = preg_replace('[spouse\'s]', $subject->possessive_first, $description, 1);
        $description = str_replace('[spouse\'s]', $subject->possessive, $description);
        $description = preg_replace('[spouse]', $subject->first_name, $description, 1);
        $description = str_replace('[a]', $subject->accusative, $description);
        $description = str_replace('[n]', $subject->nominative, $description);
        $description = str_replace(array( '[', ']' ), '', $description);

        if ($address_performer === true) {
            if ($action->performer !== '') {
                $description = $directObject->first_name . ', ' . $description;
            }
        }
        $action->description = ucfirst($description);
        return [
            'success' => true,
            'data' => $action
        ];
    }
}
