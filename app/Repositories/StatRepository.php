<?php

namespace App\Repositories;

use Auth;
use Stat;

use \FavorClock;

use Carbon\Carbon;
use App\Repositories\GoalRepository;

use App\Repositories\CommonCrud;

class StatRepository extends CommonCrud
{
    public function __construct(
        GoalRepository $goalRepo
    ) {
        $this->goalRepo = $goalRepo;
    }

    /**
     * Get a calendar month of stats
     *
     * @param array $inputs
     * @return bool|Stat
     */
    public function month($date)
    {
        /*************************
        * generate month calendar
        **************************/

        // determine legnth of month
        $days_in_month = date('t', strtotime($date));

        // define names of days
        $days_of_week = [
            'Sun',
            'Mon',
            'Tue',
            'Wed',
            'Thu',
            'Fri',
            'Sat'
        ];

        // create entry for each day of month
        for ($row = 1; $row <= 6; $row ++) {
            for ($col = 0; $col <= 6; $col ++) {
                $days[] = [
                    'name' => $days_of_week[$col],
                    'model' => 'day'
                ];
            }
        }

        // determine on which day of week the month begins
        $first_day_of_month = date('D', strtotime($date));
        foreach ($days as $index => $day) {
            if ($day['name'] == $first_day_of_month) {
                $starting_index = $index;
                break;
            }
        }

        // add date, day of month, and uvents to each entry
        $day_of_month = 1;
        for ($index = $starting_index; $index <= count($days); $index ++) {
            if ($day_of_month <= $days_in_month) {
                $days[$index]['day_of_month'] = $day_of_month;
                $y_m = date('Y-m', strtotime($date));
                $y_m_d = $y_m .= '-' . $day_of_month;
                $days[$index]['date'] = date('Y-m-d', strtotime($y_m_d));
                $days[$index]['date_js'] = date('Y,m,d', strtotime($y_m_d));
                $days[$index]['date_formatted'] = date('l, M jS', strtotime($y_m_d));
                $days[$index]['appointments'] = [];
                $day_of_month ++;
            }
        }

        // combine data
        foreach ($days as $index => $day) {

            // mark current day
            if (date('Y-m') == date('Y-m', strtotime($date)) && isset($day['date_formatted']) && $day['day_of_month'] == date('j')) {
                $days[$index]['active'] = 1;
            }

            // add stats
            foreach (Auth::user()->stats as $stat) {
                if (isset($day['date']) && $stat['date'] == $day['date']) {
                    $days[$index]['stats'] = $stat;
                }
            }

            // add appointments
            foreach (Auth::user()->appointments as $appointment) {
                if (isset($day['date']) && $appointment['date_start'] == $day['date']) {
                    $days[$index]['appointments'][] = $appointment;
                }
            }
        }

        // get recurring appointments
        $recurring = \Appointment::where('account_id', Auth::user()->account_id)->where('recurring', '1')->get();

        // return data
        return [
            'date' => $date,
            'days' => $days,
            'month' => [
                'days_in_month' => $days_in_month,
                'number' => date('m', strtotime($date)),
                'name' => date('F', strtotime($date))
            ],
            'recurring' => $recurring,
            'today' => [
                'date' => date('Y-m-d'),
            ],
            'year' => date('Y', strtotime($date)),
        ];
    }

    public function daysSince($key, $prefix = null) {
        if ($stat = Stat::
            where('account_id', auth()->user()->account_id)
            ->where($prefix . $key, true)
            ->orderBy('date', 'DESC')
            ->first())
        {
            $date = new Carbon($stat->date);
            $now = Carbon::now();
            $difference = ($date->diff($now)->days < 1)
                ? 'today'
                : $date->diffForHumans($now);
            return str_replace('before', 'ago', $difference);
        }
        return '-';
    }

    // assign a letter grade to a percentage
    public function grade($percentage)
    {
        $grades = [
            97 => 'A+',
            93 => 'A',
            90 => 'A-',
            87 => 'B+',
            83 => 'B',
            80 => 'B-',
            77 => 'C+',
            73 => 'C',
            70 => 'C-',
            67 => 'D+',
            63 => 'D',
            60 => 'D-'
        ];
        foreach ($grades as $number => $letter) {
            if ($percentage >= $number) {
                return $letter;
            }
            return 'F';
        }
    }

    public function stats()
    {
        // get goals
        $goals = [
            'user' => \User::find(auth()->user()->id)->goal()->first(),
            'spouse' => \User::find(auth()->user()->spouse_id)->goal()->first(),
        ];

        // return data
        $data = [
            'goals' => $goals,
            'results' => [
                $this->combineGoalsAndStats($goals, 'sex', 'Sex'),
                $this->combineGoalsAndStats($goals, 'orgasm', auth()->user()->husband . "'s Orgasms", 'm_'),
                $this->combineGoalsAndStats($goals, 'orgasm', auth()->user()->wife . "'s Orgasms", 'f_'),
                $this->combineGoalsAndStats($goals, 'quality_time', 'Quality Time'),
                $this->combineGoalsAndStats($goals, 'romantic_date', 'Romantic Dates'),
            ]
        ];

        // caluclate overall grade
        $totalPoints = 0;
        $qualifiedSets = 0;
        $data['overallGrade'] = '-';
        foreach ($data['results'] as $index => $set) {
            if ($set['percentage'] !== '-') {
                $qualifiedSets ++;
                $totalPoints += $set['percentage'];
            }
        }
        if ($totalPoints > 0) {
            $overallPercentage = round($totalPoints / $qualifiedSets, 2);
            $data['overallGrade'] = $this->grade($overallPercentage);
        }

        return $data;
    }

    public function combineGoalsAndStats($goals, $key, $name, $prefix = null)
    {
        // get results
        $rate = '-';
        $stat = Stat::
            where('account_id', auth()->user()->account_id)
            ->where($prefix . $key, true)
            ->orderBy('date', 'ASC')
            ->first();
        if ($stat) {
            $date = new Carbon($stat->date);
            $now = Carbon::now();
            $daysSinceFirstStat = ($date->diff($now)->days < 1) ? 0 : $date->diffInDays($now);
            if ($daysSinceFirstStat > 0) {
                $statCount = Stat::
                    where('account_id', auth()->user()->account_id)
                    ->where($prefix . $key, true)
                    ->count();
                \Log::info('$daysSinceFirstStat = ' . $daysSinceFirstStat);
                \Log::info('$statCount = ' . $statCount);
                $rate = round($daysSinceFirstStat / $statCount, 1);
            }
        }

        $userGoal = isset($goals['user']) ? $goals['user']->$key : '-';
        $spouseGoal = isset($goals['spouse']) ? $goals['spouse']->$key : '-';
        if ($prefix) {
            if (auth()->user()->gender == $prefix[0]) {
                $spouseGoal = '-';
            } else {
                $userGoal = '-';
            }
        }
        if ($userGoal !== '-' && $spouseGoal !== '-' && $userGoal !== 0 && $spouseGoal !== 0) {
            $goal = round(($userGoal + $spouseGoal) / 2, 1);
        } else {
            $goal = '-';
        }
        if ($goal == '-') {
            if ($userGoal !== '-') {
                $goal = $userGoal;
            } elseif ($spouseGoal !== '-') {
                $goal = $spouseGoal;
            }
        }
        if ($goal !== '-' && $rate !== '-' && $goal !== 0 && $rate !== 0) {
            $percentage = round(($goal / $rate) * 100, 2);
        } else {
            $percentage = '-';
        }
        if ($percentage !== '-') {
            $grade = $this->grade($percentage);
        } else {
            $grade = '-';
        }
        if ($prefix || ($userGoal == $goal && $spouseGoal == $goal)) {
            $goal = '-';
        }
        if ($prefix) {
            $gender = $prefix[0];
        } else {
            $gender = null;
        }
        if ($rate !== '-') {
            $str = 'Every ' . $rate . ' day';
            if ($rate != 1) {
                $str .= "s";
            } else {
                $str = "Every day";
            }
            $rate = $str;
        }
        if ($goal !== '-') {
            $str = 'Every ' . $goal . ' day';
            if ($goal != 1) {
                $str .= "s";
            } else {
                $str = "Every day";
            }
            $goal = $str;
        }

        return [
            'key' => $key,
            'name' => $name,
            'goal_user' => $userGoal,
            'goal_spouse' => $spouseGoal,
            'goal_compromise' => $goal,
            'rate' => $rate,
            'since_last' => $this->daysSince($key, $prefix),
            'percentage' => $percentage,
            'grade' => $grade,
            'gender' => $gender
        ];
    }

    public function year()
    {
        $data = [
            'labels' => [],
            'datasets' => [
                'm_orgasm' => [
                    'label' => auth()->user()->husband . "'s Orgasms",
                    'data' => [0,0,0,0,0,0,0,0,0,0,0,0,0]
                ],
                'f_orgasm' => [
                    'label' => auth()->user()->wife . "'s Orgasms",
                    'data' => [0,0,0,0,0,0,0,0,0,0,0,0,0]
                ],
                'sex' => [
                    'label' => "Had Sex",
                    'data' => [0,0,0,0,0,0,0,0,0,0,0,0,0]
                ],
                'quality_time' => [
                    'label' => "Shared Quality Time",
                    'data' => [0,0,0,0,0,0,0,0,0,0,0,0,0]
                ],
                'romantic_date' => [
                    'label' => "Had Date",
                    'data' => [0,0,0,0,0,0,0,0,0,0,0,0,0]
                ],
            ]
        ];
        $stats = Stat::
            where('account_id', auth()->user()->account_id)
            ->where('date', '>', date('Y-m-d', strtotime('1 year ago')))
            ->orderBy('date', 'ASC')
            ->get();
        for ($x = 12; $x >- 1; $x --) {
            $year = date('Y', strtotime($x . ' months ago'));
            $month = date('M', strtotime($x . ' months ago'));
            $data['labels'][] = $month;
            foreach ($stats as $stat) {
                if (date('M', strtotime($stat->date)) == $month && date('Y', strtotime($stat->date)) == $year) {
                    if ($stat->sex) {
                        $data['datasets']['sex']['data'][$x] ++;
                    }
                    if ($stat->m_orgasm) {
                        $data['datasets']['m_orgasm']['data'][$x] ++;
                    }
                    if ($stat->f_orgasm) {
                        $data['datasets']['f_orgasm']['data'][$x] ++;
                    }
                    if ($stat->quality_time) {
                        $data['datasets']['quality_time']['data'][$x] ++;
                    }
                    if ($stat->romantic_date) {
                        $data['datasets']['romantic_date']['data'][$x] ++;
                    }
                }
            }
        }
        $data['datasets']['m_orgasm']['data'] = array_reverse($data['datasets']['m_orgasm']['data']);
        $data['datasets']['f_orgasm']['data'] = array_reverse($data['datasets']['f_orgasm']['data']);
        $data['datasets']['sex']['data'] = array_reverse($data['datasets']['sex']['data']);
        $data['datasets']['quality_time']['data'] = array_reverse($data['datasets']['quality_time']['data']);
        $data['datasets']['romantic_date']['data'] = array_reverse($data['datasets']['romantic_date']['data']);
        return $data;
    }
}
