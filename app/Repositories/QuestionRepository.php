<?php

namespace App\Repositories;

use \Question;
use App\Repositories\CommonCrud;

class QuestionRepository extends CommonCrud
{
    // run query
    public function query($params = null)
    {
        if (!$question_history = session('question_history')) {
            $question_history = [];
        }
        $query = 'Question::whereNotIn("id", $question_history)->';
        if ($params) {
            foreach ($params as $key => $value) {
                if ($value) {
                    if ($key == 'asker') {
                        $query .= 'where(function($q) { $q->where("asker", "' . $value . '")->orWhere("asker", ""); })->';
                    } else {
                        $query .= 'where("' . $key . '", "' . $value . '")->';
                    }
                }
            }
        }
        $query .= 'orderByRaw("RAND()")->first();';
        eval('$question = ' . $query);
        return $question;
    }

    /**
     * Generate a random question
     *
     * @param string $performer
     * @return Question
     */
    public function random($params = [])
    {
        $private = false;
        if (isset($params['private']) && $params['private']) {
            $private = true;
            unset($params['private']);
        }

        if (!isset($params['asker'])) {
            $params['asker'] = auth()->user()->gender;
        }

        // run query
        $question = $this->query($params);

        // if no question is found, clear question history and start over
        if (!session('tried_question_history_reset') && !isset($question)) {
            session('tried_question_history_reset', true);
            session()->forget('question_history');
            return $this->random($params);
        }

        // if still no question is found
        if (!isset($question)) {
            return [
                'success' => false,
                'message' => [
                    'type' => 'danger',
                    'body' => "There is no question that meets the specified parameters."
                ]
            ];
        }

        // add question to session history
        $question_history = session('question_history');
        $question_history[] = $question->id;
        session()->put('question_history', $question_history);

        return $this->format($question, $private);
    }

    // format questions
    public function format($questions, $private = false) {
        if (!is_array($questions)) {
            $questions = [$questions];
        }
        foreach ($questions as $index => $question) {

            // if for a conversation
            if (!$private) {
                $questions[$index]['description'] = str_replace("[s]", "", $question['description']);
                $questions[$index]['description'] = str_replace("[es]", "", $question['description']);

            // if for a private questionnaire
            } else {
                $question['description'] = replaceFirst(" I'm ",  " " . auth()->user()->spouse->first_name . "'s ", $question['description']);
                if (
                    strpos($question['description'], " " . auth()->user()->spouse->first_name . " ") !== false
                    || strpos($question['description'], " " . auth()->user()->spouse->first_name . "'s ") !== false
                ) {

                } else {
                    $question['description'] = replaceFirst(" I ",  " " . auth()->user()->spouse->first_name . " ", $question['description']);
                }
                $question['description'] = replaceFirst(" we ",  " you and " . auth()->user()->spouse->first_name . " ", $question['description']);
                if (
                    strpos($question['description'], " " . auth()->user()->spouse->first_name . " ") !== false
                    || strpos($question['description'], " " . auth()->user()->spouse->first_name . "'s ") !== false
                ) {
                    $question['description'] = replaceFirst(" me ", " " . auth()->user()->spouse->accusative . " ", $question['description']);
                    $question['description'] = replaceFirst(" me.", " " . auth()->user()->spouse->accusative . ".", $question['description']);
                    $question['description'] = replaceFirst(" me,", " " . auth()->user()->spouse->accusative . ",", $question['description']);
                    $question['description'] = replaceFirst(" me?", " " . auth()->user()->spouse->accusative . "?", $question['description']);
                } else {
                    $question['description'] = replaceFirst(" me ", " " . auth()->user()->spouse->first_name . " ", $question['description']);
                    $question['description'] = replaceFirst(" me.", " " . auth()->user()->spouse->first_name . ".", $question['description']);
                    $question['description'] = replaceFirst(" me,", " " . auth()->user()->spouse->first_name . ",", $question['description']);
                    $question['description'] = replaceFirst(" me?", " " . auth()->user()->spouse->first_name . "?", $question['description']);
                }
                if (
                    strpos($question['description'], " " . auth()->user()->spouse->first_name . " ") !== false
                    || strpos($question['description'], " " . auth()->user()->spouse->first_name . "'s ") !== false
                ) {

                } else {
                    $question['description'] = replaceFirst(" my ", " " . auth()->user()->spouse->first_name . "'s ", $question['description']);
                    $question['description'] = replaceFirst(" our ", " yours and " . auth()->user()->spouse->first_name . "'s ", $question['description']);
                }
                $replacements = [
                    " I " => " " . auth()->user()->spouse->nominative . " ",
                    " we " => " you ",
                    " me " => " " . auth()->user()->spouse->accusative . " ",
                    " have'nt " => " hasn't ",
                    " I'm " => " " . auth()->user()->spouse->nominative . "'s ",
                    "tell me " => "write ",
                    "tell " . auth()->user()->spouse->first_name . " " => "write ",
                    "[s]" => "s",
                    " me " => " " . auth()->user()->spouse->first_name . " ",
                    " me." => " " . auth()->user()->spouse->first_name . ".",
                    " me," => " " . auth()->user()->spouse->first_name . ",",
                    " me?" => " " . auth()->user()->spouse->first_name . "?",
                    " " . auth()->user()->spouse->first_name . " haven't " => " " . auth()->user()->spouse->first_name . " hasn't ",
                    "[es]" => "es",
                    " my " => " " . auth()->user()->spouse->possessive . " ",
                    " our " => " your ",
                    " carry " => " carries "
                ];
                foreach ($replacements as $key => $value) {
                    $question['description'] = str_replace($key, $value, $question['description']);
                }
                $questions[$index]['description'] = ucfirst($question['description']);
            }
        }
        if (count($questions) == 1) {
            return $questions[0];
        }
        return $questions;
    }
}
