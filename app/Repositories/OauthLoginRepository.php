<?php

namespace App\Repositories;

use \OauthLogin;
use \User;

use App\Repositories\CommonCrud;

class OauthLoginRepository extends CommonCrud
{
    /**
    * Create an oAuth record
    *
    * @param array $data
    * @param User $user
    * @return OauthLogin $oauthLogin
    */
    public function create(array $data, User $user)
    {
        $oauthLogin = OauthLogin::
            where('service', $data['service'])->
            where('user_id', $user->id)->
            first();
        if ($oauthLogin) {
            $oauthLogin->update($data);
            $oauthLogin = OauthLogin::find($oauthLogin->id);
        } else {
            $oauthLogin = OauthLogin::create($data);
            $user->oauthLogins()->save($oauthLogin);
        }
        return $oauthLogin;
    }

    /**
    * Get a user's oAuth token
    *
    * @param string $service
    * @param int $user_id
    * @return string $token
    */
    public function token($service, $user_id)
    {
        $oauthLogin = OauthLogin::
            where('service', $service)->
            where('user_id', $user_id)->
            first();
        if (isset($oauthLogin)) {
            return $oauthLogin->token;
        }
    }
}
