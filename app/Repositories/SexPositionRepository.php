<?php

namespace App\Repositories;

use \SexPosition;
use stdClass;
use App\Repositories\CommonCrud;

class SexPositionRepository extends CommonCrud
{
    // run query
    public function query($params = null)
    {
        if (!$sexPosition_history = session('sexPosition_history')) {
            $sexPosition_history = [];
        }
        $query = 'SexPosition::whereNotIn("id", $sexPosition_history)->';
        $query .= 'orderByRaw("RAND()")->first();';
        eval('$sexPosition = ' . $query);
        return $sexPosition;
    }

    /**
     * Generate a random sexPosition
     *
     * @param string $performer
     * @return SexPosition
     */
    public function random($params = null)
    {
        // run query
        $sexPosition = $this->query($params);

        // if no sexPosition is found, clear sexPosition history and start over
        if (!session('tried_sexPosition_history_reset') && !isset($sexPosition)) {
            session('tried_sexPosition_history_reset', true);
            session()->forget('sexPosition_history');
            return $this->random($params);
        }

        // if still no sexPosition is found
        if (!isset($sexPosition)) {
            return [
                'success' => false,
                'message' => [
                    'type' => 'danger',
                    'body' => "There is no sexPosition that meets the specified parameters."
                ]
            ];
        }

        // add sexPosition to session history
        $sexPosition_history = session('sexPosition_history');
        $sexPosition_history[] = $sexPosition->id;
        session()->put('sexPosition_history', $sexPosition_history);

        return [
            'success' => true,
            'sexPosition' => $sexPosition
        ];
    }
}
