<?php

namespace App\Repositories;

use App\Repositories\CommonCrud;

class AuthRepository extends CommonCrud
{
    /**
    * Log in a user
    *
    * @return array
    */
    public function login($data)
    {
        if (isset($data['user'])) {
            auth()->login($data['user']);
        } else {
            if ($attempt = auth()->attempt([
                'email' => $data['email'],
                'password' => $data['password']
            ], false)) {
                $user = \User::where('email', $data['email'])->first();
                auth()->login($user);
            }
        }
        if (auth()->check()) {
            if (isset(auth()->user()->account)) {
                if (auth()->user()->account->subscribed) {
                    return [
                        'success' => true
                    ];
                } elseif (!auth()->user()->account->subscribed) {
                    return [
                        'success' => false,
                        'error_code' => 'no_subscription',
                        'message' => "You do not have an active subscription."
                    ];
                }

            // if a Wordpress user who hasn't set up an account yet
            } else {
                $user = auth()->user();
                auth()->logout();
                return [
                    'user' => $user,
                    'success' => false,
                    'error_code' => 'no_account'
                ];
            }
        }
        return [
            'success' => false,
            'error_code' => 'incorrect',
            'message' => "Incorrect email or password."
        ];
    }

    public function store($user = null)
    {
        $data = request()->all();
        if (isset($user)) {
            $data['email'] = $user->email;
            $data['password'] = $user->password;
        }

        // redirect wp_users
        if ($user = \User::where('email', $data['email'])->orWhere('login', $data['email'])->first()) {

            // if not already redirected from Wordpress
            if (!isset($data['wp_user_id'])) {
                if ($user->wp_user_id) {
                    $data = [
                        'email' => $data['email'],
                        'password' => $data['password'],
                        'host_url' => env('HOST_URL'),
                        'app_url' => env('APP_URL'),
                        'csrf_token' => csrf_token()
                    ];
                    return view('sessions/post-to-wordpress', compact('data'));
                }

                // if already authenticated as WP user
            } else {

                // if app users have already been created
                if ($user->spouse_id && isset($user->account)) {
                    $user->account->update([
                        'wp_subscribed' => $data['subscribed']
                    ]);
                    auth()->login($user);
                    return redirect('/');

                    // create app users
                } else {
                    $oauth = [
                        'email' => $user->email,
                        'service' => 'Wordpress',
                        'wp_subscribed' => $data['subscribed'],
                        'wp_user_id' => $data['wp_user_id']
                    ];
                    $users = [
                        'Husband' => [
                            'first_name' => $data['husband']
                        ],
                        'Wife' => [
                            'first_name' => $data['wife']
                        ]
                    ];
                    session([
                        'oauth' => $oauth,
                        'users' => $users
                    ]);
                    return redirect('/register');
                }
            }
        }

        if (isset($users)) {

            // if logging in newly-created users for the first time
            if (!isset($users['wp_user'])) {
                $data['user'] = \User::where('account_id', $users['Husband']['account_id'])->where('gender', 'm')->first();

            // if a Wordpress user needs an account with this system
            } else {
                $users = $this->userRepo->createCouple($users);
                return $this->store($users);
            }
        }
        $response = $this->login($data);
        if ($response['success']) {
            return redirect('/');
        } else {
            return redirect('login')->with('message_danger', $response['message']);
        }
    }
}
