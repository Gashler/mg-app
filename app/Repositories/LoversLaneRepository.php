<?php

namespace App\Repositories;

use \Category;
use \LoversLane;

use App\Repositories\CommonCrud;

class LoversLaneRepository extends CommonCrud
{

    /**
     * Create a new game
     *
     * @return LoversLane
     */
    public function create()
    {
        if(auth()->user()->gender == 'm') {
            $m_ready = 1;
            $f_ready = 0;
            $m_money = auth()->user()->money;
            $f_money = auth()->user()->spouse->money;
        }
        else {
            $m_ready = 0;
            $f_ready = 1;
            $m_money = auth()->user()->spouse->money;
            $f_money = auth()->user()->money;
        }
        $genders = ['m', 'f'];

        // get categories
        $categories = Category::all();

        $data = [
            'account_id' => auth()->user()->account_id,
            'data' => [
                'currentSpace' => 0,
                'disclaimerWarning' => null,
                'disclaimerDanger' => null,
                'fee' => 0,
                'heatIndex' => 4,
                'turn' => $genders[rand(0,1)],
                'm_space' => 0,
                'm_naked' => false,
                'm_jail' => null,
                'f_space' => 0,
                'f_naked' => false,
                'f_jail' => null,
                'loaded' => false,
                'level' => 1,
                'paydayAmount' => 200,
                'raiseLevel' => 7,
                'regenerating' => false,
                'showAlternateTurnButton' => false,
                'showContent' => false,
                'showTime' => false,
                'showStartButton' => false,
                'showNakedButton' => false,
                'showPayButton' => false,
                'showRegenerateButton' => false,
                'showWaitButton' => false,
                'showPerformButton' => false,
                'showContinueButton' => false,
                'showStore' => false,
                'canAffordProperties' => false,
                'destination' => 0,
                'spaceSize' => 125,
                'defaultTime' => 60,
                'time' => 0,
                'turns' => 0,
                'paydayAmount' => 200,
                'content' => [
                    'header' => null,
                    'body' => null
                ],
                'heat' => [
                    [
                        'level' => 5,
                        'name' => 'Orgasmic',
                        'active' => 0
                    ],
                    [
                        'level' => 4,
                        'name' => 'Passionate',
                        'active' => 0
                    ],
                    [
                        'level' => 3,
                        'name' => 'Erotic',
                        'active' => 0
                    ],
                    [
                        'level' => 2,
                        'name' => 'Sexy',
                        'active' => 0
                    ],
                    [
                        'level' => 1,
                        'name' => 'Warming Up',
                        'active' => 1
                    ]
                ],
                'spaces' => [
                    [
                        'name' => 'Payday',
                        'type' => 'payday',
                        'classes' => 'corner',
                        'icon' => 'money-bag'
                    ],
                    [
                        'name' => 'Arousel Avenue',
                        'type' => 'lot',
                        'price' => 100,
                        'category' => 'club',
                    ],
                    [
                        'name' => 'Bawdy Boulevard',
                        'type' => 'lot',
                        'price' => 150,
                        'category' => 'club',
                    ],
                    [
                        'name' => 'Horny Hollows',
                        'type' => 'lot',
                        'price' => 200,
                        'category' => 'club',
                    ],
                    [
                        'name' => 'Strip',
                        'type' => 'strip',
                        'classes' => 'corner',
                        'icon' => 'thong'
                    ],
                    [
                        'name' => 'Erotic Escape',
                        'type' => 'lot',
                        'price' => 250,
                        'category' => 'spa',
                    ],
                    [
                        'name' => 'Flirtatious Flats',
                        'type' => 'lot',
                        'price' => 300,
                        'category' => 'spa',
                    ],
                    [
                        'name' => 'Lovers Lane',
                        'type' => 'lot',
                        'price' => 350,
                        'category' => 'spa',
                    ],
                    [
                        'name' => 'Jail',
                        'type' => 'jail',
                        'classes' => 'corner',
                        'icon' => 'key-lock'
                    ],
                    [
                        'name' => 'Luscious Lounge',
                        'type' => 'lot',
                        'price' => 400,
                        'category' => 'cabaret',
                    ],
                    [
                        'name' => 'Lustful Lane',
                        'type' => 'lot',
                        'price' => 450,
                        'category' => 'cabaret',
                    ],
                    [
                        'name' => 'Provocative Parkway',
                        'type' => 'lot',
                        'price' => 500,
                        'category' => 'cabaret',
                    ],
                    [
                        'name' => 'Bankrupt',
                        'type' => 'bankrupt',
                        'classes' => 'corner',
                        'icon' => 'heart-arrow'
                    ],
                    [
                        'name' => 'Romantic Road',
                        'type' => 'lot',
                        'price' => 550,
                        'category' => 'hotel',
                    ],
                    [
                        'name' => 'Sexy Street',
                        'type' => 'lot',
                        'price' => 600,
                        'category' => 'hotel',
                    ],
                    [
                        'name' => 'Voluptuous Valley',
                        'type' => 'lot',
                        'price' => 650,
                        'category' => 'hotel',
                    ]
                ]
            ]
        ];

        // generate property prices
        foreach ($data['data']['spaces'] as $index => $space) {
            if (isset($space['type']) && $space['type'] == 'lot') {
                foreach ($categories as $category) {
                    $data['data']['spaces'][$index]['prices'][$category->key] = [
                        'category_id' => $category->id,
                        'key' => $category->key,
                        'name' => $category->name,
                        'price' => round($space['price'] * $category->rate, -1)
                    ];
                }

                // order by price
                $data['data']['spaces'][$index]['prices'] = array_values(array_sort($data['data']['spaces'][$index]['prices'], function($value) {
                    return $value['price'];
                }));
            }
        }

        LoversLane::create($data);
        return LoversLane::where('account_id', auth()->user()->account_id)->first();
    }
}
