<?php

namespace App\Repositories;

use Auth;
use Goal;

use Carbon\Carbon;
use App\Repositories\CommonCrud;

class GoalRepository extends CommonCrud
{

    /**
     * Get a calendar month of goals
     *
     * @param array $inputs
     * @return bool|Goal
     */
    public function month($date)
    {
        /*************************
        * generate month calendar
        **************************/

        // determine legnth of month
        $days_in_month = date('t', strtotime($date));

        // define names of days
        $days_of_week = [
            'Sun',
            'Mon',
            'Tue',
            'Wed',
            'Thu',
            'Fri',
            'Sat'
        ];

        // create entry for each day of month
        for ($row = 1; $row <= 6; $row ++) {
            for ($col = 0; $col <= 6; $col ++) {
                $days[] = [
                    'name' => $days_of_week[$col],
                    'model' => 'day'
                ];
            }
        }

        // determine on which day of week the month begins
        $first_day_of_month = date('D', strtotime($date));
        foreach ($days as $index => $day) {
            if ($day['name'] == $first_day_of_month) {
                $starting_index = $index;
                break;
            }
        }

        // add date, day of month, and uvents to each entry
        $day_of_month = 1;
        for ($index = $starting_index; $index <= count($days); $index ++) {
            if ($day_of_month <= $days_in_month) {
                $days[$index]['day_of_month'] = $day_of_month;
                $y_m = date('Y-m', strtotime($date));
                $y_m_d = $y_m .= '-' . $day_of_month;
                $days[$index]['date'] = date('Y-m-d', strtotime($y_m_d));
                $days[$index]['date_js'] = date('Y,m,d', strtotime($y_m_d));
                $days[$index]['date_formatted'] = date('l, M jS', strtotime($y_m_d));
                $days[$index]['appointments'] = [];
                $day_of_month ++;
            }
        }

        // combine data
        foreach ($days as $index => $day) {

            // mark current day
            if (date('Y-m') == date('Y-m', strtotime($date)) && isset($day['date_formatted']) && $day['day_of_month'] == date('j')) {
                $days[$index]['active'] = 1;
            }

            // add goals
            foreach (Auth::user()->goals as $goal) {
                if (isset($day['date']) && $goal['date'] == $day['date']) {
                    $days[$index]['goals'] = $goal;
                }
            }

            // add appointments
            foreach (Auth::user()->appointments as $appointment) {
                if (isset($day['date']) && $appointment['date_start'] == $day['date']) {
                    $days[$index]['appointments'][] = $appointment;
                }
            }
        }

        // get recurring appointments
        $recurring = \Appointment::where('account_id', Auth::user()->account_id)->where('recurring', '1')->get();

        // return data
        return [
            'date' => $date,
            'days' => $days,
            'month' => [
                'days_in_month' => $days_in_month,
                'number' => date('m', strtotime($date)),
                'name' => date('F', strtotime($date))
            ],
            'recurring' => $recurring,
            'today' => [
                'date' => date('Y-m-d'),
            ],
            'year' => date('Y', strtotime($date)),
        ];
    }

    public function daysSinceSex() {
        if ($goal = Goal::
            where('account_id', auth()->user()->account_id)
            ->where('sex', true)
            ->orderBy('date', 'DESC')
            ->first())
        {
            $date = new Carbon($goal->date);
            $now = Carbon::now();
            $difference = ($date->diff($now)->days < 1)
                ? 'today'
                : $date->diffForHumans($now);
            return str_replace('before', 'ago', $difference);
        }
        return '-';
    }

    public function daysSinceOrgasm() {
        $data = [
            'm' => '-',
            'f' => '-'
        ];
        foreach($data as $key => $value) {
            if ($goal = Goal::
                where('account_id', auth()->user()->account_id)
                ->where($key.'_orgasmed', true)
                ->orderBy('date', 'DESC')
                ->first())
            {
                $date = new Carbon($goal->date);
                $now = Carbon::now();
                $difference = ($date->diff($now)->days < 1)
                    ? 'today'
                    : $date->diffForHumans($now);
                $value = str_replace('before', 'ago', $difference);
            }
            $data[$key] = $value;
        }
        return $data;
    }

    public function sexRate() {
        if ($goal = Goal::
            where('account_id', auth()->user()->account_id)
            ->where('sex', true)
            ->orderBy('date', 'ASC')
            ->first())
        {
            $date = new Carbon($goal->date);
            $now = Carbon::now();
            $daysSinceFirstGoal = ($date->diff($now)->days < 1)
                ? 'today'
                : $date->diffInDays($now);
            $goalCount = Goal::
                where('account_id', auth()->user()->account_id)
                ->where('sex', true)
                ->count();
            $average = round($daysSinceFirstGoal / $goalCount, 2);
            return 'Every '.$average.' days';
        }
        return '-';
    }

    public function year()
    {
        $data = [
            'labels' => [],
            'datasets' => [
                'm_orgasm' => [
                    'label' => auth()->user()->husband . "'s Orgasms",
                    'data' => [0,0,0,0,0,0,0,0,0,0,0,0,0]
                ],
                'f_orgasm' => [
                    'label' => auth()->user()->wife . "'s Orgasms",
                    'data' => [0,0,0,0,0,0,0,0,0,0,0,0,0]
                ],
                'sex' => [
                    'label' => "Had Sex",
                    'data' => [0,0,0,0,0,0,0,0,0,0,0,0,0]
                ],
                'quality_time' => [
                    'label' => "Shared Quality Time",
                    'data' => [0,0,0,0,0,0,0,0,0,0,0,0,0]
                ],
                'sex' => [
                    'label' => "Had Date",
                    'data' => [0,0,0,0,0,0,0,0,0,0,0,0,0]
                ],
            ]
        ];
        $goals = Goal::
            where('account_id', auth()->user()->account_id)
            ->where('date', '>', date('Y-m-d', strtotime('1 year ago')))
            ->where('sex', true)
            ->orWhere('m_orgasm', true)
            ->orWhere('f_orgasm', true)
            ->orderBy('date', 'ASC')
            ->get();
        for ($x = 12; $x >- 1; $x --) {
            $year = date('Y', strtotime($x . ' months ago'));
            $month = date('M', strtotime($x . ' months ago'));
            $data['labels'][] = $month;
            foreach ($goals as $goal) {
                if (date('M', strtotime($goal->date)) == $month && date('Y', strtotime($goal->date)) == $year) {
                    if ($goal->sex) {
                        $data['datasets']['sex']['data'][$x] ++;
                    }
                    if ($goal->m_orgasm) {
                        $data['datasets']['m_orgasm']['data'][$x] ++;
                    }
                    if ($goal->f_orgasm) {
                        $data['datasets']['f_orgasm']['data'][$x] ++;
                    }
                    if ($goal->quality_time) {
                        $data['datasets']['quality_time']['data'][$x] ++;
                    }
                    if ($goal->romantic_date) {
                        $data['datasets']['romantic_date']['data'][$x] ++;
                    }
                }
            }
        }
        $data['datasets']['m_orgasm']['data'] = array_reverse($data['datasets']['m_orgasm']['data']);
        $data['datasets']['f_orgasm']['data'] = array_reverse($data['datasets']['f_orgasm']['data']);
        $data['datasets']['sex']['data'] = array_reverse($data['datasets']['sex']['data']);
        $data['datasets']['quality_time']['data'] = array_reverse($data['datasets']['quality_time']['data']);
        $data['datasets']['romantic_date']['data'] = array_reverse($data['datasets']['romantic_date']['data']);
        return $data;
    }
}
