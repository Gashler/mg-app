<?php

namespace App\Repositories;

use Log;

use \Poker;

use App\Repositories\CommonCrud;

class PokerRepository extends CommonCrud
{
    /**
     * Create a new game
     *
     * @return Poker
     */
    public function create()
    {
        // define variables
        if (auth()->user()->gender == 'm') {
            $m_money = auth()->user()->money;
            $f_money = auth()->user()->spouse->money;
            $m_ready = 1;
            $f_ready = 0;
        } else {
            $f_money = auth()->user()->money;
            $m_money = auth()->user()->spouse->money;
            $m_ready = 0;
            $f_ready = 1;
        }
        if (auth()->user()->money <= 0 || auth()->user()->spouse->money <= 0) {
            $noAnte = true;
        } else {
            $noAnte = false;
        }
        $turn = rand(0, 1) ? 'm' : 'f';
        $data = [
            'amount' => 0,
            'ante' => 5,
            'anted' => false,
            'articlesPerGame' => 4,
            'called' => false,
            'cardHeight' => 210,
            'cardWidth' => 150,
            'f_hand' => null,
            'f_money' => $f_money,
            'f_ready' => $f_ready,
            'f_status' => null,
            'f_strip' => 0,
            'f_updated_at' => 0,
            'firstBet' => true,
            'firstGame' => true,
            'stripThreshold' => 125,
            'm_hand' => null,
            'm_money' => $m_money,
            'm_ready' => $m_ready,
            'm_status' => null,
            'm_strip' => 0,
            'm_updated_at' => 0,
            'newGame' => true,
            'noAnte' => $noAnte,
            'phase' => 0,
            'possiblenumberOfArticlesPerGame' => 0,
            'potOld' => 0,
            'pot' => 0,
            'raiseTotal' => 0,
            'recommendedAmount' => 0,
            'turn' => $turn,
            'updateInterval' => 2500,
            'updates' => null
        ];
        $data = $this->applyTimestamps($data);
        $game = Poker::create([
            'f_data' => $data,
            'm_data' => $data
        ]);
        auth()->user()->account->poker()->save($game);
        $game->data = $this->simplifyData($game->f_data);
        unset($game->f_data, $game->m_data);
        return $game;
    }

    // apply timestamps
    public function applyTimestamps(array $updates)
    {
        $data = [];
        foreach ($updates as $key => $value) {
            $data[$key] = [
                'updated_at' => microtime(true),
                'value' => $value
            ];
        }
        return $data;
    }

    // get spouse's updates
    public function getSpouseUpdates(string $spouse_updated_at)
    {
        $updates = [];
        $spouseGenderPrefix = auth()->user()->spouse->gender . '_';
        $poker = Poker::where('account_id', auth()->user()->account->id)->first();
        // Log::info('line 100, $poker[' . $spouseGenderPrefix . '"data"] = ', $poker[$spouseGenderPrefix . 'data']);
        foreach ($poker[$spouseGenderPrefix . 'data'] as $key => $value) {
            if ($value['updated_at'] >= $spouse_updated_at) {
                $updates[$key] = $value['value'];
            }
        }
        return $updates;
    }

    // simplify data
    public function simplifyData(array $data)
    {
        $simplifiedData = [];
        foreach ($data as $key => $set) {
            $simplifiedData[$key] = $set['value'];
        }
        return $simplifiedData;
    }
}
