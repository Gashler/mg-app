<?php

namespace App\Repositories;

use \Account;
use \Config;
use \Role;
use \User;
use \Log;
use \DB;

use Carbon\Carbon;
use App\Repositories\OauthLoginRepository;
use App\Services\GetresponseService;

class CommonCrud
{
    /**
    * Find record
    */
    public function find(int $id, array $with = [])
    {
        return ucfirst($this->model)::with($with)->find($id)->first();
    }

    /**
    * Find record
    */
    public function where(array $params, int $limit = null, array $with = [])
    {
        if (!$limit) {
            $take = 'find';
            $method = 'get';
        } else {
            $take = 'take';
            $method = 'first';
        }
        return ucfirst($this->model)::with($with)->where($params)->$take($limit)->$method();
    }

    /**
    * Update records
    */
    public function update(int $id, array $params)
    {
        ucfirst($this->model)::find($id)->update($params);
    }
}
