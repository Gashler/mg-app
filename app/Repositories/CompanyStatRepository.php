<?php

namespace App\Repositories;

use Auth;
use Stat;
use Carbon\Carbon;
use App\Repositories\CommonCrud;

class CompanyStatRepository extends CommonCrud
{
    // generate chart data
    public function chartData($params = [])
    {
        // determine date range
        if (!isset($params['start_date'])) {
            $params['start_date'] = date('Y-m-d h:i:s', strtotime('30 days ago'));
        }
        if (!isset($params['end_date'])) {
            $params['end_date'] = date('Y-m-d h:i:s');
        }
        $start_date = new Carbon($params['start_date']);
        $end_date = new Carbon($params['end_date']);
        $range = $start_date->diff($end_date)->days;

        // define data sets
        $data = [
            'labels' => [],
            'datasets' => [
                'subscribers' => [
                    'key' => "subscribers",
                    'label' => "Subscribers"
                ],
                'verified_users' => [
                    'key' => "verified_users",
                    'label' => "Verified Users"
                ],
                'unverified_users' => [
                    'key' => "unverified_users",
                    'label' => "Unverified Users"
                ],
                'verification_rates' => [
                    'key' => "verification_rates",
                    'label' => "Verification Rate"
                ]
            ]
        ];
        foreach ($data['datasets'] as $key => $value) {
            for ($x = 1; $x <= $range; $x ++) {
                $data['datasets'][$key]['data'][] = 0;
            }
        }

        // define blacklists (for testing)
        $emailBlacklist = [
            'americanknight@gmail.com',
            'pablo.deguablo@gmail.com',
            'sappytree@gmail.com',
            'teresa.gashler@gmail.com',
            '25dollarportraits@gmail.com',
            '25dollarheadshots@gmail.com',
            'deadlyparties@gmail.com',
            'marriedgames@gmail.com',
            'stockmusicking@gmail.com',
            'surprise-vacation@gmail.com'
        ];
        $cardBlacklist = [
            [
                'card_brand' => 'Visa',
                'card_last_four' => '3252'
            ],
            [
                'card_brand' => 'Visa',
                'card_last_four' => '3255'
            ],
            [
                'card_brand' => 'Visa',
                'card_last_four' => '3217'
            ],
            [
                'card_brand' => 'Visa',
                'card_last_four' => '0896'
            ]
        ];
        $countedAccounts = [];

        $users = \User::where('created_at', '>=', $start_date)->whereNotIn('email', $emailBlacklist)->get();
        for ($x = $range; $x >= 0; $x --) {
            $day = date('D', strtotime($x . ' days ago'));
            $month = date('M', strtotime($x . ' days ago'));
            $year = date('Y', strtotime($x . ' days ago'));
            $data['labels'][] = $day;
            foreach ($users as $user) {
                $array = explode(' ', $user->created_at);
                $created_at = $array[0];
                if ($created_at == date('Y-m-d', strtotime($x . ' days ago'))) {
                    // new subscribers
                    if (isset($user->account->stripe_id)
                        && !in_array($user->account_id, $countedAccounts)
                    ) {
$blacklisted = false;
if (isset($user->account->card_brand) && isset($user->account->card_last_four)) {
                        foreach ($cardBlacklist as $card) {
                            if ($user->account->card_brand == $card['card_brand']
                                && $user->account->card_last_four == $card['card_last_four']
                            ) {
                                $blacklisted = true;
                                break;
                            }
                        }
}
                        if (!$blacklisted) {
                            $data['datasets']['subscribers']['data'][$x] ++;
                            $countedAccounts[] = $user->account_id;
                        }
                    }

                    // verified users
                    if ($user->verified == 1) {
                        $data['datasets']['verified_users']['data'][$x] ++;
                    } else {
                        $data['datasets']['unverified_users']['data'][$x] ++;
                    }
                }
            }
        }

        // determine verification rates
        foreach ($data['datasets']['verified_users']['data'] as $verified_index => $verified) {
            foreach ($data['datasets']['unverified_users']['data'] as $unverified_index => $unverified) {
                if ($verified_index == $unverified_index) {
                    $total = $verified + $unverified;
                    if ($verified > 0 && $total > 0) {
                        $rate = round(($verified / $total) * 100);
                        $data['datasets']['verification_rates']['data'][$verified_index] = $rate;
                    }
                }
            }
        }

        $data['datasets']['subscribers']['data'] = array_reverse($data['datasets']['subscribers']['data']);
        $data['datasets']['verified_users']['data'] = array_reverse($data['datasets']['verified_users']['data']);
        $data['datasets']['unverified_users']['data'] = array_reverse($data['datasets']['unverified_users']['data']);
        $data['datasets']['verification_rates']['data'] = array_reverse($data['datasets']['verification_rates']['data']);
        return $data;
    }
}
