<?php

namespace Tests\Unit;

use App\Services\MailService;
use Tests\TestCase;

class MailServiceTest extends TestCase
{
    protected $service;

    /**
     * Run before tests
     *
     * @return void
     */
    protected function setUp(): void
    {
        @session_start();
        parent::setUp();
        $this->service = new MailService();
    }

    /**
     * Assertions on the basic instantiation of the MailService
     *
     * @return void
     */
    public function testMailServiceInstance(): void
    {
        $this->assertInstanceOf(MailService::class, $this->service, 'Expect service to be instance of App\Services\MailService');
    }

    /**
     * Assertions on the response returned by the send method of the MailService
     * class
     *
     * @return void
     */
    public function testSend(): void
    {
        $user = \User::where('email', 'americanknight@gmail.com')->first();
        $response = $this->service->send('account-upgraded', $user);
        $this->assertFalse($response['error'], 'Expect MailService@send to not return an error');
    }
}
