{{ Form::open(array('url' => 'foo/bar', 'files' => true)) }}
    <div class="form-group">
        <input type="file" id="files" name="media[]" multiple="true" ng-model="files">
        <!-- <small>Max Media Size: 1M</small> -->
    </div>

    <div class="form-group">
    	<button ng-if="!loading_media" class="btn btn-default" id="insertImage" ng-click="uploadMedia()" type="button"><i class="flaticon-upload"></i> Upload</button>
        <img src="/img/loading.gif" ng-if="loading_media">
    </div>
{{ Form::close() }}
