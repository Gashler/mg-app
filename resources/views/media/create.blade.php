@extends('layouts.default')
@section('content')
<div class="create">
	<div class="row">
		<div class="col col-md-12">
			@include('_helpers.breadcrumbs')
		    <h1 class="no-top">Upload Media</h1>
		</div>
	</div>
	<div class="row">
		<div class="col col-xl-3 col-lg-4 col-md-6 col-sm-6">
		    {{ Form::open(array('url' => 'media', 'files' => true)) }}
			    
                <div class="form-group">
                    <input type="file" name="media">
                    <!-- <small>Max Media Size: 1M</small> -->
                </div>
                
                <div class="form-group">
                	{{ Form::label('title', 'Title') }}
<input type="text" ng-model="obj.title" class="form-control" ng-blur="update()">
                </div>
                
                <div class="form-group">
                	{{ Form::label('description', 'Description') }}
<textarea ng-model="obj.description" class="form-control" ng-blur="update()"></textarea>
                </div>
                
                @if (Auth::user()->hasRole(['Admin', 'Editor']))
	                <div class="form-group">
	                	<label>
	                		{{ Form::checkbox('members') }} Share with Members
	                	</label>
	                </div>
	            @endif
		
			    {{ Form::submit('Upload Media', array('class' => 'btn btn-primary')) }}
	
		    {{ Form::close() }}
	    </div>
	</div>
</div>
@stop