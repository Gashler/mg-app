@extends('layouts.default')
@section('content')
<div class="show">
	<div class="row">
		<div class="col-md-12">
			@include('_helpers.breadcrumbs')
			<h1 class="no-top">Media Details</h1>
			@if (Auth::user()->hasRole(['Admin', 'Editor']))
				<div class="page-actions">
				    <div class="btn-group" id="record-options">
					    <a class="btn btn-default" href="{{ url('media/'.$media->id .'/edit') }}" title="Edit"><i class="flaticon-pencil"></i></a>
					    @if ($media->disabled == 0)
						    {{ Form::open(array('url' => 'media/disable', 'method' => 'DISABLE')) }}
						    	<input type="hidden" name="ids[]" value="{{ $media->id }}">
						    	<button class="btn btn-default active" title="Currently enabled. Click to disable.">
						    		<i class="flaticon-eye-female"></i>
						    	</button>
						    {{ Form::close() }}
						@else
						    {{ Form::open(array('url' => 'media/enable', 'method' => 'ENABLE')) }}
						    	<input type="hidden" name="ids[]" value="{{ $media->id }}">
						    	<button class="btn btn-default" title="Currently disabled. Click to enable.">
						    		<i class="flaticon-eye-female"></i>
						    	</button>
						    {{ Form::close() }}
						@endif
					    {{ Form::open(array('url' => 'media/' . $media->id, 'method' => 'DELETE', 'onsubmit' => 'return confirm("Are you sure you want to delete this media? This cannot be undone.");')) }}
					    	<button class="btn btn-default" title="Delete">
					    		<i class="flaticon-trash" title="Delete"></i>
					    	</button>
					    {{ Form::close() }}
					</div>
				</div>
			@endif
			@if (Auth::user()->hasRole(['Member']))
			    <div class="btn-group" id="record-options-rep">
				    <a class="btn btn-default" href="{{ url('media/'.$media->id .'/edit') }}" title="Edit"><i class="flaticon-pencil"></i></a>
				    {{ Form::open(array('url' => 'media/' . $media->id, 'method' => 'DELETE', 'onsubmit' => 'return confirm("Are you sure you want to delete this media? This cannot be undone.");')) }}
				    	<button class="btn btn-default" title="Delete">
				    		<i class="flaticon-trash" title="Delete"></i>
				    	</button>
				    {{ Form::close() }}
				</div>
			@endif
		</div><!-- col -->
	</div><!-- row -->
	<div class="row">
		@if ($media->type == 'Image')
			<div class="col col-md-6 full-image-container">
				<a href="/uploads/{{ $media->url }}">
					<img src="/uploads/{{ $media->url }}" class="full-image">
				</a>
			</div>
		@endif
		<div class="col col-md-6">
		    <table class="table">

		        <tr>
		            <th>Title:</th>
		            <td>{{ $media->title }}</td>
		        </tr>
		        
		        @if ($media->description != '')
			        <tr>
			            <th>Description:</th>
			            <td>{{ $media->description }}</td>
			        </tr>
			    @endif
		        
		        <tr>
		            <th>Type:</th>
		            <td>{{ $media->type }}</td>
		        </tr>
		        
		        <tr>
		            <th>URL:</th>
		            <td>{{ url() }}/uploads/{{ $media->url }}</td>
		        </tr>
		        
		        <tr>
		            <th>Owner:</th>
		            <td><a href='/users/{{ $media->user_id }}'>{{ $media->owner }}</a></td>
		        </tr>
		        
		        @if (Auth::user()->hasRole(['Admin', 'Editor']) && $media->reps)
			        <tr>
			            <th>Shared with Members:</th>
			            <td><i class="flaticon-check"></i></td>
			        </tr>
		        @endif
		        
		    </table>
		</div><!-- col -->
	</div><!-- row -->
</div><!-- show -->
@stop
