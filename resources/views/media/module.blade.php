<div class="form-group" ng-controller="MediaController">
    {{ Form::label('media', 'Media') }}
    <ul class="list-group" ng-if="obj.medias.length > 0">
        <li class="list-group-item" ng-repeat="media in obj.medias">
            <img if="media.type == 'Image'" ng-src="@{{media.xxs}}">
            <span if="media.type !== 'Image'" ng-bind="media.title"></span>
            <i class="flaticon-x pull-right" ng-click="detach('media', media.id, $index)"></i>
        </li>
    </ul>
    <div class="btn-group">
        <button type="button" class="btn btn-default" ng-click="mediaForm()"><i class="flaticon-upload"></i> Upload</button>
        <button type="button" class="btn btn-default" ng-click="mediaLibrary()"><i class="flaticon-tiles"></i> Library</button>
    </div>

    <!-- upload media form -->
    <div id="mediaForm" class="popup">
        <div class="panel panel-primary">
            {{ Form::open(array('url' => 'foo/bar', 'files' => true)) }}
                <div class="panel-heading">
                    <h3 class="panel-title">Upload Media<span class="x pull-right" ng-click="togglePopup('#mediaForm')">&times;</span></h3>
                </div>
                <div class="panel-body">

                    <div class="form-group">
                        <input type="file" id="files" name="media[]" multiple="true" ng-model="files">
                        <!-- <small>Max Media Size: 1M</small> -->
                    </div>

                </div><!-- panel-body -->
                <div class="panel-footer">
                    <button type="button" class="btn btn-default" ng-click="togglePopup('#mediaForm')">Close</button>
                    <button ng-if="!loading_media" class="btn btn-primary" id="insertImage" ng-click="uploadMedia()" type="button"><i class="flaticon-upload"></i> Upload</button>
                    <img src="/img/loading.gif" ng-if="loading_media">
                </div>
            {{ Form::close() }}
        </div><!-- panel -->
    </div><!-- popup -->

    <!-- media library -->
    <div id="mediaLibrary" class="popup">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Media Library<span class="x pull-right" ng-click="togglePopup('#mediaLibrary')">&times;</span></h3>
            </div>
            <div class="panel-body">

                <ul class="tiles">
                    <li ng-repeat="media in medias" ng-click="media.selected=!media.selected; hoverOn(media); selected = media" ng-mouseenter="hoverOn(media)" ng-mouseleave="hoverOff(media)">
                        <div class="tile-highlight" ng-class="{'selected' : media.selected }"></div>
                        <img ng-src="@{{ media.xs }}">
                    </li>
                </ul>

            </div><!-- panel-body -->
            <div class="panel-footer">

                <button type="button" class="btn btn-default" ng-click="togglePopup('#mediaLibrary')">Close</button>
                <button type="button" class="btn btn-primary" ng-click="insertMedia()">Insert</button>

            </div>
        </div><!-- panel -->
    </div><!-- popup -->

</div><!-- form-group -->
