        <div class="navbar-header">
            <a class="navbar-brand header" id="logo" href="/">@{{ site.company_name }}</a>
        </div>
        <div class="">
            <ul class="nav navbar-nav navbar-right" id="top-right-menu">
                @if (Auth::check())
                    <li class="user-money"><a class="plain"><i ng-class="'flaticon-' + user.gender + ' ' + user.gender + '-color'"></i> $@{{ user.money }}</a></li>
                    <li class="user-money"><a class="plain"><i ng-class="'flaticon-' + user.spouse.gender + ' ' + user.spouse.gender + '-color'"></i> $@{{ user.spouse.money }}</a></li>
                    <li class="dropdown" id="top-right-menu">
                        <a class="dropdown-toggle inline-block link hidable-md" id="user-menu" data-toggle="dropdown">
                            <i class="flaticon-user"></i><span class="hidable-xs">&nbsp;@{{ user.first_name }}</span>
                            <b class="caret hidable-sm"></b>
                        </a>
                        <a class="dropdown-toggle display-none" id="main-menu-toggle">
                            <i class="fa fa-bars"></i>
                        </a>
                        <ul class="dropdown-menu" class="showable-md">
                            <li><a href="/#/account"><span class="fa fa-user"></span> &nbsp;Our Account</a></li>
                            {{-- <li><a href="/#/switch-user"><span class="fa fa-sign-out"></span> &nbsp;Switch User</a></li> --}}
                            <li><a href="/logout"><span class="fa fa-sign-out"></span> &nbsp;Sign Out</a></li>
                        </ul>
                    </li>
                @else
                    <li><a href="/login"><span class="fa fa-sign-in"></span> &nbsp;Sign In</a></li>
                @endif
            </ul>
        </div><!-- /.nav-collapse -->
