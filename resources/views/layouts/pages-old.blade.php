<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
        <script src="/js/jquery.min.js"></script>
        <script src="/js/angular.min.js"></script>
        <script src="/js/app.js"></script>
        <link href="/css/pages.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="/css/flaticon/flaticon.css">
    </head>
    <body ng-app>
        @section('content')
        @show
        @include('layouts.google-analytics')
    </body>
</html>
