@if(auth()->check())
    <div ng-if="user.role_name == 'Admin' || (user.account.stripe_id && user.account.subscribed)" id="sidebar" class="admin">
        <div class="list-group main-menu" id="main-menu-back" style="display:none;">
            <a href="javascript:void(0)" class="list-group-item">
                <i class="fa fa-mail-reply"></i>
            </a>
        </div>
        <div class="list-group main-menu" id="main-menu-meta">
            <a data-category="dashboard" title="Dashboard" id="dashboard" href="/#/dashboard" class="list-group-item"><i class="fa fa-dashboard"></i> <span class="text">Dashboard</span></a>
            <a data-category="conversation" href="javascript:void(0)" data-href="/#/conversation" class='list-group-item'>
                <i class="flaticon-comment"></i> <span class="text">Conversation</span>
            </a>
            <a data-category="game" href="javascript:void(0)" data-href="/#/game" class='list-group-item'>
                <i class="flaticon-die"></i> <span class="text">Games</span>
            </a>
            {{-- <a data-category="media" href="javascript:void(0)" data-href="/#/media" class='list-group-item'>
                <i class="flaticon-tiles"></i> <span class="text">Media</span>
            </a> --}}
            <a data-category="ideas" href="javascript:void(0)" data-href="/#/ideas" class="list-group-item">
                <i class="flaticon-idea"></i> <span class="text">Ideas</span>
            </a>
            <a data-category="role-playing" href="javascript:void(0)" data-href="/#/roleplays" class="list-group-item">
                <i class="flaticon-mask"></i> <span class="text">Role-playing</span>
            </a>
            <a data-category="services" href="javascript:void(0)" data-href="/#/services" class='list-group-item'>
                <i class="flaticon-bills"></i> <span class="text">Services</span>
            </a>
            @if(auth()->user()->hasRole(['Admin']))
                <a data-category="content" href="javascript:void(0)" data-href="/#/users" class='list-group-item'>
                    <i class="fa fa-list-ul"></i> <span class="text">Content</span>
                </a>
                <a data-category="settings" href="javascript:void(0)" data-href="/#/users" class='list-group-item'>
                    <i class="flaticon-cog"></i> <span class="text">Settings</span>
                </a>
                <a data-category="users" href="javascript:void(0)" data-href="/#/users" class='list-group-item'>
                    <i class="flaticon-user"></i> <span class="text">Users</span>
                </a>
            @endif
        </div>
        <div class="list-group main-menu" id="main-menu" style="display:none;">
            <a data-category="game" href="/#/game/dice" data-href="/#/game/dice" class="list-group-item">
                <span class="text">Love Dice</span>
            </a>
            <a data-category="game" href="/#/game/lovers-lane" data-href="/#/game/lovers-lane" class="list-group-item">
                <span class="text">Lovers Lane</span>
            </a>
            <a data-category="game" href="/#/game/strip-poker" data-href="/#/game/strip-poker" class="list-group-item">
                <span class="text">Strip Poker</span>
            </a>
            @if ((auth()->check())&&(auth()->user()->hasRole(['Admin','Editor','Member'])))
                <a href="javascript:void(0)" data-href="/#/events" class='list-group-item' data-toggle="popover" data-html="true" data-content="
                    <a href='/#/events'><i class='flaticon-plus'></i> Upcoming Events</a>
                    <a href='/#/past-events'><i class='flaticon-back'></i> Past Events</a>
                    <a href='/#/events/create'><i class='flaticon-plus'></i> New Event</a>
                ">
                    <span class="text">Calendar</span>
                </a>
            @endif
            @if ((auth()->check())&&(auth()->user()->hasRole(['Admin','Editor','Member'])))
                {{-- <a data-category="media" href="javascript:void(0)" data-href="/#/media" class='list-group-item' data-toggle="popover" data-content="
                    @if ((auth()->check())&&(auth()->user()->hasRole(['Member'])))
                        <a href='/#/media/user/{{ auth()->user()->id }}'><i class='flaticon-user'></i> Media Gallery</a>
                    @endif
                    @if ((auth()->check())&&(auth()->user()->hasRole(['Admin','Editor'])))
                        <a href='/#/media/user/0'><i class='flaticon-tiles'></i> Company Media</a>
                        <a href='/#/media-shared-with-reps'><i class='flaticon-tiles'></i> Media Shared with Members</a>
                        <a href='/#/media-reps'><i class='flaticon-users'></i> Media Uploaded by Members</a>
                    @endif
                    @if ((auth()->check())&&(auth()->user()->hasRole(['Member'])))
                        <a href='/#/media'><i class='flaticon-tiles'></i> Media Library</a>
                    @endif
                    <a href='/#/media/create'><i class='flaticon-plus'></i> Upload Media</a>
                ">
                    <span class="text">Media Gallery</span>
                </a>
                <a data-category="media" href="javascript:void(0)" data-href="/#/media" class='list-group-item' data-toggle="popover" data-content="
                    @if ((auth()->check())&&(auth()->user()->hasRole(['Member'])))
                        <a href='/#/media/user/{{ auth()->user()->id }}'><i class='flaticon-user'></i> Media Gallery</a>
                    @endif
                    @if ((auth()->check())&&(auth()->user()->hasRole(['Admin','Editor'])))
                        <a href='/#/media/user/0'><i class='flaticon-tiles'></i> Company Media</a>
                        <a href='/#/media-shared-with-reps'><i class='flaticon-tiles'></i> Media Shared with Members</a>
                        <a href='/#/media-reps'><i class='flaticon-users'></i> Media Uploaded by Members</a>
                    @endif
                    @if ((auth()->check())&&(auth()->user()->hasRole(['Member'])))
                        <a href='/#/media'><i class='flaticon-tiles'></i> Media Library</a>
                    @endif
                    <a href='/#/media/create'><i class='flaticon-plus'></i> Upload Media</a>
                ">
                    <span class="text">Resources</span>
                </a> --}}
            @endif
            @if ((auth()->check())&&(auth()->user()->hasRole(['Admin','Editor'])))
                <a href="javascript:void(0)" data-href="/#/pages" class='list-group-item' data-toggle="popover" data-html="true" data-content="
                    <a href='/#/pages'><i class='fa fa-file'></i> All Pages</a>
                    <a href='/#/pages/create'><i class='flaticon-plus'></i> New Page</a>
                ">
                    <span class="text">Pages</span>
                </a>
            @endif
            @if ((auth()->check())&&(auth()->user()->hasRole(['Admin','Editor','Member'])))
                <a href="javascript:void(0)" data-href="/#/entries" class='list-group-item' data-toggle="popover" data-html="true" data-content="
                    @if (auth()->user()->hasRole(['Admin', 'Editor']))
                        <a target='_blank' href='/#/public-entries'><i class='fa fa-globe'></i> Public Entries Page</a>
                    @endif
                    <a href='/#/entries/{{ auth()->user()->id }}'><i class='fa fa-thumb-tack'></i> My Entries</a>
                    <a href='/#/entries'><i class='fa fa-thumb-tack'></i> Anonymous Entries</a>
                    @if ((auth()->check())&&(auth()->user()->hasRole(['Admin','Editor','Member'])))
                        <a href='/#/entries/create'><i class='flaticon-plus'></i> New Entry</a>
                    @endif
                ">
                    <span class="text">Entries</span>
                </a>
            @endif
            @if ((auth()->check())&&(auth()->user()->hasRole(['Admin','Editor','Member','Trial','Subscriber'])))
                <a href="javascript:void(0)" data-href="/#/posts" class='list-group-item' data-toggle="popover" data-html="true" data-content="
                    @if (auth()->user()->hasRole(['Member', 'Trial', 'Subscriber']))
                        <a href='/#/posts/{{ auth()->user()->id }}'><i class='fa fa-thumb-tack'></i> All Articles</a>
                    @endif
                    @if (auth()->user()->hasRole(['Admin', 'Editor']))
                        <a target='_blank' href='/#/public-posts'><i class='fa fa-globe'></i> Public Posts Page</a>
                        <a href='/#/posts/{{ auth()->user()->id }}'><i class='fa fa-thumb-tack'></i> My Posts</a>
                        <a href='/#/posts/create'><i class='flaticon-plus'></i> New Post</a>
                    @endif
                ">
                    @if (auth()->user()->hasRole(['Admin', 'Editor']))
                        <span class="text">Posts</span>
                    @else
                        <span class="text">Articles</span>
                    @endif
                </a>
            @endif
            @if ((auth()->check())&&(auth()->user()->hasRole(['Admin','Editor','Member'])))
                {{-- <a data-category="services" href="javascript:void(0)" data-href="/#/services" class='list-group-item' data-toggle="popover" data-content="
                    <a href='/#/services'><i class='flaticon-bills'></i> All Services</a>
                    <a href='/#/serviceCategories'><i class='flaticon-bills'></i> All Service Services</a>
                    <a href='/#/services/create'><i class='flaticon-plus'></i> New Service</a>
                    <a href='/#/serviceCategories/create'><i class='flaticon-plus'></i> New Service Category</a>
                ">
                    <span class="text">{{ auth()->user()->husband }}'s Services</span>
                </a> --}}
                <a data-category="role-playing" href="/#/role-playing/generator" class="list-group-item">
                    <span class="text">Generator</span>
                </a>
                <a data-category="role-playing" href="/#/role-playing/outlines" class="list-group-item">
                    <span class="text">Outlines</span>
                </a>
                <a data-category="role-playing" href="/#/role-playing/scripts" class="list-group-item">
                    <span class="text">Scripts</span>
                </a>
                <a data-category="ideas" href="javascript:void(0)" class='list-group-item' data-toggle="popover" data-html="true" data-content="
                    <a href='/#/posts/1'><i class='flaticon-sex'></i> Sex Advice</a>
                    <a href='/#/posts/3'><i class='flaticon-die'></i> Game Ideas</a>
                    <a href='/#/posts/2'><i class='flaticon-heart'></i> Marriage Makeover</a>
                    <a href='/#/posts/4'><i class='flaticon-calendar'></i> Dating Ideas</a>
                ">
                    <span class="text">Inspiration</span>
                </a>
                <a data-category="ideas" href="/#/ideas/foreplay-generator" class="list-group-item">
                    <span class="text">Foreplay Generator</span>
                </a>
                <a data-category="ideas" href="/#/ideas/sex-positions" class="list-group-item">
                    <span class="text">Sex Positions</span>
                </a>
                <a data-category="conversation" href="/#/conversation/conversation-starters" class="list-group-item">
                    <span class="text">Conversation Starters</span>
                </a>
                <a data-category="conversation" href="javascript:void(0)" class='list-group-item' data-toggle="popover" data-html="true" data-content="
                    <a href='/#/surveys/My'> My Surveys</a>
                    <a href='/#/surveys/{{ auth()->user()->spouse->first_name }}'>{{ auth()->user()->spouse->first_name }}'s Surveys</a>
                ">
                    <span class="text">Surveys</span>
                </a>
                <a data-category="conversation" href="/#/conversation/entries" class="list-group-item">
                    <span class="text">Romantic Diary</span>
                </a>
                <a data-category="services" href="/#/services/favor-clock" class="list-group-item">
                    <span class="text">Favor Clock</span>
                </a>
                <a data-category="services" href="/#/services/My" class="list-group-item">
                    <span class="text">My Services</span>
                </a>
                <a data-category="services" href="/#/services/purchased/My" class="list-group-item">
                    <span class="text">Services {{ auth()->user()->spouse->first_name }} owes me</span>
                </a>
                <a data-category="services" href="/#/services/purchased/{{ auth()->user()->spouse->first_name }}" class="list-group-item">
                    <span class="text">Services I Owe {{ auth()->user()->spouse->first_name }}</span>
                </a>
                <a data-category="services" href="/#/services/{{ auth()->user()->spouse->first_name }}" class="list-group-item">
                    <span class="text">{{ auth()->user()->spouse->first_name }}'s Services</span>
                </a>
                {{-- <a data-category="role-playing" href="javascript:void(0)" class='list-group-item' data-toggle="popover" data-content="
                    <a href='/#/roleplayoutlines'><i class='flaticon-mask'></i> All Outlines</a>
                    <a href='/#/roleplayoutlines/user/{{ auth()->user()->id }}'><i class='flaticon-user'></i> My Outlines</a>
                    <a href='/#/roleplayoutlines/create'><i class='flaticon-plus'></i> Create New Outline</a>
                ">
                    <span class="text">Outlines</span>
                </a>
                <a data-category="role-playing" href="javascript:void(0)" class='list-group-item' data-toggle="popover" data-content="
                    <a href='/#/roleplays'><i class='flaticon-mask'></i> All Role-plays</a>
                    <a href='/#/roleplays/user/{{ auth()->user()->id }}'><i class='flaticon-user'></i> My Role-plays</a>
                    <a href='/#/roleplay-generator'><i class='flaticon-loading'></i> Role-play Generator</a>
                    <a href='/#/roleplays/create'><i class='flaticon-plus'></i> Create New Prompt</a>
                ">
                    <span class="text">Prompts</span>
                </a>
                <a data-category="role-playing" href="javascript:void(0)" class='list-group-item' data-toggle="popover" data-content="
                    <a href='/#/roleplays'><i class='flaticon-mask'></i> All Role-plays</a>
                    <a href='/#/roleplays/user/{{ auth()->user()->id }}'><i class='flaticon-user'></i> My Role-plays</a>
                    <a href='/#/roleplay-generator'><i class='flaticon-loading'></i> Role-play Generator</a>
                    <a href='/#/roleplays/create'><i class='flaticon-plus'></i> Create New Role-play</a>
                ">
                    <span class="text">Scripts</span>
                </a> --}}
            @endif
            @if ((auth()->check())&&(auth()->user()->hasRole(['Admin'])))
                <a data-category="content" href="javascript:void(0)" data-href="/#/action" class='list-group-item' data-toggle="popover" data-content="
                    <a href='/#/action'><i class='flaticon-lips'></i> All Actions</a>
                    <a href='/#/action/create'><i class='flaticon-plus'></i> New Action</a>
                ">
                    <i class="flaticon-lips"></i> <span class="text">Actions</span>
                </a>
                <a data-category="content" href="javascript:void(0)" data-href="/#/uvent" class='list-group-item' data-toggle="popover" data-content="
                    <a href='/#/uvent'><i class='flaticon-swing'></i> All Events</a>
                    <a href='/#/uvent/create'><i class='flaticon-plus'></i> New Event</a>
                ">
                    <i class="flaticon-swing"></i> <span class="text">Events</span>
                </a>
                <a data-category="content" href="javascript:void(0)" data-href="/#/game" class='list-group-item' data-toggle="popover" data-content="
                    <a href='/#/games'><i class='flaticon-die'></i> All Games</a>
                    <a href='/#/games/create'><i class='flaticon-plus'></i> New Game</a>
                ">
                    <i class="flaticon-die"></i> <span class="text">Games</span>
                </a>
                <a data-category="content" href="javascript:void(0)" data-href="/#/objects" class='list-group-item' data-toggle="popover" data-content="
                    <a href='/#/objects'><i class='flaticon-lipstick'></i> All Objects</a>
                    <a href='/#/objects/create'><i class='flaticon-plus'></i> New Object</a>
                ">
                    <i class="flaticon-lipstick"></i> <span class="text">Objects</span>
                </a>
                <a data-category="users" href="javascript:void(0)" data-href="/#/users" class='list-group-item' data-toggle="popover" data-content="
                    <a href='/#/users'><i class='flaticon-user'></i> All Users</a>
                    <a href='/#/users/create'><i class='flaticon-plus'></i> New User</a>
                ">
                    <i class="flaticon-user"></i> <span class="text">Users</span>
                </a>
                <a data-category="settings" href="javascript:void(0)" data-href="/#/settings" class='list-group-item' data-toggle="popover" data-content="
                    <a href='/#/settings'><i class='flaticon-cog'></i> All Settings</a>
                    <a href='/#/settings/create'><i class='flaticon-plus'></i> New Setting</a>
                ">
                    <span class="text">Settings</span>
                </a>
                <a data-category="content" href="javascript:void(0)" data-href="/#/verb" class='list-group-item' data-toggle="popover" data-content="
                    <a href='/#/verb'><i class='flaticon-lips'></i> All Verbs</a>
                    <a href='/#/verb/create'><i class='flaticon-plus'></i> New Verb</a>
                ">
                    <i class="flaticon-lips"></i> <span class="text">Verbs</span>
                </a>
            @endif
        </div>
        <div id="main-menu-mobile-user" class="main-menu-mobile">
            <li><a href="/#/profile/user/edit"><span class="fa fa-user"></span> &nbsp;{{ auth()->user()->first_name }}</a></li>
            <li><a href="/#/account"><span class="fa fa-user"></span> &nbsp;Our Account</a></li>
            <li><a href="{{ env('APP_URL') }}/logout"><span class="fa fa-sign-out"></span> &nbsp;Log Out</a></li>
        </div>
    </div>
@endif
