<!DOCTYPE html>
<html ng-app="app" ng-controller="BaseController">

<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('site.company_name') }}</title>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/bootstrap-xl.css">
    <link rel="stylesheet" href="/css/flaticon/flaticon.css">
    <link rel="stylesheet" href="/css/theme.css">
    <link rel="stylesheet" href="/packages/bootstrap-select/bootstrap-select.min.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/packages/jquery-ui/jquery-ui-1.10.4.custom.min.css">
    @yield('style')
    <script>
        // convert PHP variables to JavaScript
        var domain = '{{ Config::get("site.domain") }}';
        var path = '{{ Request::path() }}';
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.8.0/angular.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-route/1.8.0/angular-route.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-sanitize/1.8.0/angular-sanitize.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/packages/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="/packages/jquery-ui/jquery-ui-1.10.4.custom.min.js"></script>
    <script src="/js/jstz.min.js"></script>
    <script src="/js/app.js"></script>
    <script src="/js/routes.js"></script>
    <script src="/js/functions.js"></script>
    <script src="/js/controllers/datepickerController.js"></script>
    <script src="/packages/jquery-ui/timepicker.js"></script>
    <script src="/packages/dirpagination/dirPagination.js"></script>
    <script src="/packages/tinymce/tinymce.min.js"></script>
    <script src="/js/html2canvas.js"></script>
    <script src="/js/controllers/BaseController.js"></script>
    <script src="/js/controllers/RegistrationController.js"></script>
    {{-- <script src="/packages/angular-dragdrop/src/angular-dragdrop.min.js"></script> --}}
    <script src="/js/angular-drag-and-drop-lists.min.js"></script>
    <script src="https://js.stripe.com/v2/"></script>
    {{-- <script src="/js/stripe.js"></script> --}}
    <script src="/node_modules/masonry-layout/dist/masonry.pkgd.min.js"></script>
    {{-- <script src="/node_modules/chartjs/chart.js"></script> --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.bundle.min.js"></script>
    <script src="/js/parsley.js"></script>
    <script>
        Stripe.setPublishableKey("{{ env('STRIPE_KEY') }}");
    </script>
    <?php
    function addScripts($path)
    {
        // include all javascript files
        $dirIterator = new RecursiveDirectoryIterator($path);
        $iterator = new RecursiveIteratorIterator(
            $dirIterator,
            RecursiveIteratorIterator::SELF_FIRST
        );

        foreach ($iterator as $file) {
            if ($file->getExtension() == 'js') {
                $url = '/' . $file;
                echo "<script src='" . $url . "'></script>\n";
            }
        }
    }
    $paths = [
        "modules",
        "components"
    ];
    foreach ($paths as $path) {
        addScripts($path);
    }
    ?>

    @if (!Session::has('timezone'))
    <!-- get timezone -->
    {{-- <script>
                // if (localStorage['timezone'] == undefined) {
                    // get timezone
                    var tz = jstz.determine();
                    // Determines the time zone of the browser client
                    var timezone = tz.name();
                    //'Asia/Kolhata' for Indian Time.
                    $.post('/set-timezone', { timezone : timezone });

                // }
            </script> --}}
    @endif
    @show
    @section('scripts')
    @show
    @section('scripts2')
    @show
    @section('scripts3')
    @show
</head>

<body class="layout-{{ $layout }} @yield('classes')" @if(isset($registering)) ng-controller="RegistrationController" @endif>
    <div id="black-filter"></div>
    <div id="container">