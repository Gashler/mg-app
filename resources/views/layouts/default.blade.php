<?php $layout = 'default' ?>
@include('layouts.header')
<div id="header-menu" class="navbar navbar-fixed-top navbar-inverse" role="navigation">
    @include('layouts.header-menu')
</div><!-- /.navbar -->
<div id="main">
    @if (
    auth()->check()
    && auth()->user()->account->subscribed
    && !auth()->user()->disabled
    )
    @include('layouts.main-menu')
    @endif
    <div id="content-container">
        <div id="content">
            <div class="row">
                <div class="col col-md-12">
                    @include('_helpers.errors')
                    @include('_helpers.message')
                </div>
            </div>
            @section('content')
            @show
            <div ng-if="baseReady && user !== undefined" ng-view></div>
            <div class="clear"></div>
            <hr>
            <footer>
                <nav class="margin-bottom-2">
                    <a href="{{ env('HOST_URL') }}/contact?member=1">Contact</a>
                    <a href="{{ env('HOST_URL') }}/privacy?member=1">Privacy</a>
                    <a href="{{ env('HOST_URL') }}/terms?member=1">Terms</a>
                </nav>
                <p>&copy; {{Config::get('site.company_name')}} {{ date('Y') }}</p>
            </footer>
            <div class="alert-main-container">
                <div class="alert alert-main alert-@{{ message.type }}" ng-repeat="message in messages">
                    <div ng-if="message.data.length == 1">@{{ message.data[0] }}</div>
                    <ul ng-if="message.data.length > 1">
                        <li ng-repeat="submessage in message.data">@{{ submessage }}</li>
                    </ul>
                    <button class="btn btn-default pull-right" ng-click="dismissMessage($index)">Dismiss</button>
                    {{-- <div ng-if="message.conversion" ng-include="'/modules/conversion-tracking/' + message.conversion + '.html'"></div> --}}
                </div>
            </div>
            {{-- @if (auth()->check() && auth()->user()->role_name == 'Member' && (isset(auth()->user()->account->stripe_id) && auth()->user()->account->subscribed && auth()->user()->verified))
            @include('music-player')
            @endif --}}
        </div>
        @include('layouts.ad')
    </div><!-- content -->
</div>
<!--/row-->
@include('layouts.footer')