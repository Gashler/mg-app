<div id="monkey">
    {{-- Load the ad --}}
    <?php
        $ads = [];
        $email = '';
        if (auth()->check()) {
            $email = '?email=' . urlencode(auth()->user()->email);
        }
        if ($handle = opendir(base_path() . '/public/img/monkey')) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != ".." && $entry != ".DS_Store") {
                    $ads[] = $entry;
                }
            }
            closedir($handle);
        }
    ?>
    <a href="https://makeover.marriedgames.org/#/landing{{ $email }}">
        <img id="monkey-img" src="/img/monkey/{{ $ads[rand(0, count($ads) - 1)] }}" />
    </a>
</div>
{{-- Periodically repladce the ad --}}
<script>
    let monkeys = [
        @foreach ($ads as $ad)
        '{{ $ad }}',
        @endforeach
    ];
    let randomMonkey;
    let oldMonkey;
    let email;
    setInterval(function() {
        oldMonkey = randomMonkey;
        while (randomMonkey == oldMonkey) {
            randomMonkey = monkeys[Math.floor(Math.random() * Math.floor(monkeys.length))];
        }
        $('#monkey-img').attr('src', "{{ env('APP_URL') }}/img/monkey/" + randomMonkey);
    }, 15000);
</script>