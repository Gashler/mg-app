<!DOCTYPE html>
<html>
    <head>
        <title>Online Sex Games for Married Couples | MarriedGames.org</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angular.js/1.6.1/angular-route.min.js"></script>
        <script src="/js/pages.js"></script>
        <script src="/js/controllers/HomeController.js"></script>
        <link href="/css/pages.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="/css/flaticon/flaticon.css">
        <link rel="stylesheet" href="/css/font-awesome.min.css">
    </head>
    <body ng-app="app">
        @section('content')
        @show
        @include('layouts.google-analytics')
    </body>
</html>
