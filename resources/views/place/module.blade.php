<div id="placeForm" data-model="place" class="popup" ng-controller="PlaceController">
    <div class="panel panel-primary">
        <div class="panel-heading overflow-hidden">
            <h3 class="panel-title">
                <i class="flaticon-tiles"></i>
                <span ng-if="place.name == null">Edit Place</span>
                <span ng-if="place.name != null" ng-bind="place.name"></span>
                <i class="x pull-right flaticon-x" ng-click="togglePopup('#placeForm')"></i>
            </h3>
        </div>
        <div class="panel-body">

            <div class="btn-group pull-right">
                <button class="btn btn-default" ng-click="deletePlace(obj.id)"><i class="flaticon-trash"></i></button>
            </div>

            <div class="form-group">
                {{ Form::label('name', 'Name') }}
<input type="text" ng-model="obj.name" class="form-control" ng-blur="update()">
            </div>

            <div class="form-group">
                {{ Form::label('description', 'Description') }}
<textarea ng-model="obj.description" class="form-control" ng-blur="update()"></textarea>
            </div>

            <div class="form-group" ng-show="obj.options.length == 0">
                {{ Form::label('result_id', 'Result') }}
<input type="hidden" ng-model="obj.result_type">
                <select name="result_id" id="result_id" ng-model="obj.result_id" class="form-control" ng-blur="updatePlace()">
            		<option value="-1">End Game</option>
            		<option value="0">Repeat</option>
            		<option ng-show="place.id !== obj.id" value="@{{place.id}}" ng-repeat="place in game.places">@{{place.name}}</option>
                </select>
                <small ng-if="game.places.length == 1">(For more options, add more events.)</small>
            </div>

            @include('media.module')
            @include('timer.module')
            @include('character.module')
            @include('object.module')

            <button type="button" class="btn btn-default" ng-click="togglePopup('#placeForm')">Close</button>

        </div><!-- panel-body -->
    </div><!-- panel -->
</div><!-- popup -->
<script src="/js/controllers/PlaceController.js"></script>
