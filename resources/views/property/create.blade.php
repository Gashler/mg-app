@extends('layouts.default')
@section('content')
	<div class="create">
		<div class="row">
			<div class="col col-md-12">
				@include('_helpers.breadcrumbs')
			    <h1 class="no-top">New Property</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
			    {{ Form::open(array('url' => 'property')) }}

				    
				    <div class="form-group">
				        {{ Form::label('morph_type', '[formatted_property]') }}
				        {{ Form::text('morph_type', Input::old('morph_type'), array('class' => 'form-control')) }}
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('name', '[formatted_property]') }}
				        {{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('morph_id', '[formatted_property]') }}
				        {{ Form::text('morph_id', Input::old('morph_id'), array('class' => 'form-control')) }}
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('column', '[formatted_property]') }}
				        {{ Form::text('column', Input::old('column'), array('class' => 'form-control')) }}
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('value', '[formatted_property]') }}
				        {{ Form::text('value', Input::old('value'), array('class' => 'form-control')) }}
				    </div>
				    

				    {{ Form::submit('Add Property', array('class' => 'btn btn-primary')) }}

			    {{ Form::close() }}
		    </div>
		</div>
	</div>
@stop
@section('scripts')
	<script src="/js/controllers/PropertyController.js"></script>
@stop
