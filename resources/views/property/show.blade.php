@extends('layouts.default')
@section('content')
	<div class="show">
		<div class="row page-actions">
			@include('_helpers.breadcrumbs')
			<h1 class="no-top">Viewing Property</h1>
			@if (Auth::user()->hasRole(['Superadmin', 'Admin']))
			    <div class="btn-group" id="record-options">
				    <a class="btn btn-default" href="{{ url('property/'.$property->id .'/edit') }}" title="Edit"><i class="flaticon-pencil"></i></a>
				    @if ($property->disabled == 0)
					    {{ Form::open(array('url' => 'Property/disable', 'method' => 'DISABLE')) }}
					    	<input type="hidden" name="ids[]" value="{{ $property->id }}">
					    	<button class="btn btn-default active" title="Currently enabled. Click to disable.">
					    		<i class="flaticon-eye-female"></i>
					    	</button>
					    {{ Form::close() }}
					@else
					    {{ Form::open(array('url' => 'Property/enable', 'method' => 'ENABLE')) }}
					    	<input type="hidden" name="ids[]" value="{{ $property->id }}">
					    	<button class="btn btn-default" title="Currently disabled. Click to enable.">
					    		<i class="flaticon-eye-female"></i>
					    	</button>
					    {{ Form::close() }}
					@endif
				    {{ Form::open(array('url' => 'property/' . $property->id, 'method' => 'DELETE', 'onsubmit' => 'return confirm("Are you sure you want to delete this property? This cannot be undone.");')) }}
				    	<button class="btn btn-default" title="Delete">
				    		<i class="flaticon-trash" title="Delete"></i>
				    	</button>
				    {{ Form::close() }}
				</div>
			@endif
		</div><!-- row -->
		<div class="row">
			<div class="col col-xl-4 col-lg-6 col-md-8 col-sm-12">
			    <table class="table">
			        
			        <tr>
			            <th>[formatted_property]:</th>
			            <td>{{ $property->morph_type }}</td>
			        </tr>
			        
			        <tr>
			            <th>[formatted_property]:</th>
			            <td>{{ $property->name }}</td>
			        </tr>
			        
			        <tr>
			            <th>[formatted_property]:</th>
			            <td>{{ $property->morph_id }}</td>
			        </tr>
			        
			        <tr>
			            <th>[formatted_property]:</th>
			            <td>{{ $property->column }}</td>
			        </tr>
			        
			        <tr>
			            <th>[formatted_property]:</th>
			            <td>{{ $property->value }}</td>
			        </tr>
			        
			    </table>
		    </div>
		</div>
	</div>
@stop
