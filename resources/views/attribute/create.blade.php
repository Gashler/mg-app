@extends('layouts.default')
@section('content')
	<div class="create">
		<div class="row">
			<div class="col col-md-12">
				@include('_helpers.breadcrumbs')
			    <h1 class="no-top">New Attribute</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
			    {{ Form::open(array('url' => 'attribute')) }}
	
				    
				    <div class="form-group">
				        {{ Form::label('name', 'Name') }}
<input type="text" ng-model="obj.name" class="form-control" ng-blur="update()">
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('attributeable_type', 'Attributeable Type') }}
<input type="text" ng-model="obj.attributeable_type" class="form-control" ng-blur="update()">
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('value', 'Value') }}
<input type="text" ng-model="obj.value" class="form-control" ng-blur="update()">
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('attributeable_id', 'Attributeable Id') }}
<input type="text" ng-model="obj.attributeable_id" class="form-control" ng-blur="update()">
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('default', 'Default') }}
<input type="text" ng-model="obj.default" class="form-control" ng-blur="update()">
				    </div>
				    
	
				    {{ Form::submit('Add Attribute', array('class' => 'btn btn-primary')) }}
	
			    {{ Form::close() }}
		    </div>
		</div>
	</div>
@stop
