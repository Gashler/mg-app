@extends('layouts.default')
@section('content')
	@include('_helpers.breadcrumbs')
	<div class="row">
		<div class="col col-md-12">
			<h1>Send Email</h1>
			<div class="row">
		</div>
	</div>
		<div class="col col-md-6">
			{{ Form::open( array('route' => array('blast_email'))) }}
				@if (isset($leads))
<input type="hidden" ng-model="obj.leads">
				@endif
				<div class="form-group">
					{{ Form::label('subject_line','To:')}}<br>
					@foreach ($users as $user)
						<span>
							<input type='hidden' name='user_ids[]' value='{{ $user->id }}'>
							<span class="label label-default">{{ $user->husband }} {{ $user->wife }} &nbsp;<i class="flaticon-x"></i></span>
						</span>
					@endforeach
					<br>
					<small>(Users who have opted out of receiving emails will not be included.)</small>
				</div>
		
				<div class="form-group">
					{{ Form::label('subject_line','*Subject:')}}
<input type="text" ng-model="obj.subject_line" class="form-control" ng-blur="update()">
				</div>
		
				<div class="form-group">
					{{ Form::label('body','Message:')}}
<textarea ng-model="obj.body" class="form-control" ng-blur="update()"></textarea>
				</div>
		
				<div class="form-group">
					{{ Form::submit('Send Message', ['class' => 'btn btn-primary']) }}
				</div>
		
			{{ Form::close() }}
		</div>
	</div>
@stop
@section('modals')
	@include('_helpers.wysiwyg_modals')
@stop