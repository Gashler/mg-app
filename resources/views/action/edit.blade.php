@extends('layouts.default')
@section('content')
	<div class="edit">
		<div class="row">
			<div class="col col-md-12">
				@include('_helpers.breadcrumbs')
			    <h1>Edit Action</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
			    {{ Form::model($action, array('route' => array('action.update', $action->id), 'method' => 'PUT')) }}

			    @include('action.form')

			    {{ Form::submit('Update Action', array('class' => 'btn btn-primary')) }}

			    {{Form::close()}}
			</div>
		</div>
	</div>
@stop
