<div class="form-group">
    {{ Form::label('description', 'Description') }}
<textarea ng-model="obj.description" class="form-control" ng-blur="update()"></textarea>
</div>

<div class="form-group">
    {{ Form::label('gender', 'Performed by Gender') }}
    {{ Form::select('gender', [
        '' => 'Either',
        'm' => 'Husband',
        'f' => 'Wife'
    ], null, array('class' => 'form-control')) }}
</div>

<div class="form-group">
    {{ Form::label('level', 'Rating') }}
    {{ Form::select('level', [
        '1' => 'G',
        '2' => 'PG',
        '3' => 'PG-13',
        '4' => 'R',
        '5' => 'NC-17'
    ], null, array('class' => 'form-control')) }}
</div>
