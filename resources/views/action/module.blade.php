<div class="form-group">
    {{ Form::label('actions', 'Actions') }}
    <ul class="list-group" ng-if="obj.actions.length > 0">
        <li ng-repeat="action in obj.actions" class="list-group-item">
            <span ng-click="editAction(action.id)" class="link">
                @{{ action.action | limitTo: 60 }}@{{ action.action.length > 60 ? ' ...' : '' }}
            </span>
            <i class="link flaticon-x pull-right" ng-click="deleteAction(action.id, $index)"></i>
        </li>
    </ul>
    <button type="button" class="btn btn-default" ng-click="createAction()"><i class="flaticon-plus"></i> Add Action</button>
    <div id="actionForm" class="popup">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Create Action<span class="x pull-right" ng-click="togglePopup('#actionForm')">&times;</span></h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    {{ Form::label('action_type', 'Type') }}
                    {{ Form::select('action_type', [
                        'select' => 'Assign action now',
                        'random_now' => 'Assign random action now',
                        'random_then' => 'Assign random action during event'
                    ], 'select', ['class' => 'form-control', 'ng-model' => 'action.type']) }}
                </div>

                <div class="form-group">
                    {{ Form::label('action_performer', 'Performer') }}
                    <select id="action_performer" name="action_performer" class="form-control" ng-model="action.performer">
                        <option value="">Either</option>
                        <option ng-value="@{{uvent.turn}}">Performed by player</option>
                        <option ng-value="@{{uvent.turn.spouse}}">Performed by player's spouse</option>
                    </select>
                </div>

                <div ng-if="action.type == 'random_now'" class="form-group">
                    <button ng-click="getRandomAction()" class="btn btn-default">Assign Random Action</button>
                </div>

                <div ng-if="action.type == 'select'" class="form-group">
                    <button ng-click="browseActions()" class="btn btn-default">Browse Actions</button>
                    <div id="browseActions" class="popup" ng-if="actions !== 'undefined'">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Select Actions<span class="x pull-right" ng-click="togglePopup('#browseActions')">&times;</span></h3>
                            </div>
                            <div class="panel-body">
                                @include('action.select')
                                <button type="button" class="btn btn-primary" ng-click="selectActions()">Assign Actions</button>
                                <button type="button" class="btn btn-default" ng-click="togglePopup('#actionForm')">Close</button>
                            </div><!-- panel-body -->
                        </div>
                    </div><!-- popup -->
                </div>

                <button type="button" class="btn btn-default" ng-click="togglePopup('#actionForm')">Close</button>

            </div><!-- panel-body -->
        </div><!-- panel -->
    </div><!-- actionForm -->
</div>
