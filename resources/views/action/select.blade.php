<table class="table">
    <thead>
        <tr>

            <th>
            	<input type="checkbox">
            </th>

        	<th class="link" ng-click="orderByField='description'; reverseSort = !reverseSort">Description
    			<span ng-show="orderByField == 'description'">
        			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
        			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
    			</span>
    		</th>

        	<th class="link" ng-click="orderByField='performer'; reverseSort = !reverseSort">Performer
    			<span ng-show="orderByField == 'performer'">
        			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
        			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
    			</span>
    		</th>

            <th class="link" ng-click="orderByField='level'; reverseSort = !reverseSort">Rating
    			<span ng-show="orderByField == 'level'">
        			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
        			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
    			</span>
    		</th>

        </tr>
    </thead>
    <tbody>
        <tr ng-class="{ 'even' : $even, highlight : action.new == 1, semitransparent : action.disabled == 1 }" ng-repeat="action in actions | filter:search | orderBy: '-updated_at' | orderBy:orderByField:reverseSort">

            <td ng-click="checkbox()">
            	<input class="bulk-check" type="checkbox" name="ids[]" value="@{{action.id}}" ng-model="action.checked">
            </td>

            <td>
                <a href="/action/@{{action.id}}">@{{ action.action | limitTo: 120 }}@{{ action.action.length > 120 ? ' ...' : '' }}</a>
            </td>

            <td>
                <span ng-bind="action.performer"></span>
            </td>

            <td>
                <span ng-bind="action.rating"></span>
            </td>

        </tr>
        <tr dir-paginate-end></tr>
    </tbody>
</table>
