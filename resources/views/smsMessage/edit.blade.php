@extends('layouts.default')
@section('content')
<div class="edit">
	<div class="row">
		<div class="col col-md-12">
			@include('_helpers.breadcrumbs')
		    <h1>Edit smsMessage</h1>
		    {{ Form::model($smsMessage, array('route' => array('smsMessages.update', $smsMessage->id), 'method' => 'PUT')) }}
		
		    
		    <div class="form-group">
		        {{ Form::label('sender_id', 'Sender Id') }}
<input type="text" ng-model="obj.sender_id" class="form-control" ng-blur="update()">
		    </div>
		    
		    <div class="form-group">
		        {{ Form::label('recipient_id', 'Recipient Id') }}
<input type="text" ng-model="obj.recipient_id" class="form-control" ng-blur="update()">
		    </div>
		    
		    <div class="form-group">
		        {{ Form::label('body', 'Body') }}
<input type="text" ng-model="obj.body" class="form-control" ng-blur="update()">
		    </div>
		    
		    <div class="form-group">
		        {{ Form::label('disabled', 'Disabled') }}
<input type="text" ng-model="obj.disabled" class="form-control" ng-blur="update()">
		    </div>
		    
		
		    {{ Form::submit('Update SmsMessage', array('class' => 'btn btn-primary')) }}
		
		    {{Form::close()}}
		</div>
	</div>
</div>
@stop

