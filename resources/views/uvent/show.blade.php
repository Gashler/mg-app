@extends('layouts.default')
@section('content')
	<div class="show">
		<div class="row page-actions">
			@include('_helpers.breadcrumbs')
			<h1 class="no-top">Viewing Uvent</h1>
			@if (Auth::user()->hasRole(['Superadmin', 'Admin']))
			    <div class="btn-group" id="record-options">
				    <a class="btn btn-default" href="{{ url('uvent/'.$uvent->id .'/edit') }}" title="Edit"><i class="flaticon-pencil"></i></a>
				    @if ($uvent->disabled == 0)
					    {{ Form::open(array('url' => 'Uvent/disable', 'method' => 'DISABLE')) }}
					    	<input type="hidden" name="ids[]" value="{{ $uvent->id }}">
					    	<button class="btn btn-default active" title="Currently enabled. Click to disable.">
					    		<i class="flaticon-eye-female"></i>
					    	</button>
					    {{ Form::close() }}
					@else
					    {{ Form::open(array('url' => 'Uvent/enable', 'method' => 'ENABLE')) }}
					    	<input type="hidden" name="ids[]" value="{{ $uvent->id }}">
					    	<button class="btn btn-default" title="Currently disabled. Click to enable.">
					    		<i class="flaticon-eye-female"></i>
					    	</button>
					    {{ Form::close() }}
					@endif
				    {{ Form::open(array('url' => 'uvent/' . $uvent->id, 'method' => 'DELETE', 'onsubmit' => 'return confirm("Are you sure you want to delete this uvent? This cannot be undone.");')) }}
				    	<button class="btn btn-default" title="Delete">
				    		<i class="flaticon-trash" title="Delete"></i>
				    	</button>
				    {{ Form::close() }}
				</div>
			@endif
		</div><!-- row -->
		<div class="row">
			<div class="col col-xl-4 col-lg-6 col-md-8 col-sm-12">
			    <table class="table">
			        
			        <tr>
			            <th>Uventable Id:</th>
			            <td>{{ $uvent->uventable_id }}</td>
			        </tr>
			        
			        <tr>
			            <th>result Id:</th>
			            <td>{{ $uvent->result_id }}</td>
			        </tr>
			        
			        <tr>
			            <th>Content Id:</th>
			            <td>{{ $uvent->content_id }}</td>
			        </tr>
			        
			        <tr>
			            <th>Date Start:</th>
			            <td>{{ $uvent->date_start }}</td>
			        </tr>
			        
			        <tr>
			            <th>Date End:</th>
			            <td>{{ $uvent->date_end }}</td>
			        </tr>
			        
			        <tr>
			            <th>Name:</th>
			            <td>{{ $uvent->name }}</td>
			        </tr>
			        
			        <tr>
			            <th>Uventable Type:</th>
			            <td>{{ $uvent->uventable_type }}</td>
			        </tr>
			        
			        <tr>
			            <th>result Type:</th>
			            <td>{{ $uvent->result_type }}</td>
			        </tr>
			        
			        <tr>
			            <th>Spouse:</th>
			            <td>{{ $uvent->spouse }}</td>
			        </tr>
			        
			        <tr>
			            <th>Content Type:</th>
			            <td>{{ $uvent->content_type }}</td>
			        </tr>
			        
			        <tr>
			            <th>Description:</th>
			            <td>{{ $uvent->description }}</td>
			        </tr>
			        
			        <tr>
			            <th>Options:</th>
			            <td>{{ $uvent->options }}</td>
			        </tr>
			        
			        <tr>
			            <th>Timer:</th>
			            <td>{{ $uvent->timer }}</td>
			        </tr>
			        
			        <tr>
			            <th>Result:</th>
			            <td>{{ $uvent->result }}</td>
			        </tr>
			        
			        <tr>
			            <th>Lock Result To Timer:</th>
			            <td>{{ $uvent->lock_result_to_timer }}</td>
			        </tr>
			        
			        <tr>
			            <th>Generate Content:</th>
			            <td>{{ $uvent->generate_content }}</td>
			        </tr>
			        
			        <tr>
			            <th>Rand Content:</th>
			            <td>{{ $uvent->rand_content }}</td>
			        </tr>
			        
			    </table>
		    </div>
		</div>
	</div>
@stop
