@extends('layouts.default')
@section('content')
<div class="index">
    {{ Form::open(array('url' => 'Uvent/disable', 'method' => 'POST')) }}
	    <div ng-controller="UventController">
	    	<div class="page-actions">
		        <div class="row">
		            <div class="col-md-12">
		                <h1 class="no-top pull-left no-pull-xs">All Uvents</h1>
		            	<div class="pull-right hidable-xs">
		                    <div class="input-group pull-right">
		                    	<span class="input-group-addon no-width">Count</span>
		                    	<input class="form-control itemsPerPage width-auto" ng-model="pageSize" type="number" min="1">
		                    </div>
		                    <h4 class="pull-right margin-right-1">Page <span ng-bind="currentPage"></span></h4>
		            	</div>
			    	</div>
		        </div><!-- row -->
		        <div class="row">
		        	@if (Auth::user()->hasRole(['Superadmin', 'Admin']))
			    		<div class="col-md-6 col-sm-6 col-xs-12 page-actions-left">
			                <div class="pull-left">
			                    <a class="btn btn-primary pull-left margin-right-1" title="New" href="{{ url('uvent/create') }}"><i class="flaticon-plus"></i></a>
			                    <div class="pull-left">
			                        <div class="input-group">
			                            <select class="form-control selectpicker actions">
			                                <option value="Uvent/disable" selected>Disable</option>
			                                <option value="Uvent/enable">Enable</option>
			                                <option value="Uvent/delete">Delete</option>
			                            </select>
			                            <div class="input-group-btn no-width">
			                                <button class="btn btn-default applyAction" disabled>
			                                    <i class="flaticon-check"></i>
			                                </button>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			        	</div>
			        	<div class="col-md-6 col-sm-6 col-xs-12">
					@else
						<div class="col-md-12 col-sm-12 col-xs-12">
					@endif
		                <div class="pull-right">
		                    <div class="input-group">
		                        <input class="form-control ng-pristine ng-valid" placeholder="Search" name="new_tag" ng-model="search.$" onkeypress="return disableEnterKey(event)" type="text">
		                        <span class="input-group-btn no-width">
		                            <button class="btn btn-default" type="button">
		                                <i class="flaticon-search"></i>
		                            </button>
		                        </span>
		                    </div>
		                </div>
		            </div><!-- col -->
		        </div><!-- row -->
		    </div><!-- page-actions -->
	        <div class="row">
	            <div class="col col-md-12">
	                <table class="table">
	                    <thead>
	                        <tr>
	                            <th>
	                            	<input type="checkbox">
	                            </th>
                            	
                            	<th class="link" ng-click="orderByField='uventable_id'; reverseSort = !reverseSort">Uventable Id
                        			<span ng-show="orderByField == 'uventable_id'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='result_id'; reverseSort = !reverseSort">result Id
                        			<span ng-show="orderByField == 'result_id'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='content_id'; reverseSort = !reverseSort">Content Id
                        			<span ng-show="orderByField == 'content_id'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='date_start'; reverseSort = !reverseSort">Date Start
                        			<span ng-show="orderByField == 'date_start'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='date_end'; reverseSort = !reverseSort">Date End
                        			<span ng-show="orderByField == 'date_end'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='name'; reverseSort = !reverseSort">Name
                        			<span ng-show="orderByField == 'name'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='uventable_type'; reverseSort = !reverseSort">Uventable Type
                        			<span ng-show="orderByField == 'uventable_type'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='result_type'; reverseSort = !reverseSort">result Type
                        			<span ng-show="orderByField == 'result_type'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='spouse'; reverseSort = !reverseSort">Spouse
                        			<span ng-show="orderByField == 'spouse'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='content_type'; reverseSort = !reverseSort">Content Type
                        			<span ng-show="orderByField == 'content_type'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='description'; reverseSort = !reverseSort">Description
                        			<span ng-show="orderByField == 'description'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='options'; reverseSort = !reverseSort">Options
                        			<span ng-show="orderByField == 'options'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='timer'; reverseSort = !reverseSort">Timer
                        			<span ng-show="orderByField == 'timer'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='result'; reverseSort = !reverseSort">Result
                        			<span ng-show="orderByField == 'result'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='lock_result_to_timer'; reverseSort = !reverseSort">Lock Result To Timer
                        			<span ng-show="orderByField == 'lock_result_to_timer'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='generate_content'; reverseSort = !reverseSort">Generate Content
                        			<span ng-show="orderByField == 'generate_content'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='rand_content'; reverseSort = !reverseSort">Rand Content
                        			<span ng-show="orderByField == 'rand_content'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='updated_at'; reverseSort = !reverseSort">Modified
                        			<span ng-show="orderByField == 'updated_at'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        <tr ng-class="{ 'even' : $even, highlight : uvent.new == 1, semitransparent : uvent.disabled == 1 }" dir-paginate-start="uvent in uvents | filter:search | orderBy: '-updated_at' | orderBy:orderByField:reverseSort | itemsPerPage: pageSize" current-page="currentPage">
	                            <td ng-click="checkbox()">
	                            	<input class="bulk-check" type="checkbox" name="ids[]" value="@{{uvent.id}}">
	                            </td>
								
					            <td>
					                <a href="/uvent/@{{uvent.id}}"><span ng-bind="uvent.uventable_id"></span></a>
					            </td>
					            
					            <td>
					                <a href="/uvent/@{{uvent.id}}"><span ng-bind="uvent.result_id"></span></a>
					            </td>
					            
					            <td>
					                <a href="/uvent/@{{uvent.id}}"><span ng-bind="uvent.content_id"></span></a>
					            </td>
					            
					            <td>
					                <a href="/uvent/@{{uvent.id}}"><span ng-bind="uvent.date_start"></span></a>
					            </td>
					            
					            <td>
					                <a href="/uvent/@{{uvent.id}}"><span ng-bind="uvent.date_end"></span></a>
					            </td>
					            
					            <td>
					                <a href="/uvent/@{{uvent.id}}"><span ng-bind="uvent.name"></span></a>
					            </td>
					            
					            <td>
					                <a href="/uvent/@{{uvent.id}}"><span ng-bind="uvent.uventable_type"></span></a>
					            </td>
					            
					            <td>
					                <a href="/uvent/@{{uvent.id}}"><span ng-bind="uvent.result_type"></span></a>
					            </td>
					            
					            <td>
					                <a href="/uvent/@{{uvent.id}}"><span ng-bind="uvent.spouse"></span></a>
					            </td>
					            
					            <td>
					                <a href="/uvent/@{{uvent.id}}"><span ng-bind="uvent.content_type"></span></a>
					            </td>
					            
					            <td>
					                <a href="/uvent/@{{uvent.id}}"><span ng-bind="uvent.description"></span></a>
					            </td>
					            
					            <td>
					                <a href="/uvent/@{{uvent.id}}"><span ng-bind="uvent.options"></span></a>
					            </td>
					            
					            <td>
					                <a href="/uvent/@{{uvent.id}}"><span ng-bind="uvent.timer"></span></a>
					            </td>
					            
					            <td>
					                <a href="/uvent/@{{uvent.id}}"><span ng-bind="uvent.result"></span></a>
					            </td>
					            
					            <td>
					                <a href="/uvent/@{{uvent.id}}"><span ng-bind="uvent.lock_result_to_timer"></span></a>
					            </td>
					            
					            <td>
					                <a href="/uvent/@{{uvent.id}}"><span ng-bind="uvent.generate_content"></span></a>
					            </td>
					            
					            <td>
					                <a href="/uvent/@{{uvent.id}}"><span ng-bind="uvent.rand_content"></span></a>
					            </td>
					            
					            <td>
					            	<a href="/uvent/@{{uvent.id}}"><span ng-bind="uvent.updated_at"></span></a>
					            </td>
	                        </tr>
	                        <tr dir-paginate-end></tr>
	                    </tbody>
	                </table>
	                <div class="align-center">
	                	<img ng-if="loading" src="/img/loading.gif">
	                </div>
	                <div ng-controller="PaginationController" class="other-controller">
	                    <div class="text-center">
	                        <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="/packages/dirpagination/dirPagination.tpl.html"></dir-pagination-controls>
	                    </div>
	                </div>
	            </div><!-- col -->
	        </div><!-- row -->
        {{ Form::close() }}
    </div><!-- app -->
@stop
@section('scripts')
	<script>
	
		// initialize Angular
		var app = angular.module('app', ['angularUtils.directives.dirPagination']);
	
		// uvent controller
		function UventController($scope, $http) {
	
			// initialize variables
			$scope.loading = true;
	
			$http.get('/Uvent/all').success(function(uvents) {
			
				// get data
				$scope.uvents = uvents;
	
				// bulk action checkboxes
				$scope.checkbox = function() {
					var checked = false;
					$('.bulk-check').each(function() {
						if ($(this).is(":checked")) checked = true;
					});
					if (checked == true) $('.applyAction').removeAttr('disabled');
					else $('.applyAction').attr('disabled', 'disabled');
				};
				
				// disable loading image
				$scope.loading = false;
	
			});
	
			$scope.currentPage = 1;
			$scope.pageSize = 15;
	
		}
	
		// pagination controller
		function PaginationController($scope) {
			$scope.pageChangeHandler = function(num) { };
		}
	
	</script>
@stop