<div class="form-group">
    {{ Form::label('name', 'Name') }}
<input type="text" ng-model="obj.name" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('result_id', 'result Id') }}
<input type="text" ng-model="obj.result_id" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('content_id', 'Content Id') }}
<input type="text" ng-model="obj.content_id" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('date_start', 'Date Start') }}
<input type="text" ng-model="obj.date_start" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('date_end', 'Date End') }}
<input type="text" ng-model="obj.date_end" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('result_type', 'result Type') }}
<input type="text" ng-model="obj.result_type" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('spouse', 'Spouse') }}
<input type="text" ng-model="obj.spouse" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('content_type', 'Content Type') }}
<input type="text" ng-model="obj.content_type" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('description', 'Description') }}
<textarea ng-model="obj.description" class="form-control" ng-blur="update()"></textarea>
</div>

<div class="form-group">
    {{ Form::label('options', 'Options') }}
<input type="text" ng-model="obj.options" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('timer', 'Timer') }}
<input type="text" ng-model="obj.timer" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('result', 'Result') }}
<input type="text" ng-model="obj.result" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('lock_result_to_timer', 'Lock Result To Timer') }}
<input type="text" ng-model="obj.lock_result_to_timer" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('generate_content', 'Generate Content') }}
<input type="text" ng-model="obj.generate_content" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('rand_content', 'Rand Content') }}
<input type="text" ng-model="obj.rand_content" class="form-control" ng-blur="update()">
</div>