@extends('layouts.default')
@section('content')
	<div class="edit">
		<div class="row">
			<div class="col col-md-12">
				@include('_helpers.breadcrumbs')
			    <h1>Edit Uvent</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
			    {{ Form::model($uvent, array('route' => array('uvent.update', $uvent->id), 'method' => 'PUT')) }}
	
			    
			    <div class="form-group">
			        {{ Form::label('uventable_id', 'Uventable Id') }}
<input type="text" ng-model="obj.uventable_id" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('result_id', 'result Id') }}
<input type="text" ng-model="obj.result_id" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('content_id', 'Content Id') }}
<input type="text" ng-model="obj.content_id" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('date_start', 'Date Start') }}
<input type="text" ng-model="obj.date_start" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('date_end', 'Date End') }}
<input type="text" ng-model="obj.date_end" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('name', 'Name') }}
<input type="text" ng-model="obj.name" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('uventable_type', 'Uventable Type') }}
<input type="text" ng-model="obj.uventable_type" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('result_type', 'result Type') }}
<input type="text" ng-model="obj.result_type" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('spouse', 'Spouse') }}
<input type="text" ng-model="obj.spouse" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('content_type', 'Content Type') }}
<input type="text" ng-model="obj.content_type" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('description', 'Description') }}
<input type="text" ng-model="obj.description" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('options', 'Options') }}
<input type="text" ng-model="obj.options" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('timer', 'Timer') }}
<input type="text" ng-model="obj.timer" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('result', 'Result') }}
<input type="text" ng-model="obj.result" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('lock_result_to_timer', 'Lock Result To Timer') }}
<input type="text" ng-model="obj.lock_result_to_timer" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('generate_content', 'Generate Content') }}
<input type="text" ng-model="obj.generate_content" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('rand_content', 'Rand Content') }}
<input type="text" ng-model="obj.rand_content" class="form-control" ng-blur="update()">
			    </div>
			    
	
			    {{ Form::submit('Update Uvent', array('class' => 'btn btn-primary')) }}
	
			    {{Form::close()}}
			</div>
		</div>
	</div>
@stop
