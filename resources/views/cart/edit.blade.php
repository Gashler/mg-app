@extends('layouts.default')
@section('content')
<div class="edit">
	<div class="row">
		<div class="col col-md-12">
			@include('_helpers.breadcrumbs')
		    <h1>Edit cart</h1>
		    {{ Form::model($cart, array('route' => array('carts.update', $cart->id), 'method' => 'PUT')) }}
		
		    
		    <div class="form-group">
		        {{ Form::label('service_id', 'Service Id') }}
<input type="text" ng-model="obj.service_id" class="form-control" ng-blur="update()">
		    </div>
		    
		    <div class="form-group">
		        {{ Form::label('disabled', 'Disabled') }}
<input type="text" ng-model="obj.disabled" class="form-control" ng-blur="update()">
		    </div>
		    
		
		    {{ Form::submit('Update Cart', array('class' => 'btn btn-primary')) }}
		
		    {{Form::close()}}
		</div>
	</div>
</div>
@stop

