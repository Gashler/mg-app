@extends('layouts.default')
@section('content')
	<div ng-app ng-controller="RoleplayGeneratorController">
		{{ Form::open(['url' => 'roleplays']) }}
			<div class="create">
				<div class="row">
					<div class="col col-md-12">
						@include('_helpers.breadcrumbs')
						<h1 class="no-top">New Role-play<img ng-if="loading" src="/img/loading.gif"></h1>
						<button type="button" class="btn btn-default" id="generateRoleplay" ng-click="getDataButton()">
							<i class="flaticon-loading"></i> Generate
						</button>
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-6">
			
						<div class="form-group">
							{{ Form::label('title', 'Title') }}
<input type="text" ng-model="obj.title" class="form-control" ng-blur="update()">
						</div>
			
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-4 col-sm-6">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h2 class="panel-title">{{ Auth::user()->husband }}'s Character</h2>
							</div>
							<div class="panel-body">
								<div class="form-group">
									{{ Form::label('his_name', 'Name') }}
									<div class="input-group">
<input type="text" ng-model="obj.his_name" class="form-control" ng-blur="update()">
										<span class="input-group-btn">
											<button class="btn btn-default refresh" type="button" ng-click="generate('his_name')">
												<i class="flaticon-loading"></i>
											</button>
										</span>
									</div>
								</div>
			
								<div class="form-group">
									{{ Form::label('his_personality', 'Personality') }}
									<div class="input-group">
<input type="text" ng-model="obj.his_personality" class="form-control" ng-blur="update()">
										<span class="input-group-btn">
											<button class="btn btn-default refresh" type="button" ng-click="generate('his_personality')">
												<i class="flaticon-loading"></i>
											</button>
										</span>
									</div>
								</div>
			
								<div class="form-group">
									{{ Form::label('his_role', 'Role') }}
									<div class="input-group">
<input type="text" ng-model="obj.his_role" class="form-control" ng-blur="update()">
										<span class="input-group-btn">
											<button class="btn btn-default refresh" type="button" ng-click="generate('his_role')">
												<i class="flaticon-loading"></i>
											</button>
										</span>
									</div>
								</div>
							</div>
						</div>
			
					</div>
					<div class="col-lg-3 col-md-4 col-sm-6">
			
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h2 class="panel-title">{{ Auth::user()->wife }}'s Character</h2>
							</div>
							<div class="panel-body">
			
								<div class="form-group">
									{{ Form::label('her_name', 'Name') }}
									<div class="input-group">
<input type="text" ng-model="obj.her_name" class="form-control" ng-blur="update()">
										<span class="input-group-btn">
											<button class="btn btn-default refresh" type="button" ng-click="generate('her_name')">
												<i class="flaticon-loading"></i>
											</button>
										</span>
									</div>
								</div>
			
								<div class="form-group">
									{{ Form::label('her_personality', 'Personality') }}
									<div class="input-group">
<input type="text" ng-model="obj.her_personality" class="form-control" ng-blur="update()">
										<span class="input-group-btn">
											<button class="btn btn-default refresh" type="button" ng-click="generate('her_personality')">
												<i class="flaticon-loading"></i>
											</button>
										</span>
									</div>
								</div>
			
								<div class="form-group">
									{{ Form::label('her_role', 'Role') }}
									<div class="input-group">
<input type="text" ng-model="obj.her_role" class="form-control" ng-blur="update()">
										<span class="input-group-btn">
											<button class="btn btn-default refresh" type="button" ng-click="generate('her_role')">
												<i class="flaticon-loading"></i>
											</button>
										</span>
									</div>
								</div>
			
							</div>
						</div>
			
					</div><!-- col -->
					<div class="hidable-md col-md-6 page-decoration">
						<i class="flaticon-carnival34"></i>
					</div>
				</div>
				<div class="row">
					<div class="col-lg-3 col-md-4 col-sm-6">
			
						<div class="form-group">
							{{ Form::label('story_type', 'Story Type') }}
							<div class="input-group">
<input type="text" ng-model="obj.story_type" class="form-control" ng-blur="update()">
								<span class="input-group-btn">
									<button class="btn btn-default refresh" type="button" ng-click="generate('story_type')">
										<i class="flaticon-loading"></i>
									</button>
								</span>
							</div>
						</div>
			
						<div class="form-group">
							{{ Form::label('location', 'Setting') }}
							<div class="input-group">
<input type="text" ng-model="obj.location" class="form-control" ng-blur="update()">
								<span class="input-group-btn">
									<button class="btn btn-default refresh" type="button" ng-click="generate('location')">
										<i class="flaticon-loading"></i>
									</button>
								</span>
							</div>
						</div>
			
						<div class="form-group">
							{{ Form::label('first_line_sayer', 'First Line Sayer') }}
							<div class="input-group">
<input type="text" ng-model="obj.first_line_sayer" class="form-control" ng-blur="update()">
								<span class="input-group-btn">
									<button class="btn btn-default refresh" type="button" ng-click="generateFirstLineSayer()">
										<i class="flaticon-loading"></i>
									</button>
								</span>
							</div>
						</div>
			
						<div class="form-group">
							{{ Form::label('first_line', 'First Line') }}
							<div class="input-group">
<input type="text" ng-model="obj.first_line" class="form-control" ng-blur="update()">
								<span class="input-group-btn">
									<button class="btn btn-default refresh" type="button" ng-click="generate('first_line')">
										<i class="flaticon-loading"></i>
									</button>
								</span>
							</div>
						</div>
			
						<div class="form-group">
							{{ Form::label('description', 'Notes') }}
<textarea ng-model="obj.description" class="form-control" ng-blur="update()"></textarea>
						</div>
			
						<div class="form-group">
							{{ Form::label('public', 'Privacy') }}
							{{ Form::select('public', [
							'0' => 'Keep this roleplay private',
							'1' => 'Share anonymously with other couples'
							], null, ['class' => 'form-control selectpicker	']) }}
						</div>
			
						@if (Auth::user()->hasRole(['Admin', 'Editor']))
							<div class="form-group">
								{{ Form::label('user_id', 'User ID') }}
<input type="text" ng-model="obj.user_id" class="form-control" ng-blur="update()">
							</div>
							
							<div class="form-group">
								{{ Form::label('disabled', 'Disabled') }}
<input type="text" ng-model="obj.disabled" class="form-control" ng-blur="update()">
							</div>
						@endif
						
						{{ Form::submit('Save Role-play', ['class' => 'btn btn-primary']) }}
						
					</div>
				</div><!-- col -->
			</div><!-- row -->
		{{ Form::close() }}
	</div>
@stop
@section('scripts')
	<script src="/js/controllers/RoleplayGeneratorController.js"></script>
@stop
