@extends('layouts.default')
@section('content')
<div ng-app="app" class="index">
    {{ Form::open(array('url' => 'roleplays/delete', 'method' => 'POST')) }}
    	@if (isset($my))
<input type="hidden" ng-model="obj.my">
    	@endif
	    <div ng-controller="RoleplayController" class="my-controller">
	    	<div class="page-actions">
		        <div class="row">
		            <div class="col-md-12">
		                <h1 class="no-top pull-left no-pull-xs">{{ $title }}</h1>
		            	<div class="pull-right hidable-xs">
		                    <div class="input-group pull-right">
		                    	<span class="input-group-addon no-width">Count</span>
		                    	<input class="form-control itemsPerPage width-auto" ng-model="pageSize" type="number" min="1">
		                    </div>
		                    <h4 class="pull-right margin-right-1">Page <span ng-bind="currentPage"></span></h4>
		            	</div>
			    	</div>
		        </div><!-- row -->
		        <div class="row">
		        	@if (Auth::user()->hasRole(['Admin', 'Editor']) || isset($my))
			    		<div class="col-md-6 col-sm-6 col-xs-12 page-actions-left">
			                <div class="pull-left">
			                	<div class="btn-group pull-left margin-right-1">
			                    	<a class="btn btn-default" title="Create Role-play" href="/roleplays/create"><i class="flaticon-plus"></i> Create</a>
			                    	<a class="btn btn-default" title="Generate Role-play" href="/roleplay-generator"><i class="flaticon-loading"></i> Generate</a>
			                	</div>
			                    <div class="pull-left">
			                        <div class="input-group">
			                            <select class="form-control selectpicker actions">
			                                <option value="roleplays/delete">Delete</option>
			                            	@if (Auth::user()->hasRole(['Admin', 'Editor']))
				                                <option value="roleplays/disable" selected>Disable</option>
				                                <option value="roleplays/enable">Enable</option>
				                            @endif
			                            </select>
			                            <div class="input-group-btn no-width">
			                                <button class="btn btn-default applyAction" disabled>
			                                    <i class="flaticon-check"></i>
			                                </button>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			        	</div>
			        	<div class="col-md-6 col-sm-6 col-xs-12">
					@else
						<div class="col-md-12 col-sm-12 col-xs-12">
	                	<div class="btn-group pull-left margin-right-1">
	                    	<a class="btn btn-default" title="Create Role-play" href="/roleplays/create"><i class="flaticon-plus"></i> Create</a>
	                    	<a class="btn btn-default" title="Generate Role-play" href="/roleplay-generator"><i class="flaticon-loading"></i> Generate</a>
	                	</div>
	                @endif
		                <div class="pull-right">
		                    <div class="input-group">
		                        <input class="form-control ng-pristine ng-valid" placeholder="Search" name="new_tag" ng-model="search.$" onkeypress="return disableEnterKey(event)" type="text">
		                        <span class="input-group-btn no-width">
		                            <button class="btn btn-default" type="button">
		                                <i class="flaticon-search"></i>
		                            </button>
		                        </span>
		                    </div>
		                </div>
		            </div><!-- col -->
		        </div><!-- row -->
		    </div><!-- page-actions -->
	        <div class="row">
	            <div class="col col-md-12">
	                <table class="table">
	                    <thead>
	                        <tr>
	                        	
	                        	@if (Auth::user()->hasRole(['Admin', 'Editor']) || isset($my))
		                            <th>
		                            	<input type="checkbox">
		                            </th>
		                        @endif
                            	
                            	<th class="link" ng-click="orderByField='title'; reverseSort = !reverseSort">Title
                            		<span>
                            			<span ng-show="orderByField == 'title'">
	                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
	                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                            			</span>
                            		</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='story_type'; reverseSort = !reverseSort">Story Type
                            		<span>
                            			<span ng-show="orderByField == 'story_type'">
	                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
	                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                            			</span>
                            		</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='location'; reverseSort = !reverseSort">Setting
                            		<span>
                            			<span ng-show="orderByField == 'location'">
	                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
	                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                            			</span>
                            		</span>
                        		</th>
                        		
	                        </tr>
	                    </thead>
	                    <tbody>
	                        <tr ng-class="{ highlight : roleplay.new == 1, semitransparent : roleplay.disabled == 1 }" dir-paginate-start="roleplay in roleplays | filter:search | orderBy: '-updated_at' | orderBy:orderByField:reverseSort | itemsPerPage: pageSize" current-page="currentPage">
	                            @if (Auth::user()->hasRole(['Admin', 'Editor']) || isset($my))
		                            <td ng-click="checkbox()">
		                            	<input class="bulk-check" type="checkbox" name="ids[]" value="@include('_helpers.roleplay_id')">
		                            </td>
	                            @endif
								
					            <td>
					                <a href="/roleplays/@include('_helpers.roleplay_id')"><span ng-bind="roleplay.title"></span></a>
					            </td>
					            
					            <td>
					                <span ng-bind="roleplay.story_type"></span>
					            </td>
					            
					            <td>
					                <span ng-bind="roleplay.location"></span>
					            </td>
					            
	                        </tr>
	                        <tr dir-paginate-end></tr>
	                    </tbody>
	                </table>
	                @include('_helpers.loading')
	                <div ng-controller="OtherController" class="other-controller">
	                    <div class="text-center">
	                        <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="/packages/dirpagination/dirPagination.tpl.html"></dir-pagination-controls>
	                    </div>
	                </div>
	            </div><!-- col -->
	        </div><!-- row -->
        {{ Form::close() }}
    </div><!-- app -->
@stop
@section('scripts')
<script>

	var app = angular.module('app', ['angularUtils.directives.dirPagination']);
	
	function RoleplayController($scope, $http) {
	
		$http.get('/api/{{ $object }}').success(function(roleplays) {
			$scope.roleplays = roleplays;
			
			@include('_helpers.bulk_action_checkboxes')
			
		});
		
		$scope.currentPage = 1;
		$scope.pageSize = 15;
		$scope.meals = [];
		
		$scope.pageChangeHandler = function(num) {
			
		};
		
	}
	
	function OtherController($scope) {
		$scope.pageChangeHandler = function(num) {
		};
	}

</script>
@stop