@extends('layouts.default')
@section('style')
	<style>
		input[type="text"] { padding:0 !important; border:0 !important; box-shadow:0 !important; display:inline-block !important; }
	</style>
@stop
@section('content')
{{ Form::open(array('url' => 'roleplays')) }}
<div class="create" ng-app ng-controller="RoleplayGeneratorController">
	<div class="row">
		<div class="col col-md-12">
			@include('_helpers.breadcrumbs')
			<h1 class="no-top">Role-play Generator<img ng-if="loading" src="/img/loading.gif"></h1>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-6" ng-show="loaded">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h2 class="panel-title">
						<span ng-bind="roleplay.title"></span>
						<i class="flaticon-loading pull-right cursor-pointer" ng-click="getDataButton()"></i>
					</h2>
				</div>
				<table class="table table-striped">
					<tr>
						<th style="width:165px;">Story Type:</th>
						<td><span ng-bind="roleplay.story_type"></span></td>
					</tr>
					<tr>
						<th>Setting:</th>
						<td><span ng-bind="an(roleplay.location)"></span> <span ng-bind="roleplay.location" class="lowercase"></span></td>
					</tr>
					<tr>
						<th>{{ Auth::user()->husband }}'s Character:</th>
						<td>
							<span ng-bind="roleplay.his_name"></span>, <span ng-bind="an(roleplay.his_personality)"></span> <span ng-bind="roleplay.his_personality" class="lowercase"></span> <span ng-bind="roleplay.his_role" class="lowercase"><br>
							<strong>First Line:</strong> <span ng-bind="roleplay.first_line"></span>
						</td>
					</tr>
					<tr>
						<th>{{ Auth::user()->wife }}'s Character:</th>
						<td><span ng-bind="roleplay.her_name"></span>, <span ng-bind="an(roleplay.her_personality)"></span> <span ng-bind="roleplay.her_personality" class="lowercase"></span> <span ng-bind="roleplay.her_role" class="lowercase"></td>
					</tr>
					<tr>
						<th>First Line:</th>
						<td><span ng-bind="roleplay.first_line_sayer" class="italic"></span>: "<span ng-bind="roleplay.first_line"></span>"</td>
					</tr>
				</table>
			</div><!-- panel -->
			<input type="hidden" name="title" value="{{'{'.'{roleplay.title}'.'}'}}">
			<input type="hidden" name="his_name" value="{{'{'.'{roleplay.his_name}'.'}'}}">
			<input type="hidden" name="his_personality" value="{{'{'.'{roleplay.his_personality}'.'}'}}">
			<input type="hidden" name="his_role" value="{{'{'.'{roleplay.his_role}'.'}'}}">
			<input type="hidden" name="her_name" value="{{'{'.'{roleplay.her_name}'.'}'}}">
			<input type="hidden" name="her_personality" value="{{'{'.'{roleplay.her_personality}'.'}'}}">
			<input type="hidden" name="her_role" value="{{'{'.'{roleplay.her_role}'.'}'}}">
			<input type="hidden" name="story_type" value="{{'{'.'{roleplay.story_type}'.'}'}}">
			<input type="hidden" name="location" value="{{'{'.'{roleplay.location}'.'}'}}">
			<input type="hidden" name="first_line_sayer" value="{{'{'.'{roleplay.first_line_sayer}'.'}'}}">
			<input type="hidden" name="first_line" value="{{'{'.'{roleplay.first_line}'.'}'}}">
			<input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
			<div class="btn-group">
				<button class="btn btn-default" ng-click="edit = true">
					<input type="hidden" ng-if="edit" name="edit" value="true">
					<i class="flaticon-pencil"></i> Edit
				</button>
				<button class="btn btn-default"><i class="fa fa-circle"></i> Save</button>
			</div>
		</div><!-- col -->
		<div class="col-md-6 center" ng-show="!loaded">
			<img src="/img/loading.gif">
		</div><!-- col -->
		<div class="hidable-sm col-md-6 page-decoration">
			<i class="flaticon-carnival34"></i>
		</div>
	</div><!-- row -->
</div>
{{ Form::close() }}
@stop
@section('scripts')
	<script>
		generate = true;
	</script>
	<script src="/js/controllers/RoleplayGeneratorController.js"></script>
@stop
