@extends('layouts.default')
@section('content')
{{ Form::open(array('url' => 'roleplays')) }}
<div class="create">
	<div class="row">
		<div class="col col-md-12">
			@include('_helpers.breadcrumbs')
			<h1 class="no-top">New Roleplay</h1>
			<button type="button" class="btn btn-primary" id="generateRoleplay">
				Generate <i class="flaticon-loading"></i>
			</button>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-6">

			<div class="form-group">
				{{ Form::label('title', 'Title') }}
<input type="text" ng-model="obj.title" class="form-control" ng-blur="update()">
			</div>

		</div>
	</div>
	<div class="row">
		<div class="col-lg-3 col-md-4 col-sm-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<h2 class="panel-title">{{ Auth::user()->husband }}'s Character</h2>
				</div>
				<div class="panel-body">
					<div class="form-group">
						{{ Form::label('his_name', 'Name') }}
						<div class="input-group">
<input type="text" ng-model="obj.his_name" class="form-control" ng-blur="update()">
							<span class="input-group-btn">
								<button class="btn btn-default refresh" type="button" data-object="first-name-male">
									<i class="flaticon-loading"></i>
								</button> </span>
						</div>
					</div>

					<div class="form-group">
						{{ Form::label('his_personality', 'Personality') }}
						<div class="input-group">
<input type="text" ng-model="obj.his_personality" class="form-control" ng-blur="update()">
							<span class="input-group-btn">
								<button class="btn btn-default refresh" type="button" data-object="personality">
									<i class="flaticon-loading"></i>
								</button> </span>
						</div>
					</div>

					<div class="form-group">
						{{ Form::label('his_name', 'Role') }}
						<div class="input-group">
<input type="text" ng-model="obj.his_role" class="form-control" ng-blur="update()">
							<span class="input-group-btn">
								<button class="btn btn-default refresh" type="button" data-object="character-role-male">
									<i class="flaticon-loading"></i>
								</button> </span>
						</div>
					</div>
				</div>
			</div>

		</div>
		<div class="col-lg-3 col-md-4 col-sm-6">

			<div class="panel panel-primary">
				<div class="panel-heading">
					<h2 class="panel-title">{{ Auth::user()->wife }}'s Character</h2>
				</div>
				<div class="panel-body">

					<div class="form-group">
						{{ Form::label('her_name', 'Name') }}
						<div class="input-group">
<input type="text" ng-model="obj.her_name" class="form-control" ng-blur="update()">
							<span class="input-group-btn">
								<button class="btn btn-default refresh" type="button" data-object="first-name-female">
									<i class="flaticon-loading"></i>
								</button> </span>
						</div>
					</div>

					<div class="form-group">
						{{ Form::label('her_personality', 'Personality') }}
						<div class="input-group">
<input type="text" ng-model="obj.her_personality" class="form-control" ng-blur="update()">
							<span class="input-group-btn">
								<button class="btn btn-default refresh" type="button" data-object="personality">
									<i class="flaticon-loading"></i>
								</button> </span>
						</div>
					</div>

					<div class="form-group">
						{{ Form::label('his_name', 'Role') }}
						<div class="input-group">
<input type="text" ng-model="obj.her_role" class="form-control" ng-blur="update()">
							<span class="input-group-btn">
								<button class="btn btn-default refresh" type="button" data-object="character-role-female">
									<i class="flaticon-loading"></i>
								</button> </span>
						</div>
					</div>

				</div>
			</div>

		</div>
	</div>
	<div class="row">
		<div class="col-lg-3 col-md-4 col-sm-6">

			<div class="form-group">
				{{ Form::label('story_type', 'Story Type') }}
				<div class="input-group">
<input type="text" ng-model="obj.story_type" class="form-control" ng-blur="update()">
					<span class="input-group-btn">
						<button class="btn btn-default refresh" type="button" data-object="story-type">
							<i class="flaticon-loading"></i>
						</button> </span>
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('setting', 'Setting') }}
				<div class="input-group">
<input type="text" ng-model="obj.setting" class="form-control" ng-blur="update()">
					<span class="input-group-btn">
						<button class="btn btn-default refresh" type="button" data-object="location">
							<i class="flaticon-loading"></i>
						</button> </span>
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('first_line_sayer', 'First Line Sayer') }}
				<div class="input-group">
<input type="text" ng-model="obj.first_line_sayer" class="form-control" ng-blur="update()">
					<span class="input-group-btn">
						<button class="btn btn-default refresh" type="button" data-object="random-spouse">
							<i class="flaticon-loading"></i>
						</button> </span>
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('first_line', 'First Line') }}
				<div class="input-group">
<input type="text" ng-model="obj.first_line" class="form-control" ng-blur="update()">
					<span class="input-group-btn">
						<button class="btn btn-default refresh" type="button" data-object="first-line">
							<i class="flaticon-loading"></i>
						</button> </span>
				</div>
			</div>

			<div class="form-group">
				{{ Form::label('description', 'Notes') }}
<textarea ng-model="obj.description" class="form-control" ng-blur="update()"></textarea>
			</div>

			<div class="form-group">
				{{ Form::label('public', 'Privacy') }}
				{{ Form::select('public', [
				'0' => 'Keep this roleplay private',
				'1' => 'Share anonymously with other couples'
				], null, array('class' => 'form-control selectpicker	')) }}
			</div>

			@if (Auth::user()->hasRole(['Admin', 'Editor']))
			<div class="form-group">
				{{ Form::label('user_id', 'User ID') }}
<input type="text" ng-model="obj.user_id" class="form-control" ng-blur="update()">
			</div>

			<div class="form-group">
				{{ Form::label('disabled', 'Disabled') }}
<input type="text" ng-model="obj.disabled" class="form-control" ng-blur="update()">
			</div>
			@endif

			{{ Form::submit('Add Roleplay', array('class' => 'btn btn-primary')) }}

		</div>
	</div>
</div>
{{ Form::close() }}
@stop
@section('scripts')
<script>
	// refresh field
	$('.refresh').click(function() {
		var object = $(this).attr('data-object');
		var input = $(this).parents('.input-group').children('input');
		$(this).parents('.form-group').children('label').after('<img class="loading-inline" src="/img/loading.gif">');
		if (object == 'random-spouse') {
			data = generateFirstLineSayer();
			applyData(data);
		} else {
			$.get('/api/' + object, function(data) {
				applyData(data);
			});
		}
		function applyData(data) {
			data = data.substr(0, 1).toUpperCase() + data.substr(1);
			$(input).val(data);
			$('.loading-inline').remove();
		}
	});
	
	// generate first line sayer
	function generateFirstLineSayer() {
		var rand = Math.floor((Math.random() * 2));
		if (rand == 0) return '{{ Auth::user()->husband }}';
		else return '{{ Auth::user()->wife }}';
	}

	// generate roleplay
	$('#generateRoleplay').click(function() {
		$.get('/api/generate-roleplay', function(data) {
			$('[name="title"]').val(data.title);
			$('[name="his_name"]').val(data.his_name);
			$('[name="her_name"]').val(data.her_name);
			$('[name="his_personality"]').val(data.his_personality);
			$('[name="her_personality"]').val(data.her_personality);
			$('[name="his_role"]').val(data.his_role);
			$('[name="her_role"]').val(data.her_role);
			$('[name="setting"]').val(data.location);
			$('[name="story_type"]').val(data.story_type);
			$('[name="first_line"]').val(data.first_line);
			first_line_sayer = generateFirstLineSayer();
			$('[name="first_line_sayer"]').val(first_line_sayer);
		});
	});

</script>
@stop
