@extends('layouts.default')
@section('content')
	<div class="show">
		<div class="row page-actions">
			@include('_helpers.breadcrumbs')
			<h1 class="no-top">Viewing Option</h1>
			@if (Auth::user()->hasRole(['Superadmin', 'Admin']))
			    <div class="btn-group" id="record-options">
				    <a class="btn btn-default" href="{{ url('option/'.$option->id .'/edit') }}" title="Edit"><i class="flaticon-pencil"></i></a>
				    @if ($option->disabled == 0)
					    {{ Form::open(array('url' => 'Option/disable', 'method' => 'DISABLE')) }}
					    	<input type="hidden" name="ids[]" value="{{ $option->id }}">
					    	<button class="btn btn-default active" title="Currently enabled. Click to disable.">
					    		<i class="flaticon-eye-female"></i>
					    	</button>
					    {{ Form::close() }}
					@else
					    {{ Form::open(array('url' => 'Option/enable', 'method' => 'ENABLE')) }}
					    	<input type="hidden" name="ids[]" value="{{ $option->id }}">
					    	<button class="btn btn-default" title="Currently disabled. Click to enable.">
					    		<i class="flaticon-eye-female"></i>
					    	</button>
					    {{ Form::close() }}
					@endif
				    {{ Form::open(array('url' => 'option/' . $option->id, 'method' => 'DELETE', 'onsubmit' => 'return confirm("Are you sure you want to delete this option? This cannot be undone.");')) }}
				    	<button class="btn btn-default" title="Delete">
				    		<i class="flaticon-trash" title="Delete"></i>
				    	</button>
				    {{ Form::close() }}
				</div>
			@endif
		</div><!-- row -->
		<div class="row">
			<div class="col col-xl-4 col-lg-6 col-md-8 col-sm-12">
			    <table class="table">
			        
			        <tr>
			            <th>morph Id:</th>
			            <td>{{ $option->morph_id }}</td>
			        </tr>
			        
			        <tr>
			            <th>Result Id:</th>
			            <td>{{ $option->result_id }}</td>
			        </tr>
			        
			        <tr>
			            <th>Order:</th>
			            <td>{{ $option->order }}</td>
			        </tr>
			        
			        <tr>
			            <th>Name:</th>
			            <td>{{ $option->name }}</td>
			        </tr>
			        
			        <tr>
			            <th>morph Type:</th>
			            <td>{{ $option->morph_type }}</td>
			        </tr>
			        
			        <tr>
			            <th>Result Type:</th>
			            <td>{{ $option->result_type }}</td>
			        </tr>
			        
			        <tr>
			            <th>Description:</th>
			            <td>{{ $option->description }}</td>
			        </tr>
			        
			    </table>
		    </div>
		</div>
	</div>
@stop
