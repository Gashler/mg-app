<div class="form-group" ng-controller="OptionController">
    {{ Form::label('options', 'Options') }}
    <ul class="list-group" ng-if="obj.options.length > 0">
        <li ng-repeat="option in obj.options" class="list-group-item">
            <span ng-click="editOption(option.id)" class="link">
                <span ng-if="option.description == null || option.description == ''" ng-bind="'Option ' + ($index + 1)"></span>
                <span ng-if="option.description != null && option.description != ''" ng-bind="option.description"></span>
            </span>
            <i class="link flaticon-x pull-right" ng-click="deleteOption(option.id, $index)"></i>
        </li>
    </ul>
    <button type="button" class="btn btn-default" ng-click="createOption()"><i class="flaticon-plus"></i> Add Option</button>
    <img ng-if="loading_option" src="/img/loading.gif">
    <div id="optionForm" class="popup">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Create Option<span class="x pull-right" ng-click="togglePopup('#optionForm')">&times;</span></h3>
            </div>
            <div class="panel-body">

                <div class="form-group">
                    {{ Form::label('description', 'Description') }}
<input type="text" ng-model="obj.description" class="form-control" ng-blur="update()">
                </div>

                <div class="form-group">
                    {{ Form::label('result_id', 'Result') }}
<input type="hidden" ng-model="obj.result_type">
                    <select name="result_id" id="result_id" ng-model="option.result_id" class="form-control" ng-blur="updateOption()">
                		<option value="-1">End Game</option>
                		<option value="0">Repeat</option>
                		<option ng-show="u.id !== uvent.id" value="@{{u.id}}" ng-repeat="u in game.uvents">@{{u.name}}</option>
                    </select>
                    <small ng-if="game.uvents.length == 1">(For more options, add more events.)</small>
                </div>

                <div class="form-group">
                    {{ Form::label('effects', 'Effects') }}
                    <ul class="list-group" ng-if="option.effects.length > 0">
                        <li class="list-group-item" ng-repeat="effect in option.effects">
                            Effect @{{effect.id}}
                            <i class="link flaticon-x pull-right" ng-click="deleteEffect(effect.id, $index)"></i>
                        </li>
                    </ul>
                    <button type="button" class="btn btn-default" ng-click="createEffect()"><i class="flaticon-plus"></i> Add Effect</button>
                	<img ng-if="loading_effect" src="/img/loading.gif">
                    <div id="effectForm" class="popup">
                        <div class="panel panel-primary">
                	    	<div class="panel-heading">
                	    		<h3 class="panel-title">Create Effect<span class="x pull-right" ng-click="togglePopup('#effectForm')">&times;</span></h3>
                	    	</div>
                        	<div class="panel-body">
                        		@include('effect.form')
                        		<button type="button" class="btn btn-default" ng-click="togglePopup('#effectForm')">Close</button>
                        	</div><!-- panel-body -->
                        </div><!-- panel -->
                	</div><!-- effectForm -->
                </div>

                <button type="button" class="btn btn-default" ng-click="togglePopup('#optionForm')">Close</button>

            </div><!-- panel-body -->
        </div><!-- panel -->
    </div><!-- optionForm -->
</div>
<script src="/js/controllers/OptionController.js"></script>
