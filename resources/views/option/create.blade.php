@extends('layouts.default')
@section('content')
	<div class="create">
		<div class="row">
			<div class="col col-md-12">
				@include('_helpers.breadcrumbs')
			    <h1 class="no-top">New Option</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
			    {{ Form::open(array('url' => 'option')) }}
	
				    
				    <div class="form-group">
				        {{ Form::label('morph_id', 'morph Id') }}
<input type="text" ng-model="obj.morph_id" class="form-control" ng-blur="update()">
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('result_id', 'Result Id') }}
<input type="text" ng-model="obj.result_id" class="form-control" ng-blur="update()">
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('order', 'Order') }}
<input type="text" ng-model="obj.order" class="form-control" ng-blur="update()">
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('name', 'Name') }}
<input type="text" ng-model="obj.name" class="form-control" ng-blur="update()">
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('morph_type', 'morph Type') }}
<input type="text" ng-model="obj.morph_type" class="form-control" ng-blur="update()">
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('result_type', 'Result Type') }}
<input type="text" ng-model="obj.result_type" class="form-control" ng-blur="update()">
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('description', 'Description') }}
<input type="text" ng-model="obj.description" class="form-control" ng-blur="update()">
				    </div>
				    
	
				    {{ Form::submit('Add Option', array('class' => 'btn btn-primary')) }}
	
			    {{ Form::close() }}
		    </div>
		</div>
	</div>
@stop
