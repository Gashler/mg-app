<div class="form-group" ng-controller="TimerController">
    {{ Form::label('timers', 'Timers') }}
    <ul class="list-group" ng-if="obj.timer !== null && obj.timer !== '0'">
        <li class="list-group-item">
            <span ng-click="editTimer(obj.timer.id)" class="link">
                Count @{{ obj.timer.direction }} from @{{ obj.timer.value }}
            </span>
            <i class="link flaticon-x pull-right" ng-click="deleteTimer(obj.timer.id, $index)"></i>
        </li>
    </ul>
    <button ng-if="obj.timer == null || obj.timer == '0'" type="button" class="btn btn-default" ng-click="createTimer()"><i class="flaticon-timer"></i> Add Timer</button>
    <div id="timerForm" class="popup">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Create Timer<i class="x pull-right flaticon-x" ng-click="togglePopup('#timerForm')"></i></h3>
            </div>
            <div class="panel-body">

                <div class="form-group">
                    {{ Form::label('direction', 'Type') }}
                    {{ Form::select('direction', [
                        'down' => 'Count Down',
                        'up' => 'Count Up'
                    ], 'down', array('class' => 'form-control', 'ng-model = "timer.direction"', 'ng-blur' => 'updateTimer()')) }}
                </div>

                <div class="form-group">
                    {{ Form::label('min', 'From') }}
<input type="text" ng-model="obj.min" class="form-control" ng-blur="update()">
                </div>

                <div ng-if="timer.direction == 'up'" class="form-group">
                    {{ Form::label('max', 'To') }}
<input type="hidden" ng-model="obj.min">
                    <label>
                        <input type="checkbox" ng-model="timer.set_max">
                        Set Maximum Time
                    </label>
<input type="text" ng-model="obj.max" class="form-control" ng-blur="update()">
                </div>

                <!-- <div class="form-group">
                    {{ Form::label('result_type', 'Result Type') }}
<input type="text" ng-model="obj.result_type" class="form-control" ng-blur="update()">
                </div> -->

                <div class="form-group">
                    {{ Form::label('result_id', 'Result') }}
<input type="hidden" ng-model="obj.result_type">
                    <select name="result_id" id="result_id" ng-model="timer.result_id" class="form-control" ng-blur="updateTimer()">
                		<option value="-1">End Game</option>
                		<option value="0">Do Nothing</option>
                		<option ng-show="u.id !== uvent.id" value="@{{u.id}}" ng-repeat="u in game.uvents">@{{u.name}}</option>
                    </select>
                    <small ng-if="game.uvents.length == 1">(For more options, add more events.)</small>
                </div>

                <!-- <div class="form-group">
                    {{ Form::label('active', 'Active') }}
<input type="text" ng-model="obj.active" class="form-control" ng-blur="update()">
                </div> -->

                <button type="button" class="btn btn-default" ng-click="togglePopup('#timerForm')">Close</button>
            </div><!-- panel-body -->
        </div><!-- panel -->
    </div><!-- timerForm -->
</div>
<script src="/js/controllers/TimerController.js"></script>
