@extends('layouts.default')
@section('content')
	<div class="edit">
		<div class="row">
			<div class="col col-md-12">
				@include('_helpers.breadcrumbs')
			    <h1>Edit Timer</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
			    {{ Form::model($timer, array('route' => array('timer.update', $timer->id), 'method' => 'PUT')) }}
	
			    
			    <div class="form-group">
			        {{ Form::label('direction', 'Direction') }}
<input type="text" ng-model="obj.direction" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('morph_type', 'morph Type') }}
<input type="text" ng-model="obj.morph_type" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('result_type', 'Result Type') }}
<input type="text" ng-model="obj.result_type" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('min', 'Min') }}
<input type="text" ng-model="obj.min" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('max', 'Max') }}
<input type="text" ng-model="obj.max" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('value', 'Value') }}
<input type="text" ng-model="obj.value" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('morph_id', 'morph Id') }}
<input type="text" ng-model="obj.morph_id" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('result_id', 'Result Id') }}
<input type="text" ng-model="obj.result_id" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('active', 'Active') }}
<input type="text" ng-model="obj.active" class="form-control" ng-blur="update()">
			    </div>
			    
	
			    {{ Form::submit('Update Timer', array('class' => 'btn btn-primary')) }}
	
			    {{Form::close()}}
			</div>
		</div>
	</div>
@stop
