<div class="form-group">
    {{ Form::label('direction', 'Type') }}
    {{ Form::select('direction', [
        'down' => 'Count Down',
        'up' => 'Count Up'
    ], 'down', array('class' => 'form-control', 'ng-model = "timer.direction"', 'ng-blur' => 'updateTimer()')) }}
</div>

<div class="form-group">
    {{ Form::label('min', 'From') }}
<input type="text" ng-model="obj.min" class="form-control" ng-blur="update()">
</div>

<div ng-if="timer.direction == 'up'" class="form-group">
    {{ Form::label('max', 'To') }}
<input type="hidden" ng-model="obj.min">
    <label>
        <input type="checkbox" ng-model="timer.set_max">
        Set Maximum Time
    </label>
<input type="text" ng-model="obj.max" class="form-control" ng-blur="update()">
</div>

<!-- <div class="form-group">
    {{ Form::label('result_type', 'Result Type') }}
<input type="text" ng-model="obj.result_type" class="form-control" ng-blur="update()">
</div> -->

<div class="form-group">
    {{ Form::label('result_id', 'Result') }}
<input type="hidden" ng-model="obj.result_type">
    <select name="result_id" id="result_id" ng-model="timer.result_id" class="form-control" ng-blur="updateTimer()">
		<option value="-1">End Game</option>
		<option value="0">Do Nothing</option>
		<option ng-show="u.id !== uvent.id" value="@{{u.id}}" ng-repeat="u in game.uvents">@{{u.name}}</option>
    </select>
    <small ng-if="game.uvents.length == 1">(For more options, add more events.)</small>
</div>

<!-- <div class="form-group">
    {{ Form::label('active', 'Active') }}
<input type="text" ng-model="obj.active" class="form-control" ng-blur="update()">
</div> -->
