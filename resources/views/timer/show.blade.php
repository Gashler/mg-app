@extends('layouts.default')
@section('content')
	<div class="show">
		<div class="row page-actions">
			@include('_helpers.breadcrumbs')
			<h1 class="no-top">Viewing Timer</h1>
			@if (Auth::user()->hasRole(['Superadmin', 'Admin']))
			    <div class="btn-group" id="record-options">
				    <a class="btn btn-default" href="{{ url('timer/'.$timer->id .'/edit') }}" title="Edit"><i class="flaticon-pencil"></i></a>
				    @if ($timer->disabled == 0)
					    {{ Form::open(array('url' => 'Timer/disable', 'method' => 'DISABLE')) }}
					    	<input type="hidden" name="ids[]" value="{{ $timer->id }}">
					    	<button class="btn btn-default active" title="Currently enabled. Click to disable.">
					    		<i class="flaticon-eye-female"></i>
					    	</button>
					    {{ Form::close() }}
					@else
					    {{ Form::open(array('url' => 'Timer/enable', 'method' => 'ENABLE')) }}
					    	<input type="hidden" name="ids[]" value="{{ $timer->id }}">
					    	<button class="btn btn-default" title="Currently disabled. Click to enable.">
					    		<i class="flaticon-eye-female"></i>
					    	</button>
					    {{ Form::close() }}
					@endif
				    {{ Form::open(array('url' => 'timer/' . $timer->id, 'method' => 'DELETE', 'onsubmit' => 'return confirm("Are you sure you want to delete this timer? This cannot be undone.");')) }}
				    	<button class="btn btn-default" title="Delete">
				    		<i class="flaticon-trash" title="Delete"></i>
				    	</button>
				    {{ Form::close() }}
				</div>
			@endif
		</div><!-- row -->
		<div class="row">
			<div class="col col-xl-4 col-lg-6 col-md-8 col-sm-12">
			    <table class="table">
			        
			        <tr>
			            <th>Direction:</th>
			            <td>{{ $timer->direction }}</td>
			        </tr>
			        
			        <tr>
			            <th>morph Type:</th>
			            <td>{{ $timer->morph_type }}</td>
			        </tr>
			        
			        <tr>
			            <th>Result Type:</th>
			            <td>{{ $timer->result_type }}</td>
			        </tr>
			        
			        <tr>
			            <th>Min:</th>
			            <td>{{ $timer->min }}</td>
			        </tr>
			        
			        <tr>
			            <th>Max:</th>
			            <td>{{ $timer->max }}</td>
			        </tr>
			        
			        <tr>
			            <th>Value:</th>
			            <td>{{ $timer->value }}</td>
			        </tr>
			        
			        <tr>
			            <th>morph Id:</th>
			            <td>{{ $timer->morph_id }}</td>
			        </tr>
			        
			        <tr>
			            <th>Result Id:</th>
			            <td>{{ $timer->result_id }}</td>
			        </tr>
			        
			        <tr>
			            <th>Active:</th>
			            <td>{{ $timer->active }}</td>
			        </tr>
			        
			    </table>
		    </div>
		</div>
	</div>
@stop
