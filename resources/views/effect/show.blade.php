@extends('layouts.default')
@section('content')
	<div class="show">
		<div class="row page-actions">
			@include('_helpers.breadcrumbs')
			<h1 class="no-top">Viewing Effect</h1>
			@if (Auth::user()->hasRole(['Superadmin', 'Admin']))
			    <div class="btn-group" id="record-options">
				    <a class="btn btn-default" href="{{ url('effect/'.$effect->id .'/edit') }}" title="Edit"><i class="flaticon-pencil"></i></a>
				    @if ($effect->disabled == 0)
					    {{ Form::open(array('url' => 'Effect/disable', 'method' => 'DISABLE')) }}
					    	<input type="hidden" name="ids[]" value="{{ $effect->id }}">
					    	<button class="btn btn-default active" title="Currently enabled. Click to disable.">
					    		<i class="flaticon-eye-female"></i>
					    	</button>
					    {{ Form::close() }}
					@else
					    {{ Form::open(array('url' => 'Effect/enable', 'method' => 'ENABLE')) }}
					    	<input type="hidden" name="ids[]" value="{{ $effect->id }}">
					    	<button class="btn btn-default" title="Currently disabled. Click to enable.">
					    		<i class="flaticon-eye-female"></i>
					    	</button>
					    {{ Form::close() }}
					@endif
				    {{ Form::open(array('url' => 'effect/' . $effect->id, 'method' => 'DELETE', 'onsubmit' => 'return confirm("Are you sure you want to delete this effect? This cannot be undone.");')) }}
				    	<button class="btn btn-default" title="Delete">
				    		<i class="flaticon-trash" title="Delete"></i>
				    	</button>
				    {{ Form::close() }}
				</div>
			@endif
		</div><!-- row -->
		<div class="row">
			<div class="col col-xl-4 col-lg-6 col-md-8 col-sm-12">
			    <table class="table">
			        
			        <tr>
			            <th>morph Type:</th>
			            <td>{{ $effect->morph_type }}</td>
			        </tr>
			        
			        <tr>
			            <th>morph Col:</th>
			            <td>{{ $effect->morph_col }}</td>
			        </tr>
			        
			        <tr>
			            <th>morph Id:</th>
			            <td>{{ $effect->morph_id }}</td>
			        </tr>
			        
			        <tr>
			            <th>Min:</th>
			            <td>{{ $effect->min }}</td>
			        </tr>
			        
			        <tr>
			            <th>Max:</th>
			            <td>{{ $effect->max }}</td>
			        </tr>
			        
			        <tr>
			            <th>morph Val:</th>
			            <td>{{ $effect->morph_val }}</td>
			        </tr>
			        
			        <tr>
			            <th>Rand:</th>
			            <td>{{ $effect->rand }}</td>
			        </tr>
			        
			        <tr>
			            <th>Negative:</th>
			            <td>{{ $effect->negative }}</td>
			        </tr>
			        
			    </table>
		    </div>
		</div>
	</div>
@stop
