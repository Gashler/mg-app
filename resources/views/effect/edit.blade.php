@extends('layouts.default')
@section('content')
	<div class="edit">
		<div class="row">
			<div class="col col-md-12">
				@include('_helpers.breadcrumbs')
			    <h1>Edit Effect</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
			    {{ Form::model($effect, array('route' => array('effect.update', $effect->id), 'method' => 'PUT')) }}
	
			    
			    <div class="form-group">
			        {{ Form::label('morph_type', 'morph Type') }}
<input type="text" ng-model="obj.morph_type" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('morph_col', 'morph Col') }}
<input type="text" ng-model="obj.morph_col" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('morph_id', 'morph Id') }}
<input type="text" ng-model="obj.morph_id" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('min', 'Min') }}
<input type="text" ng-model="obj.min" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('max', 'Max') }}
<input type="text" ng-model="obj.max" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('morph_val', 'morph Val') }}
<input type="text" ng-model="obj.morph_val" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('rand', 'Rand') }}
<input type="text" ng-model="obj.rand" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('negative', 'Negative') }}
<input type="text" ng-model="obj.negative" class="form-control" ng-blur="update()">
			    </div>
			    
	
			    {{ Form::submit('Update Effect', array('class' => 'btn btn-primary')) }}
	
			    {{Form::close()}}
			</div>
		</div>
	</div>
@stop
