<div class="form-group">
    {{ Form::label('morph_type', 'morph Type') }}
    <input type="text" ng-model="obj.morph_type" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('morph_col', 'morph Col') }}
    <input type="text" ng-model="obj.morph_col" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('morph_id', 'morph Id') }}
    <input type="text" ng-model="obj.morph_id" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('min', 'Min') }}
    <input type="text" ng-model="obj.min" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('max', 'Max') }}
    <input type="text" ng-model="obj.max" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('morph_val', 'morph Val') }}
    <input type="text" ng-model="obj.morph_val" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('rand', 'Rand') }}
    <input type="text" ng-model="obj.rand" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('negative', 'Negative') }}
    <input type="text" ng-model="obj.negative" class="form-control" ng-blur="update()">
</div>
