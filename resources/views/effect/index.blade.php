@extends('layouts.default')
@section('content')
<div class="index">
    {{ Form::open(array('url' => 'Effect/disable', 'method' => 'POST')) }}
	    <div ng-controller="EffectController">
	    	<div class="page-actions">
		        <div class="row">
		            <div class="col-md-12">
		                <h1 class="no-top pull-left no-pull-xs">All Effects</h1>
		            	<div class="pull-right hidable-xs">
		                    <div class="input-group pull-right">
		                    	<span class="input-group-addon no-width">Count</span>
		                    	<input class="form-control itemsPerPage width-auto" ng-model="pageSize" type="number" min="1">
		                    </div>
		                    <h4 class="pull-right margin-right-1">Page <span ng-bind="currentPage"></span></h4>
		            	</div>
			    	</div>
		        </div><!-- row -->
		        <div class="row">
		        	@if (Auth::user()->hasRole(['Superadmin', 'Admin']))
			    		<div class="col-md-6 col-sm-6 col-xs-12 page-actions-left">
			                <div class="pull-left">
			                    <a class="btn btn-primary pull-left margin-right-1" title="New" href="{{ url('effect/create') }}"><i class="flaticon-plus"></i></a>
			                    <div class="pull-left">
			                        <div class="input-group">
			                            <select class="form-control selectpicker actions">
			                                <option value="Effect/disable" selected>Disable</option>
			                                <option value="Effect/enable">Enable</option>
			                                <option value="Effect/delete">Delete</option>
			                            </select>
			                            <div class="input-group-btn no-width">
			                                <button class="btn btn-default applyAction" disabled>
			                                    <i class="flaticon-check"></i>
			                                </button>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			        	</div>
			        	<div class="col-md-6 col-sm-6 col-xs-12">
					@else
						<div class="col-md-12 col-sm-12 col-xs-12">
					@endif
		                <div class="pull-right">
		                    <div class="input-group">
		                        <input class="form-control ng-pristine ng-valid" placeholder="Search" name="new_tag" ng-model="search.$" onkeypress="return disableEnterKey(event)" type="text">
		                        <span class="input-group-btn no-width">
		                            <button class="btn btn-default" type="button">
		                                <i class="flaticon-search"></i>
		                            </button>
		                        </span>
		                    </div>
		                </div>
		            </div><!-- col -->
		        </div><!-- row -->
		    </div><!-- page-actions -->
	        <div class="row">
	            <div class="col col-md-12">
	                <table class="table">
	                    <thead>
	                        <tr>
	                            <th>
	                            	<input type="checkbox">
	                            </th>
                            	
                            	<th class="link" ng-click="orderByField='morph_type'; reverseSort = !reverseSort">morph Type
                        			<span ng-show="orderByField == 'morph_type'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='morph_col'; reverseSort = !reverseSort">morph Col
                        			<span ng-show="orderByField == 'morph_col'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='morph_id'; reverseSort = !reverseSort">morph Id
                        			<span ng-show="orderByField == 'morph_id'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='min'; reverseSort = !reverseSort">Min
                        			<span ng-show="orderByField == 'min'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='max'; reverseSort = !reverseSort">Max
                        			<span ng-show="orderByField == 'max'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='morph_val'; reverseSort = !reverseSort">morph Val
                        			<span ng-show="orderByField == 'morph_val'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='rand'; reverseSort = !reverseSort">Rand
                        			<span ng-show="orderByField == 'rand'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='negative'; reverseSort = !reverseSort">Negative
                        			<span ng-show="orderByField == 'negative'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='updated_at'; reverseSort = !reverseSort">Modified
                        			<span ng-show="orderByField == 'updated_at'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        <tr ng-class="{ 'even' : $even, highlight : effect.new == 1, semitransparent : effect.disabled == 1 }" dir-paginate-start="effect in effects | filter:search | orderBy: '-updated_at' | orderBy:orderByField:reverseSort | itemsPerPage: pageSize" current-page="currentPage">
	                            <td ng-click="checkbox()">
	                            	<input class="bulk-check" type="checkbox" name="ids[]" value="@{{effect.id}}">
	                            </td>
								
					            <td>
					                <a href="/effect/@{{effect.id}}"><span ng-bind="effect.morph_type"></span></a>
					            </td>
					            
					            <td>
					                <a href="/effect/@{{effect.id}}"><span ng-bind="effect.morph_col"></span></a>
					            </td>
					            
					            <td>
					                <a href="/effect/@{{effect.id}}"><span ng-bind="effect.morph_id"></span></a>
					            </td>
					            
					            <td>
					                <a href="/effect/@{{effect.id}}"><span ng-bind="effect.min"></span></a>
					            </td>
					            
					            <td>
					                <a href="/effect/@{{effect.id}}"><span ng-bind="effect.max"></span></a>
					            </td>
					            
					            <td>
					                <a href="/effect/@{{effect.id}}"><span ng-bind="effect.morph_val"></span></a>
					            </td>
					            
					            <td>
					                <a href="/effect/@{{effect.id}}"><span ng-bind="effect.rand"></span></a>
					            </td>
					            
					            <td>
					                <a href="/effect/@{{effect.id}}"><span ng-bind="effect.negative"></span></a>
					            </td>
					            
					            <td>
					            	<a href="/effect/@{{effect.id}}"><span ng-bind="effect.updated_at"></span></a>
					            </td>
	                        </tr>
	                        <tr dir-paginate-end></tr>
	                    </tbody>
	                </table>
	                <div class="align-center">
	                	<img ng-if="loading" src="/img/loading.gif">
	                </div>
	                <div ng-controller="PaginationController" class="other-controller">
	                    <div class="text-center">
	                        <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="/packages/dirpagination/dirPagination.tpl.html"></dir-pagination-controls>
	                    </div>
	                </div>
	            </div><!-- col -->
	        </div><!-- row -->
        {{ Form::close() }}
    </div><!-- app -->
@stop
@section('scripts')
	<script>
	
		// initialize Angular
		var app = angular.module('app', ['angularUtils.directives.dirPagination']);
	
		// effect controller
		function EffectController($scope, $http) {
	
			// initialize variables
			$scope.loading = true;
	
			$http.get('/Effect/all').success(function(effects) {
			
				// get data
				$scope.effects = effects;
	
				// bulk action checkboxes
				$scope.checkbox = function() {
					var checked = false;
					$('.bulk-check').each(function() {
						if ($(this).is(":checked")) checked = true;
					});
					if (checked == true) $('.applyAction').removeAttr('disabled');
					else $('.applyAction').attr('disabled', 'disabled');
				};
				
				// disable loading image
				$scope.loading = false;
	
			});
	
			$scope.currentPage = 1;
			$scope.pageSize = 15;
	
		}
	
		// pagination controller
		function PaginationController($scope) {
			$scope.pageChangeHandler = function(num) { };
		}
	
	</script>
@stop