@extends('layouts.default')
@section('content')
	<div class="create">
		<div class="row">
			<div class="col col-md-12">
				@include('_helpers.breadcrumbs')
			    <h1 class="no-top">New Map</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
			    {{ Form::open(array('url' => 'map')) }}
	
				    
				    <div class="form-group">
				        {{ Form::label('name', '[formatted_property]') }}
<input type="text" ng-model="obj.name" class="form-control" ng-blur="update()">
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('morph_name', '[formatted_property]') }}
<input type="text" ng-model="obj.morph_name" class="form-control" ng-blur="update()">
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('morph_id', '[formatted_property]') }}
<input type="text" ng-model="obj.morph_id" class="form-control" ng-blur="update()">
				    </div>
				    
	
				    {{ Form::submit('Add Map', array('class' => 'btn btn-primary')) }}
	
			    {{ Form::close() }}
		    </div>
		</div>
	</div>
@stop
@section('scripts')
	<script src="/js/controllers/MapController.js"></script>
@stop