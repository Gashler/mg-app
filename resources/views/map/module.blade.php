<div class="panel panel-primary" ng-controller="MapController">
    <div class="panel-heading">
        <h2 class="panel-title"><i class="flaticon-tiles"></i> Maps</h2>
    </div>
    <div class="panel-body">
        <div class="form-group">
            {{ Form::label('maps', 'Maps') }}
            <ul class="list-group">
                <li class="list-group-item" ng-repeat="map in obj.maps">
                    <span ng-if="$index == 0" class="label label-success">Start</span>
                    <span ng-click="edit(map)" class="link">
                        <span ng-show="map.name == null || map.name == ''" ng-bind="'Map ' + (map.id)"></span>
                        <span ng-show="map.name != null && map.name != ''" ng-bind="map.name | limitTo : 40"></span>
                    </span>
                    <i class="pull-right flaticon-x" ng-click="deleteMap(map.id, $index)"></i>
                </li>
            </ul>
            <img ng-if="loading_map" src="/img/loading.gif">
            <div id="mapForm" data-model="map" class="popup align-center">
                <div class="panel panel-primary inline-block no-max-width align-left">
                    <div class="panel-heading overflow-hidden">
                        <h3 class="panel-title">
                            Map Editor
                            <i class="x pull-right flaticon-x" ng-click="togglePopup('#mapForm')"></i>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="btn-group margin-bottom-4">
                            <button ng-click="set_start=!set_start" class="btn btn-default" ng-class="{ active : set_start }"><i class="flaticon-map-marker"></i> Set Start</button>
                        </div>
                        <div id="map">
                            <table>
                                <tr ng-repeat="(row_index, row) in obj.rows">
                                    <td ng-repeat="(col_index, col) in row.cols" class="tile">
                                        <div ng-if="col.place == null" class="space" ng-click="createPlace(row.id, row_index, col.id, col_index)"></div>
                                        <div ng-if="col.place !== null" class="place" ng-click="clickPlace(col.place)">
                                            <span ng-if="col.place.start" class="label label-success">Start</span>
                                            <div ng-bind="col.place.name"></div>
                                            <div ng-repeat="character in col.place.characters">
                                                <span ng-bind="character.first_name"></span>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <div place></div>

                    </div><!-- panel-body -->
                </div><!-- panel -->
            </div><!-- popup -->
        </div><!-- form-group -->
    </div><!-- panel-body -->
</div><!-- panel -->
<script src="/js/controllers/MapController.js"></script>
