@extends('layouts.default')
@section('content')
	<div class="edit">
		<div class="row">
			<div class="col col-md-12">
				@include('_helpers.breadcrumbs')
			    <h1>Edit Map</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
			    {{ Form::model($map, array('route' => array('map.update', $map->id), 'method' => 'PUT')) }}
	
				    @include(map.form)
	
			    	{{ Form::submit('Update Map', array('class' => 'btn btn-primary')) }}
	
			    {{Form::close()}}
			</div>
		</div>
	</div>
@stop
@section('scripts')
	<script>
		map_id = {{ $model->id }}
	</script>
	<script src="/js/controllers/MapController.js"></script>
@stop