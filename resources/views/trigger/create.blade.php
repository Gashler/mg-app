@extends('layouts.default')
@section('content')
	<div class="create">
		<div class="row">
			<div class="col col-md-12">
				@include('_helpers.breadcrumbs')
			    <h1 class="no-top">New Trigger</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
			    {{ Form::open(array('url' => 'trigger')) }}

				    
				    <div class="form-group">
				        {{ Form::label('logic_id', '[formatted_property]') }}
				        {{ Form::text('logic_id', Input::old('logic_id'), array('class' => 'form-control')) }}
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('subject_id', '[formatted_property]') }}
				        {{ Form::text('subject_id', Input::old('subject_id'), array('class' => 'form-control')) }}
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('value', '[formatted_property]') }}
				        {{ Form::text('value', Input::old('value'), array('class' => 'form-control')) }}
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('type', '[formatted_property]') }}
				        {{ Form::text('type', Input::old('type'), array('class' => 'form-control')) }}
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('subject_type', '[formatted_property]') }}
				        {{ Form::text('subject_type', Input::old('subject_type'), array('class' => 'form-control')) }}
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('operator', '[formatted_property]') }}
				        {{ Form::text('operator', Input::old('operator'), array('class' => 'form-control')) }}
				    </div>
				    

				    {{ Form::submit('Add Trigger', array('class' => 'btn btn-primary')) }}

			    {{ Form::close() }}
		    </div>
		</div>
	</div>
@stop
@section('scripts')
	<script src="/js/controllers/TriggerController.js"></script>
@stop
