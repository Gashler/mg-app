@extends('layouts.default')
@section('content')
<div class="create">
	<div class="row">
		<div class="col col-md-12">
			@include('_helpers.breadcrumbs')
		    <h1 class="no-top">New Role</h1>
		    {{ Form::open(array('url' => 'roles')) }}
		
			    
			    <div class="form-group">
			        {{ Form::label('name', 'Name') }}
<input type="text" ng-model="obj.name" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('disabled', 'Disabled') }}
<input type="text" ng-model="obj.disabled" class="form-control" ng-blur="update()">
			    </div>
			    
		
			    {{ Form::submit('Add Role', array('class' => 'btn btn-primary')) }}
	
		    {{ Form::close() }}
	    </div>
	</div>
</div>
@stop