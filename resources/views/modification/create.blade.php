@extends('layouts.default')
@section('content')
	<div class="create">
		<div class="row">
			<div class="col col-md-12">
				@include('_helpers.breadcrumbs')
			    <h1 class="no-top">New Modification</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
			    {{ Form::open(array('url' => 'modification')) }}
	
				    
				    <div class="form-group">
				        {{ Form::label('modifiable_type', 'Modifiable Type') }}
<input type="text" ng-model="obj.modifiable_type" class="form-control" ng-blur="update()">
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('col', 'Col') }}
<input type="text" ng-model="obj.col" class="form-control" ng-blur="update()">
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('game_id', 'Game Id') }}
<input type="text" ng-model="obj.game_id" class="form-control" ng-blur="update()">
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('account_id', 'Account Id') }}
<input type="text" ng-model="obj.account_id" class="form-control" ng-blur="update()">
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('modifiable_id', 'Modifiable Id') }}
<input type="text" ng-model="obj.modifiable_id" class="form-control" ng-blur="update()">
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('value', 'Value') }}
<input type="text" ng-model="obj.value" class="form-control" ng-blur="update()">
				    </div>
				    
	
				    {{ Form::submit('Add Modification', array('class' => 'btn btn-primary')) }}
	
			    {{ Form::close() }}
		    </div>
		</div>
	</div>
@stop
