@extends('layouts.default')
@section('content')
	<div class="create">
		<div class="row">
			<div class="col col-md-12">
				@include('_helpers.breadcrumbs')
			    <h1 class="no-top">New Propertyoption</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
			    {{ Form::open(array('url' => 'propertyoption')) }}

				    
				    <div class="form-group">
				        {{ Form::label('key', '[formatted_property]') }}
				        {{ Form::text('key', Input::old('key'), array('class' => 'form-control')) }}
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('name', '[formatted_property]') }}
				        {{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('value', '[formatted_property]') }}
				        {{ Form::text('value', Input::old('value'), array('class' => 'form-control')) }}
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('property_id', '[formatted_property]') }}
				        {{ Form::text('property_id', Input::old('property_id'), array('class' => 'form-control')) }}
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('description', '[formatted_property]') }}
				        {{ Form::text('description', Input::old('description'), array('class' => 'form-control')) }}
				    </div>
				    

				    {{ Form::submit('Add Propertyoption', array('class' => 'btn btn-primary')) }}

			    {{ Form::close() }}
		    </div>
		</div>
	</div>
@stop
@section('scripts')
	<script src="/js/controllers/PropertyoptionController.js"></script>
@stop
