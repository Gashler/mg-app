@extends('layouts.default')
@section('content')
<div class="index">
    {{ Form::open(array('url' => 'Propertyoption/disable', 'method' => 'POST')) }}
	    <div ng-controller="PropertyoptionController">
	    	<div class="page-actions">
		        <div class="row">
		            <div class="col-md-12">
		                <h1 class="no-top pull-left no-pull-xs">All Propertyoptions</h1>
		            	<div class="pull-right hidable-xs">
		                    <div class="input-group pull-right">
		                    	<span class="input-group-addon no-width">Count</span>
		                    	<input class="form-control itemsPerPage width-auto" ng-model="pageSize" type="number" min="1">
		                    </div>
		                    <h4 class="pull-right margin-right-1">Page <span ng-bind="currentPage"></span></h4>
		            	</div>
			    	</div>
		        </div><!-- row -->
		        <div class="row">
		        	@if (Auth::user()->hasRole(['Superadmin', 'Admin']))
			    		<div class="col-md-6 col-sm-6 col-xs-12 page-actions-left">
			                <div class="pull-left">
			                    <a class="btn btn-primary pull-left margin-right-1" title="New" href="{{ url('propertyoption/create') }}"><i class="flaticon-plus"></i></a>
			                    <div class="pull-left">
			                        <div class="input-group">
			                            <select class="form-control selectpicker actions">
			                                <option value="Propertyoption/disable" selected>Disable</option>
			                                <option value="Propertyoption/enable">Enable</option>
			                                <option value="Propertyoption/delete">Delete</option>
			                            </select>
			                            <div class="input-group-btn no-width">
			                                <button class="btn btn-default applyAction" disabled>
			                                    <i class="flaticon-check"></i>
			                                </button>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			        	</div>
			        	<div class="col-md-6 col-sm-6 col-xs-12">
					@else
						<div class="col-md-12 col-sm-12 col-xs-12">
					@endif
		                <div class="pull-right">
		                    <div class="input-group">
		                        <input class="form-control ng-pristine ng-valid" placeholder="Search" name="new_tag" ng-model="search.$" onkeypress="return disableEnterKey(event)" type="text">
		                        <span class="input-group-btn no-width">
		                            <button class="btn btn-default" type="button">
		                                <i class="flaticon-search"></i>
		                            </button>
		                        </span>
		                    </div>
		                </div>
		            </div><!-- col -->
		        </div><!-- row -->
		    </div><!-- page-actions -->
	        <div class="row">
	            <div class="col col-md-12">
	                <table class="table">
	                    <thead>
	                        <tr>
	                            <th>
	                            	<input type="checkbox">
	                            </th>
                            	
                            	<th class="link" ng-click="orderByField='key'; reverseSort = !reverseSort">[formatted_property]
                        			<span ng-show="orderByField == 'key'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='name'; reverseSort = !reverseSort">[formatted_property]
                        			<span ng-show="orderByField == 'name'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='value'; reverseSort = !reverseSort">[formatted_property]
                        			<span ng-show="orderByField == 'value'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='property_id'; reverseSort = !reverseSort">[formatted_property]
                        			<span ng-show="orderByField == 'property_id'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='description'; reverseSort = !reverseSort">[formatted_property]
                        			<span ng-show="orderByField == 'description'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='updated_at'; reverseSort = !reverseSort">Modified
                        			<span ng-show="orderByField == 'updated_at'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        <tr ng-class="{ 'even' : $even, highlight : propertyoption.new == 1, semitransparent : propertyoption.disabled == 1 }" dir-paginate-start="propertyoption in propertyoptions | filter:search | orderBy: '-updated_at' | orderBy:orderByField:reverseSort | itemsPerPage: pageSize" current-page="currentPage">
	                            <td ng-click="checkbox()">
	                            	<input class="bulk-check" type="checkbox" name="ids[]" value="@{{propertyoption.id}}">
	                            </td>
								
					            <td>
					                <a href="/propertyoption/@{{propertyoption.id}}"><span ng-bind="propertyoption.key"></span></a>
					            </td>
					            
					            <td>
					                <a href="/propertyoption/@{{propertyoption.id}}"><span ng-bind="propertyoption.name"></span></a>
					            </td>
					            
					            <td>
					                <a href="/propertyoption/@{{propertyoption.id}}"><span ng-bind="propertyoption.value"></span></a>
					            </td>
					            
					            <td>
					                <a href="/propertyoption/@{{propertyoption.id}}"><span ng-bind="propertyoption.property_id"></span></a>
					            </td>
					            
					            <td>
					                <a href="/propertyoption/@{{propertyoption.id}}"><span ng-bind="propertyoption.description"></span></a>
					            </td>
					            
					            <td>
					            	<a href="/propertyoption/@{{propertyoption.id}}"><span ng-bind="propertyoption.updated_at"></span></a>
					            </td>
	                        </tr>
	                        <tr dir-paginate-end></tr>
	                    </tbody>
	                </table>
	                <div class="align-center">
	                	<img ng-if="loading" src="/img/loading.gif">
	                </div>
	                <div ng-controller="PaginationController" class="other-controller">
	                    <div class="text-center">
	                        <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="/packages/dirpagination/dirPagination.tpl.html"></dir-pagination-controls>
	                    </div>
	                </div>
	            </div><!-- col -->
	        </div><!-- row -->
        {{ Form::close() }}
    </div><!-- app -->
@stop
@section('scripts')
	<script>
	
		// initialize Angular
		var app = angular.module('app', ['angularUtils.directives.dirPagination']);
	
		// propertyoption controller
		function PropertyoptionController($scope, $http) {
	
			// initialize variables
			$scope.loading = true;
	
			$http.get('/Propertyoption/all').success(function(propertyoptions) {
			
				// get data
				$scope.propertyoptions = propertyoptions;
	
				// bulk action checkboxes
				$scope.checkbox = function() {
					var checked = false;
					$('.bulk-check').each(function() {
						if ($(this).is(":checked")) checked = true;
					});
					if (checked == true) $('.applyAction').removeAttr('disabled');
					else $('.applyAction').attr('disabled', 'disabled');
				};
				
				// disable loading image
				$scope.loading = false;
	
			});
	
			$scope.currentPage = 1;
			$scope.pageSize = 15;
	
		}
	
		// pagination controller
		function PaginationController($scope) {
			$scope.pageChangeHandler = function(num) { };
		}
	
	</script>
@stop