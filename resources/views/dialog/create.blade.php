@extends('layouts.default')
@section('content')
	<div class="create">
		<div class="row">
			<div class="col col-md-12">
				@include('_helpers.breadcrumbs')
			    <h1 class="no-top">New Dialog</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
			    {{ Form::open(array('url' => 'dialog')) }}

				    
				    <div class="form-group">
				        {{ Form::label('character_id', '[formatted_property]') }}
				        {{ Form::text('character_id', Input::old('character_id'), array('class' => 'form-control')) }}
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('group_id', '[formatted_property]') }}
				        {{ Form::text('group_id', Input::old('group_id'), array('class' => 'form-control')) }}
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('order', '[formatted_property]') }}
				        {{ Form::text('order', Input::old('order'), array('class' => 'form-control')) }}
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('body', '[formatted_property]') }}
				        {{ Form::text('body', Input::old('body'), array('class' => 'form-control')) }}
				    </div>
				    

				    {{ Form::submit('Add Dialog', array('class' => 'btn btn-primary')) }}

			    {{ Form::close() }}
		    </div>
		</div>
	</div>
@stop
@section('scripts')
	<script src="/js/controllers/DialogController.js"></script>
@stop
