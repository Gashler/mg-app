@extends('layouts.default')
@section('content')
<div class="index">
    {{ Form::open(array('url' => 'Dialog/disable', 'method' => 'POST')) }}
	    <div ng-controller="DialogController">
	    	<div class="page-actions">
		        <div class="row">
		            <div class="col-md-12">
		                <h1 class="no-top pull-left no-pull-xs">All Dialogs</h1>
		            	<div class="pull-right hidable-xs">
		                    <div class="input-group pull-right">
		                    	<span class="input-group-addon no-width">Count</span>
		                    	<input class="form-control itemsPerPage width-auto" ng-model="pageSize" type="number" min="1">
		                    </div>
		                    <h4 class="pull-right margin-right-1">Page <span ng-bind="currentPage"></span></h4>
		            	</div>
			    	</div>
		        </div><!-- row -->
		        <div class="row">
		        	@if (Auth::user()->hasRole(['Superadmin', 'Admin']))
			    		<div class="col-md-6 col-sm-6 col-xs-12 page-actions-left">
			                <div class="pull-left">
			                    <a class="btn btn-primary pull-left margin-right-1" title="New" href="{{ url('dialog/create') }}"><i class="flaticon-plus"></i></a>
			                    <div class="pull-left">
			                        <div class="input-group">
			                            <select class="form-control selectpicker actions">
			                                <option value="Dialog/disable" selected>Disable</option>
			                                <option value="Dialog/enable">Enable</option>
			                                <option value="Dialog/delete">Delete</option>
			                            </select>
			                            <div class="input-group-btn no-width">
			                                <button class="btn btn-default applyAction" disabled>
			                                    <i class="flaticon-check"></i>
			                                </button>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			        	</div>
			        	<div class="col-md-6 col-sm-6 col-xs-12">
					@else
						<div class="col-md-12 col-sm-12 col-xs-12">
					@endif
		                <div class="pull-right">
		                    <div class="input-group">
		                        <input class="form-control ng-pristine ng-valid" placeholder="Search" name="new_tag" ng-model="search.$" onkeypress="return disableEnterKey(event)" type="text">
		                        <span class="input-group-btn no-width">
		                            <button class="btn btn-default" type="button">
		                                <i class="flaticon-search"></i>
		                            </button>
		                        </span>
		                    </div>
		                </div>
		            </div><!-- col -->
		        </div><!-- row -->
		    </div><!-- page-actions -->
	        <div class="row">
	            <div class="col col-md-12">
	                <table class="table">
	                    <thead>
	                        <tr>
	                            <th>
	                            	<input type="checkbox">
	                            </th>
                            	
                            	<th class="link" ng-click="orderByField='character_id'; reverseSort = !reverseSort">[formatted_property]
                        			<span ng-show="orderByField == 'character_id'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='group_id'; reverseSort = !reverseSort">[formatted_property]
                        			<span ng-show="orderByField == 'group_id'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='order'; reverseSort = !reverseSort">[formatted_property]
                        			<span ng-show="orderByField == 'order'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='body'; reverseSort = !reverseSort">[formatted_property]
                        			<span ng-show="orderByField == 'body'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='updated_at'; reverseSort = !reverseSort">Modified
                        			<span ng-show="orderByField == 'updated_at'">
                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                        			</span>
                        		</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        <tr ng-class="{ 'even' : $even, highlight : dialog.new == 1, semitransparent : dialog.disabled == 1 }" dir-paginate-start="dialog in dialogs | filter:search | orderBy: '-updated_at' | orderBy:orderByField:reverseSort | itemsPerPage: pageSize" current-page="currentPage">
	                            <td ng-click="checkbox()">
	                            	<input class="bulk-check" type="checkbox" name="ids[]" value="@{{dialog.id}}">
	                            </td>
								
					            <td>
					                <a href="/dialog/@{{dialog.id}}"><span ng-bind="dialog.character_id"></span></a>
					            </td>
					            
					            <td>
					                <a href="/dialog/@{{dialog.id}}"><span ng-bind="dialog.group_id"></span></a>
					            </td>
					            
					            <td>
					                <a href="/dialog/@{{dialog.id}}"><span ng-bind="dialog.order"></span></a>
					            </td>
					            
					            <td>
					                <a href="/dialog/@{{dialog.id}}"><span ng-bind="dialog.body"></span></a>
					            </td>
					            
					            <td>
					            	<a href="/dialog/@{{dialog.id}}"><span ng-bind="dialog.updated_at"></span></a>
					            </td>
	                        </tr>
	                        <tr dir-paginate-end></tr>
	                    </tbody>
	                </table>
	                <div class="align-center">
	                	<img ng-if="loading" src="/img/loading.gif">
	                </div>
	                <div ng-controller="PaginationController" class="other-controller">
	                    <div class="text-center">
	                        <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="/packages/dirpagination/dirPagination.tpl.html"></dir-pagination-controls>
	                    </div>
	                </div>
	            </div><!-- col -->
	        </div><!-- row -->
        {{ Form::close() }}
    </div><!-- app -->
@stop
@section('scripts')
	<script>
	
		// initialize Angular
		var app = angular.module('app', ['angularUtils.directives.dirPagination']);
	
		// dialog controller
		function DialogController($scope, $http) {
	
			// initialize variables
			$scope.loading = true;
	
			$http.get('/Dialog/all').success(function(dialogs) {
			
				// get data
				$scope.dialogs = dialogs;
	
				// bulk action checkboxes
				$scope.checkbox = function() {
					var checked = false;
					$('.bulk-check').each(function() {
						if ($(this).is(":checked")) checked = true;
					});
					if (checked == true) $('.applyAction').removeAttr('disabled');
					else $('.applyAction').attr('disabled', 'disabled');
				};
				
				// disable loading image
				$scope.loading = false;
	
			});
	
			$scope.currentPage = 1;
			$scope.pageSize = 15;
	
		}
	
		// pagination controller
		function PaginationController($scope) {
			$scope.pageChangeHandler = function(num) { };
		}
	
	</script>
@stop