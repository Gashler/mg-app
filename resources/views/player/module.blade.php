<div class="panel panel-primary" ng-controller="CharacterController">
    <div class="panel-heading">
        <h2 class="panel-title"><i class="flaticon-user"></i> Players</h2>
    </div>
    <div class="panel-body">
        <ul class="list-group">
            <li class="list-group-item" ng-repeat="player in obj.players">
                <span ng-bind="player.name"></span>
                <i ng-if="obj.players.length > 1" class="flaticon-x pull-right" ng-click="deleteCharacter(player.id, $index, 'player')"></i>
            </li>
        </ul>
        <button ng-if="obj.players.length < 2" class="btn btn-default" ng-click="createCharacter(place_id = null, gender = null, type = 'player')"><i class="flaticon-plus"></i> Add Player</button>
        <img ng-if="loading_character" src="/img/loading.gif">
    </div>
</div><!-- panel -->
<script src="/js/controllers/CharacterController.js"></script>
