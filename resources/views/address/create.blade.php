@extends('layouts.default')
@section('content')
<div class="create">
	<div class="row">
		<div class="col col-md-12">
			@include('_helpers.breadcrumbs')
		    <h1 class="no-top">New Address</h1>
		</div>
	</div>
	<div class="row">
		<div class="col col-xl-3 col-lg-4 col-md-6 col-sm-6">
		    {{ Form::open(array('url' => 'addresses')) }}
		
			    
			    <div class="form-group">
			        {{ Form::label('address_1', 'Address 1') }}
<input type="text" ng-model="obj.address_1" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('address_2', 'Address 2') }}
<input type="text" ng-model="obj.address_2" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('city', 'City') }}
<input type="text" ng-model="obj.city" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('state', 'State') }}
<input type="text" ng-model="obj.state" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('addressable_id', 'Addressable Id') }}
<input type="text" ng-model="obj.addressable_id" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('zip', 'Zip') }}
<input type="text" ng-model="obj.zip" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('disabled', 'Disabled') }}
<input type="text" ng-model="obj.disabled" class="form-control" ng-blur="update()">
			    </div>
			    
		
			    {{ Form::submit('Add Address', array('class' => 'btn btn-primary')) }}
	
		    {{ Form::close() }}
	    </div>
	</div>
</div>
@stop