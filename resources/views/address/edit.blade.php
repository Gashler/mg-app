@extends('layouts.default')
@section('content')
<div class="edit">
	<div class="row">
		<div class="col col-md-12">
			@include('_helpers.breadcrumbs')
		    <h1>Edit address</h1>
		</div>
	</div>
	<div class="row">
		<div class="col col-xl-3 col-lg-4 col-md-6 col-sm-6">
		    {{ Form::model($address, array('route' => array('addresses.update', $address->id), 'method' => 'PUT')) }}
		
		    
		    <div class="form-group">
		        {{ Form::label('address_1', 'Address 1') }}
<input type="text" ng-model="obj.address_1" class="form-control" ng-blur="update()">
		    </div>
		    
		    <div class="form-group">
		        {{ Form::label('address_2', 'Address 2') }}
<input type="text" ng-model="obj.address_2" class="form-control" ng-blur="update()">
		    </div>
		    
		    <div class="form-group">
		        {{ Form::label('city', 'City') }}
<input type="text" ng-model="obj.city" class="form-control" ng-blur="update()">
		    </div>
		    
		    <div class="form-group">
		        {{ Form::label('state', 'State') }}
<input type="text" ng-model="obj.state" class="form-control" ng-blur="update()">
		    </div>
		    
		    <div class="form-group">
		        {{ Form::label('label', 'Type') }}
		        {{ Form::select('label', [
		        	'Billing' => 'Billing',
		        	'Shipping' => 'Shipping'
		        ], null, array('class' => 'form-control')) }}
		    </div>
		    
		    <div class="form-group">
		        {{ Form::label('zip', 'Zip') }}
<input type="text" ng-model="obj.zip" class="form-control" ng-blur="update()">
		    </div>
		    
		    <!-- <div class="form-group">
		        {{ Form::label('disabled', 'Disabled') }}
<input type="text" ng-model="obj.disabled" class="form-control" ng-blur="update()">
		    </div> -->
		    
		
		    {{ Form::submit('Update Address', array('class' => 'btn btn-primary')) }}
		
		    {{Form::close()}}
		</div>
	</div>
</div>
@stop

