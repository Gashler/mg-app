		<div class="panel panel-primary">
			<div class="panel-heading">
				<h2 class="panel-title"><i class="flaticon-love4"></i> Upcoming Events</h2>
			</div>
			@if (count($events) > 0)
				<table class="table table-striped">
					<thead>
						<th>Date</th>
						<th>Event</th>
					</thead>
					<tbody>
						@foreach ($events as $event)
							<tr>
								<td style="width:130px;">
									{{ date('D, M jS', $event->date_start) }}<br>
									<small>{{ date('g:i a', $event->date_start) }} - {{ date('g:i a', $event->date_end) }}</small>
								</td>
								<td><a href="/events/{{ $event->id }}">{{ $event->name }}</a></td>
							</tr>
						@endforeach	
					</tbody>
				</table>
				<div class="panel-footer overflow-hidden">
					<a class="btn btn-primary pull-right margin-right-1" href="/events">All Events</a>
				</div>
			@endif
		</div><!-- panel -->