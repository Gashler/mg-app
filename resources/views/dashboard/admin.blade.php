@extends('layouts.default')
@section('content')
<div class="row">
    <div class="col col-md-12">
        <h1>Dashboard</h1>
    </div>
</div><!-- row -->
<div class="row">
    <div class="col col-xl-3 col-lg-4 col-md-6 col-sm-6">
        @include('dashboard.widgets.recent_post')
    </div><!-- col -->
    <div class="col col-xl-3 col-lg-4 col-md-6 col-sm-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h2 class="panel-title">Company Statistics</h2>
            </div>
            <table class="table table-striped">
                <tr>
                    <th>Total Members</th>
                    <td><a href="/downline/all/0">{{ $members }}</a></td>
                </tr>
            </table>
        </div><!-- panel -->
    </div><!-- col -->
</div><!-- row -->
@stop
