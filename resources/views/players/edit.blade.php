@extends('layout')

@section('header')
    <div class="page-header">
        <h1><i class="glyphicon glyphicon-edit"></i> Players / Edit #{{$player->id}}</h1>
    </div>
@endsection

@section('content')
    @include('error')

    <div class="row">
        <div class="col-md-12">

            <form action="{{ route('players.update', $player->id) }}" method="POST">
                <input type="hidden" name="_method" value="PUT">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="form-group @if($errors->has('character_id')) has-error @endif">
                       <label for="character_id-field">Character_id</label>
                    <input type="text" id="character_id-field" name="character_id" class="form-control" value="{{ $player->character_id }}"/>
                       @if($errors->has("character_id"))
                        <span class="help-block">{{ $errors->first("character_id") }}</span>
                       @endif
                    </div>
                    <div class="form-group @if($errors->has('game_id')) has-error @endif">
                       <label for="game_id-field">Game_id</label>
                    <input type="text" id="game_id-field" name="game_id" class="form-control" value="{{ $player->game_id }}"/>
                       @if($errors->has("game_id"))
                        <span class="help-block">{{ $errors->first("game_id") }}</span>
                       @endif
                    </div>
                <div class="well well-sm">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a class="btn btn-link pull-right" href="{{ route('players.index') }}"><i class="glyphicon glyphicon-backward"></i>  Back</a>
                </div>
            </form>

        </div>
    </div>
@endsection