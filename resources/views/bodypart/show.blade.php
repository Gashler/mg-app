@extends('layouts.default')
@section('content')
	<div class="show">
		<div class="row page-actions">
			@include('_helpers.breadcrumbs')
			<h1 class="no-top">Viewing Bodypart</h1>
			@if (Auth::user()->hasRole(['Superadmin', 'Admin']))
			    <div class="btn-group" id="record-options">
				    <a class="btn btn-default" href="{{ url('bodypart/'.$bodypart->id .'/edit') }}" title="Edit"><i class="flaticon-pencil"></i></a>
				    @if ($bodypart->disabled == 0)
					    {{ Form::open(array('url' => 'Bodypart/disable', 'method' => 'DISABLE')) }}
					    	<input type="hidden" name="ids[]" value="{{ $bodypart->id }}">
					    	<button class="btn btn-default active" title="Currently enabled. Click to disable.">
					    		<i class="flaticon-eye-female"></i>
					    	</button>
					    {{ Form::close() }}
					@else
					    {{ Form::open(array('url' => 'Bodypart/enable', 'method' => 'ENABLE')) }}
					    	<input type="hidden" name="ids[]" value="{{ $bodypart->id }}">
					    	<button class="btn btn-default" title="Currently disabled. Click to enable.">
					    		<i class="flaticon-eye-female"></i>
					    	</button>
					    {{ Form::close() }}
					@endif
				    {{ Form::open(array('url' => 'bodypart/' . $bodypart->id, 'method' => 'DELETE', 'onsubmit' => 'return confirm("Are you sure you want to delete this bodypart? This cannot be undone.");')) }}
				    	<button class="btn btn-default" title="Delete">
				    		<i class="flaticon-trash" title="Delete"></i>
				    	</button>
				    {{ Form::close() }}
				</div>
			@endif
		</div><!-- row -->
		<div class="row">
			<div class="col col-xl-4 col-lg-6 col-md-8 col-sm-12">
			    <table class="table">
			        
			        <tr>
			            <th>Name:</th>
			            <td>{{ $bodypart->name }}</td>
			        </tr>
			        
			        <tr>
			            <th>Gender:</th>
			            <td>{{ $bodypart->gender }}</td>
			        </tr>
			        
			    </table>
		    </div>
		</div>
	</div>
@stop
