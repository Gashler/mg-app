<div class="form-group">
    {{ Form::label('characters', 'Characters') }}
    <ul class="list-group" ng-if="obj.characters.length > 0">
        <li ng-repeat="character in obj.characters" class="list-group-item">
            <span ng-click="edit(character)" class="link" ng-bind="character.first_name"></span>
            <i class="link flaticon-x pull-right" ng-click="delete('character', character.id, $index)"></i>
        </li>
    </ul>
    <button type="button" class="btn btn-default" ng-click="createCharacter(obj.id)"><i class="flaticon-plus"></i> Add Character</button>
    <img ng-if="loading_character" src="/img/loading.gif">
    <div id="characterForm" class="popup">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="flaticon-user"></i> @{{obj.first_name}} @{{obj.last_name}}<span class="x pull-right" ng-click="togglePopup('#characterForm')">&times;</span></h3>
            </div>
            <div class="panel-body">

                <div class="form-group">
                    <label>First Name</label>
                    <input type="text" class="form-control" ng-model="obj.first_name" ng-blur="update()">
                </div>

                <div class="form-group">
                    <label>Last Name</label>
                    <input type="text" class="form-control" ng-model="obj.last_name" ng-blur="update()">
                </div>

                <div class="form-group">
                    <label>Gender</label>
                    <select type="textarea" class="form-control" ng-model="obj.gender" ng-blur="update()">
                        <option value="m">Male</option>
                        <option value="f">Female</option>
                    </select>
                </div>

                <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" ng-model="obj.description" ng-blur="update()"></textarea>
                </div>

                @include('logic.module');

                <button type="button" class="btn btn-default" ng-click="togglePopup('#characterForm')">Close</button>
            </div><!-- panel-body -->
        </div><!-- panel -->
    </div><!-- characterForm -->
</div>
