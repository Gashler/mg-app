@extends('layouts.default')
@section('content')
	<div class="show">
		<div class="row page-actions">
			@include('_helpers.breadcrumbs')
			<h1 class="no-top">Viewing Level</h1>
			@if (Auth::user()->hasRole(['Superadmin', 'Admin']))
			    <div class="btn-group" id="record-options">
				    <a class="btn btn-default" href="{{ url('level/'.$level->id .'/edit') }}" title="Edit"><i class="flaticon-pencil"></i></a>
				    @if ($level->disabled == 0)
					    {{ Form::open(array('url' => 'Level/disable', 'method' => 'DISABLE')) }}
					    	<input type="hidden" name="ids[]" value="{{ $level->id }}">
					    	<button class="btn btn-default active" title="Currently enabled. Click to disable.">
					    		<i class="flaticon-eye-female"></i>
					    	</button>
					    {{ Form::close() }}
					@else
					    {{ Form::open(array('url' => 'Level/enable', 'method' => 'ENABLE')) }}
					    	<input type="hidden" name="ids[]" value="{{ $level->id }}">
					    	<button class="btn btn-default" title="Currently disabled. Click to enable.">
					    		<i class="flaticon-eye-female"></i>
					    	</button>
					    {{ Form::close() }}
					@endif
				    {{ Form::open(array('url' => 'level/' . $level->id, 'method' => 'DELETE', 'onsubmit' => 'return confirm("Are you sure you want to delete this level? This cannot be undone.");')) }}
				    	<button class="btn btn-default" title="Delete">
				    		<i class="flaticon-trash" title="Delete"></i>
				    	</button>
				    {{ Form::close() }}
				</div>
			@endif
		</div><!-- row -->
		<div class="row">
			<div class="col col-xl-4 col-lg-6 col-md-8 col-sm-12">
			    <table class="table">
			        
			        <tr>
			            <th>Name:</th>
			            <td>{{ $level->name }}</td>
			        </tr>
			        
			    </table>
		    </div>
		</div>
	</div>
@stop
