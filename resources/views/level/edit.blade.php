@extends('layouts.default')
@section('content')
	<div class="edit">
		<div class="row">
			<div class="col col-md-12">
				@include('_helpers.breadcrumbs')
			    <h1>Edit Level</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
			    {{ Form::model($level, array('route' => array('level.update', $level->id), 'method' => 'PUT')) }}
	
			    
			    <div class="form-group">
			        {{ Form::label('name', 'Name') }}
<input type="text" ng-model="obj.name" class="form-control" ng-blur="update()">
			    </div>
			    
	
			    {{ Form::submit('Update Level', array('class' => 'btn btn-primary')) }}
	
			    {{Form::close()}}
			</div>
		</div>
	</div>
@stop
