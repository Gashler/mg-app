@extends('layouts.default')
@section('style')
	{{ HTML::style('css/poker.css') }}
@stop
@section('content')
	<div ng-controller="PokerController">
		<h1 class="pull-left"><i class="flaticon-poker5"></i> Strip Poker</h1>
		<button id="settings" class="small pull-right btn btn-default"><i class="fa fa-gear"></i></button>
		<div id="settingsContent" class="display-none">
			<ul class="list-group">
				<li class="list-group-item btn-default cursor-pointer" id="newGame">New Game</li>
				<li class="list-group-item btn-default cursor-pointer" id="resetMoney">Reset Money</li>
			</ul>
		</div><!-- settingsContent -->
		<div class="clear"></div>
		<div id="topHeaders">
			<div ng-if="game.phase > 0" class="well inline-block no-bottom">
				<strong ng-bind="game.user.spouse.first_name"></strong>: $<span id="spouseMoney"><span ng-bind="game[spouse + 'money']"></span>
			</div>
			<p ng-if="message !== ''" class="alert alert-warning inline-block no-bottom" ng-bind="message" id="message"></p>
			<button ng-if="noAnte" class='btn btn-default popClose resetMoney margin-top-2' id="resetMoney">Reset Love Bucks</button>
		</div><!-- topHeaders -->
		<div ng-show="game.phase == 0 && !noAnte" class="margin-top-2">
			<div class="panel panel-primary inline-block">
				<div class="panel-heading">
					<h2 class="panel-title"><i class="flaticon-info-circle"></i> How to Play</h2>
				</div>
				<div class="panel-body">
					<ol>
						<li>Both of you put on 4 articles of clothing.</li>
						<li>Log in on two different devices.</li>
						<li>Open this game on both devices.</li>
						<li>The game will automatically start.</li>
					</ol>
				</div>
			</div>			
		</div>
		<div id="gameContent" ng-show="game.phase > 0">
			<div id="clear2" class="clear"></div>
			<div id="load" class="display-none"></div>
			<div id="clear3" class="clear"></div>
			<div id="clear4" class="clear" class="display-none"></div>
			<div id="loadSpouseHand" class="display-none"></div>
			<div id="spouseHand" class="hand">
				@for ($x = 1; $x <= 5; $x ++)
					<div class="cardContainer">
						<div class="card">
							<figure class="back"></figure>
						</div><!-- card -->
					</div><!-- cardContainer -->
				@endfor
			</div><!-- spouseHand -->
			<div id="userHandContainer">
				<div class="well pull-left no-bottom" id="userMoneyContainer"><strong>You</strong>: $<span id="userMoney" ng-bind="game[user + 'money']"></span></div>&nbsp;
				<div ng-show="game.phase > 0" class="well inline-block no-bottom">
					<strong>Pot</strong>: $<span id="pot" ng-bind="game.pot"></span>
				</div>
				<nav id="options" class="pull-right btn-group">
					<button autocomplete="off" disabled id="discard" class="btn btn-default">Discard</button>
					<button autocomplete="off" disabled id="fold" class="btn btn-default">Fold</button>
					<button autocomplete="off" disabled id="stay" class="btn btn-default">Stay</button>
					<button autocomplete="off" disabled id="raise" class="btn btn-default">Raise</button>
					<button autocomplete="off" disabled id="call" class="btn btn-default">Call</button>
				</nav>
				<div class="clear"></div>
				<div id="userHand" class="hand"></div>
			</div><!-- userHandContainer -->
		</div><!-- gameContent -->
	</div><!-- app -->
@stop
@section('scripts')
	<script src="/js/controllers/poker.js"></script>
@stop