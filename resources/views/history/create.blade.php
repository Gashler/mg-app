@extends('layouts.default')
@section('content')
	<div class="create">
		<div class="row">
			<div class="col col-md-12">
				@include('_helpers.breadcrumbs')
			    <h1 class="no-top">New History</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
			    {{ Form::open(array('url' => 'history')) }}

				    
				    <div class="form-group">
				        {{ Form::label('body', '[formatted_property]') }}
				        {{ Form::text('body', Input::old('body'), array('class' => 'form-control')) }}
				    </div>
				    

				    {{ Form::submit('Add History', array('class' => 'btn btn-primary')) }}

			    {{ Form::close() }}
		    </div>
		</div>
	</div>
@stop
@section('scripts')
	<script src="/js/controllers/HistoryController.js"></script>
@stop
