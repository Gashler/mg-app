@extends('layouts.default')
@section('content')
<div class="show">
	<div class="row">
		<div class="col-md-12">
			@include('_helpers.breadcrumbs')
			<h1 class="no-top">{{ $service->name }}</h1>
			@if (Auth::user()->hasRole(['Admin', 'Editor']))
				<div class="page-actions">
				    <div class="btn-group" id="record-options">
					    <a class="btn btn-default" href="{{ url('services/'.$service->id .'/edit') }}" title="Edit"><i class="flaticon-pencil"></i></a>
					    @if ($service->disabled == 0)
						    {{ Form::open(array('url' => 'services/disable', 'method' => 'DISABLE')) }}
						    	<input type="hidden" name="ids[]" value="{{ $service->id }}">
						    	<button class="btn btn-default active" title="Currently enabled. Click to disable.">
						    		<i class="flaticon-eye-female"></i>
						    	</button>
						    {{ Form::close() }}
						@else
						    {{ Form::open(array('url' => 'services/enable', 'method' => 'ENABLE')) }}
						    	<input type="hidden" name="ids[]" value="{{ $service->id }}">
						    	<button class="btn btn-default" title="Currently disabled. Click to enable.">
						    		<i class="flaticon-eye-female"></i>
						    	</button>
						    {{ Form::close() }}
						@endif
					    {{ Form::open(array('url' => 'services/' . $service->id, 'method' => 'DELETE', 'onsubmit' => 'return confirm("Are you sure you want to delete this service? This cannot be undone.");')) }}
					    	<button class="btn btn-default" title="Delete">
					    		<i class="flaticon-trash" title="Delete"></i>
					    	</button>
					    {{ Form::close() }}
					</div>
				</div>
			@endif
		</div><!-- col -->
	</div><!-- row -->
	<div class="row">
		<div class="col col-md-6 center">
			<img src="/uploads/{{ $service->image }}" class="full-image">
		</div>
		<div class="col col-md-6">
		    <table class="table">
		        
		        <tr>
		            <th>Name:</th>
		            <td>{{ $service->name }}</td>
		        </tr>
		        
		        <tr>
		            <th>Blurb:</th>
		            <td>{{ $service->blurb }}</td>
		        </tr>
		        
		        <tr>
		            <th>Description:</th>
		            <td>{{ $service->description }}</td>
		        </tr>
		        
		        <tr>
		            <th>Price:</th>
		            <td>{{ $service->price }}</td>
		        </tr>
		        
		        <tr>
		            <th>Quantity:</th>
		            <td>{{ $service->quantity }}</td>
		        </tr>
		        
		        <tr>
		            <th>Category Id:</th>
		            <td>{{ $service->category_id }}</td>
		        </tr>
		        
		        <tr>
		            <th>Disabled:</th>
		            <td>{{ $service->disabled }}</td>
		        </tr>
		        
		    </table>
	    </div>
	</div>
</div>
@stop
