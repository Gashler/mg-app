@extends('layouts.default')
@section('content')
<div class="create">
	{{ Form::open(array('url' => 'services', 'files' => true)) }}
		<div class="row">
			<div class="col col-md-12">
				@include('_helpers.breadcrumbs')
			    <h1 class="no-top">New Service</h1>
			</div>
		</div>
		<div class="row">
			<div class="col col-lg-6">			
				    		    
			    <div class="form-group">
			        {{ Form::label('name', 'Name') }}
<input type="text" ng-model="obj.name" class="form-control" ng-blur="update()">
			    </div>
	    
			    <div class="form-group">
			        {{ Form::label('blurb', 'Brief Description') }}
<textarea ng-model="obj.blurb" class="form-control" ng-blur="update()"></textarea>
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('description', 'Long Description') }}
<textarea ng-model="obj.description" class="form-control" ng-blur="update()"></textarea>
			    </div>
			    
			</div><!-- col -->
		</div><!-- row -->
		<div class="row">
			<div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
				    
				<div class="form-group">
					{{ Form::label('image', 'Upload Image') }}
					{{ Form::file('image', null, array('class' => 'form-control')) }}
				</div>
				
				<div class="form-group">
					{{ Form::label('name', 'Or Choose from Media Library') }}
					<div class="input-group">
						<span class="input-group-btn">
							<button data-toggle="modal" data-target="#mediaLibrary" type="button" class="btn btn-default" id="media-library">
								<i class="fa fa-th-large"></i>
							</button>
						</span>
<input type="text" ng-model="obj.image_url" class="form-control" ng-blur="update()">
					</div>
				</div>
			    
			    <div class="form-group">
			        {{ Form::label('price', 'Price') }}
			        <div class="input-group">
			        	<span class="input-group-addon">$</span>
<input type="text" ng-model="obj.price" class="form-control" ng-blur="update()">
			        </div>
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('quantity', 'Quantity') }}
<input type="text" ng-model="obj.quantity" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			    	{{ Form::label('category_id', 'Category') }}
			    	<br>
			    	{{ Form::select('category_id', $selectCategories, null, ['class' => 'form-control']) }}
			    </div>
			    
			    <div class="form-group">
					{{ Form::label('tags', 'Tags') }}
			    	<div class="input-group">
<input type="text" ng-model="obj." class="form-control" ng-blur="update()">
						<div class='input-group-btn'>
							<button type="button" class='btn btn-default' id="addTag"><i class='flaticon-plus'></i></button>
			            </div>
			    	</div>
			    	<div class="tag-list"></div>
		        </div>
			    
			    <div class="form-group">
			        {{ Form::label('disabled', 'Status') }}
			        <br>
			    	{{ Form::select('disabled', [
			    		0 => 'Active',
			    		1 => 'Disabled'
			    	], null, ['class' => 'form-control']) }}
			    </div>
			    <br>
			    {{ Form::submit('Add Service', array('class' => 'btn btn-primary')) }}
		
		    </div>
		</div>
	{{ Form::close() }}
</div>
@stop
@section('modals')
	@include('_helpers.wysiwyg_modals')
@stop