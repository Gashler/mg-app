@extends('layouts.pages')
@section('content')
    <?php
        $icon1 = 32;
        $icon2 = 48;
        $theme_url = env('HOST_URL') . "/wp-content/themes/" . env('HOST_THEME');
    ?>
<style>
    /* home-1 */

    #home-1-icons li div:first-child { background:rgb(255,0,128); border-radius:50%; border:10px solid rgb(255,0,128); box-shadow:1px 1px 3px rgba(0,0,0,.5); height:<?php echo $icon2 ?>px; width:<?php echo $icon2 ?>px; background-image:url("{{ $theme_url }}/images/icons.png"); background-size:480px 480px; }

    /* apps */

    .apps:nth-of-type(odd) { background:rgb(192,0,96); }
    .apps { padding:5%; }
    .apps > ul { margin:0; padding:0; }
    .apps > ul > li { list-style:none; margin-bottom:5%; max-width:750px; display:inline-block; vertical-align:top; }
    .apps > ul > li:nth-of-type(odd) { margin-right:5%; }
    .apps > ul > li .icon { vertical-align:middle; background:rgb(255,0,128); border-radius:50%; padding:2px 6px; border:5px solid rgb(255,0,128); box-shadow:1px 1px 3px rgba(0,0,0,.5); display:inline-block; }
    .apps > ul > li .icon > span { display:inline-block; margin-bottom:-7px; background:rgb(255,0,128); height:<?php echo $icon1 ?>px; width:<?php echo $icon1 ?>px; background-image:url("../img/icons.png"); background-size:320px 320px; }
    .apps > ul > li > img { float:left; margin:0 3% 3% 0; width:100%; max-width:400px; }

</style>
<header id="header" style="position:relative; z-index:1;">
    <a style="float:right;" href="/login">Member Login</a>
</header>
<h1 style="position:absolute; top:0; left:0; z-index:1;"><img id="logo" src="{{ $theme_url }}/images/home/2014-04/marriedgames-logo.png" alt="Sex Games for Couples | MarriedGames.org"></h1>
<div style="display:table; width:100%;">
    <section id="home-1">
        <!--<img id="home-1-dice" width="300" height="284" src="{{ $theme_url }}/images/home/2014-04/dice-2.jpg" alt="dice" style="position:absolute; bottom:25%; left:20%; -webkit-transform:rotate(45deg);">-->
        <!--<img id="home-1-lingerie" width="451" height="600" src="{{ $theme_url }}/images/home/2014-04/lingerie.jpg" alt="lingerie" style="position:absolute; top:0%; right:5%; -webkit-transform:rotate(45deg);">-->
        <div id="home-1-content" style="position:relative; z-index:1; margin-top:-97px;">
            <div id="home-1-main-content">
                <h3 style="font-size:4em; margin-bottom:.25em;">Sex ... made easier!</h3>
                <ul id="home-1-icons" style="text-align:center;">
                    <li><div style="background-position:-<?php echo 8 * $icon2 ?>px -<?php echo 3 * $icon2 ?>px;"></div><div>Games</div></li>
                    <li><div style="background-position:-<?php echo 1 * $icon2 ?>px -<?php echo 2 * $icon2 ?>px;"></div><div>Ideas</div></li>
                    <li><div style="background-position:-<?php echo 2 * $icon2 ?>px -<?php echo 2 * $icon2 ?>px;"></div><div>Tools</div></li>
                    <li><div style="background-position:-<?php echo 3 * $icon2 ?>px -<?php echo 2 * $icon2 ?>px;"></div><div>Reports</div></li>
                    <li><div style="background-position:-<?php echo 4 * $icon2 ?>px -<?php echo 2 * $icon2 ?>px;"></div><div>Role-plays</div></li>
                    <li><div style="background-position:-<?php echo 5 * $icon2 ?>px -<?php echo 2 * $icon2 ?>px;"></div><div>Services</div></li>
                </ul>
                <h2 style="font-family:arial; font-weight:300; font-size:12pt; margin:1em 0 0; opacity:.5;">The #1 Most Popular Lovemaking Suite in the Cloud</h2>
                <p style="font-family:arial; opacity:.5; font-weight:100; margin-top:0; font-size:12pt;">Used by thousands of couples for better sex</p>
                {{-- <div>
                    <a class="btn-login" href="/oauth?service=Facebook&register=1" style="background: #3b5998">
                        <i class="flaticon-facebook"></i> Sign up With Facebook
                    </a>
                    <a class="btn-login" href="/oauth?service=Google&register=1" style="background: rgb(255,0,0)">
                        <i class="flaticon-google"></i> Sign up With Google
                    </a>
                </div>
                <p style="font-size:10pt;">Or:</p>
                <form action="/register" class="inline-block" method="GET" data-parsley-validate="" novalidate="">
                    {{ csrf_field() }}
                    <div class="input-group">
                        <div class="input-group">
                            <input
                                ng-model="email"
                                type="email"
                                name="email"
                                placeholder="Enter Your Email Address"
                                class="form-control"
                                required=""
                            >
                            <button ng-disabled="!email">{{ config('site.trial_name') }}</button>
                        </div>
                    </div>
                </form> --}}
                <a class="button" style="transform:scale(1.75); margin-top: 1em;" href="/register">Play Now</a>
            </div>
            <div id="home-1-sidebar">
                <!-- Facebook like box -->
                <div id="sidebar">
                    <div style="width:195px; height:305px; background:rgb(128,0,64);">
                        <div id="fb-root"></div>
                        <script>(function(d, s, id) {
                          var js, fjs = d.getElementsByTagName(s)[0];
                          if (d.getElementById(id)) return;
                          js = d.createElement(s); js.id = id;
                          js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                          fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
                        <div style="background:white;" class="fb-like-box" data-href="https://www.facebook.com/pages/Marriedgamesorg/210751609048781" data-width="195" data-height="305" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
                        <!--<div id="subscribe" style="margin-top:2em;">
                            <h2 style="font-size:1em;">Subscribe to Sexy Tips</h2>
                            <div>
                                <p style="font-size:10pt;">Get daily ideas for improving your sex life.</p>
                                <form action="//?wpr-optin=1" method="post">
                                    <input type="hidden" name="blogsubscription" value="cat" />
                                    <input type="hidden" name="cat" value="5" />
                                    <input type="hidden" name="newsletter" value="1" />
                                    <input type="hidden" name="newsletter" value="2" />
                                    <input type="hidden" name="fid" value="2">
                                    <input style="margin-bottom:.5em;" type="text" name="name" id="name" placeholder="Your Name"><br>
                                    <input style="margin-bottom:.5em;" type="text" name="email" id="email" placeholder="Your Email"><br>
                                    <input type="submit" value="Subscribe" />
                                </form>
                            </div>
                        </div><!-- subscribe -->
                    </div>
                </div><!-- sidebar -->
            </div>
        </div>
        <div class="scroll-down scroll-1" id="take-tour">
            Take Tour<br>
            &darr;
        </div>
    </section>
</div>
<section id="home-quote">
    <div style="margin:0 auto; max-width:550px; display:table;">
        <p style="font-size:8em; display:table-cell; vertical-align:top; font-family:Georgia; padding-right:10px;">"</p>
        <p style="display:table-cell;">
            MarriedGames.org is a great site and an excellent product, the best I've seen of its kind. It offers exactly what my wife and I were looking for, and it's certainly the top among its peers. It's been a great motivator to make time. And that, as you probably know, is the biggest challenge couples face in trying to reconnect."
        </p>
    </div>
    <p style="text-align:right; max-width:550px; margin:0 auto;"><small>-- Ryan R., Santa Ana, CA</small></p>
</section>
<section id="home-2">
    <div class="content">
        <h3>Bring Back the Passion</h3>
        <p>Get all the games, tools, and resources you'll need to make it feel like the first time every time. Imagine your entire love life managed in the cloud. Think more. Think bigger.</p>
        <dl>
            <dt><strong>Learn More</strong> <div class="about"></div></dt>
            <dd>
                MarriedGames.org offers premiun online sexy and erotic games for couples to play, including board games, question games, bedroom games, and more for sizzling fun, improving communication, and strengthening marriages.
            </dd>
        </dl>
    </div>
    <img class="bg" src="{{ $theme_url }}/images/home/2014-04/home-2-bg.jpg" alt="Play Free Premium Online Sex Games for Couples">
    <div style="bottom:15%;" class="scroll-down scroll-2">
        The Apps<br>
            &darr;
    </div>
</section>
<section id="home-3" class="apps">
    <h3>Games</h3>
    <br>
    <ul>
        <li>
            <h2><span class="icon"><span style="background-position:0 -<?php echo 2 * $icon1 ?>px;"></span></span> "Lovers Lane" - the Online Sexy Board Game</h2>
            <img src="//i2.wp.com/marriedgames.org/wp-content/themes/marriedgames.org/images/home/images/home_06-e.jpg?w=960" width="400" alt="Sex games">
            <p>Take turns exchanging sexual favors as you build clubs (for flirtatious activities), spas (for relaxation and sensual activities), theaters (for visual stimulation), and hotels (for taking things all the way).</p>
        </li>
        <li>
            <h2><span class="icon"><span style="background-position:0 -<?php echo 2 * $icon1 ?>px;"></span></span> Sex Dice</h2>
            <img src="{{ $theme_url }}/images/home/2014-04/screenshot-sex-dice.jpg" width="400" alt="xxx games">
            <p>Perhaps the easiest way to add fun and surprise to the bedroom. Use the default dice to come up with out-of-the-box lovemaking activities, or customize the dice for your desires or to create your own games with.</p>
        </li>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 9 * $icon1 ?>px -<?php echo 3 * $icon1 ?>px;"></span></span> Strip Poker</h2>
            <img src="{{ $theme_url }}/images/home/2014-04/screenshot-strip-poker.png" width="400" alt="Erotic games">
            <p>No need for cards, chips, shuffling, or counting, play against each other on two laptops or mobile devices. Erotic playtime has never been easier! Try to get your spouse naked as you up the stakes with each exciting bet.</p>
        </li>
    </ul>
    <div style="text-align:center;">
        {{-- <a class="button pulse" href="/register">Play Now</a> --}}
        <a href="/register" class="button pulse"><!--<img src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" alt="{{ config('site.trial_name') }}">-->{{ config('site.trial_name') }}</a><!--<br><small>Then only 99&cent; / yr</small>--><br><!--<img class="credit-cards" width="150" height="14" src="{{ $theme_url }}/images/credit-cards.png" alt="We accept all major credit cards through PayPal">-->
    </div>
</section>
<section id="home-4" class="apps">
    <h3>Ideas</h3>
    <br>
    <ul>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 1 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Foreplay Generator</h2>
            <img src="//i2.wp.com/marriedgames.org/wp-content/themes/marriedgames.org/images/home/images/home_12-e.jpg?w=960" width="400" alt="Sex games for couples">
            <p>Every click generates a new foreplay action customized for your relationship. With hundreds of possibilities, this app alone does wonders for making sex fresh.</p>
        </li>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 8 * $icon1 ?>px -<?php echo 3 * $icon1 ?>px;"></span></span> Sexy Questions</h2>
            <img src="//i2.wp.com/marriedgames.org/wp-content/themes/marriedgames.org/images/home/images/home_12-e.jpg?w=960" width="400" alt="Adult board games">
            <p>Need help getting in the mood? Instantly draw from hundreds of sexy and intimate questions, customized for your relationship, to help improve your communication about sex and to get the romantic wheels turning.</p>
        </li>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 2 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Sex Position Generator</h2>
            <img src="//i2.wp.com/marriedgames.org/wp-content/themes/marriedgames.org/images/home/images/home_09-e.jpg?w=960" width="400" alt="Sex video games">
            <p>Break out of the old routine and explore new positions and passions for hitting the right spot. Learn from tasteful illustrations and simple instructions cusmtomized for your relationship.</p>
        </li>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 7 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Romantic Ideas</h2>
            <img src="//i2.wp.com/marriedgames.org/wp-content/themes/marriedgames.org/images/home/images/home_22-e.jpg?w=960" width="400" alt="dirty games">
            <p>With new articles posted each day, from both the MarriedGames.org team and other couples who want to share their successes, you'll never need to look to another source for date ideas, marriage advice, sex tips, and more.</p>
        </li>
    </ul>
    <div style="text-align:center;">
        {{-- <a class="button pulse" href="/register">Play Now</a> --}}
        <a href="/register" class="button pulse"><!--<img src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" alt="{{ config('site.trial_name') }}">-->{{ config('site.trial_name') }}</a><!--<br><small>Then only 99&cent; / yr</small>--><br><!--<img class="credit-cards" width="150" height="14" src="{{ $theme_url }}/images/credit-cards.png" alt="We accept all major credit cards through PayPal">-->
    </div>
</section>
<section id="home-5" class="apps">
    <h3>Tools</h3>
    <br>
    <ul>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 1 * $icon1 ?>px -<?php echo 3 * $icon1 ?>px;"></span></span> Sex Life Reports</h2>
            <img src="{{ $theme_url }}/images/home/2014-04/screenshot-reports-a.jpg" width="400" alt="sex board games">
            <p>Finally you can get the facts about your sex life. Keep an easy log of the days on which you have sex, get reminders about how long it's been, view your history, and get basic statistics. Plus many more features to come!</p>
        </li>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 6 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Romantic Diary</h2>
            <img src="//i2.wp.com/marriedgames.org/wp-content/themes/marriedgames.org/images/home/images/home-romantic-diary-a.jpg?w=960" width="400" alt="couples sex games">
            <p>Don't forget your most intimate and cherished moments! Safely record memoirs about great dates, sex, fantasies, feelings, love notes, or relationship goals. Get ideas from easy writing prompts or from what other couples are sharing.</p>
        </li>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 5 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Sexual Favor Clock</h2>
            <img src="//i2.wp.com/marriedgames.org/wp-content/themes/marriedgames.org/images/home/images/home_16-e.jpg?w=960" width="400" alt="rpg sex games">
            <p>Enjoy the Law of Karma at its finest. Every second of sensual service you give to your spouse will be tracked and rewarded with sweet paybacks. Simply select who's serving who and start the clock ticking.</p>
        </li>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 4 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Sexy Services</h2>
            <img src="//i2.wp.com/marriedgames.org/wp-content/themes/marriedgames.org/images/home/images/home_21-e.jpg?w=960" width="400" alt="Online Sexy Services game for couples">
            <p>Award each other with "love bucks," list your own sexual services and prices, and purchase your spouse's services. Additionally, you can request the erotic service you've been longing for and let your spouse set their price.</p>
        </li>
    </ul>
    <div style="text-align:center;">
        {{-- <a class="button pulse" href="/register">Play Now</a> --}}
        <a href="/register" class="button pulse"><!--<img src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" alt="{{ config('site.trial_name') }}">-->{{ config('site.trial_name') }}</a><!--<br><small>Then only 99&cent; / yr</small>--><br><!--<img class="credit-cards" width="150" height="14" src="{{ $theme_url }}/images/credit-cards.png" alt="We accept all major credit cards through PayPal">-->
    </div>
</section>
<section id="home-6" class="apps">
    <h3>Sexy Role-playing</h3>
    <br>
    <ul>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 3 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Generator</h2>
            <img src="{{ $theme_url }}/images/home/2014-04/screenshot-role-playing-generator-a.jpg" width="400" alt="Online sexy role-playing generator for couples">
            <p>Sometimes nothing is more erotic than putting yourselves in other people's shoes. With this easy generator of settings, characters, and more, the possibilities are virtually endless. You can also customize the options to meet your fantasies.</p>
        </li>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 3 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Outlines</h2>
            <img src="{{ $theme_url }}/images/home/2014-04/screenshot-role-playing-outlines-a.jpg" width="400" alt="Online sexy role-playing outlines for couples">
            <p>For when you want a tried and true plan for amazing sex, follow a simple outline. With some designed for pleasing him, some designed for pleasing her, and many more to come, you'll have no excuse for the same old.</p>
        </li>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 3 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Scripts</h2>
            <img src="{{ $theme_url }}/images/home/2014-04/screenshot-role-playing-scripts-b.jpg" width="400" alt="Online sexy role-playing scripts for couples">
            <p>Combine ultimate leisure with your ultimate sexual fantasies. Simply follow one of the sexy scripts, telling you what to say and what to do, ensuring an orgasmic ending. It doesn't get any easier than that!</p>
        </li>
    </ul>
    <div style="text-align:center;">
        {{-- <a class="button pulse" href="/register">Play Now</a> --}}
        <a href="/register" class="button pulse"><!--<img src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" alt="{{ config('site.trial_name') }}">-->{{ config('site.trial_name') }}</a><!--<br><small>Then only 99&cent; / yr</small>--><br><!--<img class="credit-cards" width="150" height="14" src="{{ $theme_url }}/images/credit-cards.png" alt="We accept all major credit cards through PayPal">-->
    </div>
</section>
<section id="home-7" class="apps">
    <h3>Member Perks</h3>
    <br>
    <ul>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 8 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Husband and Wife Store</h2>
            <img src="{{ $theme_url }}/images/home/2014-04/screenshot-husband-and-wife-store-a.jpg" width="400" alt="Online lingerie and adult novelty store without pornography">
            <p>Gain exclusive access to the Husband and Wife Store, a safe, anonymous, and clean way to buy discounted lingerie, lovemaking accessories, and more. Unlike most any other store with adult novelties, you won't see any pornography or offensive images.</p>
        </li>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 9 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Marriage and Relationship Support</h2>
            <img src="//i2.wp.com/marriedgames.org/wp-content/themes/marriedgames.org/images/home/images/home_25-f.jpg?w=960" width="400" alt="Online marriage and relationship support for couples">
            <p>Here at MarriedGames.org, we've spent a long time researching and developing solutions to help couples improve their communication and relationships, and we'd love to help you out! Get friendly, anonymous advice when you need it.</p>
        </li>
    </ul>
</section>
<section id="home-8">
    <div class="content">
        <h3>Ready to Play?</h3>
        <p>Quality bedroom games increase sexual excitement, raise dopamine levels, and strengthen romantic ties, all of which lead to stronger and more frequent orgasms and marital satisfaction. Don't miss out!</p>
        <div style="display:inline-block; text-align:center;">
            {{-- <a class="button pulse" href="/register">Play Now</a> --}}
            <a class="button cta pulse" href="/register">{{ config('site.trial_name') }}</a><br>
        </div>
    </div>
    <img class="bg" src="{{ $theme_url }}/images/home/2014-04/home-8-bg-a.jpg" alt="Bedroom games for couples">
</section>
<footer id="footer">
    <div id="footerContent">
        <nav class="left" id="footerLinks">
            <a href="//company/about/">About</a>
            <a href="//company/contact-us/">Contact</a>
            <a href="//company/privacy/">Privacy</a>
            <a href="//company/terms/">Terms</a>
            <a href="//category/romantic-ideas/">Blog</a>
            <p style="color:gray;">Copyright &copy; <?php echo date('Y') ?> MarriedGames.org</p>
        </nav><!-- footerLinks -->
        <div class="right" id="footerSocialLinks">
            <a target="_blank" href="https://www.facebook.com/pages/Marriedgamesorg/210751609048781" style="background-position:0"></a><!-- facebook  -->
            <a target="_blank" href="https://plus.google.com/b/107895751291101812066/+MarriedgamesOrg/posts" style="background-position:200px"></a><!-- google plus  -->
            <a target="_blank" href="https://twitter.com/marriedgames" style="background-position:150px"></a><!-- twitter  -->
            <a target="_blank" href="//www.pinterest.com/marriedgames/" style="background-position:100px"></a><!-- pinterest  -->
            <a target="_blank" href="//www.linkedin.com/pub/kyle-thomas/96/671/697/" style="background-position:50px"></a><!-- linkedin  -->
        </div><!-- footerSocialLinks -->
    </div><!-- footerContent -->
    <div id="popularSearches" style="overflow:hidden;">
        <h2>Popular Searches</h2>
        <ul>
            <li>
                <a href="{{ env('HOST_URL') }}/sex-games/">Sex Games</a>
            </li>
            <li>
                <a href="{{ env('HOST_URL') }}/xxx-games/">XXX Games</a>
            </li>
            <li>
                <a href="{{ env('HOST_URL') }}/love-games/">Love Games</a>
            </li>
            <li>
                <a href="{{ env('HOST_URL') }}/erotic-games">Erotic Games</a>
            </li>
            <li>
                <a href="{{ env('HOST_URL') }}/sex-games-for-couples">Sex Games for Couples</a>
            </li>
            <li>
                <a href="{{ env('HOST_URL') }}/adult-board-games/">Adult Board Games</a>
            </li>
            <li>
                <a href="{{ env('HOST_URL') }}/sex-video-games/">Sex Video Games</a>
            </li>
            <li>
                <a href="{{ env('HOST_URL') }}/dirty-games/">Dirty Games</a>
            </li>
            <li>
                <a href="{{ env('HOST_URL') }}/sex-board-games">Sex Board Games</a>
            </li>
            <li>
                <a href="{{ env('HOST_URL') }}/couples-sex-games">Couples Sex Games</a>
            </li>
            <li>
                <a href="{{ env('HOST_URL') }}/landing-pages/rpg-sex-games/">RPG Sex Games</a>
            </li>
            <li>
                <a href="{{ env('HOST_URL') }}/games/">Sexy games for couples</a>
            </li>
            <li>
                <a href="{{ env('HOST_URL') }}/games/sexy-services/">Adult board games</a>
            </li>
            <li>
                <a href="{{ env('HOST_URL') }}/ideas/browse-all-positions/">Husband and Wife Games</a>
            </li>
            <li>
                <a href="{{ env('HOST_URL') }}/sexy-games-and-tools/sex-positions/generator/">Bedroom Games</a>
            </li>
            <li>
                <a href="{{ env('HOST_URL') }}/sexy-games-and-tools/sexual-favor-clock/">Adult games for couples</a>
            </li>
            <li>
                <a href="{{ env('HOST_URL') }}/sexy-role-playing/generator/">Married sex games</a>
            </li>
            <li>
                <a href="{{ env('HOST_URL') }}/sexy-role-playing/outlines/">Games for lovers</a>
            </li>
            <li>
                <a href="{{ env('HOST_URL') }}/sexy-role-playing/scripts/">Sex games for lovers</a>
            </li>
        </ul>
    </div><!-- popularSearches -->
</footer>
<!-- free trial popup -->

<script>

    // pulse buttons

    jQuery(document).ready(function() {
        brighten();
        function brighten() {
            jQuery(".button.pulse").css("background-color", "green");
            setTimeout(function() {
                darken();
            }, 3000);
        }
        function darken() {
            jQuery(".button.pulse").css("background-color", "rgb(255,0,128)");
            setTimeout(function() {
                brighten();
            }, 3000);
        }
    });

    // toggle definition list items

    jQuery("body:not(.page-id-1870) dt").click(function() {
        jQuery(this).next("dd").slideToggle();
    });

    // size container to window height

    function resizeWindow() {
        var windowHeight = jQuery(window.top).height();
        jQuery("#home-1").css("height", windowHeight);
    }
    if (jQuery(window).height() >= 500) resizeWindow();
    jQuery(window).resize(function() {
        if (jQuery(window).height() >= 500) resizeWindow();
    });

    jQuery(document).ready(function (){
        jQuery(".scroll-1").click(function (){
            jQuery('html, body').animate({
                scrollTop: jQuery("#home-quote").offset().top
            }, 1000);
        });
        jQuery(".scroll-2").click(function (){
            jQuery('html, body').animate({
                scrollTop: jQuery("#home-3").offset().top
            }, 1000);
        });
    });

    // pop

    function pop(content) {
        jQuery("body").prepend("<div class='black'></div><div class='pop'><div class='x'>&times;</div>" + content + "</div>");
        jQuery(".black, .pop").fadeIn(function() {
            jQuery(".black, .x").click(function() {
                jQuery(".black, .pop").fadeOut(function() {
                    closePopup();
                });
            });
        });
    }
    function closePopup() {
        jQuery(".black, .pop").fadeOut(function() {
            jQuery(".black, .pop").remove();
        });
    }
    jQuery(".closePopup").click(function() {
        closePopup();
    });

    // popbox
    function popbox(content) {
        var dark;
        if (jQuery(".dark").length == 0) dark = "<div class='dark'></div>";
        else dark = "";
        var popbox = dark + "<div class='popboxWrapperWrapper'><div class='popboxWrapper'><div class='popboxContainer'><div class='popbox'>" + content + "</div></div></div></div>";
        if (jQuery("popboxWrapperWrapper").length > 0) jQuery(".popboxWrapperWrapper").after(popbox);
        else jQuery("body").prepend(popbox);
        jQuery(".dark, .popboxContainer").fadeIn(function() {
            jQuery(".close").click(function() {
                closePopbox();
            });
            jQuery(".dark").click(function() {
                if (!(jQuery(this).hasClass("static"))) closePopbox();
            });
        });
    }
    function closePopbox() {
        if (jQuery(".popboxWrapperWrapper").length == 1) jQuery(".dark").fadeOut(function() {
            jQuery(this).remove();
        });
        if (jQuery(".popboxWrapperWrapper").length > 1) {
            jQuery(".popboxWrapperWrapper:last").fadeOut(function() {
                jQuery(this).remove();
            });
            if (jQuery(".popboxWrapperWrapper").length == 1) jQuery(".dark").fadeOut(function() {
                jQuery(this).remove();
            });
        }
        else {
            jQuery(".popboxWrapperWrapper").fadeOut(function() {
                jQuery(this).remove();
            });
        }
    }
    // end popbox

</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-43056743-1', 'marriedgames.org');
  ga('send', 'pageview');

</script>
@stop
