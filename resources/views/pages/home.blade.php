@extends('layouts.pages')
@section('content')
    <?php
        $icon1 = 32;
        $icon2 = 48;
        $theme_url = env('HOST_URL') . "/wp-content/themes/" . env('HOST_THEME');
    ?>
<div ng-controller="HomeController" id="home">
    <img id="home-hero" class="showable-md" src="/img/home/home-1-sm.jpg">
    <div id="home-1-1">
        <img
            id="logo"
            style="max-width: 200px;"
            src="/img/logo-thick.png"
            alt="MarriedGames.org"
            class="pull-left"
        >
        <a
            href="/login"
            class="pull-right btn-sm"
        >Sign In</a>
    </div>
    <section id="home-1" class="flex">
        <div id="home-1-2">
            <h1>Games for Lovers.</h1>
            <h2>Strengthen Your Marriage. Have fun.</h2>
            <a href="/register" class="btn">Play Now</a>
        </div>
    </section>
    <div ng-cloak class="tabs-content">
        <div class="align-center">
            <div class="flex" style="align-items: center;">
                <h3 class="align-left">
                    Spice up your sex life, improve communication, bring back the passion.
                </h3>
                {{-- <a href="/register" class="btn" style="min-width: 267px;">{{ config('site.trial_name') }}</a> --}}
            </div>
            <div class="clear"></div>
            <br>
            <div class="flex" id="home-features">
                <div>
                    <img src="/img/home/feature-games.jpg">
                    <h4>Games</h4>
                    <p>Board game, sex dice, strip poker, favor clock, sexual services, and more.</p>
                </div>
                <div>
                    <img src="/img/home/feature-ideas.jpg">
                    <h4>Ideas</h4>
                    <p>Foreplay and sex position generators, role-playing scripts, advice, and more.</p>
                </div>
                <div>
                    <img src="/img/home/feature-reports.jpg">
                    <h4>Reports</h4>
                    <p>Set Goals, track progress, improve communication, enjoy a better love life.</p>
                </div>
            </div>
            <p ng-init="learnMore = false" ng-show="!learnMore">
                <a ng-click="learnMore = !learnMore">Learn More</a>
            </p>
            <div ng-show="learnMore" style="max-width: 600px; margin: 0 auto;">
                <section>
                    <section>
                        <h3>Games</h3>
                        <br>
                        <ul>
                            <li>
                                <h2><span class="icon"><span style="background-position:0 -<?php echo 2 * $icon1 ?>px;"></span></span> "Lovers Lane" - the Online Sexy Board Game</h2>
                                <img src="//i2.wp.com/marriedgames.org/wp-content/themes/marriedgames.org/images/home/images/home_06-e.jpg?w=960" width="400" alt="Sex games">
                                <p>Take turns exchanging sexual favors as you build clubs (for flirtatious activities), spas (for relaxation and sensual activities), theaters (for visual stimulation), and hotels (for taking things all the way).</p>
                            </li>
                            <li>
                                <h2><span class="icon"><span style="background-position:0 -<?php echo 2 * $icon1 ?>px;"></span></span> Sex Dice</h2>
                                <img src="{{ $theme_url }}/images/home/2014-04/screenshot-sex-dice.jpg" width="400" alt="xxx games">
                                <p>Perhaps the easiest way to add fun and surprise to the bedroom. Use the default dice to come up with out-of-the-box lovemaking activities, or customize the dice for your desires or to create your own games with.</p>
                            </li>
                            <li>
                                <h2><span class="icon"><span style="background-position:-<?php echo 9 * $icon1 ?>px -<?php echo 3 * $icon1 ?>px;"></span></span> Strip Poker</h2>
                                <img src="{{ $theme_url }}/images/home/2014-04/screenshot-strip-poker.png" width="400" alt="Erotic games">
                                <p>No need for cards, chips, shuffling, or counting, play against each other on two laptops or mobile devices. Erotic playtime has never been easier! Try to get your spouse naked as you up the stakes with each exciting bet.</p>
                            </li>
                        </ul>
                        <div style="text-align:center;">
                            <a href="/register" class="btn">{{ config('site.trial_name') }}</a>
                        </div>
                    </section>
                    <section>
                        <h3>Ideas</h3>
                        <br>
                        <ul>
                            <li>
                                <h2><span class="icon"><span style="background-position:-<?php echo 1 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Foreplay Generator</h2>
                                <img src="//i2.wp.com/marriedgames.org/wp-content/themes/marriedgames.org/images/home/images/home_02-e.jpg?w=960" width="400" alt="Sex games for couples">
                                <p>Every click generates a new foreplay action customized for your relationship. With hundreds of possibilities, this app alone does wonders for making sex fresh.</p>
                            </li>
                            <li>
                                <h2><span class="icon"><span style="background-position:-<?php echo 8 * $icon1 ?>px -<?php echo 3 * $icon1 ?>px;"></span></span> Sexy Questions</h2>
                                <img src="//i2.wp.com/marriedgames.org/wp-content/themes/marriedgames.org/images/home/images/home_12-e.jpg?w=960" width="400" alt="Adult board games">
                                <p>Need help getting in the mood? Instantly draw from hundreds of sexy and intimate questions, customized for your relationship, to help improve your communication about sex and to get the romantic wheels turning.</p>
                            </li>
                            <li>
                                <h2><span class="icon"><span style="background-position:-<?php echo 2 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Sex Position Generator</h2>
                                <img src="/img/home/home_09-e.jpg?w=960" width="400" alt="Sex video games">
                                <p>Break out of the old routine and explore new positions and passions for hitting the right spot. Learn from tasteful illustrations and simple instructions cusmtomized for your relationship.</p>
                            </li>
                            <li>
                                <h2><span class="icon"><span style="background-position:-<?php echo 7 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Romantic Ideas</h2>
                                <img src="//i2.wp.com/marriedgames.org/wp-content/themes/marriedgames.org/images/home/images/home_22-e.jpg?w=960" width="400" alt="dirty games">
                                <p>With new articles posted each day, from both the MarriedGames.org team and other couples who want to share their successes, you'll never need to look to another source for date ideas, marriage advice, sex tips, and more.</p>
                            </li>
                        </ul>
                        <div style="text-align:center;">
                            <a
                            href="/register"
                            class="btn"
                            >{{ config('site.trial_name') }}</a>
                        </div>
                    </section>
                    <section>
                        <h3>Tools</h3>
                        <br>
                        <ul>
                            <li>
                                <h2><span class="icon"><span style="background-position:-<?php echo 1 * $icon1 ?>px -<?php echo 3 * $icon1 ?>px;"></span></span> Sex Life Reports</h2>
                                <img src="{{ $theme_url }}/images/home/2014-04/screenshot-reports-a.jpg" width="400" alt="sex board games">
                                <p>Finally you can get the facts about your sex life. Keep an easy log of the days on which you have sex, get reminders about how long it's been, view your history, and get basic statistics. Plus many more features to come!</p>
                            </li>
                            <li>
                                <h2><span class="icon"><span style="background-position:-<?php echo 6 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Romantic Diary</h2>
                                <img src="//i2.wp.com/marriedgames.org/wp-content/themes/marriedgames.org/images/home/images/home-romantic-diary-a.jpg?w=960" width="400" alt="couples sex games">
                                <p>Don't forget your most intimate and cherished moments! Safely record memoirs about great dates, sex, fantasies, feelings, love notes, or relationship goals. Get ideas from easy writing prompts or from what other couples are sharing.</p>
                            </li>
                            <li>
                                <h2><span class="icon"><span style="background-position:-<?php echo 5 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Sexual Favor Clock</h2>
                                <img src="//i2.wp.com/marriedgames.org/wp-content/themes/marriedgames.org/images/home/images/home_16-e.jpg?w=960" width="400" alt="rpg sex games">
                                <p>Enjoy the Law of Karma at its finest. Every second of sensual service you give to your spouse will be tracked and rewarded with sweet paybacks. Simply select who's serving who and start the clock ticking.</p>
                            </li>
                            <li>
                                <h2><span class="icon"><span style="background-position:-<?php echo 4 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Sexy Services</h2>
                                <img src="//i2.wp.com/marriedgames.org/wp-content/themes/marriedgames.org/images/home/images/home_21-e.jpg?w=960" width="400" alt="Online Sexy Services game for couples">
                                <p>Award each other with "love bucks," list your own sexual services and prices, and purchase your spouse's services. Additionally, you can request the erotic service you've been longing for and let your spouse set their price.</p>
                            </li>
                        </ul>
                        <div style="text-align:center;">
                            <a href="/register" class="btn">{{ config('site.trial_name') }}</a>
                        </div>
                    </section>
                    <section>
                        <h3>Sexy Role-playing</h3>
                        <br>
                        <ul>
                            <li>
                                <h2><span class="icon"><span style="background-position:-<?php echo 3 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Generator</h2>
                                <img src="{{ $theme_url }}/images/home/2014-04/screenshot-role-playing-generator-a.jpg" width="400" alt="Online sexy role-playing generator for couples">
                                <p>Sometimes nothing is more erotic than putting yourselves in other people's shoes. With this easy generator of settings, characters, and more, the possibilities are virtually endless. You can also customize the options to meet your fantasies.</p>
                            </li>
                            <li>
                                <h2><span class="icon"><span style="background-position:-<?php echo 3 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Outlines</h2>
                                <img src="{{ $theme_url }}/images/home/2014-04/screenshot-role-playing-outlines-a.jpg" width="400" alt="Online sexy role-playing outlines for couples">
                                <p>For when you want a tried and true plan for amazing sex, follow a simple outline. With some designed for pleasing him, some designed for pleasing her, and many more to come, you'll have no excuse for the same old.</p>
                            </li>
                            <li>
                                <h2><span class="icon"><span style="background-position:-<?php echo 3 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Scripts</h2>
                                <img src="{{ $theme_url }}/images/home/2014-04/screenshot-role-playing-scripts-b.jpg" width="400" alt="Online sexy role-playing scripts for couples">
                                <p>Combine ultimate leisure with your ultimate sexual fantasies. Simply follow one of the sexy scripts, telling you what to say and what to do, ensuring an orgasmic ending. It doesn't get any easier than that!</p>
                            </li>
                        </ul>
                        <div style="text-align:center;">
                            <a href="/register" class="btn">{{ config('site.trial_name') }}</a>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
