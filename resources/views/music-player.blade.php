@if (auth()->check() && auth()->user()->hasRole(['Member']))
    <div id="audioPlayer" class="align-center">
        <audio
            id="audioPlayerAudio"
            autoplay
            preload="none"
        >
        </audio>
        <div class="btn-group">
            {{-- <button class="btn btn-default"><i class="fa fa-info-circle"></i></button> --}}
            <button class="btn btn-default" id="audioControl"><i class="fa fa-pause"></i></button>
            <button class="btn btn-default" id="next"><i class="fa fa-fast-forward" onclick="setSrc()"></i></button>
        </div>
    </div>
    <script>

        var playedTracks = [];

        var tracks = [
            'https://www.dropbox.com/s/ma4icnyxbes9lu2/3%20HOURS%20Best%20Relaxing%20Music%20MIX%20Deep%20LOUNGE%20CHILL%20OUT%20HOUSE%20Background%20Relax%20Sleep%20Study%20Meditation.mp3?dl=1',
            'https://www.dropbox.com/s/6vjmfru9vsog3to/3%20HOURS%20Relaxing%20Saxophone%20Music%20%20Colours%20of%20Fall%20%20Background%20for%20Healing%20Love%20Date%20Massage.mp3?dl=1',
            'https://www.dropbox.com/s/6vjmfru9vsog3to/3%20HOURS%20Relaxing%20Saxophone%20Music%20%20Colours%20of%20Fall%20%20Background%20for%20Healing%20Love%20Date%20Massage.mp3?dl=1',
            'https://www.dropbox.com/s/5r96xyuh12nql77/Beautiful%20Relaxing%20Music%20%20Romantic%20Music%20Piano%20Music%20Violin%20Music%20Cello%20Music%20Sleep%20Music%20%E2%98%8593.mp3?dl=1',
            'https://www.dropbox.com/s/r1hge9h0ddqallb/Beautiful%20Romantic%20Music%20%20Relaxing%20Music%20Piano%20Music%20Violin%20Music%20Guitar%20Music%20Sleep%20Music%20%E2%98%85101.mp3?dl=1',
            'https://www.dropbox.com/s/sn4idl19bi4j4e3/Country%20Folk%20Music%20Romantic%20Instrumental.mp3?dl=1',
            'https://www.dropbox.com/s/lakb9qjgdmv540j/Instrumental%20Jazz%20for%20Making%20love%20instrumental%202017%20Bakground%20Saxo%20Music%20TVWorldRelax.mp3?dl=1',
            'https://www.dropbox.com/s/jl0qvkpb0gw1iz8/Instrumental%20Music%20-%20Mix%20%28Smooth%20Jazz%20-%20Funk%20-%20Fusion%20-%20Chill%20Out%20-%20Lounge%20-%20Piano%29.mp3?dl=1',
            'https://www.dropbox.com/s/w1dwd249mr5wcky/SEXY%20%E2%80%93%20INSTRUMENTAL%20DE%20RAP%20%20USO%20LIBRE%20%20BASE%20DE%20RAP%20%20HIP%20HOP%20BEAT%20INSTRUMENTAL.mp3?dl=1',
            'https://www.dropbox.com/s/x4lro0jfdcxk2st/Song%20instrumentals%20film%20romantic.mp3?dl=1'
        ];

        function setSrc()
        {
            if (playedTracks.length > 0) {
                var oldSrc = $('#audioPlayerAudio').attr('src');
                var newSrc = oldSrc;
                var rand = tracks.indexOf(oldSrc);
                while (playedTracks.includes(rand)) {
                    rand = Math.floor(Math.random() * tracks.length);
                }
            } else {
                var rand = Math.floor(Math.random() * tracks.length);
            }
            $('#audioPlayerAudio').attr('src', tracks[rand])
            playedTracks.push(rand);
            if (playedTracks.length == tracks.length) {
                playedTracks = [];
            }
        }
        setSrc();

        var audioPlayerAudio = document.getElementById('audioPlayerAudio'),
            ctrl = document.getElementById('audioControl');

        ctrl.onclick = function () {

            // Update the Button
            var pause = ctrl.innerHTML === '<i class="fa fa-pause"></i>';
            ctrl.innerHTML = pause ? '<i class="fa fa-play"></i>' : '<i class="fa fa-pause"></i>';

            // Update the Audio
            var method = pause ? 'pause' : 'play';
            audioPlayerAudio[method]();

            // Prevent Default Action
            return false;
        };
    </script>
@endif
