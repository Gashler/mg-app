@extends('layouts.default')
@section('style')
	{{ HTML::style('css/lovers-lane.css') }}
@stop
@section('content')
	<div ng-controller="loversLaneController as game">
		<h1>@{{game.title}}</h1>
		<div id="game">
			<table class="male">
				<tbody>
					<tr>
						<th>{{ Auth::user()->first_name }}'s Money</th>
						<th>{{ Auth::user()->spouse->first_name }}'s Money</th>
					</tr>
					<tr>
						<td>$<span class="money m">2400</span></td>
						<td>$<span class="money f">2400</span></td>
					</tr>
				</tbody>
			</table>
			<div></div>
			<table id="board" cellspacing="1">
				<tbody>
					<tr class="row" id="r1">
						<td class="spaceContainer go corner">
							<div  class="space go current" id="1" data-category="club">
								<div class="player m">
									<div class="piece m">
										<div class="piece-content">
											<i class="flaticon-male244"></i>
										</div>
									</div>
									<div class="shadow"></div>
								</div>
								<div class="player f">
									<div class="piece f">
										<div class="piece-content">
											<i class="flaticon-female243"></i>
										</div>
									</div>
									<div class="shadow"></div>
								</div>								
								<div class="label special">Go</div><br />
							</div>
						</td>
						<td class="spaceContainer">
							<div class="space" id="2" data-cost="2" data-category="club">
								<div class="label">Bawdy Boulevard</div>
								<div class="cost">$200</div>
							</div>
						</td>
						<td class="spaceContainer">
							<div class="space" id="3" data-cost="2" data-category="club">
								<div class="label">Horny Hollows</div>
								<div class="cost">$200</div>
							</div>
						</td>
						<td class="spaceContainer">
							<div class="space" id="4" data-cost="3" data-category="club" data-category="club">
								<div class="label">Lovers Lane</div>
								<div class="cost">$300</div>
							</div>
						</td>
						<td class="spaceContainer strip corner">
							<div class="space strip" id="5">
								<div class="label special">Strip</div><br />
							</div>
						</td>
					</tr><!-- r1 -->
					<tr class="row">
						<td class="spaceContainer">
							<div class="space" id="16" data-cost="9" data-category="hotel">
								<div class="label">Sexy Street</div>
								<div class="cost">$900</div>
							</div>
						</td>
						<td colspan="3" rowspan="3" id="centerContentContainer">
							<div id="centerContent">
								<div class="turn" ng-class="game.turn">
									<div ng-show="!allow_rolling" ng-bind="game.player.first_name + '\'s turn'"></div>
									<div ng-show="allow_rolling" class="btn btn-default" ng-click="roll()" ng-bind="game.player.first_name + '\'s turn'">Roll</div>
								</div>
								<div id="text"></div>
							</div>
						</td>
						<td class="spaceContainer">
							<div class="space" id="6" data-cost="4" data-category="spa">
								<div class="label">Arousal Avenue</div>
								<div class="cost">$400</div>
							</div>
						</td>
					</tr>
					<tr class="row" id="r3">
						<td class="spaceContainer">
							<div class="space" id="15" data-cost="8" data-category="hotel">
								<div class="label">Luscious Lounge</div>
								<div class="cost">$800</div>
							</div>
						</td>
						<td class="spaceContainer">
							<div class="space" id="7" data-cost="4" data-category="spa">
								<div class="label">Erotic Environment</div>
								<div class="cost">$400</div>
							</div>
						</td>
					</tr><!-- r3 -->
					<tr class="row" id="r4">
						<td class="spaceContainer">
							<div class="space" id="14" data-cost="8" data-category="hotel">
								<div class="label">Lustful Lane</div>
								<div class="cost">$800</div>
							</div>
						</td>
						<td class="spaceContainer">
							<div class="space" id="8" data-cost="5" data-category="spa">
								<div class="label">Flirtatious Flats</div>
								<div class="cost">$800</div>
							</div>				
						</td>
					</tr><!-- r5 -->
					<tr class="row" id="r5">
						<td class="spaceContainer bankrupt corner">
							<div class="space bankrupt" id="13">
								<div class="label special">Bankrupt</div><br />
							</div>
						</td>
						<td class="spaceContainer">
							<div class="space" id="12" data-cost="7" data-category="theater">
								<div class="label">Provocative Parkway</div>
								<div class="cost">$700</div>
							</div>			
						</td>
						<td class="spaceContainer">
							<div class="space" id="11" data-cost="6" data-category="theater">
								<div class="label">Voluptuous Valley</div>
								<div class="cost">$600</div>
							</div>
						</td>
						<td class="spaceContainer">
							<div class="space" id="10" data-cost="6" data-category="theater">
								<div class="label">Romantic Road</div>
								<div class="cost">$600</div>
							</div>
						</td>
						<td class="spaceContainer jail corner">
							<div class="space jail" id="9">
								<div class="label special">Jail</div><br />
							</div>
						</td>
					</tr><!-- r5 -->
				</tbody>
			</table><!-- board -->
		</div><!-- game -->
		<div>
			<button class="small" id="save">Save Game</button>
			<button class="small" id="reset">Reset Game</button>
		</div>
		<div id="game-sidebar">
			<div class="meter">
				<strong>Heat</strong>
				<div>Orgasmic</div>
				<div>Passionate</div>
				<div>Erotic</div>
				<div>Sexy</div>
				<div class="active">Warming Up</div>
			</div>
		</div>
	</div><!-- game-container -->
@stop
@section('scripts')
	<script src="/js/controllers/loversLaneController.js"></script>
@stop