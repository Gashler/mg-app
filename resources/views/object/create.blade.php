@extends('layouts.default')
@section('content')
	<div class="create">
		<div class="row">
			<div class="col col-md-12">
				@include('_helpers.breadcrumbs')
			    <h1 class="no-top">New Object</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
			    {{ Form::open(array('url' => 'object')) }}
	
				    
				    <div class="form-group">
				        {{ Form::label('name', 'Name') }}
<input type="text" ng-model="obj.name" class="form-control" ng-blur="update()">
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('morph_type', 'morph Type') }}
<input type="text" ng-model="obj.morph_type" class="form-control" ng-blur="update()">
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('type', 'Type') }}
<input type="text" ng-model="obj.type" class="form-control" ng-blur="update()">
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('description', 'Description') }}
<input type="text" ng-model="obj.description" class="form-control" ng-blur="update()">
				    </div>
				    
				    <div class="form-group">
				        {{ Form::label('morph_id', 'morph Id') }}
<input type="text" ng-model="obj.morph_id" class="form-control" ng-blur="update()">
				    </div>
				    
	
				    {{ Form::submit('Add Object', array('class' => 'btn btn-primary')) }}
	
			    {{ Form::close() }}
		    </div>
		</div>
	</div>
@stop
