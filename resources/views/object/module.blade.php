<div class="form-group" ng-controller="ObjectController">
    {{ Form::label('objects', 'Objects') }}
    <ul class="list-group" ng-if="obj.objects.length > 0">
        <li ng-repeat="object in obj.objects" class="list-group-item">
            <span ng-click="editObject(obj.id)" class="link" ng-bind="obj.name"></span>
            <i class="link flaticon-x pull-right" ng-click="deleteObject(obj.id, $index)"></i>
        </li>
    </ul>
    <button type="button" class="btn btn-default" ng-click="createObject()"><i class="flaticon-plus"></i> Add Object</button>
    <img ng-if="loading_object" src="/img/loading.gif">
    <div id="objectForm" class="popup">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Create Object<span class="x pull-right" ng-click="togglePopup('#objectForm')">&times;</span></h3>
            </div>
            <div class="panel-body">

                <div class="form-group">
                    {{ Form::label('name', 'Name') }}
<input type="text" ng-model="obj.name" class="form-control" ng-blur="update()">
                </div>

                <div class="form-group">
                    {{ Form::label('description', 'Description') }}
<textarea ng-model="obj.description" class="form-control" ng-blur="update()"></textarea>
                </div>

                <div class="form-group">
                    <label>
                        <input type="checkbox" ng-model="obj.takeable" ng-blur="updateObject()">
                        Takeable
                    </label>
                </div>

                <button type="button" class="btn btn-default" ng-click="togglePopup('#objectForm')">Close</button>

            </div><!-- panel-body -->
        </div><!-- panel -->
    </div><!-- objectForm -->
</div>
<script src="/js/controllers/ObjectController.js"></script>
