<div class="form-group">
    {{ Form::label('name', 'Name') }}
<input type="text" ng-model="obj.name" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('description', 'Description') }}
<textarea ng-model="obj.description" class="form-control" ng-blur="update()"></textarea>
</div>

<div class="form-group">
    <label>
        <input type="checkbox" ng-model="o.takeable" ng-blur="updateObject()">
        Takeable
    </label>
</div>

<!-- <div class="form-group">
    {{ Form::label('morph_id', 'morph Id') }}
<input type="text" ng-model="obj.morph_id" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('morph_type', 'morph Type') }}
<input type="text" ng-model="obj.morph_type" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('type', 'Type') }}
<input type="text" ng-model="obj.type" class="form-control" ng-blur="update()">
</div> -->
