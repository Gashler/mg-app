@extends('layouts.default')
@section('content')
	<div class="edit">
		<div class="row">
			<div class="col col-md-12">
				@include('_helpers.breadcrumbs')
			    <h1>Edit Col</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
			    {{ Form::model($col, array('route' => array('col.update', $col->id), 'method' => 'PUT')) }}
	
				    @include(col.form)
	
			    	{{ Form::submit('Update Col', array('class' => 'btn btn-primary')) }}
	
			    {{Form::close()}}
			</div>
		</div>
	</div>
@stop
@section('scripts')
	<script>
		col_id = {{ $model->id }}
	</script>
	<script src="/js/controllers/ColController.js"></script>
@stop