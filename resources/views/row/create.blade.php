@extends('layouts.default')
@section('content')
	<div class="create">
		<div class="row">
			<div class="col col-md-12">
				@include('_helpers.breadcrumbs')
			    <h1 class="no-top">New Row</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
			    {{ Form::open(array('url' => 'row')) }}
	
				    
				    <div class="form-group">
				        {{ Form::label('game_id', '[formatted_property]') }}
<input type="text" ng-model="obj.game_id" class="form-control" ng-blur="update()">
				    </div>
				    
	
				    {{ Form::submit('Add Row', array('class' => 'btn btn-primary')) }}
	
			    {{ Form::close() }}
		    </div>
		</div>
	</div>
@stop
@section('scripts')
	<script src="/js/controllers/RowController.js"></script>
@stop