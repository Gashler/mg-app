@extends('layouts.default')
@section('content')
	<div class="edit">
		<div class="row">
			<div class="col col-md-12">
				@include('_helpers.breadcrumbs')
			    <h1>Edit Row</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
			    {{ Form::model($row, array('route' => array('row.update', $row->id), 'method' => 'PUT')) }}
	
				    @include(row.form)
	
			    	{{ Form::submit('Update Row', array('class' => 'btn btn-primary')) }}
	
			    {{Form::close()}}
			</div>
		</div>
	</div>
@stop
@section('scripts')
	<script>
		row_id = {{ $model->id }}
	</script>
	<script src="/js/controllers/RowController.js"></script>
@stop