<?php
    $icon1 = 32;
    $icon2 = 48;


    // get offer

    // $offer = $_GET['offer'];

?>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300' rel='stylesheet' type='text/css'>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<style>

    @font-face {
        font-family: sexy;
        src: url("{{ env('HOST_URL') }}/wp-content/themes/{{ env('HOST_THEME') }}/css/mountains-of-christmas.eot") /* EOT file for IE */
    }
    @font-face {
        font-family: sexy;
        src: url("{{ env('HOST_URL') }}/wp-content/themes/{{ env('HOST_THEME') }}/css/mountains-of-christmas.woff") /* TTF file for CSS3 browsers */
    }
    html { background:rgb(128,0,64); }
    body { font-family:Open Sans, Sans; margin:0; color:white; font-size:14pt; }
    p { font-weight:300; }
    a { color:white; }
    h1 { margin:0; }
    h3 { font-family:sexy; font-size:64px; font-weight:300; margin:0; line-height:1em; }
    h2 { font-family:sexy; font-size:32px; font-weight:100; margin:0 0 2%; }
    h2 a { text-decoration:none; }
    h2 a:hover { text-decoration:underline; }

    @media (max-width:800px) {
        h3 { font-size:48px; }
        h2 { font-size:24px; }
        p { font-size:12pt; }
    }

    .about { background:url("{{ env('HOST_URL') }}/wp-content/themes/{{ env('HOST_THEME') }}/images/icons.png"); width:25px; height:25px; background-size:250px 250px; background-position:-25px -25px; display:inline-block; vertical-align:middle; margin-left:.25em; }
    .cents { font-size:8pt; display:inline-block; position:relative; top:-5px; }

    #logo { position:relative; z-index:1; }
    #header { position:absolute; top:0; right:0; left:0; background:rgba(0,0,0,.25); padding:20px 30px; overflow:hidden; }

    @media (max-width:800px) {
        #logo { max-width:350px; width:100%; }
    }
    @media (max-width:550px) {
        #header { position:static; }
    }

    button, .button, #wrapper input[type="submit"] {
        display: inline-block;
        padding: 10px 15px !important;
        font-size: 1em !important;
        border: 0 !important;
        background: rgb(255,0,128);
        border-radius: 2px !important;
        color: white !important;
        font-weight: 400 !important;
        box-shadow: 1px 1px 3px rgba(0,0,0,.5);
        cursor: pointer !important;
        text-decoration:none !important;
        margin-bottom:.5em;
        transition: background-color 3s;
    }

    button:disabled, .button:disabled, #wrapper input[type="submit"]:disabled, button:disabled:hover, button:disabled:hover, #wrapper input[type="submit"]:disabled:hover {
        background: rgb(192,192,192) !important;
        color:gray !important;
        border:none !important;
        box-shadow:none !important;
        cursor:auto !important;
        text-shadow:none !important;
    }

    button:hover, .button:hover, .button-small:hover, #wrapper input[type="submit"]:hover {
        box-shadow:1px 1px 3px rgba(0,0,0,.5), 0 0 40px rgba(255,200,225, .5) inset !important;
        text-decoration:none !important;
        color:white !important !important;
    }

    button:active, .button:active, .button-small:active, #wrapper input[type="submit"]:active {
        background: rgb(192,0,96) !important;
        box-shadow: 2px 2px 4px rgba(0,0,0,.5) inset !important;
    }

    .button.green {
        background:#a1ff00 !important;
        color:black !important;
        font-weight:bold;
    }

    /* home sections */

    section { position:relative; }

    /* home-1 */

    #home-1 { display:table-cell; vertical-align:middle; text-align:center; width:100%; }
    #home-1-icons { padding:0; }
    #home-1-icons li { margin:0; padding:0; opacity:.5; display:inline-block; vertical-align:top; text-align:center; }
    #home-1-icons li div:first-child { background:rgb(255,0,128); border-radius:50%; border:10px solid rgb(255,0,128); box-shadow:1px 1px 3px rgba(0,0,0,.5); height:<?php echo $icon2 ?>px; width:<?php echo $icon2 ?>px; background-image:url("{{ env('HOST_URL') }}/wp-content/themes/{{ env('HOST_THEME') }}/images/icons.png"); background-size:480px 480px; }
    #home-1-icons li div:nth-child(2) { opacity:.5; font-size:10pt; }
    #home-1-main-content { display:inline-block; vertical-align:top; margin-right:40px; margin-left:235px; }
    #home-1-sidebar { display:inline-block; vertical-align:top; }

    /*@media (max-width:1100px) {
        #home-1-lingerie { width:200px; }
        #home-1-dice { width:200px; bottom:15% !important; left:10% !important; }
    }*/

    @media (max-width:1000px) {
        #home-1-main-content { margin:0; }
        #home-1-sidebar { display:none; }
    }

    @media (max-width:500px) {
        #home-1 { height:auto !important; padding-bottom:10%; }
        #header { position:static !important; background:rgb(255,0,128); text-align:center; }
        #header > a { float:none !important; }
        h1 { position:static !important; }
        /*#home-1-lingerie, #home-1-dice { display:none; }*/
        #home-1-content { margin-top:0 !important; }
        #home-1-content h3 { margin-top:5% !important; background:none; }
        .scroll-down { display:none; }
        section:nth-of-type(odd) h3 { background:rgb(150,0,75); padding:5%; border-radius:2%; }
        section:nth-of-type(even) h3 { background:rgb(96,0,48); padding:5%; border-radius:2%; }
    }

    /* end home-1 */

    /* scroll down */

    .scroll-down { cursor:default; transition:background 1s; position:absolute; bottom:10%; right:2.5%; padding:60px 50px; font-size:2em; font-family:sexy; background:rgb(64,0,32); border-radius:50%; text-align:center; }
    .scroll-down:hover { background:rgb(192,0,96); cursor:pointer; }

    @media (max-width:800px) {
        .scroll-down { font-size:14pt; padding:30px 25px; }
    }

    /* end take tour */

    /* home quote */

    #home-quote { padding:5%; }

    /* end home quote */

    /* home-2 */

    #home-2 .bg { width:100%; }
    #home-2 .content { position:absolute; top:15%; right:7.5%; max-width:550px; color:rgb(128,0,64); }
    @media (max-width:1450px) {
        #home-2 .bg { position:static; }
        #home-2 .content { position:static; color:white; max-width:none; text-align:center; background:rgb(255,0,128); padding:5%; }
        #home-2 .content p { max-width:550px; margin:1em auto 0; }
    }

    /* end home-2 */

    /* apps */

    .apps:nth-of-type(odd) { background:rgb(192,0,96); }
    .apps { padding:5%; }
    .apps > ul { margin:0; padding:0; }
    .apps > ul > li { list-style:none; margin-bottom:5%; max-width:750px; display:inline-block; vertical-align:top; }
    .apps > ul > li:nth-of-type(odd) { margin-right:5%; }
    .apps > ul > li .icon { vertical-align:middle; background:rgb(255,0,128); border-radius:50%; padding:2px 6px; border:5px solid rgb(255,0,128); box-shadow:1px 1px 3px rgba(0,0,0,.5); display:inline-block; }
    .apps > ul > li .icon > span { display:inline-block; margin-bottom:-7px; background:rgb(255,0,128); height:<?php echo $icon1 ?>px; width:<?php echo $icon1 ?>px; background-image:url("{{ env('HOST_URL') }}/wp-content/themes/{{ env('HOST_THEME') }}/images/icons.png"); background-size:320px 320px; }
    .apps > ul > li > img { float:left; margin:0 3% 3% 0; width:100%; max-width:400px; }

    @media (max-width:800px) {
        .apps > ul { text-align:center; }
        .apps > ul > li { text-align:left; }
        .apps > ul > li > img { display:block; float:none; }
        .apps > ul > li > p { max-width:400px; }
    }

    /* end apps */

    /* home-8 */

    #home-8 .bg { width:100%; }
    #home-8 .content { position:absolute; top:15%; right:7.5%; max-width:350px; color:white; }

    @media (max-width:950px) {
        #home-8 .bg { position:static; }
        #home-8 .content { position:static; color:white; max-width:none; text-align:center; background:rgb(255,0,128); padding:5%; }
        #home-8 .content p { max-width:550px; margin:1em auto 0; }
        #home-8 .content .button { background:#a1ff00 !important; color:black !important; }
    }

    /* end home-8 */

    /* footer */

    #footer { background:rgb(50,50,50); text-align:center; margin-top:-10px; font-size:10pt; padding:2.5%; }
    #footerContent { overflow:hidden; max-width:1100px; margin:0 auto; }
    #footerLinks a { color:white !important; margin-right:1em; text-decoration:none; }
    #footerLinks a:hover { color:#8295cc !important; }
    #footerSocialLinks a { display:inline-block; width:50px; height:50px; background:url("{{ env('HOST_URL') }}/wp-content/themes/{{ env('HOST_THEME') }}/images/social-icons-footer.png"); }
    .footer { text-align:center; }
    #popularSearches ul { position:relative; right:-5%; display:block; margin:0 auto; padding:0; list-style:none; column-count:4; -moz-column-count:4; -webkit-column-count:4; text-align:left; }
    #popularSearches li { margin-bottom:.5em; }
    #popularSearches li a { text-decoration:none; }
    #popularSearches li a:hover { text-decoration:underline;}

    @media (max-width:800px) {
        #popularSearches ul { column-count:3; -moz-column-count:3; -webkit-column-count:3; }
    }
    @media (max-width:600px) {
        #popularSearches ul { column-count:2; -moz-column-count:2; -webkit-column-count:2; }
    }
    @media (max-width:400px) {
        #popularSearches ul { text-align:center; position:static; column-count:1; -moz-column-count:1; -webkit-column-count:1; }
    }

    /* end footer */

    /* pop */

    .black { display:none; position:fixed; top:0; right:0; bottom:0; left:0; background:black; opacity:.5; z-index:10000; }
    .pop { text-align:center; display:none; background:white; color:black; position:fixed; z-index:10001; top:12%; right:30%; bottom:12%; left:30%; max-height:520px; padding:2.5% 5%; overflow:auto; }
    .pop .x { position:absolute; top:0; right:0; background:white; border-bottom-left-radius:100%; box-shadow:0px 0px 5px white inset; border:1px solid rgb(192,192,192); cursor:pointer; background:rgb(225,225,225); color:black; padding:5px 9px 10px 18px; }
    @media (max-width:1600px ) {
        .pop { right:25%; left:25%; }
    }
    @media (max-width:1300px ) {
        .pop { right:18%; left:18%; }
    }
    @media (max-width:1000px ) {
        .pop { right:10%; left:10%; }
    }
    @media (max-width:600px ) {
        .pop img { display:block !important; margin:0 auto !important; left:0 !important; }
        .pop h2 { font-size:24pt !important; margin-bottom:0 !important; }
        .pop { right:5%; left:5%; padding:10%; }
    }

    .pop input[type="text"] { width:100%; }

    /* end pop */

    dt { cursor:pointer; font-weight:bold; border-top:1px solid rgb(172,172,172); padding:10px 0; }
    dt:first-of-type { border-top:none !important; }
    dt:last-of-type { padding-bottom:none; }
    dd { display:none; margin-bottom:20px; margin:0; }
    dd.active { display:block; }

    /* strike */

    .strike { display:inline-block; position:relative; }
    .strike .line { border-bottom:2px solid black; position:relative; top:-15px; }
    #home-8 .strike .line { border-color:red; }

    /* end strike */

    /* popbox */

    .dark { position:fixed; top:0; right:0; bottom:0; left:0; background:rgba(0,0,0,.5); z-index:1000; }
    .popboxWrapperWrapper { position:fixed; top:0; right:0; bottom:0; left:0; z-index:10000; }
    .popboxWrapper { display:table; width:100%; height:100%; }
    .popboxContainer { text-align:center; display:table-cell; vertical-align:middle; }
    .popbox { display:inline-block; background:white; color:black; max-height:520px; max-width:520px; padding:20px 40px; overflow-y:auto; box-shadow:0 0 50px rgba(0,0,0,.5); }
    .popbox .x { position:absolute; top:0; right:0; background:white; border-bottom-left-radius:100%; box-shadow:0px 0px 5px white inset; border:1px solid rgb(192,192,192); cursor:pointer; background:rgb(225,225,225); color:black; padding:5px 9px 10px 18px; }
    @media (max-width:1600px ) {
        .popbox { right:25%; left:25%; }
    }
    @media (max-width:1300px ) {
        .popbox { right:18%; left:18%; }
    }
    @media (max-width:1000px ) {
        .popbox { right:10%; left:10%; }
    }
    @media (max-width:600px ) {
        .popbox { right:5%; left:5%; }
    }

    .popbox input[type="text"] { width:100%; }
    .popbox h2:first-of-type { margin-top:0; }

    /* end popbox */

</style>
<header id="header" style="position:relative; z-index:1;">
    <a style="float:right;" href="/account">Member Login</a>
</header>
<h1 style="position:absolute; top:0; left:0; z-index:1;"><img id="logo" src="{{ env('HOST_URL') }}/wp-content/themes/{{ env('HOST_THEME') }}/images/home/2014-04/marriedgames-logo.png" alt="Sex Games for Couples | MarriedGames.org"></h1>
<div style="display:table; width:100%;">
    <section id="home-1">
        <!--<img id="home-1-dice" width="300" height="284" src="{{ env('HOST_URL') }}/wp-content/themes/{{ env('HOST_THEME') }}/images/home/2014-04/dice-2.jpg" alt="dice" style="position:absolute; bottom:25%; left:20%; -webkit-transform:rotate(45deg);">-->
        <!--<img id="home-1-lingerie" width="451" height="600" src="{{ env('HOST_URL') }}/wp-content/themes/{{ env('HOST_THEME') }}/images/home/2014-04/lingerie.jpg" alt="lingerie" style="position:absolute; top:0%; right:5%; -webkit-transform:rotate(45deg);">-->
        <div id="home-1-content" style="position:relative; z-index:1; margin-top:-97px;">
            <div id="home-1-main-content">
                <h3 style="font-size:4em; margin-bottom:.25em;">Sex ... made easier!</h3>
                <ul id="home-1-icons" style="text-align:center;">
                    <li><div style="background-position:-<?php echo 8 * $icon2 ?>px -<?php echo 3 * $icon2 ?>px;"></div><div>Games</div></li>
                    <li><div style="background-position:-<?php echo 1 * $icon2 ?>px -<?php echo 2 * $icon2 ?>px;"></div><div>Ideas</div></li>
                    <li><div style="background-position:-<?php echo 2 * $icon2 ?>px -<?php echo 2 * $icon2 ?>px;"></div><div>Tools</div></li>
                    <li><div style="background-position:-<?php echo 3 * $icon2 ?>px -<?php echo 2 * $icon2 ?>px;"></div><div>Reports</div></li>
                    <li><div style="background-position:-<?php echo 4 * $icon2 ?>px -<?php echo 2 * $icon2 ?>px;"></div><div>Role-plays</div></li>
                    <li><div style="background-position:-<?php echo 5 * $icon2 ?>px -<?php echo 2 * $icon2 ?>px;"></div><div>Services</div></li>
                </ul>
                <h2 style="font-family:arial; font-weight:300; font-size:12pt; margin:1em 0 0; opacity:.5;">The #1 Most Popular Lovemaking Suite in the Cloud</h2>
                <p style="font-family:arial; opacity:.5; font-weight:100; margin-top:0; font-size:12pt;">Used by thousands of couples for better sex</p>
                <a class="button scroll-1" id="tour" style="display:inline-block; vertical-align:top;">Take Tour</a>
                <div style="display:inline-block; text-align:center;">
                    <a class="button" href="{{ env('APP_URL') }}/oauth/facebook">Sign Up With Facebook</a>
                    <a class="button" href="{{ env('APP_URL') }}/oauth/google">Sign Up With Google</a>
                    <a class="button" href="{{ env('APP_URL') }}/oauth/instagram">Sign Up With Instagram</a>
                    <!-- <img class="credit-cards" width="150" height="14" src="{{ env('HOST_URL') }}/wp-content/themes/{{ env('HOST_THEME') }}/images/credit-cards.png" alt="We accept all major credit cards through PayPal"> -->
                </div>
                <!--<a href="/register" class="button green">Join for Only $2<span class="cents">.99/mo</span></a>-->
            </div>
            <div id="home-1-sidebar">
                <!-- Facebook like box -->
                <div id="sidebar">
                    <div style="width:195px; height:305px; background:rgb(128,0,64);">
                        <div id="fb-root"></div>
                        <script>(function(d, s, id) {
                          var js, fjs = d.getElementsByTagName(s)[0];
                          if (d.getElementById(id)) return;
                          js = d.createElement(s); js.id = id;
                          js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                          fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));</script>
                        <div style="background:white;" class="fb-like-box" data-href="https://www.facebook.com/pages/Marriedgamesorg/210751609048781" data-width="195" data-height="305" data-colorscheme="light" data-show-faces="true" data-header="false" data-stream="false" data-show-border="false"></div>
                        <!--<div id="subscribe" style="margin-top:2em;">
                            <h2 style="font-size:1em;">Subscribe to Sexy Tips</h2>
                            <div>
                                <p style="font-size:10pt;">Get daily ideas for improving your sex life.</p>
                                <form action="//?wpr-optin=1" method="post">
                                    <input type="hidden" name="blogsubscription" value="cat" />
                                    <input type="hidden" name="cat" value="5" />
                                    <input type="hidden" name="newsletter" value="1" />
                                    <input type="hidden" name="newsletter" value="2" />
                                    <input type="hidden" name="fid" value="2">
                                    <input style="margin-bottom:.5em;" type="text" name="name" id="name" placeholder="Your Name"><br>
                                    <input style="margin-bottom:.5em;" type="text" name="email" id="email" placeholder="Your Email"><br>
                                    <input type="submit" value="Subscribe" />
                                </form>
                            </div>
                        </div><!-- subscribe -->
                    </div>
                </div><!-- sidebar -->
            </div>
        </div>
        <div class="scroll-down scroll-1" id="take-tour">
            Take Tour<br>
            &darr;
        </div>
    </section>
</div>
<section id="home-quote">
    <div style="margin:0 auto; max-width:550px; display:table;">
        <p style="font-size:8em; display:table-cell; vertical-align:top; font-family:Georgia; padding-right:10px;">"</p>
        <p style="display:table-cell;">
            MarriedGames.org is a great site and an excellent product, the best I've seen of its kind. It offers exactly what my wife and I were looking for, and it's certainly the top among its peers. It's been a great motivator to make time. And that, as you probably know, is the biggest challenge couples face in trying to reconnect."
        </p>
    </div>
    <p style="text-align:right; max-width:550px; margin:0 auto;"><small>-- Ryan R., Santa Ana, CA</small></p>
</section>
<section id="home-2">
    <div class="content">
        <h3>Bring Back the Passion</h3>
        <p>Get all the games, tools, and resources you'll need to make it feel like the first time every time. Imagine your entire love life managed in the cloud. Think more. Think bigger.</p>
        <dl>
            <dt><strong>Learn More</strong> <div class="about"></div></dt>
            <dd>
                MarriedGames.org offers premiun online sexy and erotic games for couples to play, including board games, question games, bedroom games, and more for sizzling fun, improving communication, and strengthening marriages.
            </dd>
        </dl>
    </div>
    <img class="bg" src="{{ env('HOST_URL') }}/wp-content/themes/{{ env('HOST_THEME') }}/images/home/2014-04/home-2-bg.jpg" alt="Play Free Premium Online Sex Games for Couples">
    <div style="bottom:15%;" class="scroll-down scroll-2">
        The Apps<br>
            &darr;
    </div>
</section>
<section id="home-3" class="apps">
    <h3>Games</h3>
    <br>
    <ul>
        <li>
            <h2><span class="icon"><span style="background-position:0 -<?php echo 2 * $icon1 ?>px;"></span></span> "Lovers Lane" - the Online Sexy Board Game</h2>
            <img src="//i2.wp.com/marriedgames.org/wp-content/themes/marriedgames.org/images/home/images/home_06-e.jpg?w=960" width="400" alt="Sex games">
            <p>Take turns exchanging sexual favors as you build clubs (for flirtatious activities), spas (for relaxation and sensual activities), theaters (for visual stimulation), and hotels (for taking things all the way).</p>
        </li>
        <li>
            <h2><span class="icon"><span style="background-position:0 -<?php echo 2 * $icon1 ?>px;"></span></span> Sex Dice</h2>
            <img src="{{ env('HOST_URL') }}/wp-content/themes/{{ env('HOST_THEME') }}/images/home/2014-04/screenshot-sex-dice.jpg" width="400" alt="xxx games">
            <p>Perhaps the easiest way to add fun and surprise to the bedroom. Use the default dice to come up with out-of-the-box lovemaking activities, or customize the dice for your desires or to create your own games with.</p>
        </li>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 9 * $icon1 ?>px -<?php echo 3 * $icon1 ?>px;"></span></span> Strip Poker</h2>
            <img src="{{ env('HOST_URL') }}/wp-content/themes/{{ env('HOST_THEME') }}/images/home/2014-04/screenshot-strip-poker.png" width="400" alt="Erotic games">
            <p>No need for cards, chips, shuffling, or counting, play against each other on two laptops or mobile devices. Erotic playtime has never been easier! Try to get your spouse naked as you up the stakes with each exciting bet.</p>
        </li>
    </ul>
    <div style="text-align:center;">
        <a href="/checkout-gold-trial" class="button pulse"><!--<img src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" alt="Free 24 Hour Trial">-->Free 24 Hour Trial</a><!--<br><small>Then only 99&cent; / yr</small>--><br><img class="credit-cards" width="150" height="14" src="{{ env('HOST_URL') }}/wp-content/themes/{{ env('HOST_THEME') }}/images/credit-cards.png" alt="We accept all major credit cards through PayPal">
    </div>
</section>
<section id="home-4" class="apps">
    <h3>Ideas</h3>
    <br>
    <ul>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 1 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Foreplay Generator</h2>
            <img src="//i2.wp.com/marriedgames.org/wp-content/themes/marriedgames.org/images/home/images/home_12-e.jpg?w=960" width="400" alt="Sex games for couples">
            <p>Every click generates a new foreplay action customized for your relationship. With hundreds of possibilities, this app alone does wonders for making sex fresh.</p>
        </li>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 8 * $icon1 ?>px -<?php echo 3 * $icon1 ?>px;"></span></span> Sexy Questions</h2>
            <img src="//i2.wp.com/marriedgames.org/wp-content/themes/marriedgames.org/images/home/images/home_12-e.jpg?w=960" width="400" alt="Adult board games">
            <p>Need help getting in the mood? Instantly draw from hundreds of sexy and intimate questions, customized for your relationship, to help improve your communication about sex and to get the romantic wheels turning.</p>
        </li>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 2 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Sex Position Generator</h2>
            <img src="//i2.wp.com/marriedgames.org/wp-content/themes/marriedgames.org/images/home/images/home_09-e.jpg?w=960" width="400" alt="Sex video games">
            <p>Break out of the old routine and explore new positions and passions for hitting the right spot. Learn from tasteful illustrations and simple instructions cusmtomized for your relationship.</p>
        </li>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 7 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Romantic Ideas</h2>
            <img src="//i2.wp.com/marriedgames.org/wp-content/themes/marriedgames.org/images/home/images/home_22-e.jpg?w=960" width="400" alt="dirty games">
            <p>With new articles posted each day, from both the MarriedGames.org team and other couples who want to share their successes, you'll never need to look to another source for date ideas, marriage advice, sex tips, and more.</p>
        </li>
    </ul>
    <div style="text-align:center;">
        <a href="/checkout-gold-trial" class="button pulse"><!--<img src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" alt="Free 24 Hour Trial">-->Free 24 Hour Trial</a><!--<br><small>Then only 99&cent; / yr</small>--><br><img class="credit-cards" width="150" height="14" src="{{ env('HOST_URL') }}/wp-content/themes/{{ env('HOST_THEME') }}/images/credit-cards.png" alt="We accept all major credit cards through PayPal">
    </div>
</section>
<section id="home-5" class="apps">
    <h3>Tools</h3>
    <br>
    <ul>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 1 * $icon1 ?>px -<?php echo 3 * $icon1 ?>px;"></span></span> Sex Life Reports</h2>
            <img src="{{ env('HOST_URL') }}/wp-content/themes/{{ env('HOST_THEME') }}/images/home/2014-04/screenshot-reports-a.jpg" width="400" alt="sex board games">
            <p>Finally you can get the facts about your sex life. Keep an easy log of the days on which you have sex, get reminders about how long it's been, view your history, and get basic statistics. Plus many more features to come!</p>
        </li>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 6 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Romantic Diary</h2>
            <img src="//i2.wp.com/marriedgames.org/wp-content/themes/marriedgames.org/images/home/images/home-romantic-diary-a.jpg?w=960" width="400" alt="couples sex games">
            <p>Don't forget your most intimate and cherished moments! Safely record memoirs about great dates, sex, fantasies, feelings, love notes, or relationship goals. Get ideas from easy writing prompts or from what other couples are sharing.</p>
        </li>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 5 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Sexual Favor Clock</h2>
            <img src="//i2.wp.com/marriedgames.org/wp-content/themes/marriedgames.org/images/home/images/home_16-e.jpg?w=960" width="400" alt="rpg sex games">
            <p>Enjoy the Law of Karma at its finest. Every second of sensual service you give to your spouse will be tracked and rewarded with sweet paybacks. Simply select who's serving who and start the clock ticking.</p>
        </li>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 4 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Sexy Services</h2>
            <img src="//i2.wp.com/marriedgames.org/wp-content/themes/marriedgames.org/images/home/images/home_21-e.jpg?w=960" width="400" alt="Online Sexy Services game for couples">
            <p>Award each other with "love bucks," list your own sexual services and prices, and purchase your spouse's services. Additionally, you can request the erotic service you've been longing for and let your spouse set their price.</p>
        </li>
    </ul>
    <div style="text-align:center;">
        <a href="/checkout-gold-trial" class="button pulse"><!--<img src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" alt="Free 24 Hour Trial">-->Free 24 Hour Trial</a><!--<br><small>Then only 99&cent; / yr</small>--><br><img class="credit-cards" width="150" height="14" src="{{ env('HOST_URL') }}/wp-content/themes/{{ env('HOST_THEME') }}/images/credit-cards.png" alt="We accept all major credit cards through PayPal">
    </div>
</section>
<section id="home-6" class="apps">
    <h3>Sexy Role-playing</h3>
    <br>
    <ul>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 3 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Generator</h2>
            <img src="{{ env('HOST_URL') }}/wp-content/themes/{{ env('HOST_THEME') }}/images/home/2014-04/screenshot-role-playing-generator-a.jpg" width="400" alt="Online sexy role-playing generator for couples">
            <p>Sometimes nothing is more erotic than putting yourselves in other people's shoes. With this easy generator of settings, characters, and more, the possibilities are virtually endless. You can also customize the options to meet your fantasies.</p>
        </li>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 3 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Outlines</h2>
            <img src="{{ env('HOST_URL') }}/wp-content/themes/{{ env('HOST_THEME') }}/images/home/2014-04/screenshot-role-playing-outlines-a.jpg" width="400" alt="Online sexy role-playing outlines for couples">
            <p>For when you want a tried and true plan for amazing sex, follow a simple outline. With some designed for pleasing him, some designed for pleasing her, and many more to come, you'll have no excuse for the same old.</p>
        </li>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 3 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Scripts</h2>
            <img src="{{ env('HOST_URL') }}/wp-content/themes/{{ env('HOST_THEME') }}/images/home/2014-04/screenshot-role-playing-scripts-b.jpg" width="400" alt="Online sexy role-playing scripts for couples">
            <p>Combine ultimate leisure with your ultimate sexual fantasies. Simply follow one of the sexy scripts, telling you what to say and what to do, ensuring an orgasmic ending. It doesn't get any easier than that!</p>
        </li>
    </ul>
    <div style="text-align:center;">
        <a href="/checkout-gold-trial" class="button pulse"><!--<img src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" alt="Free 24 Hour Trial">-->Free 24 Hour Trial</a><!--<br><small>Then only 99&cent; / yr</small>--><br><img class="credit-cards" width="150" height="14" src="{{ env('HOST_URL') }}/wp-content/themes/{{ env('HOST_THEME') }}/images/credit-cards.png" alt="We accept all major credit cards through PayPal">
    </div>
</section>
<section id="home-7" class="apps">
    <h3>Member Perks</h3>
    <br>
    <ul>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 8 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Husband and Wife Store</h2>
            <img src="{{ env('HOST_URL') }}/wp-content/themes/{{ env('HOST_THEME') }}/images/home/2014-04/screenshot-husband-and-wife-store-a.jpg" width="400" alt="Online lingerie and adult novelty store without pornography">
            <p>Gain exclusive access to the Husband and Wife Store, a safe, anonymous, and clean way to buy discounted lingerie, lovemaking accessories, and more. Unlike most any other store with adult novelties, you won't see any pornography or offensive images.</p>
        </li>
        <li>
            <h2><span class="icon"><span style="background-position:-<?php echo 9 * $icon1 ?>px -<?php echo 2 * $icon1 ?>px;"></span></span> Marriage and Relationship Support</h2>
            <img src="//i2.wp.com/marriedgames.org/wp-content/themes/marriedgames.org/images/home/images/home_25-f.jpg?w=960" width="400" alt="Online marriage and relationship support for couples">
            <p>Here at MarriedGames.org, we've spent a long time researching and developing solutions to help couples improve their communication and relationships, and we'd love to help you out! Get friendly, anonymous advice when you need it.</p>
        </li>
    </ul>
</section>
<section id="home-8">
    <div class="content">
        <h3>Ready to Play?</h3>
        <p>Quality bedroom games increase sexual excitement, raise dopamine levels, and strengthen romantic ties, all of which lead to stronger and more frequent orgasms and marital satisfaction. Don't miss out!</p>
        <div style="display:inline-block; text-align:center;">
                <a class="button cta pulse" href="/checkout-gold-trial">Free 24 Hour Trial</a><br>
                <img width="150" height="14" src="{{ env('HOST_URL') }}/wp-content/themes/{{ env('HOST_THEME') }}/images/credit-cards.png" alt="We accept all major credit cards through PayPal">
                <!--<a href="lifetime-membership"><img src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif"></a>-->
        </div>
        <!--<a href="/register" class="button green">Join for Only $2<span class="cents">.99/mo</span></a>-->
    </div>
    <img class="bg" src="{{ env('HOST_URL') }}/wp-content/themes/{{ env('HOST_THEME') }}/images/home/2014-04/home-8-bg-a.jpg" alt="Bedroom games for couples">
</section>
    <section id="home-9" style="padding:5%; text-align:center;">
    <?php if (1 + 1 == 3) { ?>
        <h3 style="font-size:36px; color:white; margin:0 0 .5em; font-family:sexy;">Share the Love</h3>
        <!-- AddThis Button BEGIN -->
        <div style="text-align:center;">
            <a class="addthis_button_google_plusone" g:plusone:size="tall"></a>
            <a class="addthis_button_facebook_like" fb:like:layout="box_count"></a>
            <a class="addthis_button_tweet" tw:count="vertical"></a>
            <a href="//www.pinterest.com/pin/create/button/?url=http%3A%2F%2Fmarriedgames.org&media=http%3A%2F%2Fmarriedgames.org%2Fwp-content%2Fthemes%2Fmarriedgames.org%2Fimages%2Fpinterest-screenshot.png&description=Online%20Sex%20Games%20for%20Couples%20%7C%20MarriedGames.org" data-pin-do="buttonPin" data-pin-config="above" data-pin-color="white" data-pin-height="28"><img src="//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_white_28.png" /></a>
            <!-- Please call pinit.js only once per page -->
            <script type="text/javascript" async src="//assets.pinterest.com/js/pinit.js"></script>
            <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4f9024373ebb49dc"></script>
        </div>
        <!-- AddThis Button END -->
        <br><br>
    <?php } ?>
        <div style="text-align:center;">
            <div id="wysija" class="widget_wysija_cont html_wysija" style="overflow:hidden;">
                <p class="flash" id="checkEmail" style="display:none;">To complete your subscription, you must click on a link that's been sent to your inbox. IMPORTANT: If you don't see an email after a few minutes, be sure to check your SPAM folder and unmark the message as SPAM.</p>
                <script>
                    if (location.href.indexOf("#") > 0) {
                        var wysija = location.href.split("#");
                        if (wysija[1] == "wysija") jQuery("#checkEmail").show();
                    }
                </script>

                <h3 style="font-size:36px; color:white; margin:0; font-family:sexy;">Subscribe to Sexy Tips</h3>
                <p style="margin-bottom:-2em; font-size:11pt;">(Receive regular tips for improving your love life and updates from MarriedGames.org.)</p>
                <div style="display:inline-block; max-width:500px; margin-top:2em;" id="msg-form-wysija-html534739e0d6ce4-1" class="wysija-msg ajax"></div>
                <form id="form-wysija-html534739e0d6ce4-1" method="post" action="#wysija" class="widget_wysija html_wysija">
                    <div class="wysija-msg"></div>
                    <div class="wysija-msg ajax"></div>
                    <input type="hidden" value="d204f1c1bc" id="wysijax" />
                    <input style="padding:10px; display:inline-block;" placeholder="First Name" type="text" name="wysija[user][firstname]" class="wysija-input validate[required]" title="First name"  value="" />
                    <span class="abs-req" style="display:none;">
                        <input type="text" name="wysija[user][abs][firstname]" class="wysija-input validated[abs][firstname]" value="" />
                    </span>
                    <input style="padding:10px; display:inline-block;" placeholder="Email Address" type="text" name="wysija[user][email]" class="wysija-input validate[required,custom[email]]" title="Email"  value="" />
                    <span class="abs-req" style="display:none;">
                        <input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
                    </span>
                    <button class="wysija-submit wysija-submit-field">Subscribe!</button>
                    <input type="hidden" name="form_id" value="1" />
                    <input type="hidden" name="action" value="save" />
                    <input type="hidden" name="controller" value="subscribers" />
                    <input type="hidden" value="1" name="wysija-page" />
                    <input type="hidden" name="wysija[user_list][list_ids]" value="1,6" />
                </form>
                <br>
                <br>
                <h3 style="font-size:36px;">Subscribe with Feed Reader</h3>
                <a href="//feed"><img height="24" width="24" style="vertical-align:middle; box-shadow:1px 1px 3px rgba(0,0,0,.25);" src="{{ env('HOST_URL') }}/wp-content/themes/{{ env('HOST_THEME') }}/images/rss.png" alt="RSS Feed"></a> <a href="//feed">marriedgames.org/feed</a>

            </div>
        </div>
    </section>
<footer id="footer">
    <div id="footerContent">
        <nav class="left" id="footerLinks">
            <a href="//company/about/">About</a>
            <a href="//company/contact-us/">Contact</a>
            <a href="//company/privacy/">Privacy</a>
            <a href="//company/terms/">Terms</a>
            <a href="//category/romantic-ideas/">Blog</a>
            <p style="color:gray;">Copyright &copy; <?php echo date('Y') ?> MarriedGames.org</p>
        </nav><!-- footerLinks -->
        <div class="right" id="footerSocialLinks">
            <a target="_blank" href="https://www.facebook.com/pages/Marriedgamesorg/210751609048781" style="background-position:0"></a><!-- facebook  -->
            <a target="_blank" href="https://plus.google.com/b/107895751291101812066/+MarriedgamesOrg/posts" style="background-position:200px"></a><!-- google plus  -->
            <a target="_blank" href="https://twitter.com/marriedgames" style="background-position:150px"></a><!-- twitter  -->
            <a target="_blank" href="//www.pinterest.com/marriedgames/" style="background-position:100px"></a><!-- pinterest  -->
            <a target="_blank" href="//www.linkedin.com/pub/kyle-thomas/96/671/697/" style="background-position:50px"></a><!-- linkedin  -->
        </div><!-- footerSocialLinks -->
    </div><!-- footerContent -->
    <div id="popularSearches" style="overflow:hidden;">
        <h2>Popular Searches</h2>
        <ul>
            <li>
                <a href="//sex-games/">Sex Games</a>
            </li>
            <li>
                <a href="//xxx-games/">XXX Games</a>
            </li>
            <li>
                <a href="//love-games/">Love Games</a>
            </li>
            <li>
                <a href="//erotic-games">Erotic Games</a>
            </li>
            <li>
                <a href="//sex-games-for-couples">Sex Games for Couples</a>
            </li>
            <li>
                <a href="//adult-board-games/">Adult Board Games</a>
            </li>
            <li>
                <a href="//sex-video-games/">Sex Video Games</a>
            </li>
            <li>
                <a href="//dirty-games/">Dirty Games</a>
            </li>
            <li>
                <a href="//sex-board-games">Sex Board Games</a>
            </li>
            <li>
                <a href="//couples-sex-games">Couples Sex Games</a>
            </li>
            <li>
                <a href="//landing-pages/rpg-sex-games/">RPG Sex Games</a>
            </li>
            <li>
                <a href="//games/">Sexy games for couples</a>
            </li>
            <li>
                <a href="//games/sexy-services/">Adult board games</a>
            </li>
            <li>
                <a href="//ideas/browse-all-positions/">Husband and Wife Games</a>
            </li>
            <li>
                <a href="/sexy-games-and-tools/sex-positions/generator/">Bedroom Games</a>
            </li>
            <li>
                <a href="/sexy-games-and-tools/sexual-favor-clock/">Adult games for couples</a>
            </li>
            <li>
                <a href="//sexy-role-playing/generator/">Married sex games</a>
            </li>
            <li>
                <a href="//sexy-role-playing/outlines/">Games for lovers</a>
            </li>
            <li>
                <a href="//sexy-role-playing/scripts/">Sex games for lovers</a>
            </li>
        </ul>
    </div><!-- popularSearches -->
</footer>
<!-- free trial popup -->
<?php if (!auth()) { ?>

    <?php

        // learn if the user's IP address has an expired trial

        $con = mysqli_connect("localhost",DB_USER,DB_PASSWORD,DB_NAME);
        if (mysqli_connect_errno()) echo "Failed to connect to MySQL: " . mysqli_connect_error();
        $trialStarted;
        $trialExpired;
        $sql = "SELECT * FROM trials WHERE ip = '" . S2MEMBER_CURRENT_USER_IP . "'";
        $result = $con->query($sql);
        while($row = $result->fetch_assoc()) {
            if ($row['time']) $trialStarted = 1;
            if (time() - $row['time'] > 86400) {
                $trialExpired = 1;
            }
        }

    ?>

    <!--<div id="trial-expired" class="homePop" style="display:none; text-align:center;">
        <h2 style="font-size:3em;">50% Off Today Only</h2>
        <p>Thank you for completing a free trial with MarriedGames.org! <br><strong>Limited Time Offer:</strong> Get 50% off all membership plans when you sign up within the next 90 seconds!</p>
        <p style="font-size:2em;" class="counter">90</p>
        <a href="/checkout-gold-trial" class="button">I Want 20% Off My Membership</a>
        <p><a class="x" onclick="setCookie('offeredFreeTrial', 1, 7);">&times;</a></p>
        <p><a class="closePopup" onclick="setCookie('offeredFreeTrial', 1, 7);" style="cursor:pointer; color:rgb(128,0,65); text-decoration:underline;">No thanks</a></p>
    </div>-->
    <div id="free-trial" class="homePop" style="display:none; text-align:center;">
        <h2 style="font-size:3em; margin-bottom:-.5em;"><img alt="100% Satisfaction Guarantee" style="vertical-align:middle; position:relative; left:-66px; margin-right:-66px;" src="//wp-content/themes/marriedgames.org/images/seals/satisfaction-guarantee.png">&nbsp;Free 24 Hour Trial</h2>
        <p style="margin-top:0; font-weight:bold;">(One time offer)</p>
        <p style="text-align:left;"><strong>When you sign up in the next 90 seconds</strong>, you'll get a Free 24 Hour Trial of the complete MarriedGames.org lovemaking suite. Don't miss out on this no-risk opportunity for a better love life.</p>
        <p style="font-size:2em;" class="counter">90</p>
        <button onclick="closePopup();">No Thanks</button>
        <a href="/checkout-gold-trial" class="button">Free 24 Hour Trial</a><br>
        <img width="150" height="14" src="{{ env('HOST_URL') }}/wp-content/themes/{{ env('HOST_THEME') }}/images/credit-cards.png" alt="We accept all major credit cards through PayPal">
    </div>
    <!--<div id="free-trial" class="homePop" style="display:none; text-align:center;">
        <h2 style="font-size:3em;">Free 24 Hour Trial</h2>
        <p>Not sure if MarriedGames.org is right for you? <strong>Limited Time Offer:</strong> Get a Free 24 Hour Trial when you sign up in the next 90 seconds (<strong>no credit card required</strong>). Don't miss out on this unbeatable offer for a better love life!</p>
        <p style="font-size:2em;" class="counter">90</p>
        <a href="/play-free" class="button">Free 24 Hour Trial</a>
        <p><a class="x" onclick="setCookie('offeredFreeTrial', 1, 7);">&times;</a></p>
        <p><a class="closePopup" onclick="setCookie('offeredFreeTrial', 1, 7);" style="cursor:pointer; color:rgb(128,0,65); text-decoration:underline;">No thanks</a></p>
    </div>-->
<script>

        jQuery(document).ready(function() {

            var offer = "<?php echo $offer ?>";
            var trialStarted = "<?php echo $trialStarted ?>";

            // get cookie

            function getCookie(cname) {
                var name = cname + "=";
                var ca = document.cookie.split(';');
                for(var i = 0; i < ca.length; i ++) {
                    var c = ca[i].trim();
                    if (c.indexOf(name)==0) return c.substring(name.length,c.length);
                }
                return "";
            }

            // set cookie

            function setCookie(cname,cvalue,exdays) {
                var d = new Date();
                d.setTime(d.getTime()+(exdays*24*60*60*1000));
                var expires = "expires="+d.toGMTString();
                document.cookie = cname + "=" + cvalue + "; " + expires;
            }

            // free trial

            function closePopup() {
                jQuery(".black, .pop").fadeOut(function() {
                    jQuery(".black, .pop").remove();
                });
            }
            jQuery(".closePopup").click(function() {
                closePopup();
            });
            function popFreeTrial(offer) {
                if (offer == undefined) content = "#free-trial";
                else if (offer == "cctrial") content = "#cctrial";
                pop(jQuery(content).html());
                var counter = 91;
                setInterval(function() {
                    if (counter > 0) {
                        counter --;
                        jQuery(".pop .counter").html(counter);
                    }
                    else {
                        closePopup();
                        setCookie("offeredFreeTrial", 1, 7);
                    }
                }, 1000);
            }
            if (trialStarted !== "1") {
                if (offer == "") {
                    var offeredFreeTrial;
                    if (getCookie("offeredFreeTrial") == "") {
                        jQuery(document).scroll(function() {
                            if (offeredFreeTrial !== true) {
                                if(jQuery(this).scrollTop()>=jQuery('#home-8').position().top){
                                    jQuery(this).off("scrollTop");
                                    offeredFreeTrial = true;
                                    popFreeTrial();
                                }
                            }
                        });
                        setTimeout(function() {
                            if (offeredFreeTrial !== true) {
                                popFreeTrial();
                            }
                            offeredFreeTrial = true;
                        }, 90000);
                    }
                    else offeredFreeTrial = true;
                }
                else if (offer == "cctrial") {
                    jQuery(document).scroll(function() {
                        if (offeredFreeTrial !== true) {
                            if(jQuery(this).scrollTop()>=jQuery('#home-8').position().top){
                                jQuery(this).off("scrollTop");
                                offeredFreeTrial = true;
                                popFreeTrial("cctrial");
                            }
                        }
                    });
                    setTimeout(function() {
                        if (offeredFreeTrial !== true) {
                            popFreeTrial("cctrial");
                        }
                        offeredFreeTrial = true;
                    }, 10000);
                }
            }

            // free trial expired

            var trialExpired = "<?php echo $trialExpired ?>";
            if (parseInt(trialExpired) == 1) {
                if (offeredFreeTrial == true) {
                    popFreeTrial(1);
                }
                jQuery(".credit-cards").hide();
                jQuery(".cta").attr("href", "/account").html("Login to Play &rsaquo;").before('<a class="button" href="/checkout-gold-trial" style="margin-right:.5em;">Free 24 Hour Trial</a>');
                jQuery("#tour").hide();
            }

            //var time = <?php echo time() ?>;
            //if (time - getCookie("trial") > 400) jQuery("body").prepend('<meta http-equiv="refresh" content="0; url=//checkout-gold-trial/" />');

        });

</script>
<?php } ?>
<script>

    // pulse buttons

    jQuery(document).ready(function() {
        brighten();
        function brighten() {
            jQuery(".button.pulse").css("background-color", "green");
            setTimeout(function() {
                darken();
            }, 3000);
        }
        function darken() {
            jQuery(".button.pulse").css("background-color", "rgb(255,0,128)");
            setTimeout(function() {
                brighten();
            }, 3000);
        }
    });

    // toggle definition list items

    jQuery("body:not(.page-id-1870) dt").click(function() {
        jQuery(this).next("dd").slideToggle();
    });

    // size container to window height

    function resizeWindow() {
        var windowHeight = jQuery(window).height();
        jQuery("#home-1").css("height", windowHeight);
    }
    if (jQuery(window).height() >= 500) resizeWindow();
    jQuery(window).resize(function() {
        if (jQuery(window).height() >= 500) resizeWindow();
    });

    jQuery(document).ready(function (){
        jQuery(".scroll-1").click(function (){
            jQuery('html, body').animate({
                scrollTop: jQuery("#home-quote").offset().top
            }, 1000);
        });
        jQuery(".scroll-2").click(function (){
            jQuery('html, body').animate({
                scrollTop: jQuery("#home-3").offset().top
            }, 1000);
        });
    });

    // pop

    function pop(content) {
        jQuery("body").prepend("<div class='black'></div><div class='pop'><div class='x'>&times;</div>" + content + "</div>");
        jQuery(".black, .pop").fadeIn(function() {
            jQuery(".black, .x").click(function() {
                jQuery(".black, .pop").fadeOut(function() {
                    closePopup();
                });
            });
        });
    }
    function closePopup() {
        jQuery(".black, .pop").fadeOut(function() {
            jQuery(".black, .pop").remove();
        });
    }
    jQuery(".closePopup").click(function() {
        closePopup();
    });

    // popbox

    function popbox(content) {
        var dark;
        if (jQuery(".dark").length == 0) dark = "<div class='dark'></div>";
        else dark = "";
        var popbox = dark + "<div class='popboxWrapperWrapper'><div class='popboxWrapper'><div class='popboxContainer'><div class='popbox'>" + content + "</div></div></div></div>";
        if (jQuery("popboxWrapperWrapper").length > 0) jQuery(".popboxWrapperWrapper").after(popbox);
        else jQuery("body").prepend(popbox);
        jQuery(".dark, .popboxContainer").fadeIn(function() {
            jQuery(".close").click(function() {
                closePopbox();
            });
            jQuery(".dark").click(function() {
                if (!(jQuery(this).hasClass("static"))) closePopbox();
            });
        });
    }
    function closePopbox() {
        if (jQuery(".popboxWrapperWrapper").length == 1) jQuery(".dark").fadeOut(function() {
            jQuery(this).remove();
        });
        if (jQuery(".popboxWrapperWrapper").length > 1) {
            jQuery(".popboxWrapperWrapper:last").fadeOut(function() {
                jQuery(this).remove();
            });
            if (jQuery(".popboxWrapperWrapper").length == 1) jQuery(".dark").fadeOut(function() {
                jQuery(this).remove();
            });
        }
        else {
            jQuery(".popboxWrapperWrapper").fadeOut(function() {
                jQuery(this).remove();
            });
        }
    }

    // end popbox

</script>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-43056743-1', 'marriedgames.org');
  ga('send', 'pageview');

</script>
