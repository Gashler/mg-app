@extends('layouts.default')
@section('content')
<div class="create">
	<div class="row">
		<div class="col col-lg-4 col-xs-12">
			@include('_helpers.breadcrumbs')
			<h1 class="no-top">New User</h1>
		</div>
	</div>
	<div class="row">
		<div class="col col-xl-3 col-lg-4 col-md-6 col-sm-6">
			{{ Form::open(array('url' => 'users')) }}


			<div class="form-group">
				{{ Form::label('public_id', 'Public ID') }}
				<br><small>Example for John Doe: jdoe</small>
				<input type="text" ng-model="obj.public_id" class="form-control" ng-blur="update()">
			</div>

			<div class="form-group">
				{{ Form::label('husband', 'First Name') }}
				<input type="text" ng-model="obj.husband" class="form-control" ng-blur="update()">
			</div>

			<div class="form-group">
				{{ Form::label('wife', 'Last Name') }}
				<input type="text" ng-model="obj.wife" class="form-control" ng-blur="update()">
			</div>

			<div class="form-group">
				{{ Form::label('email', 'Email') }}
				<input type="text" ng-model="obj.email" class="form-control" ng-blur="update()">
			</div>

			<div class="form-group">
				{{ Form::label('password', 'Password') }}
				{{ Form::password('password', array('class' => 'form-control')) }}
			</div>

			<div class="form-group">
				{{ Form::label('password_confirm', 'Confirm Password') }}
				{{ Form::password('password_confirmation', array('class' => 'form-control')) }}
			</div>

			<div class="form-group">
				{{ Form::radio('gender', 'M', true, array('id' => 'gender_male')) }}
				{{ Form::label('gender_male', 'Male') }}
				<br>
				{{ Form::radio('gender', 'F', false, array('id' => 'gender_female')) }}
				{{ Form::label('gender_female', 'Female') }}
			</div>

			<div class="form-group">
				{{ Form::label('dob', 'DOB') }}
				<input type="text" ng-model="obj.dob" class="form-control" ng-blur="update()">
			</div>

			<div class="form-group">
				{{ Form::label('phone', 'Phone') }}
				<input type="text" ng-model="obj.phone" class="form-control" ng-blur="update()">
			</div>

			<div class="form-group">
				{{ Form::label('role_id', 'Role') }}<br>
				{{ Form::select('role_id', array(
				    	'1' => 'Trial',
				    	'2' => 'Member',
				    	'3' => 'Editor',
				    	'4' => 'Admin'
				    ), null, array('class' => 'form-control')) }}
			</div>

			<div class="form-group">
				{{ Form::label('sponsor_id', 'Sponsor ID') }}
				<input type="text" ng-model="obj.sponsor_id" class="form-control" ng-blur="update()">
			</div>

			<div class="form-group">
				{{ Form::label('address_1', 'Address 1') }}
				<input type="text" ng-model="obj.address_1" class="form-control" ng-blur="update()">
			</div>

			<div class="form-group">
				{{ Form::label('address_2', 'Address 2') }}
				<input type="text" ng-model="obj.address_2" class="form-control" ng-blur="update()">
			</div>

			<div class="form-group">
				{{ Form::label('city', 'City') }}
				<input type="text" ng-model="obj.city" class="form-control" ng-blur="update()">
			</div>

			<div class="form-group">
				{{ Form::label('state', 'State') }}
				<br>
				{{ Form::select('state',State::orderBy('name')->pluck('name', 'abbr'), null, array('class' => 'form-control')) }}
			</div>

			<div class="form-group">
				{{ Form::label('zip', 'Zip') }}
				<input type="text" ng-model="obj.zip" class="form-control" ng-blur="update()">
			</div>

			<!-- <div class="form-group">
			        {{ Form::label('mobile_plan_id', 'Mobile Plan Id') }}
<input type="text" ng-model="obj.mobile_plan_id" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('min_commission', 'Min Commission') }}
<input type="text" ng-model="obj.min_commission" class="form-control" ng-blur="update()">
			    </div>
			    -->

			<div class="form-group">
				{{ Form::label('disabled', 'Status') }}
				<br>
				{{ Form::select('disabled', [
			    		0 => 'Active',
			    		1 => 'Disabled'
			    	], null, ['class' => 'form-control']) }}
			</div>

			{{ Form::submit('Add User', array('class' => 'btn btn-primary')) }}

			{{ Form::close() }}
		</div>
	</div>
</div>
@stop