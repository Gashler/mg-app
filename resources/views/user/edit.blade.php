@extends('layouts.default')
@section('content')
<div class="edit">
	<div class="row">
		<div class="col col-md-12">
			@include('_helpers.breadcrumbs')
			@if (Auth::user()->id == $user->id)
				<h1>Edit Profile</h1>
			@else
		    	<h1>Edit {{ $user->husband }} {{ $user->wife }}</h1>
		    @endif
		</div>
	</div>
	<div class="row">
		<div class="col col-xl-3 col-lg-4 col-md-6 col-sm-6">
		    {{ Form::model($user, array('route' => array('users.update', $user->id), 'method' => 'PUT')) }}
		
		    <div class="form-group">
		        {{ Form::label('husband', 'First Name') }}
<input type="text" ng-model="obj.husband" class="form-control" ng-blur="update()">
		    </div>
		    
		    <div class="form-group">
		        {{ Form::label('wife', 'Last Name') }}
<input type="text" ng-model="obj.wife" class="form-control" ng-blur="update()">
		    </div>
		    
		    <div class="form-group">
		        {{ Form::label('public_id', 'Public Id') }}
<input type="text" ng-model="obj.public_id" class="form-control" ng-blur="update()">
		    </div>
		    
		    <div class="form-group">
		        {{ Form::label('email', 'Email') }}
<input type="text" ng-model="obj.email" class="form-control" ng-blur="update()">
		    </div>
		    
		    <div class="form-group">
		        {{ Form::label('password', 'New Password') }}
		        {{ Form::password('password', array('class' => 'form-control')) }}
		    </div>
		    
		    <div class="form-group">
		        {{ Form::label('password_confirm', 'Confirm New Password') }}
		        {{ Form::password('password_confirmation', array('class' => 'form-control')) }}
		    </div>
		    
		    <div class="form-group">
	    		{{ Form::label('gender', 'Gender') }}<br>
	    		{{ Form::select('gender', array(
			    	'M' => 'Male',
			    	'F' => 'Female',
			    ), null, array('class' => 'selectpicker')) }}
		    </div>
		    
		    <!-- <div class="form-group">
		        {{ Form::label('key', 'Key') }}
<input type="text" ng-model="obj.key" class="form-control" ng-blur="update()">
		    </div> -->
		   
		    <div class="form-group">
		        {{ Form::label('dob', 'Date of Birth') }}
<input type="text" ng-model="obj.dob" class="form-control" ng-blur="update()">
		    </div>
		    
		    <div class="form-group">
		        {{ Form::label('phone', 'Phone') }}
<input type="text" ng-model="obj.phone" class="form-control" ng-blur="update()">
		    </div>
		    
		    @if (Auth::user()->hasRole(['Admin']) && Auth::user()->id != $user->id)
		    	<div class="form-group">
		    		{{ Form::label('roled_id', 'Role') }}<br>
		    		{{ Form::select('role_id', array(
				    	'1' => 'Trial',
				    	'2' => 'Member',
				    	'3' => 'Editor',
				    	'4' => 'Admin'
				    ), null, array('class' => 'selectpicker')) }}
			    </div>
		    
			    <div class="form-group">
			        {{ Form::label('sponsor_id', 'Sponsor Id') }}
<input type="text" ng-model="obj.sponsor_id" class="form-control" ng-blur="update()">
			    </div>
			    
			    <!-- <div class="form-group">
			        {{ Form::label('mobile_plan_id', 'Mobile Plan Id') }}
<input type="text" ng-model="obj.mobile_plan_id" class="form-control" ng-blur="update()">
			    </div>
			    
			    <div class="form-group">
			        {{ Form::label('min_commission', 'Min Commission') }}
<input type="text" ng-model="obj.min_commission" class="form-control" ng-blur="update()">
			    </div>
			   -->
			    <div class="form-group">
			        {{ Form::label('disabled', 'Status') }}
			        <br>
			    	{{ Form::select('disabled', [
			    		0 => 'Active',
			    		1 => 'Disabled'
			    	], $user->disabled, ['class' => 'selectpicker']) }}
			    </div>
			    
			@endif		    
		
		    {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
		
		    {{Form::close()}}
		</div>
	</div>
</div>
@stop

