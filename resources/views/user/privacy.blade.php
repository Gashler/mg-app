@extends('layouts.default')
@section('content')
<div class="edit">
	<div class="row">
		<div class="col col-md-12">
			@include('_helpers.breadcrumbs')
			@if (Auth::user()->id == $user->id)
				<h1>Privacy &amp; Communication Preferences</h1>
			@else
		    	<h1>Edit {{ $user->husband }} {{ $user->wife }}</h1>
		    @endif
		    <br>
		</div>
	</div>
	<div class="row">
		<div class="col col-md-6">
			{{ Form::open(array('action' => array('UserController@updatePrivacy', $user->id), 'method' => 'POST')) }}
				<div class="row">
					<div class="col col-md-6">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h2 class="panel-title"><i class="fa fa-lock"></i> Privacy Settings</h2>
							</div>
							<div class="panel-body">
								<p>Which information you would like to to share with Members who have you in their downline?*</p>
						        <label>{{ Form::checkbox('hide_gender', null, $checked['hide_gender']) }} Gender</label><br>
						        <label>{{ Form::checkbox('hide_dob', null, $checked['hide_dob']) }} Date of Birth</label><br>
						        <label>{{ Form::checkbox('hide_email', null, $checked['hide_email']) }} Email</label><br>
						        <label>{{ Form::checkbox('hide_phone', null, $checked['hide_phone']) }} Phone</label><br>
						        <label>{{ Form::checkbox('hide_billing_address', null, $checked['hide_billing_address']) }} Billing Address</label><br>
						        <label>{{ Form::checkbox('hide_shipping_address', null, $checked['hide_shipping_address']) }} Shipping Address</label><br>
							</div>
						</div>
					</div>
					<div class="col col-md-6">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h2 class="panel-title"><i class="fa fa-mobile-phone"></i> Communication Preferences</h2>
							</div>
							<div class="panel-body">
								<p>Whick kinds of communication would you like to receive from Members who have you in their downline?</p>
						        <label>{{ Form::checkbox('block_email', null, $checked['block_email']) }} Email</label><br>
						        <label>{{ Form::checkbox('block_sms', null, $checked['block_sms']) }} Text Messages (SMS)</label>
							</div>
						</div>
					</div>
				</div>
			    {{ Form::submit('Update', array('class' => 'btn btn-primary')) }}
				<br>
				<br>
				<p><small>* These preferences only apply to Members below the rank of Diamond. Members of the Diamond rank or higher will be able to view all of the above information regardless. For more information about how {{ Config::get('site.company_name') }} collects and displays your information, see the <a target="_blank" href="/privacy-policy">Privacy Policy.</a></small></p>
		    {{Form::close()}}
		</div>
	</div>
</div>
@stop

