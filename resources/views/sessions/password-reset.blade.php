@extends('layouts.default')
@section('content')
    <div class="center margin-auto" style="max-width:300px">
        <form action="/password/reset" method="post">
            {{ csrf_field() }}
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h1 class="panel-title">Reset My Password</h1>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            {{ Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Enter your email address']) }}
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button class='btn btn-primary'>Send Me Log In Instructions</button>
                </div>
            </div>
        </form>
    </div>
@stop
