@extends('layouts.default')
@section('content')
    <div class="center margin-auto" style="max-width:300px">
        {{ Form::open(array('route' => 'sessions.store')) }}
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h1 class="panel-title">Log In</h1>
                </div>
                <div class="panel-body">
                    @if (isset($errors[0]))
                        <div class="alert alert-danger">
                            @foreach($errors as $error)
                                <div>{{ $error }}</div>
                            @endforeach
                        </div>
                    @endif
                    {{-- <div class="form-group">
                        <a class="btn-login" href="/oauth?service=Facebook&register=1" style="background: #3b5998">
                            <i class="flaticon-facebook"></i> Log in With Facebook
                        </a>
                        <a class="btn-login" href="/oauth?service=Google&register=1" style="background: #F03530">
                            <i class="flaticon-google"></i> Log in With Google
                        </a>
                    </div> --}}
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user"></i></span>
                            {{ Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email / login']) }}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            {{ Form::password('password', array('class' => 'form-control', 'placeholder' => 'Password')) }}
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button class='btn btn-primary'>Log In</button>
                </div>
            </div>
        {{ Form::close() }}
        <p><a href='/password/forgot'>Forgot Password?</a>&nbsp; | &nbsp;<a href="/register">Sign up for Free</a></p>
    </div>
@stop
