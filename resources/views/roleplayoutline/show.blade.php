@extends('layouts.default')
@section('content')
<div class="show">
	<div class="row page-actions">
		@include('_helpers.breadcrumbs')
		<h1 class="no-top">{{ $roleplayoutline->name }}</h1>
		@if (Auth::user()->hasRole(['Admin']))
		    <div class="btn-group" id="record-options">
			    <a class="btn btn-default" href="{{ url('roleplayoutlines/'.$roleplayoutline->id .'/edit') }}" title="Edit"><i class="flaticon-pencil"></i></a>
			    @if ($roleplayoutline->disabled == 0)
				    {{ Form::open(array('url' => 'roleplayoutlines/disable', 'method' => 'DISABLE')) }}
				    	<input type="hidden" name="ids[]" value="{{ $roleplayoutline->id }}">
				    	<button class="btn btn-default active" title="Currently enabled. Click to disable.">
				    		<i class="flaticon-eye-female"></i>
				    	</button>
				    {{ Form::close() }}
				@else
				    {{ Form::open(array('url' => 'roleplayoutlines/enable', 'method' => 'ENABLE')) }}
				    	<input type="hidden" name="ids[]" value="{{ $roleplayoutline->id }}">
				    	<button class="btn btn-default" title="Currently disabled. Click to enable.">
				    		<i class="flaticon-eye-female"></i>
				    	</button>
				    {{ Form::close() }}
				@endif
			    {{ Form::open(array('url' => 'roleplayoutlines/' . $roleplayoutline->id, 'method' => 'DELETE', 'onsubmit' => 'return confirm("Are you sure you want to delete this roleplayoutline? This cannot be undone.");')) }}
			    	<button class="btn btn-default" title="Delete">
			    		<i class="flaticon-trash" title="Delete"></i>
			    	</button>
			    {{ Form::close() }}
			</div>
		@endif
	</div><!-- row -->
	<div class="row">
		<div class="col col-md-6">
		    <table class="table">
		        
		        <tr>
		            <th>Name:</th>
		            <td>{{ $roleplayoutline->name }}</td>
		        </tr>
		        
		        <tr>
		            <th>Description:</th>
		            <td>{{ $roleplayoutline->description }}</td>
		        </tr>
		        
		        <tr>
		            <th>Body:</th>
		            <td>{{ $roleplayoutline->body }}</td>
		        </tr>
		        
		        <tr>
		            <th>User Id:</th>
		            <td>{{ $roleplayoutline->user_id }}</td>
		        </tr>
		        
		        <tr>
		            <th>Public:</th>
		            <td>{{ $roleplayoutline->public }}</td>
		        </tr>
		        
		        <tr>
		            <th>Disabled:</th>
		            <td>{{ $roleplayoutline->disabled }}</td>
		        </tr>
		        
		    </table>
	    </div>
	</div>
</div>
@stop
