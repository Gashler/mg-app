@extends('layouts.default')
@section('content')
<div class="edit">
	<div class="row">
		<div class="col col-md-12">
			@include('_helpers.breadcrumbs')
		    <h1>Edit Roleplayoutline</h1>
		</div>
	</div>
	<div class="row">
		<div class="col col-xl-3 col-lg-4 col-md-6 col-sm-6">
		    {{ Form::model($roleplayoutline, array('route' => array('roleplayoutlines.update', $roleplayoutline->id), 'method' => 'PUT')) }}
		
		    
		    <div class="form-group">
		        {{ Form::label('name', 'Name') }}
<input type="text" ng-model="obj.name" class="form-control" ng-blur="update()">
		    </div>
		    
		    <div class="form-group">
		        {{ Form::label('description', 'Description') }}
<input type="text" ng-model="obj.description" class="form-control" ng-blur="update()">
		    </div>
		    
		    <div class="form-group">
		        {{ Form::label('body', 'Body') }}
<input type="text" ng-model="obj.body" class="form-control" ng-blur="update()">
		    </div>
		    
		    <div class="form-group">
		        {{ Form::label('user_id', 'User Id') }}
<input type="text" ng-model="obj.user_id" class="form-control" ng-blur="update()">
		    </div>
		    
		    <div class="form-group">
		        {{ Form::label('public', 'Public') }}
<input type="text" ng-model="obj.public" class="form-control" ng-blur="update()">
		    </div>
		    
		    <div class="form-group">
		        {{ Form::label('disabled', 'Disabled') }}
<input type="text" ng-model="obj.disabled" class="form-control" ng-blur="update()">
		    </div>
		    
		
		    {{ Form::submit('Update Roleplayoutline', array('class' => 'btn btn-primary')) }}
		
		    {{Form::close()}}
		</div>
	</div>
</div>
@stop

