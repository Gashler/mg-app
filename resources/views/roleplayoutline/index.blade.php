@extends('layouts.default')
@section('content')
<div ng-app="app" class="index">
    {{ Form::open(array('url' => 'roleplayoutlines/delete', 'method' => 'POST')) }}
	    <div ng-controller="RoleplayoutlineController" class="my-controller">
	    	<div class="page-actions">
		        <div class="row">
		            <div class="col-md-12">
		                <h1 class="no-top pull-left no-pull-xs">All Roleplayoutlines</h1>
		            	<div class="pull-right hidable-xs">
		                    <div class="input-group pull-right">
		                    	<span class="input-group-addon no-width">Count</span>
		                    	<input class="form-control itemsPerPage width-auto" ng-model="pageSize" type="number" min="1">
		                    </div>
		                    <h4 class="pull-right margin-right-1">Page <span ng-bind="currentPage"></span></h4>
		            	</div>
			    	</div>
		        </div><!-- row -->
		        <div class="row">
		        	@if (Auth::user()->hasRole(['Admin', 'Editor']) || isset($my))
			    		<div class="col-md-6 col-sm-6 col-xs-12 page-actions-left">
			                <div class="pull-left">
			                    <a class="btn btn-primary pull-left margin-right-1" title="New" href="{{ url('roleplayoutlines/create') }}"><i class="flaticon-plus"></i></a>
			                    <div class="pull-left">
			                        <div class="input-group">
			                            <select class="form-control selectpicker actions">
			                                <option value="roleplayoutlines/delete">Delete</option>
				                            @if (Auth::user()->hasRole(['Admin', 'Editor']))
				                                <option value="roleplayoutlines/disable" selected>Disable</option>
				                                <option value="roleplayoutlines/enable">Enable</option>
				                            @endif
			                            </select>
			                            <div class="input-group-btn no-width">
			                                <button class="btn btn-default applyAction" disabled>
			                                    <i class="flaticon-check"></i>
			                                </button>
			                            </div>
			                        </div>
			                    </div>
			                </div>
			        	</div>
			        	<div class="col-md-6 col-sm-6 col-xs-12">
					@else
						<div class="col-md-12 col-sm-12 col-xs-12">
					@endif
		                <div class="pull-right">
		                    <div class="input-group">
		                        <input class="form-control ng-pristine ng-valid" placeholder="Search" name="new_tag" ng-model="search.$" onkeypress="return disableEnterKey(event)" type="text">
		                        <span class="input-group-btn no-width">
		                            <button class="btn btn-default" type="button">
		                                <i class="flaticon-search"></i>
		                            </button>
		                        </span>
		                    </div>
		                </div>
		            </div><!-- col -->
		        </div><!-- row -->
		    </div><!-- page-actions -->
	        <div class="row">
	            <div class="col col-md-12">
	                <table class="table">
	                    <thead>
	                        <tr>
	                        	@if (Auth::user()->hasRole(['Admin', 'Editor']) || isset($my))
		                            <th>
		                            	<input type="checkbox">
		                            </th>
		                        @endif
                            	
                            	<th class="link" ng-click="orderByField='name'; reverseSort = !reverseSort">Name
                            		<span>
                            			<span ng-show="orderByField == 'name'">
	                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
	                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                            			</span>
                            		</span>
                        		</th>
                        		
                            	<th class="link" ng-click="orderByField='description'; reverseSort = !reverseSort">Description
                            		<span>
                            			<span ng-show="orderByField == 'description'">
	                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
	                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                            			</span>
                            		</span>
                        		</th>
                        		
                        		@if (Auth::user()->hasRole(['Admin']))
	                            	<th class="link" ng-click="orderByField='user_id'; reverseSort = !reverseSort">User Id
	                            		<span>
	                            			<span ng-show="orderByField == 'user_id'">
		                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
		                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
	                            			</span>
	                            		</span>
	                        		</th>
                        		@endif
                        		
                            	<th class="link" ng-click="orderByField='updated_at'; reverseSort = !reverseSort">Modified
                            		<span>
                            			<span ng-show="orderByField == 'updated_at'">
	                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
	                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                            			</span>
                            		</span>
                        		</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        <tr ng-class="{ highlight : roleplayoutline.new == 1, semitransparent : roleplayoutline.disabled == 1 }" dir-paginate-start="roleplayoutline in roleplayoutlines | filter:search | orderBy: '-updated_at' | orderBy:orderByField:reverseSort | itemsPerPage: pageSize" current-page="currentPage">
	                            @if (Auth::user()->hasRole(['Admin', 'Editor']) || isset($my))
		                            <td ng-click="checkbox()">
		                            	<input class="bulk-check" type="checkbox" name="ids[]" value="{{'{'.'{roleplayoutline.id}'.'}'}}">
		                            </td>
		                        @endif
								
					            <td>
					                <a href="/roleplayoutlines/{{'{'.'{roleplayoutline.id}'.'}'}}"><span ng-bind="roleplayoutline.name"></a>
					            </td>
					            
					            <td>
					                <span ng-bind="roleplayoutline.description">
					            </td>
					            
					            @if (Auth::user()->hasRole(['Admin']))
						            <td>
						                <span ng-bind="roleplayoutline.user_id">
						            </td>
					            @endif
					            
					            <td>
					            	<span ng-bind="roleplayoutline.updated_at">
					            </td>
	                        </tr>
	                        <tr dir-paginate-end></tr>
	                    </tbody>
	                </table>
	                @include('_helpers.loading')
	                <div ng-controller="OtherController" class="other-controller">
	                    <div class="text-center">
	                        <dir-pagination-controls boundary-links="true" on-page-change="pageChangeHandler(newPageNumber)" template-url="/packages/dirpagination/dirPagination.tpl.html"></dir-pagination-controls>
	                    </div>
	                </div>
	            </div><!-- col -->
	        </div><!-- row -->
        {{ Form::close() }}
    </div><!-- app -->
@stop
@section('scripts')
<script>

	var app = angular.module('app', ['angularUtils.directives.dirPagination']);
	
	function RoleplayoutlineController($scope, $http) {
	
		$http.get('/api/{{ $object }}').success(function(data) {
			$scope.roleplayoutlines = data;
			console.log($scope.roleplayoutlines);
			@include('_helpers.bulk_action_checkboxes')
			
		});
		
		$scope.currentPage = 1;
		$scope.pageSize = 15;
		
		$scope.pageChangeHandler = function(num) {
			
		};
		
	}
	
	function OtherController($scope) {
		$scope.pageChangeHandler = function(num) {
		};
	}

</script>
@stop