@extends('layouts.default')
@section('style')
	<style>

		#editor {
		resize:vertical;
		overflow:auto;
		border:1px solid silver;
		border-radius:5px;
		padding:1em;
		}

		#output {
		  width:99.7%;
		  height:100px
		}

	</style>
@stop
@section('content')
<div class="create">
	<div class="row">
		<div class="col col-md-12">
			@include('_helpers.breadcrumbs')
		    <h1 class="no-top">New Roleplayoutline</h1>
		</div>
	</div>
	<div class="row">
		<div class="col col-md-6">
		    {{ Form::open(array('url' => 'roleplayoutlines')) }}

			    <div class="form-group">
			        {{ Form::label('name', 'Name') }}
<input type="text" ng-model="obj.name" class="form-control" ng-blur="update()">
			    </div>

			    <div class="form-group">
			        {{ Form::label('body', 'Body') }}









<div>
  <div>
    <div id='editControls'>

	<div class="btn-group" role="group" aria-label="...">
		<div class="btn-group" role="group">
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
				Him
				<span class="caret"></span>
			</button>
			<ul class="dropdown-menu" role="menu">
				<li><a data-role='h1' href='javascript:void(0)'>Character</a></li>
				<li><a data-role='h2' href='javascript:void(0)'>Parenthetical</a></li>
				<li><a data-role='h3' href='javascript:void(0)'>Dialog</a></li>
				<li><a data-role='h4' href='javascript:void(0)'>Action</a></li>
			</ul>
		</div>
		<div class="btn-group" role="group">
			<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
				Her
				<span class="caret"></span>
			</button>
			<ul class="dropdown-menu" role="menu">
				<li><a data-role='h5' href='javascript:void(0)'>Character</a></li>
				<li><a data-role='h6' href='javascript:void(0)'>Parenthetical</a></li>
				<li><a data-role='p' href='javascript:void(0)'>Dialog</a></li>
				<li><a data-role='div' href='javascript:void(0)'>Action</a></li>
			</ul>
		</div>
        <button type="button" class="btn btn-default" data-role='bold' href='javascript:void(0)'><i class='fa fa-bold'></i></button>
        <button type="button" class="btn btn-default" data-role='italic' href='javascript:void(0)'><i class='fa fa-italic'></i></button>
        <button type="button" class="btn btn-default" data-role='underline' href='javascript:void(0)'><i class='fa fa-underline'></i></button>
	</div>
    <div id='editor' contenteditable>
      <h1>This is a title!</h1>
      <p>This is just some example text to start us off</p>
    </div>
    <textarea id='output'></textarea>
  </div>
</div>











<textarea ng-model="obj.body" class="form-control" ng-blur="update()"></textarea>
			    </div>

			    <div class="form-group">
			        {{ Form::label('description', 'Brief Description') }}
<textarea ng-model="obj.description" class="form-control" ng-blur="update()"></textarea>
			    </div>

			    <div class="form-group">
			        {{ Form::label('user_id', 'User Id') }}
<input type="text" ng-model="obj.user_id" class="form-control" ng-blur="update()">
			    </div>

			    <div class="form-group">
			        {{ Form::label('public', 'Public') }}
<input type="text" ng-model="obj.public" class="form-control" ng-blur="update()">
			    </div>

			    <div class="form-group">
			        {{ Form::label('disabled', 'Disabled') }}
<input type="text" ng-model="obj.disabled" class="form-control" ng-blur="update()">
			    </div>

			    {{ Form::submit('Add Roleplayoutline', array('class' => 'btn btn-primary')) }}

		    {{ Form::close() }}
	    </div>
	</div>
</div>
@stop
@section('scripts')
	<script>

		$('#editControls a').click(function(e) {
			switch($(this).data('role')) {
			case 'h1':
			case 'h2':
			case 'h3':
			case 'h4':
			case 'h5':
			case 'h6':
			case 'p':
			case 'div':
				document.execCommand('formatBlock', false, $(this).data('role'));
				break;
			default:
				document.execCommand($(this).data('role'), false, null);
				break;
			}
			update_output();
		})

		$('#editor').bind('blur keyup paste copy cut mouseup', function(e) {
			update_output();
		})
		function update_output() {
			$('#output').val($('#editor').html());
		}

	</script>
@stop
