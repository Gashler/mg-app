@extends('layouts.default')
@section('content')
	<div class="edit">
		<div class="row">
			<div class="col col-md-12">
				@include('_helpers.breadcrumbs')
			    <h1>Edit Logic</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-3 col-lg-4 col-md-6 col-sm-8">
			    {{ Form::model($logic, array('route' => array('logic.update', $logic->id), 'method' => 'PUT')) }}

			    <div class="form-group">
			        {{ Form::label('action_id', 'Action Id') }}
                    <input type="text" ng-model="obj.action_id" class="form-control" ng-blur="update()">
			    </div>

			    <div class="form-group">
			        {{ Form::label('value', 'Value') }}
                    <input type="text" ng-model="obj.value" class="form-control" ng-blur="update()">
			    </div>

			    <div class="form-group">
			        {{ Form::label('result_id', 'Result Id') }}
                    <input type="text" ng-model="obj.result_id" class="form-control" ng-blur="update()">
			    </div>

			    <div class="form-group">
			        {{ Form::label('operator', 'Operator') }}
                    <input type="text" ng-model="obj.operator" class="form-control" ng-blur="update()">
			    </div>

			    <div class="form-group">
			        {{ Form::label('result_type', 'Result Type') }}
                    <input type="text" ng-model="obj.result_type" class="form-control" ng-blur="update()">
			    </div>

			    {{ Form::submit('Update Logic', array('class' => 'btn btn-primary')) }}

			    {{Form::close()}}
			</div>
		</div>
	</div>
@stop
