@extends('layouts.default')
@section('content')
	<div class="show">
		<div class="row page-actions">
			@include('_helpers.breadcrumbs')
			<h1 class="no-top">Viewing Logic</h1>
			@if (Auth::user()->hasRole(['Superadmin', 'Admin']))
			    <div class="btn-group" id="record-options">
				    <a class="btn btn-default" href="{{ url('logic/'.$logic->id .'/edit') }}" title="Edit"><i class="flaticon-pencil"></i></a>
				    @if ($logic->disabled == 0)
					    {{ Form::open(array('url' => 'Logic/disable', 'method' => 'DISABLE')) }}
					    	<input type="hidden" name="ids[]" value="{{ $logic->id }}">
					    	<button class="btn btn-default active" title="Currently enabled. Click to disable.">
					    		<i class="flaticon-eye-female"></i>
					    	</button>
					    {{ Form::close() }}
					@else
					    {{ Form::open(array('url' => 'Logic/enable', 'method' => 'ENABLE')) }}
					    	<input type="hidden" name="ids[]" value="{{ $logic->id }}">
					    	<button class="btn btn-default" title="Currently disabled. Click to enable.">
					    		<i class="flaticon-eye-female"></i>
					    	</button>
					    {{ Form::close() }}
					@endif
				    {{ Form::open(array('url' => 'logic/' . $logic->id, 'method' => 'DELETE', 'onsubmit' => 'return confirm("Are you sure you want to delete this logic? This cannot be undone.");')) }}
				    	<button class="btn btn-default" title="Delete">
				    		<i class="flaticon-trash" title="Delete"></i>
				    	</button>
				    {{ Form::close() }}
				</div>
			@endif
		</div><!-- row -->
		<div class="row">
			<div class="col col-xl-4 col-lg-6 col-md-8 col-sm-12">
			    <table class="table">
			        
			        <tr>
			            <th>Action Id:</th>
			            <td>{{ $logic->action_id }}</td>
			        </tr>
			        
			        <tr>
			            <th>Value:</th>
			            <td>{{ $logic->value }}</td>
			        </tr>
			        
			        <tr>
			            <th>Result Id:</th>
			            <td>{{ $logic->result_id }}</td>
			        </tr>
			        
			        <tr>
			            <th>Operator:</th>
			            <td>{{ $logic->operator }}</td>
			        </tr>
			        
			        <tr>
			            <th>Result Type:</th>
			            <td>{{ $logic->result_type }}</td>
			        </tr>
			        
			    </table>
		    </div>
		</div>
	</div>
@stop
