<!-- logic -->
<div class="form-group">
    {{ Form::label('morph_id', 'Action Id') }}
<input type="text" ng-model="obj.morph_id" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('subject_id', 'Action Id') }}
<input type="text" ng-model="obj.subject_id" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('value', 'Value') }}
<input type="text" ng-model="obj.value" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('result_id', 'Result Id') }}
<input type="text" ng-model="obj.result_id" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('operator', 'Operator') }}
<input type="text" ng-model="obj.operator" class="form-control" ng-blur="update()">
</div>

<div class="form-group">
    {{ Form::label('result_type', 'Result Type') }}
<input type="text" ng-model="obj.result_type" class="form-control" ng-blur="update()">
</div>
