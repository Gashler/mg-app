<div class="form-group" ng-controller="LogicController">
	{{ Form::label('logics', 'Logics') }}
    <ul class="list-group" ng-if="o.logics.length > 0">
        <li ng-repeat="logic in o.logics" class="list-group-item">
            <span ng-click="editLogic(logic.id)" class="link">
                <span ng-if="logic.description == null || logic.description == ''" ng-bind="'Logic ' + ($index + 1)"></span>
                <span ng-if="logic.description != null && logic.description != ''" ng-bind="logic.description"></span>
            </span>
            <i class="link flaticon-x pull-right" ng-click="deleteLogic(logic.id, $index)"></i>
        </li>
    </ul>
	<button type="button" class="btn btn-default" ng-click="createLogic()"><i class="flaticon-plus"></i> Add Logic</button>
	<img ng-if="loading_logic" src="/img/loading.gif">
    <div id="logicForm" class="popup">
        <div class="panel panel-primary">
	    	<div class="panel-heading">
	    		<h3 class="panel-title"><i class="flaticon-logic"></i> Create Logic<span class="x pull-right" ng-click="togglePopup('#logicForm')">&times;</span></h3>
	    	</div>
        	<div class="panel-body">
        		@include('logic.form')
        		<button type="button" class="btn btn-default" ng-click="togglePopup('#logicForm')">Close</button>
        	</div><!-- panel-body -->
        </div><!-- panel -->
	</div><!-- logicForm -->
</div>
<script src="/js/controllers/LogicController.js"></script>
