<?php
	if (Auth::user()) $layout = 'default';
	else $layout = 'public';
?>
@extends('layouts.' . $layout)
@section('content')
<div ng-app="app" class="index">
    {{ Form::open(array('url' => 'entries/disable', 'method' => 'POST')) }}
	    <div ng-controller="EntryController" class="my-controller">
	    	<div class="page-actions">
		        <div class="row">
		            <div class="col-md-12">
		                <h1 class="no-top no-bottom pull-left no-pull-xs">All Announcements</h1>
		                @if (Auth::user())
			            	<div class="pull-right hidable-xs">
			                    <div class="input-group pull-right">
			                    	<span class="input-group-addon no-width">Count</span>
			                    	<input class="form-control itemsPerPage width-auto" ng-model="pageSize" type="number" min="1">
			                    </div>
			                    <h4 class="pull-right margin-right-1">Page <span ng-bind="currentEntry"></span></h4>
			            	</div>
			        	@endif
			    	</div>
		        </div><!-- row -->
		        <div class="row">
		        	@if (Auth::user())
			            <div class="col-md-6 col-sm-6 col-xs-12 page-actions-left">
			            	@if (Auth::user() && Auth::user()->hasRole(['Admin', 'Editor']))
				                <div class="pull-left">
				                    <a class="btn btn-primary pull-left margin-right-1" title="New" href="{{ url('entries/create') }}"><i class="flaticon-plus"></i></a>
				                    <div class="pull-left">
				                        <div class="input-group">
				                            <select class="form-control selectpicker actions">
				                                <option value="entries/disable" selected>Disable</option>
				                                <option value="entries/enable">Enable</option>
				                                <option value="entries/delete">Delete</option>
				                            </select>
				                            <div class="input-group-btn no-width">
				                                <button class="btn btn-default applyAction" disabled>
				                                    <i class="flaticon-check"></i>
				                                </button>
				                            </div>
				                        </div>
				                    </div>
				                </div>
			                @endif
				        </div>
				        <div class="col-md-6 col-sm-6 col-xs-12">
			                <div class="input-group pull-right no-pull-xs">
			                    <input class="form-control ng-pristine ng-valid" placeholder="Search" name="new_tag" ng-model="search.$" onkeypress="return disableEnterKey(event)" type="text">
			                    <span class="input-group-btn no-width">
			                        <button class="btn btn-default" type="button">
			                            <i class="flaticon-search"></i>
			                        </button>
			                    </span>
			                </div>
			            </div><!-- col -->
			        @else
			        	<div class="col col-md-4">
			                <div class="input-group no-pull-xs">
			                    <input class="form-control ng-pristine ng-valid" placeholder="Search" name="new_tag" ng-model="search.$" onkeypress="return disableEnterKey(event)" type="text">
			                    <span class="input-group-btn no-width">
			                        <button class="btn btn-default" type="button">
			                            <i class="flaticon-search"></i>
			                        </button>
			                    </span>
			                </div>
			        	</div>
			        @endif
			        </div><!-- row -->
		    	</div><!-- page-actions -->
	        <div class="row">
	            <div class="col col-md-12">
	            	@if (Auth::user() && Auth::user()->hasRole(['Admin', 'Editor']))
	                	<table class="table">
	               	@else
	               		<table class="table width-auto">
	               	@endif
	                    <thead>
	                        <tr>
	                        	
	                        	@if (Auth::user() && Auth::user()->hasRole(['Admin', 'Editor']))
		                            <th>
		                            	<input type="checkbox">
		                            </th>
		                        @endif
                            	
                            	<th class="link" ng-click="orderByField='created_at'; reverseSort = !reverseSort">Date
                            		<span>
                            			<span ng-show="orderByField == 'created_at'">
	                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
	                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                            			</span>
                            		</span>
                        		</th>
                            	
                            	<th class="link" ng-click="orderByField='title'; reverseSort = !reverseSort">Title
                            		<span>
                            			<span ng-show="orderByField == 'title'">
	                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
	                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
                            			</span>
                            		</span>
                        		</th>
                        		
                        		@if (Auth::user() && Auth::user()->hasRole(['Admin', 'Editor']))
                        		
	                            	<th class="link" ng-click="orderByField='url'; reverseSort = !reverseSort">URL
	                            		<span>
	                            			<span ng-show="orderByField == 'url'">
		                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
		                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
	                            			</span>
	                            		</span>
	                        		</th>
	                        		
	                            	<th class="link" ng-click="orderByField='public'; reverseSort = !reverseSort">Public
	                            		<span>
	                            			<span ng-show="orderByField == 'public'">
		                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
		                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
	                            			</span>
	                            		</span>
	                        		</th>
	                        		
	                            	<th class="link" ng-click="orderByField='public'; reverseSort = !reverseSort">Members
	                            		<span>
	                            			<span ng-show="orderByField == 'public'">
		                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
		                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
	                            			</span>
	                            		</span>
	                        		</th>
	                        		
	                            	<th class="link" ng-click="orderByField='public'; reverseSort = !reverseSort">Customers
	                            		<span>
	                            			<span ng-show="orderByField == 'public'">
		                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
		                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
	                            			</span>
	                            		</span>
	                        		</th>
	                        		                      		
	                            	<th class="link" ng-click="orderByField='updated_at'; reverseSort = !reverseSort">Modified
	                            		<span>
	                            			<span ng-show="orderByField == 'updated_at'">
		                            			<span ng-show="!reverseSort"><i class='flaticon-sort-asc'></i></span>
		                            			<span ng-show="reverseSort"><i class='flaticon-sort-desc'></i></span>
	                            			</span>
	                            		</span>
	                        		</th>
                        		@endif
                        			
	                        </tr>
	                    </thead>
	                    <tbody>
	                        <tr ng-class="{ highlight : entry.new == 1, semitransparent : entry.disabled == 1 }" dir-paginate-start="entry in entries | filter:search | orderBy: '-updated_at' | orderBy:orderByField:reverseSort | itemsPerPage: entrySize" current-entry="currentEntry">
	                            @if (Auth::user() && Auth::user()->hasRole(['Admin', 'Editor']))
		                            <td ng-click="checkbox()">
		                            	<input class="bulk-check" type="checkbox" name="ids[]" value="@include('_helpers.entry_id')">
		                            </td>
		                        @endif
							
					            <td>
					            	<span ng-bind="entry.formatted_date"></span>
					            </td>
								
					            <td>
					            	@if (Auth::user() && Auth::user()->hasRole(['Admin', 'Editor']))
					            		<a href="/entries/@include('_helpers.entry_id')/edit">
					            	@else
					            		<a href="/entries/@include('_helpers.entry_url')">
					         		@endif
					            		<span ng-bind="entry.title"></span>
					            	</a>
					            </td>
					            
					            @if (Auth::user() && Auth::user()->hasRole(['Admin', 'Editor']))
						            <td>
						                <span ng-bind="entry.url"></span>
						            </td>
						            
						            <td>
						                 <span ng-if="entry.public"><i class="flaticon-check"></i></span>
						            </td>
						            
						            <td>
						                 <span ng-if="entry.reps"><i class="flaticon-check"></i></span>
						            </td>
						            
						            <td>
						                 <span ng-if="entry.customers"><i class="flaticon-check"></i></span>
						            </td>
	
						            <td>
						            	<span ng-bind="entry.updated_at"></span>
						            </td>
						    	@endif
						            
	                        </tr>
	                        <tr dir-paginate-end></tr>
	                    </tbody>
	                </table>
	                	@include('_helpers.loading')<div ng-controller="OtherController" class="other-controller">
	                    <div class="text-center">
	                        <dir-pagination-controls boundary-links="true" on-entry-change="entryChangeHandler(newEntryNumber)" template-url="/packages/dirpagination/dirPagination.tpl.html"></dir-pagination-controls>
	                    </div>
	                </div>
	            </div><!-- col -->
	        </div><!-- row -->
        {{ Form::close() }}
    </div><!-- app -->
@stop
@section('scripts')
<script>

	var app = angular.module('app', ['angularUtils.directives.dirPagination']);
	
	function EntryController($scope, $http) {
	
		$http.get('/api/all-entries').success(function(entries) {
			$scope.entries = entries;
			console.log($scope.entries);
			@include('_helpers.bulk_action_checkboxes')
			
		});
		
		$scope.currentPage = 1;
		$scope.entrySize = 10;
		$scope.meals = [];
		
		$scope.pageChangeHandler = function(num) {
			
		};
		
	}
	
	function OtherController($scope) {
		$scope.pageChangeHandler = function(num) {
		};
	}

</script>
@stop