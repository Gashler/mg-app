<?php
	if (Auth::user()) $layout = 'default';
	else $layout = 'public';
?>
@extends('layouts.' . $layout)
@section('content')
	<div class="row">
		<div class="col col-md-6">
			@include('_helpers.breadcrumbs')
			<h1 class="no-bottom">{{ $entry->title }}</h1>
			<div class="date">
				@if ($entry->publish_date != 0)
					{{ date('M d Y', strtotime($entry->publish_date)) }}
				@else
					{{ date('M d Y', strtotime($entry->created_at)) }}
				@endif
			</div>
			@if (isset($entry->description))
				<p><em>{{ $entry->description }}</em></p>
			@endif
			{{ $entry->body }}
		</div><!-- col -->
	</div><!-- row -->
@stop
