@extends('emails.layouts.basic')

@section('body')
	{{$user['husband']}} {{$user['wife']}},
	<p />
	<p />
	{{ $body}}
@stop

@section('footer')
	@parent
@stop