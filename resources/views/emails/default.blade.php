<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
  <head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
      body { background: rgb(215,215,215); padding: 2.5%; font-family: verdana; line-height: 1.5em; font-size: 12pt; }
      article { background: white; max-width: 1200px; box-shadow: 0 0 25px rgba(0,0,0,.12); }
      header { background: url('{{ env('APP_URL') }}/img/noise.png') rgb(128,0,64); padding: 2.5%; color: white; text-align: center; }
      header p { margin: 0; font-size: 10pt; }
      section { background: white; padding: 5%; }
      footer { padding: 5%; font-size: 9pt; color: gray; line-height: 1.5em; text-align: center; }
      strong { color: rgb(128,0,64); }
      
      .btn, a.btn { display: inline-block; background: gray; padding: 2.5%; color: white !important; text-decoration: none; border-radius: 6px; box-shadow: 0 -4px rgba(0,0,0,.2) inset, 0 4px rgba(255,255,255,.4) inset; font-weight: bold; }
      .btn-primary { background: rgb(255,0,128); }
      .btn-primary:hover { box-shadow: 0 -6px rgba(0,0,0,.2) inset, 0 6px rgba(255,255,255,.4) inset; }
    </style>
  </head>
  <body style="background: rgb(215,215,215); padding: 2.5%; font-family: verdana; line-height: 1.5em; font-size: 12pt">
    <table cellpadding="0" cellspacing="0" class="article" style="background: white; max-width: 1200px; box-shadow: 0 0 25px rgba(0,0,0,.12); max-width: 600px; margin: 0 auto;">
      <tr class="header" style="background: url('{{ env('APP_URL') }}/img/noise.png') rgb(128,0,64); color: white; text-align: center;">
        <td style="padding: 5%;">
          <img src="{{ env('APP_URL') }}/img/logo-200.png" alt="{{ env('COMPANY_NAME') }}">
          <p style="margin: 0; font-size: 10pt; color: white;">Spice Up Your Love Life</p>
        </td>
      </tr>
      <tr class="section" style="background: white;">
        <td style="padding: 5%;">
		  <p>{{ $user['first_name'] }},</p>
		  {!! $email['body'] !!}
        </td>
      </tr>
    </table>
    <table cellpadding="0" cellspacing="0" class="footer" style="font-size: 9pt; color: gray; line-height: 1.5em; text-align: center; max-width: 600px; margin: 0 auto;">
      <tr>
        <td style="padding: 5%; text-align: center;">
          <p>
            The {{ env('COMPANY_NAME') }} team<br>
            Orem, UT, USA<br>
            {{ env('APP_URL') }}
          </p>
		      <p>If you have any questions, please contact customer support at <a href="{{ env('APP_URL') }}/contact">{{ env('APP_URL') }}/contact</a>.
          <p>You're receiving this email because, at some time, this email address was used to sign up for a subscription with {{ env('COMPANY_NAME') }}. If you don't wish to receive further communications, please <a href="{{ env('APP_URL') }}/unsubscribe?email={{ $user['email'] }}">unsubscribe</a>.</p>
        </td>
      </tr>
    </table>
  </body>
</html>
