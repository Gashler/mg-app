@extends('layouts.default')
@section('style')
	{{ HTML::style('css/game.css') }}
@stop
@section('content')
	<div ng-controller="GamePlayController">
		<img src="/img/loading.gif" ng-show="!loaded">
		<div class="row">
			<div class="col-md-12">
				<div class="pull-right btn-group">
					<a class="btn btn-default" ng-if="game.user_id == {{ Auth::user()->id }}" href="/game/@{{game.game_id}}/edit"><i class="fa fa-pause"></i></a>
					<a class="btn btn-default" ng-if="game.user_id == {{ Auth::user()->id }}" href="/Game/end/@{{game.id}}"><i class="flaticon-square"></i></a>
					<a class="btn btn-default" ng-if="game.user_id == {{ Auth::user()->id }}" href="/Game/end/@{{game.id}}/1"><i class="flaticon-loading"></i></a>
				</div>
				<h1 class="no-top" ng-bind="game.name"></h1>
			</div>
		</div>
		<div class="row" ng-show="loaded">
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-12">

						<!-- navigator toolbar -->
						<div class="btn-group">
							<button class="btn btn-default no-border-bottom-left-radius">
								<i class="flaticon-hand"></i>
							</button>
							<button class="btn btn-default no-border-bottom-right-radius">
								<i class="flaticon-comment"></i>
							</button>
						</div>

						<!-- navigator -->
						<div id="navigator" class="align-center well overflow-hidden no-border-top-left-radius">

							<!-- compass -->
							<div class="pull-left compass">
								<button id="n" ng-click="changePlace('n')" class="btn btn-default north direction">
									<i class="fa fa-caret-up"></i>
									<br>
									<span>N</span>
								</button>
								<br>
								<div class="btn-group">
									<button id="w" ng-click="changePlace('w')" class="btn btn-default west direction">
										<i class="fa fa-caret-left"></i> W
									</button>
									<button id="e" ng-click="changePlace('e')" class="btn btn-default east direction">
										E <i class="fa fa-caret-right"></i>
									</button>
								</div>
								<br>
								<button id="s" ng-click="changePlace('s')" class="btn btn-default south direction">
									<span>S</span>
									<br>
									<i class="fa fa-caret-down"></i>
								</button>
							</div>

							<!-- characters -->
							<div ng-if="place.characters.length > 0" class="panel panel-primary pull-right">
								<div class="panel-heading">
									<h3 class="panel-title"><i class="flaticon-user"></i> Characters</h3>
								</div>
								<ul class="list-group">
									<li class="list-group-item link" ng-repeat="character in place.characters">
										<span ng-bind="character.first_name"></span>
									</li>
								</ul>
							</div>

							<!-- place -->
							<h2 ng-bind="place.name"></h2>
						    <p ng-bind="place.description"></p>

							<!-- objects -->
							<div ng-if="place.objects.length > 0" class="panel panel-primary pull-right">
								<div class="panel-heading">
									<h3 class="panel-title">Objects</h3>
								</div>
								<ul class="list-group">
									<li class="list-group-item link" ng-repeat="object in place.objects">
										<span ng-if="o.takeable" ng-click="takeObject(object, $index)">
											<i class="flaticon-hand"></i> <span ng-bind="o.name"></span>
										</span>
										<span ng-if="!o.takeable" ng-bind="o.name"></span>
									</li>
								</ul>
							</div>

						</div><!-- well -->
					</div><!-- col -->
				</div><!-- row -->
				<div class="row">

					<div class="col-md-6">
						<div id="map" class="panel panel-primary">
						    <table style="width:auto !important;">
						        <tr ng-if="$index !== 0 && $index !== game.rows.length - 1" ng-repeat="(row_index, row) in game.rows">
						            <td ng-if="$index !== 0 && $index !== row.cols.length - 1" ng-repeat="(col_index, col) in row.cols" class="tile">
						                <div ng-if="col.place == null" class="space" ng-click="createPlace(row.id, row_index, col.id, col_index)"></div>
						                <div ng-if="col.place !== null" class="place" ng-click="editPlace(col.place)">
						                    <span ng-if="col.place.current" class="label label-default">Player!</span>
						                    <div ng-bind="col.place.name"></div>
						                </div>
						            </td>
						        </tr>
						    </table>
						</div>
					</div><!-- col -->

					<div class="col-md-6">

						<!-- events -->
						<div class="well overflow-hidden">

							<!-- images -->
							<img class="full-width border-radius margin-bottom-4" ng-src="@{{media.largest}}" ng-repeat="media in uvent.medias">

							<!-- objects -->
							<div ng-if="uvent.objects.length > 0" class="panel panel-primary pull-right">
								<div class="panel-heading">
									<h3 class="panel-title">Objects</h3>
								</div>
								<ul class="list-group">
									<li class="list-group-item link" ng-repeat="object in uvent.objects">
										<span ng-if="o.takeable" ng-click="takeObject(object, $index)">
											<i class="flaticon-hand"></i> <span ng-bind="o.name"></span>
										</span>
										<span ng-if="!o.takeable" ng-bind="o.name"></span>
									</li>
								</ul>
							</div>

							<div ng-if="uvent.timer" ng-bind="uvent.timer.value" class="pull-right font-size-4"></div>
							<!-- <h2 class="no-top" ng-bind="uvent.name"></h2> -->
							<p class="no-bottom" ng-bind="uvent.description"></p>

							<!-- actions -->
							<ul class="list-group">
								<li class="list-group-item" ng-repeat="action in uvent.actions" ng-bind="action.description"></li>
							</ul>

						</div><!-- well -->
					</div><!-- col -->
				</div><!-- row -->
				<div class="row">
					<div class="col">

						<ul class="list-group" ng-if="uvent.options.length > 0">
							<li class="link list-group-item" ng-repeat="option in uvent.options" ng-click="selectOption(option.result_id)">
								@{{option.description}}
								<i class="pull-right fa fa-angle-right"></i>
							</li>
						</ul>
						<div ng-if="uvent.options.length == 0">
							<button ng-if="uvent.result_id !== '-1' && uvent.result_id !== 0" class="btn btn-primary" ng-click="nextUvent(uvent.result_id)">Next <i class="fa fa-angle-right"></i></button>
							<button ng-if="uvent.result_id == 0" class="btn btn-primary" ng-click="nextUvent(uvent.id)">Repeat <i class="fa fa-angle-right"></i></button>
							<button ng-if="uvent.result_id == -1" class="btn btn-primary" ng-click="nextUvent(game.uvents[0].id)">Play Again <i class="fa fa-angle-right"></i></button>
						</div>
					</div><!-- col -->
				</div><!-- row -->
			</div><!-- col -->
			<div class="col-md-4">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h3 class="panel-title"><i class="flaticon-user"></i> @{{player.name}}</h3>
					</div>
					<div class="panel-body">
						<div class="panel panel-primary">
							<div class="panel-heading">
								<h3 class="panel-title">Possessions</h3>
							</div>
							<ul class="list-group">
								<li class="link list-group-item" ng-repeat="object in player.objects">
									@{{o.name}}
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div><!-- col -->
		</div><!-- row -->
	</div><!-- controller -->
@stop
@section('scripts')
	<script>
		game_id = {{ $id }};
	</script>
	<script src="/js/controllers/GamePlayController.js"></script>
@stop
