<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Site general configutaion
    |--------------------------------------------------------------------------
    |
    | This file is for storage of settings for site

    |
    */

    'company_name' => env('COMPANY_NAME'),
    'support_email' => 'marriedgames@gmail.com',
    'app_url' => env('APP_URL'),
    'host_url' => env('HOST_URL'),
    'controllers_path' =>  '',
    'default_money' => 500,
    'default_subscription_price' => 99.99,
    'flaticon_face_f_max' => 14, // Number of Flaticon Female Faces
    'flaticon_face_m_max' => 43, // Number of Flaticon Male Faces
    'free_trial_days' => 30,
    'models_path' => '', // Namespace for Models
    'models_path' => '',
    'money_name' => 'love buck',
    'money_name_plural' => 'love bucks',
    'plans' => [
        'monthly' => 5,
        'yearly' => 50
    ],
    'trial_name' => "Play Now",
    'version' => '2.0',
    'month_from_now' => date('m/d/y', strtotime('30 days'))
];
