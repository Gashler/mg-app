<?php

use OAuth\Common\Storage\Session;
/*
|--------------------------------------------------------------------------
| oAuth Config
|--------------------------------------------------------------------------
*/
return [
    'uris' => [
        'Facebook' => '/me?locale=en_US&fields=name,email,first_name,last_name',
        'Google' => 'https://www.googleapis.com/oauth2/v1/userinfo',
        'Instagram' => 'users/self'
    ],
    'storage' => new Session(),
    'consumers' => [
        'Facebook' => [
            'client_id'     => env('FACEBOOK_'.strtoupper(env('APP_ENV')).'_APP_ID'),
            'client_secret' => env('FACEBOOK_'.strtoupper(env('APP_ENV')).'_APP_SECRET'),
            'scope'         => [
                'public_profile',
                'email',
                // 'user_friends',
                // 'publish_actions'
            ]
        ],
        'Google' => [
            'client_id'     => env('GOOGLE_APP_ID'),
            'client_secret' => env('GOOGLE_APP_SECRET'),
            'scope'         => ['userinfo_email', 'userinfo_profile']
        ],
        'Instagram' => [
            'client_id'     => env('INSTAGRAM_APP_ID'),
            'client_secret' => env('INSTAGRAM_APP_SECRET'),
            'scope'         => ['basic', 'comments', 'relationships', 'likes']
        ]
    ]
];
