<?php 

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateModificationsTable extends Migration 
{

	public function up()
	{
		Schema::create('modifications', function(Blueprint $table) {
			$table->increments('id');
			$table->string('modifiable_type');
			$table->string('col');
			$table->string('game_id');
			$table->string('account_id');
			$table->integer('modifiable_id');
			$table->text('value');
			$table->timestamp('created_at');
			$table->timestamp('updated_at');
		});
	}

	public function down()
	{
		Schema::dropIfExists('modifications');
	}

}
