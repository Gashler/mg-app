	<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCategoriesTable extends Migration
{

	public function up()
	{
		Schema::create('categories', function(Blueprint $table) {
			$table->increments('id');
			$table->string('key');
			$table->string('name');
			$table->double('rate');
			$table->text('description');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('categories');
	}

}
