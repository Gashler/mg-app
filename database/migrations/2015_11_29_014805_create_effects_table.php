<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEffectsTable extends Migration
{

	public function up()
	{
		Schema::create('effects', function(Blueprint $table) {
			$table->increments('id');
            $table->string('destination_type');
            $table->integer('destination_id');
            $table->integer('qty');
            $table->integer('logic_id');
            $table->integer('max');
    		$table->integer('min');
            $table->boolean('negative');
            $table->string('operator');
            $table->integer('property_id');
            $table->integer('property_index');
			$table->boolean('rand');
            $table->string('scope');
            $table->integer('subject_id')->nullable();
            $table->string('subject_type');
            $table->string('type');
            $table->integer('propertyOption_id');
            $table->text('value');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('effects');
	}

}
