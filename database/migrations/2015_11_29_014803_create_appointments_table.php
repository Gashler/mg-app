<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAppointmentsTable extends Migration
{

    public function up()
    {
        Schema::create('appointments', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id');
            $table->boolean('alternate');
            $table->text('description')->nullable();
            $table->date('date_start');
            $table->date('date_end');
            $table->string('in_charge', 1);
            $table->time('time_start');
            $table->time('time_end');
            $table->string('name')->nullable();
            $table->boolean('recurring');
            $table->integer('recurring_interval');
            $table->string('turn');
            $table->boolean('sun');
            $table->boolean('mon');
            $table->boolean('tue');
            $table->boolean('wed');
            $table->boolean('thu');
            $table->boolean('fri');
            $table->boolean('sat');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('appointments');
    }

}
