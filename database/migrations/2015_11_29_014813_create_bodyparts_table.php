<?php 

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBodypartsTable extends Migration 
{

	public function up()
	{
		Schema::create('bodyparts', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('gender');
			$table->timestamp('created_at');
			$table->timestamp('updated_at');
		});
	}

	public function down()
	{
		Schema::dropIfExists('bodyparts');
	}

}
