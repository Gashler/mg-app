<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlacesTable extends Migration
{

    public function up()
    {
        Schema::create('places', function(Blueprint $table) {
            $table->increments('id');
            $table->boolean('block_top');
            $table->boolean('block_right');
            $table->boolean('block_bottom');
            $table->boolean('block_left');
            $table->integer('col_id');
            $table->boolean('current');
            $table->text('description')->nullable();
            $table->integer('down_id')->nullable();
            $table->integer('down_type')->nullable();
            $table->integer('left_type')->nullable();
            $table->integer('left_id')->nullable();
            $table->integer('mid')->nullable();
            $table->string('name')->nullable();
            $table->integer('right_type')->nullable();
            $table->integer('right_id')->nullable();
            $table->integer('parent_id')->nullable();
            $table->integer('row_id');
            $table->boolean('start');
            $table->integer('up_type')->nullable();
            $table->integer('up_id')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('places');
    }

}
