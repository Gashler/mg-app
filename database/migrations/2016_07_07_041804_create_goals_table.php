<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGoalsTable extends Migration
{

    public function up()
    {
        Schema::create('goals', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('orgasm');
            $table->integer('sex');
            $table->integer('romantic_date');
            $table->integer('quality_time');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('goals');
    }

}
