<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoleplayScriptsTable extends Migration
{

	public function up()
	{
		Schema::create('roleplay_scripts', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->text('description')->nullable();
			$table->text('body');
			$table->integer('user_id')->nullable();
			$table->boolean('public')->default(1);
			$table->string('icon')->nullable();
			$table->boolean('disabled');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('roleplay_scripts');
	}

}
