<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDialogsTable extends Migration
{

    public function up()
    {
        Schema::create('dialogs', function(Blueprint $table) {
            $table->increments('id');
            $table->text('body');
            $table->integer('character_id');
            $table->integer('effect_id');
            $table->integer('group_id');
            $table->integer('order');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('dialogs');
    }

}
