<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGamesTable extends Migration
{

	public function up()
	{
		Schema::create('games', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('player_id')->nullable();
			$table->integer('user_id')->nullable();
			$table->string('name')->nullable();
			$table->text('description')->nullable();
			$table->boolean('public')->nullable();
			$table->text('rating')->nullable();
			$table->integer('game_id')->nullable();
			$table->integer('width');
			$table->timestamp('created_at');
			$table->timestamp('updated_at');
		});
	}

	public function down()
	{
		Schema::dropIfExists('games');
	}

}
