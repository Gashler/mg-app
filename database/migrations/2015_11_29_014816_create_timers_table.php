<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTimersTable extends Migration
{

	public function up()
	{
		Schema::create('timers', function(Blueprint $table) {
			$table->increments('id');
			$table->string('direction');
			$table->integer('morph_id');
			$table->string('morph_type');
			$table->integer('result_id');
			$table->string('result_type');
			$table->integer('min');
			$table->boolean('set_max');
			$table->integer('max');
			$table->integer('value');
			$table->boolean('active');
			$table->boolean('game_id');
			$table->timestamp('created_at');
			$table->timestamp('updated_at');
		});
	}

	public function down()
	{
		Schema::dropIfExists('timers');
	}

}
