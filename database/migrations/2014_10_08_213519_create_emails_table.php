<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmailsTable extends Migration
{

	public function up()
	{
		Schema::create('emails', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('template_id')->default(1);
			$table->string('key');
			$table->string('subject', 100);
			$table->text('body');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('emails');
	}

}
