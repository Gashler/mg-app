<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateColsTable extends Migration
{

	public function up()
	{
		Schema::create('cols', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('row_id');
			$table->integer('order');
			$table->timestamp('created_at');
			$table->timestamp('updated_at');
		});
	}

	public function down()
	{
		Schema::dropIfExists('cols');
	}

}
