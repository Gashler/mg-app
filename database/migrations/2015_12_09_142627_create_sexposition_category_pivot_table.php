<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSexpositionCategoryPivotTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sexposition_category_pivot', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('sex_position_category_id')->unsigned()->index();
			// $table->foreign('sex_position_category_id')->references('id')->on('sex_position_categories')->onDelete('cascade');
			$table->integer('sex_position_id')->unsigned()->index();
			// $table->foreign('sex_position_id')->references('id')->on('sex_positions')->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sexposition_category_pivot');
	}

}
