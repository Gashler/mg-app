<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmailTemplatesTable extends Migration
{

	public function up()
	{
		Schema::create('email_templates', function(Blueprint $table) {
			$table->increments('id');
			$table->string('key');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('email_templates');
	}

}
