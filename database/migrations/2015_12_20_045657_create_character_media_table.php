<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCharacterMediaTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('character_media', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('character_id')->unsigned()->index();
			$table->foreign('character_id')->references('id')->on('characters')->onDelete('cascade');
			$table->integer('media_id')->unsigned()->index();
			$table->foreign('media_id')->references('id')->on('medias')->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('character_media');
	}

}
