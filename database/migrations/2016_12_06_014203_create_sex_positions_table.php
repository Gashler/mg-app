<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSexPositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sex_positions', function ($table) {
            $table->increments('id');
            $table->string('name', 50);
            $table->string('key', 25);
            $table->text('description');
            $table->float('rating');
            $table->boolean('tall')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sex_positions');
    }
}
