<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEntriesTable extends Migration
{

	public function up()
	{
		Schema::create('entries', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('account_id');
			$table->string('title', 75)->nullable();
			$table->text('body')->nullable();
			$table->boolean('public')->default(0);
			$table->boolean('disabled')->default(0);
			$table->dateTime('publish_date')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('entries');
	}

}
