<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRolesTable extends Migration
{

    public function up()
    {
        Schema::create('roles', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->boolean('disabled');
            $table->timestamps();
        });
        Role::create(["name"=>"Subscriber"]);
        Role::create(["name"=>"Trial"]);
        Role::create(["name"=>"Member"]);
        Role::create(["name"=>"Editor"]);
        Role::create(["name"=>"Admin"]);
    }

    public function down()
    {
        Schema::dropIfExists('roles');
    }

}
