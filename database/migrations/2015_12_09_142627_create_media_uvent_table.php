<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMediaUventTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('media_uvent', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('media_id')->unsigned()->index();
			$table->foreign('media_id')->references('id')->on('medias')->onDelete('cascade');
			$table->integer('uvent_id')->unsigned()->index();
			$table->foreign('uvent_id')->references('id')->on('uvents')->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('media_uvent');
	}

}
