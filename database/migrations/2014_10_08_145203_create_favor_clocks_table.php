<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFavorClocksTable extends Migration
{

    public function up()
    {
        Schema::create('favor_clocks', function(Blueprint $table) {
            $table->increments('id');
            $table->boolean('active')->default(0);
            $table->boolean('increasing')->default(1);
            $table->integer('time_owed')->default(0);
            $table->integer('account_id')->nullable();
            $table->string('ower', 1)->nullable();
            $table->string('server', 1)->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('favor_clocks');
    }

}
