<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMediaPlaceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('media_place', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('media_id')->unsigned()->index();
			$table->foreign('media_id')->references('id')->on('medias')->onDelete('cascade');
			$table->integer('place_id')->unsigned()->index();
			$table->foreign('place_id')->references('id')->on('places')->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('media_place');
	}

}
