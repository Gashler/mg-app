<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCharactersTable extends Migration
{

    public function up()
    {
        Schema::create('characters', function(Blueprint $table) {
            $table->increments('id');
            $table->text('description')->nullable();
            $table->string('first_name');
            $table->integer('game_id');
            $table->string('gender');
            $table->boolean('hidden');
            $table->string('icon');
            $table->boolean('independent');
            $table->string('last_name')->nullable();
            $table->integer('mid')->nullable();
            $table->integer('place_id');
            $table->integer('player_id');
            $table->text('type');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('characters');
    }

}
