<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMapsTable extends Migration
{

	public function up()
	{
		Schema::create('maps', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name');
			$table->string('morph_type');
			$table->integer('morph_id');
			$table->timestamp('created_at');
			$table->timestamp('updated_at');
		});
	}

	public function down()
	{
		Schema::dropIfExists('maps');
	}

}
