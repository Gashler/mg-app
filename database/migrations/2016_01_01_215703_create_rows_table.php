<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRowsTable extends Migration
{

	public function up()
	{
		Schema::create('rows', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('map_id');
			$table->integer('order');
			$table->timestamp('created_at');
			$table->timestamp('updated_at');
		});
	}

	public function down()
	{
		Schema::dropIfExists('rows');
	}

}
