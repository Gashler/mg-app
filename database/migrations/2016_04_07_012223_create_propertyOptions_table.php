<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertyOptionsTable extends Migration
{

	public function up()
	{
		Schema::create('propertyOptions', function(Blueprint $table) {
			$table->increments('id');
            $table->text('description')->nullable();
			$table->string('name');
            $table->integer('property_id');
			$table->string('value');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('propertyOptions');
	}

}
