<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMediaObjectTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('media_object', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('media_id')->unsigned()->index();
			$table->foreign('media_id')->references('id')->on('medias')->onDelete('cascade');
			$table->integer('object_id')->unsigned()->index();
			$table->foreign('object_id')->references('id')->on('objects')->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('media_object');
	}

}
