<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateNarrationsTable extends Migration
{

    public function up()
    {
        Schema::create('narrations', function (Blueprint $table) {
            $table->increments('id');
            $table->text('body')->nullable();
            $table->integer('game_id');
            $table->integer('morph_id');
            $table->string('morph_type');
            $table->string('type');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('narrations');
    }

}
