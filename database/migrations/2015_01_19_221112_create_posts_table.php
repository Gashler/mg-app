<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostsTable extends Migration
{

	public function up()
	{
		Schema::create('posts', function(Blueprint $table) {
			$table->increments('id');
			$table->string('image', 255)->nullable();
			$table->string('title')->nullable();
			$table->string('name')->nullable();
			$table->text('description')->nullable();
			$table->text('body')->nullable();
			$table->date('publish_date')->nullable();
			$table->boolean('public')->default(0);
			$table->boolean('disabled')->default(0);
			$table->integer('user_id')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('posts');
	}

}
