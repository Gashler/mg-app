<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateObjectsTable extends Migration
{

    public function up()
    {
        Schema::create('objects', function(Blueprint $table) {
            $table->increments('id');
            $table->text('description');
            $table->integer('game_id');
            $table->boolean('hidden');
            $table->integer('mid')->nullable();
            $table->string('morph_type');
            $table->integer('morph_id');
            $table->string('name');
            $table->boolean('takeable');
            $table->string('type');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('objects');
    }

}
