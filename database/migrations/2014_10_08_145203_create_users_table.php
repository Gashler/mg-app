<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration
{

    public function up()
    {
        Schema::create('users', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('account_id')->nullable();
            $table->boolean('disabled');
            $table->string('email', 100)->unique();
            $table->json('campaign_emails_received')->nullable();
            $table->date('dob')->nullable();
            $table->string('first_name',50);
            $table->string('gender',2);
            $table->string('key', 40);
            $table->string('last_name',50)->nullable();
            $table->string('login')->nullable();
            $table->integer('money');
            $table->boolean('need_password')->default(0);
            $table->string('password', 100);
            $table->string('phone',15)->nullable();
            $table->string('public_id',25)->nullable();
            $table->timestamp('received_campaign_email_at')->nullable();
            $table->integer('role_id');
            $table->integer('sponsor_id')->nullable();
            $table->integer('spouse_id');
            $table->boolean('unsubscribed')->nullable();
            $table->boolean('verified')->default(0);
            $table->integer('wp_user_id')->nullable();
            $table->integer('logins')->default(0);
            $table->timestamps();
            $table->rememberToken();
        });
    }

    public function down()
    {
        Schema::dropIfExists('users');
    }

}
