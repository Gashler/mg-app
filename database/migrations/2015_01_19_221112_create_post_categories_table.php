<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostCategoriesTable extends Migration
{

	public function up()
	{
		Schema::create('post_categories', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name')->nullable();
			$table->text('description')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('post_categories');
	}

}
