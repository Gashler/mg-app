<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePropertiesTable extends Migration
{

	public function up()
	{
		Schema::create('properties', function(Blueprint $table) {
			$table->increments('id');
            $table->boolean('column');
            $table->text('description')->nullable();
            $table->string('key');
            $table->integer('morph_id')->nullable();
			$table->string('morph_type');
            $table->string('name');
			$table->text('value');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('properties');
	}

}
