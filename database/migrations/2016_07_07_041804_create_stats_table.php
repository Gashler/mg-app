<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStatsTable extends Migration
{

    public function up()
    {
        Schema::create('stats', function(Blueprint $table) {
            $table->increments('id');
            $table->date('date');
            $table->integer('account_id');
            $table->boolean('m_orgasm');
            $table->boolean('f_orgasm');
            $table->boolean('sex');
            $table->boolean('romantic_date');
            $table->boolean('quality_time');
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    public function down()
    {
        Schema::dropIfExists('stats');
    }

}
