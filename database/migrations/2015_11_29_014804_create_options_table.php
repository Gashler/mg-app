<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOptionsTable extends Migration
{

	public function up()
	{
		Schema::create('options', function(Blueprint $table) {
			$table->increments('id');
			$table->string('morph_type');
			$table->integer('morph_id');
			$table->integer('result_id');
			$table->integer('order');
			$table->string('name');
			$table->string('result_type');
			$table->text('description');
			$table->integer('game_id');
			$table->timestamp('created_at');
			$table->timestamp('updated_at');
		});
	}

	public function down()
	{
		Schema::dropIfExists('options');
	}

}
