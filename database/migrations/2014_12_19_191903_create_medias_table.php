<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMediasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('medias', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('type');
			$table->string('url');
			$table->string('title');
			$table->text('description');
			$table->integer('account_id');
			$table->integer('user_id');
			$table->integer('height');
			$table->integer('width');
			$table->integer('size');
			$table->string('extension');
			$table->boolean('disabled');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('medias');
	}

}
