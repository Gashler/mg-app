 <?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLoversLaneTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lovers_lane', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('account_id');
			$table->boolean('multiple_devices');
			$table->text('data');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('lovers_lane');
	}

}
