<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignEmailPivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaign_email', function (Blueprint $table)
        {
            $table->integer('campaign_id');
            $table->integer('email_id');
            $table->primary(['campaign_id', 'email_id']);
            $table->integer('interval');
            $table->string('interval_type', 25)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('campaign_email');
    }
}
