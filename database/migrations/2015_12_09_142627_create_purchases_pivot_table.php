<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePurchasesPivotTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('purchases_pivot', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('service_id')->unsigned()->index();
			// $table->foreign('user_category_id')->references('id')->on('user_categories')->onDelete('cascade');
			$table->integer('user_id')->unsigned()->index();
			// $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('purchases_pivot');
	}

}
