<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUventsTable extends Migration
{

	public function up()
	{
		Schema::create('uvents', function(Blueprint $table) {
			$table->increments('id');
			$table->string('result_type')->nullable();
			$table->integer('result_id')->nullable();
			$table->integer('date_start')->nullable();
			$table->integer('date_end')->nullable();
			$table->string('name')->nullable();
			$table->text('description')->nullable();
			$table->string('turn');
			$table->string('morph_type');
			$table->integer('morph_id');
			$table->integer('parent_id');
			$table->timestamp('created_at');
			$table->timestamp('updated_at');
		});
	}

	public function down()
	{
		Schema::dropIfExists('uvents');
	}

}
