<?php 

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoleplaysTable extends Migration 
{

	public function up()
	{
		Schema::create('roleplays', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id');
			$table->string('title');
			$table->string('his_name');
			$table->string('her_name');
			$table->string('his_personality');
			$table->string('her_personality');
			$table->string('his_role');
			$table->string('her_role');
			$table->string('location');
			$table->string('story_type');
			$table->string('first_line_sayer');
			$table->text('first_line');
			$table->text('description');
			$table->boolean('public');
			$table->boolean('disabled');
			$table->timestamp('created_at');
			$table->timestamp('updated_at');
		});
	}

	public function down()
	{
		Schema::dropIfExists('roleplays');
	}

}
