<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActionUventTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('action_uvent', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('action_id')->unsigned()->index();
			$table->foreign('action_id')->references('id')->on('actions')->onDelete('cascade');
			$table->integer('uvent_id')->unsigned()->index();
			$table->foreign('uvent_id')->references('id')->on('uvents')->onDelete('cascade');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('action_uvent');
	}

}
