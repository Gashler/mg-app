<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTriggersTable extends Migration
{

    public function up()
    {
        Schema::create('triggers', function(Blueprint $table) {
            $table->increments('id');
            $table->string('act_type');
            $table->integer('distance');
            $table->integer('logic_id');
            $table->string('operator_1');
            $table->string('operator_2');
            $table->string('scope');
            $table->integer('subject_id');
            $table->string('subject_type');
            $table->string('type');
            $table->integer('value');
            $table->string('verb');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('triggers');
    }

}
