<?php 

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePagesTable extends Migration 
{

	public function up()
	{
		Schema::create('pages', function(Blueprint $table) {
			$table->increments('id');
			$table->string('title');
			$table->string('url');
			$table->string('type');
			$table->text('body');
			$table->boolean('disabled');
			$table->string('short_title');
			$table->boolean('public');
			$table->boolean('members');
			$table->boolean('subscribers');
			$table->boolean('public_header');
			$table->boolean('public_footer');
			$table->boolean('back_office_header');
			$table->boolean('back_office_footer');
			$table->string('template');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('pages');
	}

}
