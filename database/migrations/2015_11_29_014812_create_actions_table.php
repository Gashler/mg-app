	<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateActionsTable extends Migration
{

	public function up()
	{
		Schema::create('actions', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('category_id');
			$table->text('description');
			$table->string('performer')->nullable();
			$table->integer('rating_id')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('actions');
	}

}
