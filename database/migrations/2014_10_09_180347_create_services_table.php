<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateServicesTable extends Migration
{

	public function up()
	{
		Schema::create('services', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name')->nullable();
			$table->text('description')->nullable();
			$table->float('price')->default(50);
			$table->integer('user_id')->nullable();
			$table->string('performer', 1);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::dropIfExists('services');
	}

}
