<?php
$first_lines = [
	"We can do this the easy way or the hard way.",
	"Where have you been my whole life?",
	"Are you real, or am I dreaming?",
	"Take me, I'm yours.",
	"Your wish is my command.",
	"Do you know what they'd do if they found us together?",
	"Let's try to be professional, all right?",
	"You're not supposed to be here.",
	"May I help you?",
	"How would you like to have the time of your life?",
	"I'm sorry, but this could never work out.",
	"Well, I guess this is goodbye.",
	"Can I tell you a secret?",
	"Finally, we're alone.",
	"We really shouldn't be doing this.",
	"You're in big trouble.",
	"Lighten up, will ya?",
	"Do you have any idea how much I want you right now?",
	"I'm having dirty thoughts.",
	"Your wish is my command.",
	"Your wildest dreams are about to come true.",
	"Mmm ...",
	"Looks like it's just you and me.",
	"Why don't you come a little closer?",
	"Excuse me; it's rude to stare.",
	"Let's have some fun.",
	"Yowzers!",
	"I'm afraid you're going to have to come with me.",
	"I don't want any trouble.",
	"Do you want to play with me?",
];
