-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 12, 2015 at 03:48 PM
-- Server version: 5.5.44-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mg2`
--

-- --------------------------------------------------------

--
-- Table structure for table `actions`
--

CREATE TABLE IF NOT EXISTS `actions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text,
  `gender` varchar(1) DEFAULT NULL,
  `level` varchar(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=114 ;

--
-- Dumping data for table `actions`
--

INSERT INTO `actions` (`id`, `description`, `gender`, `level`, `created_at`, `updated_at`) VALUES
(2, 'flash a body part of [spouse''s] choice.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:14'),
(3, 'act as [spouse''s] prisoner and do 3 things [spouse] asks you to do.', '', '5', '0000-00-00 00:00:00', '2015-12-12 22:18:14'),
(4, 'act out a mini sex session for [spouse] to watch, playing both the roles of you and [spouse], using falsetto or a deeper voice when appropriate.', '', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:14'),
(5, 'act out [spouse''s] favorite love scene from a movie.', '', '2', '0000-00-00 00:00:00', '2015-12-12 22:18:14'),
(6, 'blindfold [spouse] and dance with [spouse], taking off your clothes during the dance, then put your clothes back on before removing the blindfold.', '', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:14'),
(7, 'caress three different parts of [spouse''s] body while [spouse] lies down blindfolded or with [spouse''s] eyes closed.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:14'),
(8, 'caress [spouse''s] body with a hot wash cloth.', '', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:14'),
(9, 'caress [spouse''s] genitals.', '', '5', '0000-00-00 00:00:00', '2015-12-12 22:18:14'),
(10, 'carress [spouse''s] thighs, teasing [spouse''s] vagina.', 'f', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:14'),
(11, 'dance really close to [spouse].', '', '2', '0000-00-00 00:00:00', '2015-12-12 22:18:14'),
(12, 'dance with [spouse], touching [spouse] anywhere but [spouse''s] private parts.', '', '2', '0000-00-00 00:00:00', '2015-12-12 22:18:14'),
(13, 'describe a practical sexual fantasy that you would like [spouse] to perform for you within the next few days.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:14'),
(14, 'perform an erotic dance for [spouse] without taking off any clothes.', '', '2', '0000-00-00 00:00:00', '2015-12-12 22:18:14'),
(15, 'gently caress [spouse''s] belly while slowly sliding your hand up and down, barely teasing [spouse''s] breasts and vagina.', 'f', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:14'),
(16, 'gently stroke your fingers around [spouse''s] clitoris.', 'f', '5', '0000-00-00 00:00:00', '2015-12-12 22:18:14'),
(17, 'gently tease [spouse''s] belly, breasts, and vagina.', 'f', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:14'),
(18, 'gently work your fingers into [spouse''s] vagina (using lubricant if desired), and rotate your fingers around.', 'f', '5', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(19, 'get on your all fours while you show off your body from every angle.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(20, 'give your hand to [spouse] and have [spouse] lead your hand across [spouse''s] body the way [spouse] wants to be rubbed.', '', '5', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(21, 'give [spouse] butterfly kisses (eyelash kisses) all over [spouse''s] body.', '', '2', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(22, 'go with [spouse] to a mirror, stand behind [spouse], dance and move seductively while [spouse] can only watch through the mirror.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(23, 'have intercourse 3 times for only 3 seconds each time.', '', '5', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(24, 'have [spouse] close [spouse''s] eyes and lay down while you blow gently all over [spouse''s] body.', '', '2', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(25, 'have [spouse] close [spouse''s] eyes while you guide [spouse] into 3 sex positions without having intercourse.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(26, 'have [spouse] tell you 10 things [spouse] likes to do while you get distracted by [spouse''s] eyes, hair, hands, face, and lips.', '', '2', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(27, 'hold onto [spouse] and pretend that you are having an orgasm.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(28, 'hold up yourself over [spouse] while [spouse] lies on [spouse''s] back and you pretend to be having sex with [spouse].', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(29, 'initiate a conversation where everything you say is a sexual innuendo.', '', '2', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(30, 'kiss [spouse] passionately all over [spouse''s] body.', '', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(31, 'kiss [spouse''s] breasts.', 'f', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(32, 'kiss, lick, and nibble on [spouse''s] penis.', 'm', '5', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(33, 'leave the room and call [spouse] (or, if you don''t have two phones, both of you close your eyes) and roleplay a sexual encounter through words.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(34, 'lick [spouse] anywhere [spouse] chooses on [spouse''s] body.', '', '5', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(35, 'lip sync and dance to a song of [spouse''s] choice.', '', '2', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(36, 'make out with [spouse].', '', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(37, 'massage which ever part of [spouse''s] body [spouse] chooses.', '', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(38, 'massage [spouse''s] back.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(39, 'massage [spouse''s] thighs.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(40, 'massage [spouse''s] bum.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(41, 'massage [spouse''s] calves and thighs.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(42, 'massage [spouse''s] face and temples.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(43, 'massage [spouse''s] feet with a body part of [spouse''s] choice.', '', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(44, 'massage [spouse''s] penis.', 'm', '5', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(45, 'massage [spouse''s] scalp.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(46, 'massage [spouse''s] shoulders and arms.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(47, 'massage [spouse''s] belly and chest, coming close to touching [spouse''s] private parts.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(48, 'massage [spouse''s] testicles.', 'm', '5', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(49, 'mimic the shape of a vagina with your fist(s) and allow [spouse] to thrust his penis through.', 'm', '5', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(50, 'move your tongue around inside of [spouse''s] mouth while [spouse] sucks on it.', '', '5', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(51, 'neck [spouse].', '', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(52, 'nibble on [spouse''s] ears.', '', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(53, 'perform a full striptease for [spouse], starting with as much clothing as comfortable and stripping down to nothing.', '', '5', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(54, 'perform a lap dance for [spouse].', 'm', '5', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(55, 'perform an exotic dance for [spouse].', '', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(56, 'play with [spouse''s] fingers as seductively as you can.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(57, 'play with [spouse''s] hair or scalp as seductively as you can.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:15'),
(58, 'pretend to be a dance instructor and teach [spouse] 3 sexy dance moves.', '', '2', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(59, 'pretend to be having sex with [spouse] as [spouse] watches.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(60, 'pretend to be taking a shower for [spouse''s] viewing pleasure, rubbing every part of your body with a wash cloth.', '', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(61, 'pretend to be taking a shower with [spouse], both of you getting naked, though only looking in each other''s eyes.', '', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(62, 'pretend to give [spouse] a bath, washing every part of [spouse''s] body.', '', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(63, 'pretend [spouse] is a photograph and you''re a model, posing for ten sexy pictures.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(64, 'pretend [spouse] is an artist and you are a statue, letting [spouse] pose you 5 times.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(65, 'put on nothing but a towel and give [spouse] quick peeks at every part of your body.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(66, 'recite the words of a romantic song or poem while improvising actions with your body.', '', '2', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(67, 'roleplay a ''getting-to-know-you'' conversation while you ''accidentally'' show off your breasts.', 'm', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(68, 'roleplay a ''getting-to-know-you'' conversation while you ''accidentally'' show off your penis.', 'f', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(69, 'rub lotion or oil onto [spouse''s] entire body.', '', '5', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(70, 'rub your breasts against [spouse''s] penis and testicles.', 'm', '5', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(71, 'rub your genitals against [spouse''s] without having intercourse.', '', '5', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(72, 'say [spouse''s] name 10 times, sounding as if progressing from ''just friends'' to having an orgasm.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(73, 'shake your body for [spouse].', '', '2', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(74, 'show off the back side of your body (wearing nothing), running your hands up and down as you tease.', '', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(75, 'show off your muscles for [spouse].', '', '2', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(76, 'sing whichever sexy or romantic song [spouse] chooses.', '', '2', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(77, 'sit across from or next to [spouse] and play footsie with [spouse].', '', '2', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(78, 'slow dance with [spouse], letting your hands wander just a bit.', '', '2', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(79, 'squeeze and tease [spouse].', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(80, 'stand behind [spouse] and, without looking, massage her breasts.', 'f', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(81, 'stand behind [spouse] and, without looking, massage her vagina.', 'f', '5', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(82, 'stand behind [spouse] and, without looking, massage his penis.', 'm', '5', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(83, 'stand up and hold [spouse] in your arms, showing off your herculean strength.', 'f', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(84, 'stand up with [spouse] and rub your bum against [spouse] while you ''dirty dance''.', '', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(85, 'start by kissing [spouse''s] hand and gradually work your way up to [spouse''s] neck.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(86, 'step over [spouse] while [spouse] lies down and you show off every part of your vagina.', 'm', '5', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(87, 'suck on [spouse''s] bottom lip.', '', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(88, 'suck on [spouse''s] fingers while gazing into [spouse''s] eyes.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(89, 'suck on [spouse''s] breasts.', 'f', '5', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(90, 'take off your clothes and cover your penis with your hands; dance around and give [spouse] a peek every so often.', 'f', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(91, 'take off your top clothing and cover your breasts with your hands; dance around and give [spouse] a peek every so often.', 'm', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(92, 'tap your fingers lightly all over [spouse''s] body.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(93, 'tell [spouse] a sexual fantasy.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(94, 'tell [spouse] about somewhere else in the house (or outside the house) you''d like to try having sex.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(95, 'tell [spouse] three specific things about [spouse] that turn you on.', '', '2', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(96, 'tell [spouse] to sit up and close [spouse''s] eyes while you sit behind [spouse], wrap your arms around [spouse], and sing or hum a soothing melody.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(97, 'touch [spouse] all over [spouse''s] face, head, neck, and shoulders with one finger.', '', '2', '0000-00-00 00:00:00', '2015-12-12 22:18:16'),
(98, 'try to turn on [spouse] using only body language.', '', '2', '0000-00-00 00:00:00', '2015-12-12 22:18:17'),
(99, 'try to turn on [spouse] using only words.', '', '2', '0000-00-00 00:00:00', '2015-12-12 22:18:17'),
(100, 'wearing nothing but a towel, give [spouse] quick peeks at every part of your body.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:17'),
(101, 'whisper seductively to [spouse] the words of a love song or poem.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:17'),
(102, 'whisper to [spouse] 3 things you want to do with [spouse] tonight.', '', '3', '0000-00-00 00:00:00', '2015-12-12 22:18:17'),
(103, 'with your own words and actions, have [spouse] direct you as you perform in Romeo and Juliet.', '', '2', '0000-00-00 00:00:00', '2015-12-12 22:18:17'),
(104, 'wrap your naked body in a sheet and allow [spouse] to handle you.', '', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:17'),
(105, 'wrap your naked body in a towel or robe, approach [spouse], and invite [spouse] to join you inside the towel or robe.', '', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:17'),
(106, 'gaze into [spouse''s] eyes for a long time.', '', '2', '0000-00-00 00:00:00', '2015-12-12 22:18:17'),
(107, 'guess what sexual action [spouse] is dying for you to perform (without allowing [spouse] to speak) and perform it.', 'm', '5', '0000-00-00 00:00:00', '2015-12-12 22:18:17'),
(108, 'grant any one wish [spouse] desires.', '', '5', '0000-00-00 00:00:00', '2015-12-12 22:18:17'),
(109, 'grant any three wishes [spouse] desires.', '', '5', '0000-00-00 00:00:00', '2015-12-12 22:18:17'),
(110, 'massage your breasts, demonstrating how you''d like [spouse] to do so.', 'm', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:17'),
(111, 'get completely naked and show off your entire body, wearing nothing but your hands.', '', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:17'),
(112, 'make passionate love to [spouse], holding nothing back, while [spouse] remains completely still.', 'm', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:17'),
(113, 'allow [spouse] to make passionate love to you, holding nothing back, while you remain completely still.', '', '4', '0000-00-00 00:00:00', '2015-12-12 22:18:17');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
