<subject>{{ $user['spouse']['first_name'] }} signed you up for an account with {{ env('COMPANY_NAME') }}</subject>
<p>
    {{ $user['spouse']['first_name'] }} signed both of you up for a free tiral of {{ env('COMPANY_NAME') }}, the #1 site for games, tools, and ideas for strengthing relationships, improving communication, and rekindling passion. To log in and confirm your email address, click on the following link:
</p>
<p>
    <a href="{{ env('APP_URL') }}/verify?email={{ $user['email'] }}&key={{ $user['key'] }}" class="btn btn-primary">Click here to complete your registration</a>
</p>
