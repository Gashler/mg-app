<subject>Log in Instructions</subject>
<p>
    You received this email because you or someone else may have forgotten the password for your {{ env('COMPANY_NAME') }} user account and requested log in instructions. If you feel that you're receiving this email in error, simply ignore it. Otherwise, click on the following link to reset your password.
</p>
<p>
    <a href="{{ env('APP_URL') }}/password/confirm-reset?email={{ $user['email'] }}&key={{ $user['key'] }}" class="btn btn-primary">Reset My Password</a>
</p>
