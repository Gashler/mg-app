<subject>
    New ${{ $object['plan']['amount'] / 100 }} Subscription
</subject>
<p>
    {{ $object['plan']['name'] }}<br>
    {{ $object['plan']['metadata']['description'] }}<br>
    Trial days: {{ $object['plan']['trial_period_days'] }}
</p>
<p>
    Name: {{ $object['user']['first_name'] }}<br>
    Spouse: {{ $object['user']['spouse']['first_name'] }}<br>
    Email: {{ $object['user']['email'] }}<br>
    Spouse's Email: {{ $object['user']['spouse']['email'] }}<br>
    Account ID: {{ $object['user']['account_id'] }}
</p>
