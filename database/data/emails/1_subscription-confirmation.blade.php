<subject>Please confirm your account with {{ config('site.company_name') }}</subject>
<p>
    Thank you for signing up for a free trial with {{ env('COMPANY_NAME') }}. To confirm your email address, please click on the following link:
</p>
<p>
    <a href="{{ env('APP_URL') }}/verify?email={{ urlencode($user['email']) }}&key={{ $user['key'] }}" class="btn btn-primary">Click here to complete your registration</a>
</p>