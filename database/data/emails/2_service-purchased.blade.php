<subject>{{ $user['spouse']['first_name'] }} has purchased one of your services</subject>
<p>
    You've received {{ $object['price'] }} {{ config('site.money_name_plural') }}! {{ $user['spouse']['first_name'] }} has purchased the following service from you:
</p>
<h2>
{{ $object['name'] }}
</h2>
<p>
{{ $object['description'] }}
</p>
<p>
    You can view the services you owe {{ $user['spouse']['first_name'] }} here:
</p>
<p>
<a href="{{ env('APP_URL') }}/#/services/purchased/My">{{ env('APP_URL') }}/services/purchased/My</a>"
</p>
<p>
    Pick out a time that works for both of you and put it on your calendar. Have fun!
</p>
