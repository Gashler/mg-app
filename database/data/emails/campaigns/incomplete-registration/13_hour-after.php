<interval-type>hours</interval-type>
<interval>1</interval>
<subject>Almost there!</subject>
<p>
    We see that you didn't finishing setting up your 30 day free trial of {{ env('APP_URL') }}. The only step left is to add a paymment method. Don't worry, you won't be charged unless you continue your subscription beyond the trial, and you can cancel online at any time. It's super easy. We hope you and {{ $user['spouse']['first_name'] }} will take advantage of everything {{ env('APP_URL') }} has to offer for improving your communication, strengthening your relationship, and rekindling your passion.
</p>
<p>
    <a href="{{ env('APP_URL') }}/subscriptions/create?email={{ $user['email'] }}&key={{ $user['key'] }}" class="btn btn-primary">Start My 30 Day Free Trial</a>
</p>
