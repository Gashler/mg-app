<interval>1</interval>
<subject>$10 Off when you sign up today</subject>
<p>
    Thanks for trying out {{ env('COMPANY_NAME') }}, which is designed to improve communication, strengthen affection, and rekindle passion. We're confident that {{ env('COMPANY_NAME') }} will help you improve your love life, and to prove it, we'll give you $10 off the normal price when you sign up today. A single subscription will give both you and {{ $user['spouse']['first_name'] }} full access. Simply click on the following button to claim your discount:
</p>
<p>
    <a href="{{ env('APP_URL') }}/subscriptions/create?id=X2vCI2Xv16ZFQQNN3Cn8&email={{ $user['email'] }}&key={{ $user['key'] }}" class="btn btn-primary">Claim My Discount</a>
</p>
