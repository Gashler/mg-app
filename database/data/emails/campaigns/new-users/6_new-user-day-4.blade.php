<interval>4</interval>
<subject>Your trial will end soon. Would you like to try a small monthly payment?</subject>
<p>
    Ah, snap, it looks like you missed your opportunity to get 50% off your subscription to {{ env('COMPANY_NAME') }}. We get that yearly payments aren't for everyone, but we still hope you and {{ $user['spouse']['first_name'] }} can improve your relationship by taking advantage of everything {{ env('COMPANY_NAME') }} has to offer, so how about a small monthly payment? For just a few bucks per month, you can continue to evaluate {{ env('COMPANY_NAME') }} and cancel whenever you'd like. And when you're ready to commit, we're going to bend the rules by extending your 50% off yearly offer. A single subscription will give both you and {{ $user['spouse']['first_name'] }} full access. Just click on either of the following buttons:
</p>
<p>
    <a href="{{ env('APP_URL') }}/subscriptions/create?id=fiZinGUkYKgYNlW0yzEh&email={{ $user['email'] }}&key={{ $user['key'] }}" class="btn btn-primary">Let's Go Month to Month</a>
    <br>
    <a href="{{ env('APP_URL') }}/subscriptions/create?id=C2laqoq0CTJcVOUoGl8U&email={{ $user['email'] }}&key={{ $user['key'] }}" class="btn btn-primary">Claim My 50% Off Deal</a>
</p>
