<interval>2</interval>
<subject>Still not sure? How would you like $20 off?</subject>
<p>
    We see that you didn't take advantage of your $10 discount. Would a $20 discount help you make up your mind? If you feel it's time to make your relationship a higher priority, then commit today by making a small investment. {{ env('COMPANY_NAME') }} has everything you need to improve communication, manage your love life, and rekindle passion, so what are you waiting for? A single subscription will give both you and {{ $user['spouse']['first_name'] }} full access, but this offer is only good when sign up today. Just click on the following button:
</p>
<p>
    <a href="{{ env('APP_URL') }}/subscriptions/create?id=C2laqoq0CTJcVOUoGl8U&email={{ $user['email'] }}&key={{ $user['key'] }}" class="btn btn-primary">Claim My Discount</a>
</p>
