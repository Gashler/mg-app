<interval>2</interval>
<subject>Final Offer - 70% Off 2 Lifetime Memberships</subject>
<p>
    We really hope you and {{ $user['spouse']['first_name'] }} will take advantage of everything {{ env('COMPANY_NAME') }} has to offer to help you strengthen your relationship, improve communication, and keep the fire burning. Would a 70% discount help you make up your mind? For only $49.99, you and {{ $user['spouse']['first_name'] }} will get unlimited access (normally $199). This is as low as we can go, and the offer is only good for the next 24 hours. Choose to commit to a better love life by making a small investment. Simply click on the button below to claim your discount:
</p>
<p>
    <a href="{{ env('APP_URL') }}/subscriptions/create?id=prod_B1NB8L0qLyBuuB&email={{ $user['email'] }}&key={{ $user['key'] }}" class="btn btn-primary">Claim My Discount</a>
</p>
