<interval-type>hours</interval-type>
<interval>12</interval>
<subject>Only a Few Hours Left to Get 2 Lifetime Memberships for Only $99</subject>
<p>
    We hope you and {{ $user['spouse']['first_name'] }} have enjoyed your free trial of {{ env('COMPANY_NAME') }}. There's only a few hours left to take advantage of your introductory offer of two life-long memberships for only $99 (normally $199). A single subscription will give both you and {{ $user['spouse']['first_name'] }} unlimited access, but only when you sign up before your trial ends. Simply click on the button below to claim your discount:
</p>
<p>
    <a href="{{ env('APP_URL') }}/subscriptions/create?id=prod_B1N9MAxGtyJ3dS&email={{ $user['email'] }}&key={{ $user['key'] }}" class="btn btn-primary">Claim My Discount</a>
</p>
