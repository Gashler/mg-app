<interval>0</interval>
<subject>Your account has been upgraded</subject>
<p><strong>Congratulations, your account has been upgraded!</strong> Not only do you have full, unrestricted access to every game, activity, tool, and resource on MarriedGames.org (at no additional charge), but you can take advantage of a special members-only invitation to view the webinar and eBook "5 Simple Steps For a More Passionate Marriage".</p>
<p><strong>This engaging program is designed</strong> to help couples improve communication, overcome common challenges for a healthy love life, and rekindle the romantic flame.</p>
<a style="display: inline-block; background: gray; padding: 2.5%; color: white; text-decoration: none; border-radius: 6px; box-shadow: 0 -4px rgba(0,0,0,.2) inset, 0 4px rgba(255,255,255,.4) inset; background: rgb(255,0,128); font-weight: bold;" class="btn btn-primary" href="https://marriedgames.org">
Log in for Full Access
</a>