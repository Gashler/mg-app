<?php 

class EffectsTableSeeder extends DatabaseSeeder 
{

	public function run()
	{
		$faker = $this->getFaker();

		for($i = 1; $i <= 10; $i++) {
			$effect = array(
				'morph_type' => $faker->word,
				'morph_col' => $faker->word,
				'morph_id' => $faker->randomDigitNotNull,
				'min' => $faker->randomDigitNotNull,
				'max' => $faker->randomDigitNotNull,
				'morph_val' => $faker->text,
				'rand' => $faker->boolean,
				'negative' => $faker->boolean,
			);
			Effect::create($effect);
		}
	}

}
