<?php

class FirstLinesTableSeeder extends DatabaseSeeder
{

    public function run()
    {
        include database_path() . '/data/first_lines.php';
        foreach($first_lines as $first_line) {
            $data = ['name' => $first_line];
            FirstLine::create($data);
        };
    }

}
