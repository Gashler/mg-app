<?php

class CampaignsTableSeeder extends DatabaseSeeder
{
    public function run()
    {
        Campaign::truncate();
        $campaigns = [
            [
                'id' => 1,
                'key' => "new-users",
                'name' => "New Users"
            ],
            [
                'id' => 2,
                'key' => "new-customers",
                'name' => "New Customers"
            ],
            [
                'id' => 3,
                'key' => '24-hour-trial',
                'name' => '24 Hour Trial'
            ],
            [
                'id' => 4,
                'key' => 'incomplete-registration',
                'name' => 'Incomplete Registration'
            ],
            [
                'id' => 5,
                'key' => 'marriage-makeover',
                'name' => 'Marriage Makeover'
            ],
        ];
        Campaign::insert($campaigns);
    }
}
