<?php

class SexPositionsTableSeeder extends DatabaseSeeder
{

	public function run()
	{
		SexPosition::truncate();
		$rows = Excel::import('database/data/sex-positions.csv', function ($reader) {
			return $reader->ignoreEmpty()->toArray();
		})->get();
		foreach ($rows as $row) {
			$categories = $row->categories;
			unset($row['categories']);
			$sexPosition = SexPosition::create($row->toArray());
			$categories = explode(',', $categories);
			foreach ($categories as $category) {
				if (isset($category)) {
					$sexPosition->categories()->save(SexPositionCategory::find($category));
				}
			}
		}
	}
}
