<?php 

class LevelsTableSeeder extends DatabaseSeeder 
{

	public function run()
	{
		$faker = $this->getFaker();

		for($i = 1; $i <= 10; $i++) {
			$level = array(
				'name' => $faker->name,
			);
			Level::create($level);
		}
	}

}
