<?php

use App\Imports\QuestionsImport;
use Maatwebsite\Excel\Facades\Excel;

class QuestionsTableSeeder extends DatabaseSeeder
{
    public function run()
    {
        Question::truncate();
        Excel::import(new QuestionsImport, 'database/data/questions.csv');
    }
}
