<?php

use App\Imports\ActionsImport;
use Maatwebsite\Excel\Facades\Excel;

class ActionsTableSeeder extends DatabaseSeeder
{
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Action::truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        Excel::import(new ActionsImport, 'database/data/actions.csv');
    }
}
