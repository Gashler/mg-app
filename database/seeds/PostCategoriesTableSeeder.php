<?php

class PostCategoriesTableSeeder extends DatabaseSeeder
{
	public function run()
	{
		PostCategory::truncate();
		$categories = [
			['name' => "Sex Advice"],
			['name' => "30 Day Marriage Makeover"],
			['name' => "Game Ideas"],
			['name' => "Dating Ideas"]
		];
		PostCategory::insert($categories);
	}
}
