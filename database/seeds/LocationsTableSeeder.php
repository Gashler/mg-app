<?php

class LocationsTableSeeder extends DatabaseSeeder
{

    public function run()
    {
        include database_path() . '/data/locations.php';
        foreach($locations as $location) {
            $data = ['name' => $location];
            Location::create($data);
        };
    }

}
