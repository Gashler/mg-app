<?php

class StoryTypesTableSeeder extends DatabaseSeeder
{

    public function run()
    {
        include database_path() . '/data/story_types.php';
        foreach($story_types as $story_type) {
            $data = ['name' => $story_type];
            StoryType::create($data);
        };
    }

}
