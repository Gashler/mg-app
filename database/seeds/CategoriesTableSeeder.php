<?php

class CategoriesTableSeeder extends DatabaseSeeder
{

	public function run()
	{
		$categories = [
			[
				'key' => 'sound-booth',
				'rate' => 1,
				'description' => "For your audio entertainment.",
				'name' => 'Sound Booth'
			],
			[
				'key' => 'spa',
				'rate' => 1.25,
				'description' => "For your relaxation.",
				'name' => 'Spa'
			],
			[
				'key' => 'cabaret',
				'rate' => 1.75,
				'description' => "For your visual entertainment.",
				'name' => 'Cabaret'
			],
			[
				'key' => 'hotel',
				'rate' => 2,
				'description' => "For activities that require a room.",
				'name' => 'Hotel'
			],
		];
		Category::insert($categories);
	}

}
