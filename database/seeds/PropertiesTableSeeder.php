<?php

class PropertiesTableSeeder extends DatabaseSeeder
{

    public function run()
    {

        // clear any existing data
        DB::table('properties')->truncate();
        DB::table('propertyOptions')->truncate();

        /*******************
        * Characters
        ********************/

        // hidden
        $property = Property::create([
            'column' => true,
            'description' => null,
            'key' => 'hidden',
            'morph_type' => 'Character',
            'name' => 'Hidden'
        ]);
        $propertyOption = PropertyOption::create([
            'property_id' => $property->id,
            'name' => 'True',
            'value' => 1
        ]);
        $propertyOption = PropertyOption::create([
            'property_id' => $property->id,
            'name' => 'False',
            'value' => 0
        ]);

        /*******************
        * Places
        ********************/

        // block top
        $property = Property::create([
            'column' => true,
            'description' => null,
            'key' => 'block_top',
            'morph_type' => 'Place',
            'name' => 'Block top'
        ]);
        $propertyOption = PropertyOption::create([
            'property_id' => $property->id,
            'name' => 'True',
            'value' => 1
        ]);
        $propertyOption = PropertyOption::create([
            'property_id' => $property->id,
            'name' => 'False',
            'value' => 0
        ]);

        // block right
        $property = Property::create([
            'column' => true,
            'description' => null,
            'key' => 'block_right',
            'morph_type' => 'Place',
            'name' => 'Block right'
        ]);
        $propertyOption = PropertyOption::create([
            'property_id' => $property->id,
            'name' => 'True',
            'value' => 1
        ]);
        $propertyOption = PropertyOption::create([
            'property_id' => $property->id,
            'name' => 'False',
            'value' => 0
        ]);

        // block bottom
        $property = Property::create([
            'column' => true,
            'description' => null,
            'key' => 'block_bottom',
            'morph_type' => 'Place',
            'name' => 'Block bottom'
        ]);
        $propertyOption = PropertyOption::create([
            'property_id' => $property->id,
            'name' => 'True',
            'value' => 1
        ]);
        $propertyOption = PropertyOption::create([
            'property_id' => $property->id,
            'name' => 'False',
            'value' => 0
        ]);

        // block left
        $property = Property::create([
            'column' => true,
            'description' => null,
            'key' => 'block_left',
            'morph_type' => 'Place',
            'name' => 'Block left'
        ]);
        $propertyOption = PropertyOption::create([
            'property_id' => $property->id,
            'name' => 'True',
            'value' => 1
        ]);
        $propertyOption = PropertyOption::create([
            'property_id' => $property->id,
            'name' => 'False',
            'value' => 0
        ]);

    }

}
