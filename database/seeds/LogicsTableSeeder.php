<?php 

class LogicsTableSeeder extends DatabaseSeeder 
{

	public function run()
	{
		$faker = $this->getFaker();

		for($i = 1; $i <= 10; $i++) {
			$logic = array(
				'action_id' => $faker->randomDigitNotNull,
				'value' => $faker->randomDigitNotNull,
				'result_id' => $faker->randomDigitNotNull,
				'operator' => $faker->word,
				'result_type' => $faker->word,
			);
			Logic::create($logic);
		}
	}

}
