<?php

class GamesTableSeeder extends DatabaseSeeder
{

	public function run()
	{
		$faker = $this->getFaker();

		for($i = 1; $i <= 10; $i++) {
			$game = array(
				'user_id' => $faker->randomDigitNotNull,
				'name' => $faker->name,
				'public' => $faker->boolean,
			);
			Game::create($game);
		}
	}

}
