<?php 

class PlacesTableSeeder extends DatabaseSeeder 
{

	public function run()
	{
		$faker = $this->getFaker();

		for($i = 1; $i <= 10; $i++) {
			$place = array(
				'game_id' => $faker->randomDigitNotNull,
				'name' => $faker->name,
				'description' => $faker->text,
			);
			Place::create($place);
		}
	}

}
