<?php

class EmailTemplatesTableSeeder extends DatabaseSeeder
{

	public function run()
	{
		$emailTemplates = [
			[
				'key' => 'default',
			]
		];
		EmailTemplate::insert($emailTemplates);
	}

}
