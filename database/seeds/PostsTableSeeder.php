<?php

use App\Imports\PostsImport;
use App\Imports\PostsMetaImport;
use Maatwebsite\Excel\Facades\Excel;

class PostsTableSeeder extends DatabaseSeeder
{
    public function run()
    {
        Post::truncate();
        DB::table('category_post_pivot')->truncate();
		Excel::import(new PostsImport, 'database/data/posts.csv');
		Excel::import(new PostsMetaImport, 'database/data/post-meta.csv');


            // get image from body
            // Log::info('$post["body"] = '. $post['body']);
            $array = explode("src=\"", $post['body']);
            if (isset($array[1])) {
                $array = explode('"', $array[1]);
                $image = $array[0];
                $post['image'] = str_replace(env('HOST_URL') . "/wp-content/uploads/", "", $image);
                // if (strpos($image, '200x300') !== false) {
                // 	$post['image'] = str_replace('200x300', '150x150', $image);
                // } else {
                // 	$array = explode('.jpg', $image);
                // 	if (isset($array[1])) {
                // 		$post['image'] = $array[0] . '-150x150.jpg';
                // 	}
                // 	$array = explode('.png', $image);
                // 	if (isset($array[1])) {
                // 		$post['image'] = $array[0] . '-150x150.png';
                // 	}
                // }
                // Log::info('$post["image"] = ' . $image);
            }

            $post = Post::create($post);
            $category = PostCategory::find($category_id);
            $post->categories()->save($category);
        }
    }
}
