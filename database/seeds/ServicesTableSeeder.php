<?php

use App\Imports\ServicesImport;
use Maatwebsite\Excel\Facades\Excel;

class ServicesTableSeeder extends DatabaseSeeder
{
    public function run()
    {
        Service::truncate();
        Excel::import(new ServicesImport, 'database/data/services.csv');
    }
}
