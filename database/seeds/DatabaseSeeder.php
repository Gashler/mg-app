<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    protected $faker;

    public function getFaker()
    {
        if (empty($this->faker)) {
            $this->faker = Faker\Factory::create();
        }

        return $this->faker;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        // seeds that must come first
        $this->call(SettingsTableSeeder::class);
        $this->call(RolesTableSeeder::class);

        // other seeds
        // $this->call(ActionsTableSeeder::class);
        // $this->call(CategoriesTableSeeder::class);
        // $this->call(CharacterRolesTableSeeder::class);
        $this->call(CampaignsTableSeeder::class);
        $this->call(EmailTemplatesTableSeeder::class);
        $this->call(EmailsTableSeeder::class);
        // $this->call(FirstLinesTableSeeder::class);
        // $this->call(LocationsTableSeeder::class);
        // $this->call(NamesTableSeeder::class);
        // $this->call(PostCategoriesTableSeeder::class);
        // $this->call(PostsTableSeeder::class);
        // $this->call(PersonalitiesTableSeeder::class);
        // $this->call(RatingsTableSeeder::class);
        // $this->call(RoleplayOutlinesTableSeeder::class);
        // $this->call(RoleplayScriptsTableSeeder::class);
        // $this->call(QuestionCategoriesTableSeeder::class);
        // $this->call(QuestionsTableSeeder::class);
        // $this->call(ServicesTableSeeder::class);
        // $this->call(SexPositionCategoriesTableSeeder::class);
        // $this->call(SexPositionsTableSeeder::class);
        // $this->call(StoryTypesTableSeeder::class);
        // $this->call(UsersTableSeeder::class);
        // $this->call(VerbsTableSeeder::class);
        // $this->call(PropertiesTableSeeder::class);
    }
}
