<?php 

class TimersTableSeeder extends DatabaseSeeder 
{

	public function run()
	{
		$faker = $this->getFaker();

		for($i = 1; $i <= 10; $i++) {
			$timer = array(
				'direction' => $faker->word,
				'morph_type' => $faker->word,
				'result_type' => $faker->word,
				'min' => $faker->randomDigitNotNull,
				'max' => $faker->randomDigitNotNull,
				'value' => $faker->randomDigitNotNull,
				'morph_id' => $faker->randomDigitNotNull,
				'result_id' => $faker->randomDigitNotNull,
				'active' => $faker->boolean,
			);
			Timer::create($timer);
		}
	}

}
