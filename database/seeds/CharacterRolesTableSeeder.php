<?php

class CharacterRolesTableSeeder extends DatabaseSeeder
{

    public function run()
    {
        include database_path() . '/data/character_roles.php';
        foreach($character_roles as $character_role) {
            $data = [
                'name' => $character_role['name'],
                'gender' => $character_role['gender']
            ];
            CharacterRole::create($data);
        };
    }

}
