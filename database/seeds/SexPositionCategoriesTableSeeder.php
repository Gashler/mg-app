<?php

class SexPositionCategoriesTableSeeder extends DatabaseSeeder
{
	public function run()
	{
		SexPositionCategory::truncate();
		$categories = [
			['name' => "Him on Top"],
			['name' => "Her on Top"],
			['name' => "Kneeling"],
			['name' => "From Behind"],
			['name' => "Sitting"],
			['name' => "Standing"],
			['name' => "Advanced"]
		];
		SexPositionCategory::insert($categories);
	}
}
