<?php 

class AttributesTableSeeder extends DatabaseSeeder 
{

	public function run()
	{
		$faker = $this->getFaker();

		for($i = 1; $i <= 10; $i++) {
			$attribute = array(
				'name' => $faker->name,
				'attributeable_type' => $faker->word,
				'value' => $faker->word,
				'attributeable_id' => $faker->randomDigitNotNull,
				'default' => $faker->boolean,
			);
			Attribute::create($attribute);
		}
	}

}
