<?php 

class ModificationsTableSeeder extends DatabaseSeeder 
{

	public function run()
	{
		$faker = $this->getFaker();

		for($i = 1; $i <= 10; $i++) {
			$modification = array(
				'modifiable_type' => $faker->word,
				'col' => $faker->word,
				'game_id' => $faker->word,
				'account_id' => $faker->word,
				'modifiable_id' => $faker->randomDigitNotNull,
				'value' => $faker->text,
			);
			Modification::create($modification);
		}
	}

}
