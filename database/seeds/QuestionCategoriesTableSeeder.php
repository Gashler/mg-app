<?php

class QuestionCategoriesTableSeeder extends DatabaseSeeder
{
	public function run()
	{
		QuestionCategory::truncate();
		$categories = [
			['name' => "Sex"],
			['name' => "Communication"],
			['name' => "Emotional"],
			['name' => "Recreation"]
		];
		QuestionCategory::insert($categories);
	}
}
