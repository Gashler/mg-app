<?php 

class OptionsTableSeeder extends DatabaseSeeder 
{

	public function run()
	{
		$faker = $this->getFaker();

		for($i = 1; $i <= 10; $i++) {
			$option = array(
				'morph_id' => $faker->randomDigitNotNull,
				'result_id' => $faker->randomDigitNotNull,
				'order' => $faker->randomDigitNotNull,
				'name' => $faker->name,
				'morph_type' => $faker->word,
				'result_type' => $faker->word,
				'description' => $faker->text,
			);
			Option::create($option);
		}
	}

}
