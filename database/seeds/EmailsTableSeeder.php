<?php

class EmailsTableSeeder extends DatabaseSeeder
{
	public function run()
	{
		// prepare data
		$query = Campaign::all();
		foreach ($query as $index => $campaign) {
			$campaigns[$campaign->key] = $campaign;
		}
		foreach ($campaigns as $campaign) {
			foreach ($campaign->emails as $email) {
				$campaign->emails()->detach($email->id);
			}
		}
		Email::truncate();

		// seed database from email files
		function scanEmailDirs($path, $parent = null, $grandparent = null, $campaigns) {
			$files = opendir($path);
			$emails = [];
			while ($file = readdir($files)) {
				if (is_dir($path . $file) && $file !== '.' && $file !== '..') {
					if (isset($parent)) {
						$grandparent = $parent;
					}
					scanEmailDirs($path . $file . "/", $file, $grandparent, $campaigns);
				} else {
					if ($file !== '.' && $file !== '..') {
						$content = file_get_contents($path . $file);
						$array = explode(".", $file);
						$file = $array[0];
						$array = explode("_", $file);
						$id = $array[0];
						$key = $array[1];
						$interval_type = null;
						if ($grandparent == 'campaigns') {
							$array = explode('<interval-type>', $content);
							if (isset($array[1])) {
								$array = explode('</interval-type>', $array[1]);
								$interval_type = $array[0];
								$content = $array[1];
							}
							$array = explode('<interval>', $content);
							$array = explode('</interval>', $array[1]);
							$interval = $array[0];
							$content = $array[1];
						}
						$array = explode('<subject>', $content);
						$array = explode('</subject>', $array[1]);
						$subject = $array[0];
						$body = $array[1];
						$email = Email::create([
							'id' => $id,
							'key' => $key,
							'subject' => $subject,
							'body' => $body
						]);
						if (isset($interval)) {
							$email->campaigns()->save($campaigns[$parent], [
								'interval_type' => $interval_type,
								'interval' => $interval
							]);
						}
					}
				}
			}
		}
		scanEmailDirs(database_path() . '/data/emails/', null, null, $campaigns);
	}

}
