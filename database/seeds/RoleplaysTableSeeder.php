<?php 

class RoleplaysTableSeeder extends DatabaseSeeder 
{

	public function run()
	{
		$faker = $this->getFaker();

		for($i = 1; $i <= 10; $i++) {
			$his_name = Name::where('gender', 'm')->orderByRaw("RAND()")->get()->first();
			$her_name = Name::where('gender', 'f')->orderByRaw("RAND()")->get()->first();
			$his_personality = Personality::orderByRaw("RAND()")->get()->first();
			$her_personality = Personality::orderByRaw("RAND()")->get()->first();
			$his_role = CharacterRole::where('gender', 'm')->orWhere('gender', '')->orderByRaw("RAND()")->get()->first();
			$her_role = CharacterRole::where('gender', 'f')->orWhere('gender', '')->orderByRaw("RAND()")->get()->first();
			$location = Location::orderByRaw("RAND()")->get()->first();
			$story_type = StoryType::orderByRaw("RAND()")->get()->first();
			$first_line = FirstLine::orderByRaw("RAND()")->get()->first();
			$title = 'The ' . ucfirst($his_role->name) . ' and the ' . ucfirst($her_role->name);
			$roleplay = array(
				'title' => $title,
				'his_name' => $his_name->name,
				'her_name' => $her_name->name,
				'story_type' => $story_type->name,
				'location' => $location->name,
				'his_personality' => $his_personality->name,
				'her_personality' => $her_personality->name,
				'user_id' => 0,
				'public' => 1,
				'disabled' => 0,
			);
			Roleplay::create($roleplay);
		}
	}

}
