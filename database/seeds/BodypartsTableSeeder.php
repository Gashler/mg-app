<?php 

class BodypartsTableSeeder extends DatabaseSeeder 
{

	public function run()
	{
		$faker = $this->getFaker();

		for($i = 1; $i <= 10; $i++) {
			$bodypart = array(
				'name' => $faker->name,
				'gender' => $faker->word,
			);
			Bodypart::create($bodypart);
		}
	}

}
