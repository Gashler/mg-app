<?php

class RoleplayScriptsTableSeeder extends DatabaseSeeder
{

	public function run()
	{
		RoleplayScript::truncate();
		$path = database_path() . '/data/scripts/';
		$dh = opendir($path);
		$contents = [];
		while ($file = readdir($dh)) {
			if ($file !== '.' && $file !== '..') {
				$name = explode(".", $file);
				$name = $name[0];
				$content = file_get_contents($path . $file);
				$content = explode('<description>', $content);
				$content = explode('</description>', $content[1]);
				$description = $content[0];
				$content = explode('<icon>', $content[1]);
				$content = explode('</icon>', $content[1]);
				$icon = $content[0];
				$body = $content[1];
				$contents[] = [
					'name' => $name,
					'description' => $description,
					'body' => $body,
                    'icon' => $icon
				];
			}
		}
		RoleplayScript::insert($contents);
	}

}
