<?php 

class ObjectsTableSeeder extends DatabaseSeeder 
{

	public function run()
	{
		$faker = $this->getFaker();

		for($i = 1; $i <= 10; $i++) {
			$object = array(
				'name' => $faker->name,
				'morph_type' => $faker->word,
				'type' => $faker->word,
				'description' => $faker->text,
				'morph_id' => $faker->randomDigitNotNull,
			);
			Object::create($object);
		}
	}

}
