<?php

use App\Repositories\UserRepository;

class UsersTableSeeder extends DatabaseSeeder
{
    public function __construct(UserRepository $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    public function run()
    {
        User::truncate();
        Eloquent::unguard();

        // get roles
        $admin = Role::where('name', 'Admin')->first();
        $member = Role::where('name', 'Member')->first();
        $editor = Role::where('name', 'Editor')->first();
        $subscriber = Role::where('name', 'Subscriber')->first();
        $trial = Role::where('name', 'Trial')->first();

        // seeding functions
        $faker = $this->getFaker();
        function randomGender() {
            $rand = rand(1,2);
            if ($rand == 1) return 'm';
            else return 'f';
        }
        function firstName($gender) {
            $first_name = Name::where('gender', $gender)->orderByRaw("RAND()")->get()->take(1);
            return $first_name[0]->name;
        }
        $gender = randomGender();

        // create users
        // $user = User::create([
            // 'account_id' => 0,
            // 'first_name' => 'Kyle',
            // 'spouse_id' => 1,
            // 'email' => 'kyle@marriedgames.com',
            // 'password' => 'Theoldmonkey99!',
            // 'phone' => '7043059500',
            // 'gender' => 'm',
            // 'dob' => '1969-12-31',
            // 'role_id' => $admin->id,
            // 'sponsor_id' => null,
            // 'disabled' => false,
            // 'public_id' => "kylethomas",
            // 'verified' => 1
        // ]);
        // $user = User::create([
            // 'account_id' => 0,
            // 'first_name' => 'Kara',
            // 'spouse_id' => 0,
            // 'email' => 'kara@marriedgames.com',
            // 'password' => 'Theoldmonkey99!',
            // 'phone' => '7043059500',
            // 'gender' => 'f',
            // 'dob' => '1969-12-31',
            // 'role_id' => $admin->id,
            // 'sponsor_id' => null,
            // 'disabled' => false,
            // 'public_id' => "karathomas",
            // 'verified' => 1
//
        // ]);

        // for ($i = 100; $i <= 120; $i++) {
        //     if ($gender == 'm') {
        //         $gender = 'f';
        //         $account_id = $i - 1;
        //     }
        //     else {
        //         $gender = 'm';
        //         $account_id = $i;
        //     }
        //     User::create([
        //         'id' => $i,
        //         'account_id' => $account_id,
        //         'spouse_id' => $i + 1,
        //         'first_name' => firstName($gender),
        //         'email' => $faker->safeEmail,
        //         'password' => 'asdf;lkj',
        //         'phone' => $faker->numerify($string = '##########'),
        //         'gender' => $gender,
        //         'dob' => $faker->date,
        //         'phone' => $faker->randomDigitNotNull,
        //         'role_id' => $faker->numberBetween($min = 1, $max = 4),
        //         'sponsor_id' => $faker->numberBetween($min = 1000, $max = 1020),
        //         'disabled' => $faker->boolean,
        //     ]);
        // }

        // subscriber
        // $account = Account::create();
        // $gender = randomGender();
        // User::create([
        //     'id' => 3,
        //     'account_id' => $account->id,
        //     'spouse_id' => 4,
        //     'first_name' => firstName($gender),
        //     'email' => 'subscriber@example.com',
        //     'password' => 'asdf;lkj',
        //     'phone' => $faker->numerify($string = '##########'),
        //     'gender' => $gender,
        //     'dob' => $faker->date,
        //     'phone' => $faker->randomDigitNotNull,
        //     'role_id' => $subscriber->id,
        //     'sponsor_id' => $faker->numberBetween($min = 1000, $max = 1020),
        //     'public_id' => $faker->word,
        //     'money' => rand(50,500),
        //     'verified' => 1
        // ]);

        // trial husband
        // $account = Account::create();
        // User::create([
        //     'id' => 4,
        //     'account_id' => $account->id,
        //     'spouse_id' => 5,
        //     'first_name' => firstName('m'),
        //     'email' => 'trial_husband@example.com',
        //     'password' => 'asdf;lkj',
        //     'phone' => $faker->numerify($string = '##########'),
        //     'gender' => 'm',
        //     'dob' => $faker->date,
        //     'phone' => $faker->randomDigitNotNull,
        //     'role_id' => $trial->id,
        //     'sponsor_id' => $faker->numberBetween($min = 1000, $max = 1020),
        //     'public_id' => $faker->word,
        //     'money' => rand(50,500),
        //     'verified' => 1
        // ]);

        // trial wife
        // User::create([
        //     'id' => 5,
        //     'account_id' => $account->id,
        //     'spouse_id' => 4,
        //     'first_name' => firstName('f'),
        //     'email' => 'trial_wife@example.com',
        //     'password' => 'asdf;lkj',
        //     'phone' => $faker->numerify($string = '##########'),
        //     'gender' => 'f',
        //     'dob' => $faker->date,
        //     'phone' => $faker->randomDigitNotNull,
        //     'role_id' => $trial->id,
        //     'sponsor_id' => $faker->numberBetween($min = 1000, $max = 1020),
        //     'public_id' => $faker->word,
        //     'money' => rand(50,500),
        //     'verified' => 1
        // ]);

        // demo members
        session('bypass_subscription', true);
        $data = [
            'registrant_key' => 'Husband',
            'users' => [
                'Husband' => [
                    'first_name' => 'Steve',
                    'email' => 'americanknight@gmail.com',
                    'password' => 'asdf;lkj',
                    'verified' => 1
                ],
                'Wife' => [
                    'first_name' => 'Teresa',
                    'email' => 'pablo.deguablo@gmail.com',
                    'password' => 'asdf;lkj',
                    'verified' => 1
                ],
            ]
        ];
        $this->userRepo->createCouple($data, true);
        session('bypass_subscription', false);

        // editor
        // $gender = 'f';
        // $account = Account::create();
        // User::create([
        //     'id' => 8,
        //     'account_id' => $account->id,
        //     'spouse_id' => 9,
        //     'first_name' => firstName($gender),
        //     'email' => 'editor@marriedgames.org',
        //     'password' => 'asdf;lkj',
        //     'phone' => $faker->numerify($string = '##########'),
        //     'gender' => $gender,
        //     'dob' => $faker->date,
        //     'phone' => $faker->randomDigitNotNull,
        //     'role_id' => $editor->id,
        //     'sponsor_id' => $faker->numberBetween($min = 1000, $max = 1020),
        //     'public_id' => $faker->word,
        //     'money' => rand(50,500),
        //     'verified' => 1
        // ]);

        // admin
        $gender = 'm';
        $account = Account::create([
            'bypass_subscription' => 1
        ]);
        User::create([
            'id' => 9,
            'account_id' => $account->id,
            'spouse_id' => 8,
            'first_name' => firstName($gender),
            'email' => 'admin@marriedgames.org',
            'password' => 'Uhohstinkywinky99!',
            'phone' => $faker->numerify($string = '##########'),
            'gender' => $gender,
            'dob' => $faker->date,
            'phone' => $faker->randomDigitNotNull,
            'role_id' => $admin->id,
            'sponsor_id' => $faker->numberBetween($min = 1000, $max = 1020),
            'public_id' => $faker->word,
            'money' => rand(50,500),
            'verified' => 1
        ]);


        /*************************
        * Import Wordpress Users
        **************************/

        // prevent duplicate testing emails
        // $blacklist = [
        //     'americanknight@gmail.com',
        //     'sappytree@gmail.com',
        //     'pablo.deguablo@gmail.com'
        // ];
        //
        // // import users
        // $rows = Excel::import('database/data/wp_users.csv', function ($reader) {
        //     return $reader->ignoreEmpty()->toArray();
        // })->get();
        // foreach ($rows as $user) {
        //     $user = $user->all();
        //     if (!in_array($user['email'], $blacklist)) {
        //         DB::table('users')->insert($user);
        //     }
        // }
    }
}
