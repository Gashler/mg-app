<?php

class MapsTableSeeder extends DatabaseSeeder
{

	public function run()
	{
		$faker = $this->getFaker();

		for($i = 1; $i <= 10; $i++) {
			$map = array(
				'name' => $faker->name,
				'morph_type' => $faker->word,
				'morph_id' => $faker->randomDigitNotNull,
			);
			Map::create($map);
		}
	}

}
