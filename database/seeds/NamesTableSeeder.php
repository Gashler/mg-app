<?php

class NamesTableSeeder extends DatabaseSeeder
{

    public function run()
    {
        include database_path() . '/data/names.php';
        foreach($name_sets as $name_set) {
            foreach($name_set['names'] as $name) {
                Name::create([
                    'name' => $name,
                    'gender' => $name_set['gender'],
                    'locale' => $name_set['locale']
                ]);
            }
        }
    }

}
