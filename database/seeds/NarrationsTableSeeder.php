<?php 

class narrationsTableSeeder extends DatabaseSeeder 
{

	public function run()
	{
		$faker = $this->getFaker();

		for($i = 1; $i <= 10; $i++) {
			$narration = array(
				'body' => $faker->text,
			);
			Narration::create($narration);
		}
	}

}
