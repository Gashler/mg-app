<?php 

class UventsTableSeeder extends DatabaseSeeder 
{

	public function run()
	{
		$faker = $this->getFaker();

		for($i = 1; $i <= 10; $i++) {
			$uvent = array(
				'uventable_id' => $faker->randomDigitNotNull,
				'result_id' => $faker->randomDigitNotNull,
				'content_id' => $faker->randomDigitNotNull,
				'date_start' => $faker->randomDigitNotNull,
				'date_end' => $faker->randomDigitNotNull,
				'name' => $faker->name,
				'uventable_type' => $faker->word,
				'result_type' => $faker->word,
				'spouse' => $faker->word,
				'content_type' => $faker->word,
				'description' => $faker->text,
				'options' => $faker->boolean,
				'timer' => $faker->boolean,
				'result' => $faker->boolean,
				'lock_result_to_timer' => $faker->boolean,
				'generate_content' => $faker->boolean,
				'rand_content' => $faker->boolean,
			);
			Uvent::create($uvent);
		}
	}

}
