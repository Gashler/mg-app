<?php

class RatingsTableSeeder extends DatabaseSeeder
{

	public function run()
	{
		$ratings = [
			[
				'name' => 'G',
				'description' => "Innocent romance",
			],
			[
				'name' => 'PG',
				'description' => "Pushing the limits",
			],
			[
				'name' => 'PG-13',
				'description' => "Partial nudity and mild sexuality",
			],
			[
				'name' => 'R',
				'description' => "Full nudity and moderate sexuality",
			],
			[
				'name' => 'NC-17',
				'description' => "Explicit sexuality",
			],
		];
		Rating::insert($ratings);
	}

}
