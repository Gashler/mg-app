<?php

class SettingsTableSeeder extends DatabaseSeeder {

    public static function storeTags($tags, $object) {
        foreach($tags as $tag) {
            $tag = TagModel::create(['name' => $tag]);
            $object->tags()->save($tag);
        };
        $object->tags()->save($tag);
    }

    public function run()
    {
        // Setting::truncate();
        //
        // // company name
        // $setting = Setting::create([
        //     'name' => 'Company Name',
        //     'key' => 'company_name',
        //     'type' => 'String',
        //     'value' => 'MarriedGames.org',
        // ]);
        // $this->storeTags(['Company', 'Branding'], $setting);
        //
        // // flaticon_face_f_max
        // $setting = Setting::create([
        //     'name' => 'Number of Flaticon Female Faces',
        //     'key' => 'flaticon_face_f_max',
        //     'type' => 'integer',
        //     'value' => '14'
        // ]);
        // $this->storeTags(['Icons'], $setting);
        //
        // // flaticon_face_m_max
        // $setting = Setting::create([
        //     'name' => 'Number of Flaticon Male Faces',
        //     'key' => 'flaticon_face_m_max',
        //     'type' => 'integer',
        //     'value' => '43'
        // ]);
        // $this->storeTags(['Icons'], $setting);
        //
        // // namespace for models
        // $setting = Setting::create([
        //     'name' => 'Namespace for Models',
        //     'key' => 'models_path',
        //     'type' => 'string',
        //     'value' => ''
        // ]);
        // $this->storeTags(['Admin'], $setting);

    }
}
