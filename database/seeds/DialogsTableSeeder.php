<?php 

class DialogsTableSeeder extends DatabaseSeeder 
{

	public function run()
	{
		$faker = $this->getFaker();

		for($i = 1; $i <= 10; $i++) {
			$dialog = array(
				'character_id' => $faker->randomDigitNotNull,
				'group_id' => $faker->randomDigitNotNull,
				'order' => $faker->randomDigitNotNull,
				'body' => $faker->text,
			);
			Dialog::create($dialog);
		}
	}

}
