<?php

class VerbsTableSeeder extends DatabaseSeeder
{

    public function run()
    {
        $verbs = [
            'Blow',
            'Caress',
            'Dance',
            'Finger',
            'Flash',
            'Flirt',
            'Fondle',
            'Grope',
            'Hold',
            'Imagine',
            'Kiss',
            'Lick',
            'Look',
            'Make Out',
            'Massage',
            'Moan',
            'Nibble',
            'Penetrate',
            'Play',
            'Rape',
            'Shake',
            'Sing',
            'Squeeze',
            'Strip',
            'Stroke',
            'Suck',
            'Talk',
            'Tease',
            'Thrust',
            'Touch'
        ];
        foreach($verbs as $verb) {
            Verb::create(['name' => $verb]);
        }
    }

}
