<?php

class PersonalitiesTableSeeder extends DatabaseSeeder
{

    public function run()
    {
        include database_path() . '/data/personalities.php';
        foreach($personalities as $personality) {
            $data = ['name' => $personality];
            Personality::create($data);
        };
    }

}
