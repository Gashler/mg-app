angular.module('app')
.directive('mgdashboardadmin', function() {
    return {
        templateUrl: '/modules/dashboard/admin.html',
        controller: function($scope, $http) {

            // count users
            $http.get('/User/count').then(function(result) {
                $scope.usersCount = result;
            });

        }
    }
});
