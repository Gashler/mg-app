angular.module('app')
.directive('mgdashboardmember', function() {
    return {
        templateUrl: '/modules/dashboard/member.html',
        controller: function($scope, $http) {

            // check authorization
            $scope.authorize();

            // count users
            $http.get('/User/count').then(function(result) {
                $scope.usersCount = result;
            });

        }
    }
});
