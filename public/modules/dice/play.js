angular.module('app')
.controller('Dice', function($scope, $http, $interval, $timeout, $route, $location, $routeParams)
{
	// define variables
	var styleSets = [1, 2, 3];
	var firstSpin = 0;
	var direction;
	if ($routeParams.set == null) {
		$scope.set = 'Sex';
	} else {
		$scope.set = $routeParams.set;
	}
	$scope.loading = true;
	$scope.animating = true;
	$scope.sides = [
		'front',
		'back',
		'top',
		'bottom',
		'left',
		'right'
	];
	$scope.dice = {
		'Sex': {
			'Person': [
				$scope.user.first_name,
				$scope.user.first_name + ' ',
				$scope.user.first_name + '  ',
				$scope.user.spouse.first_name,
				$scope.user.spouse.first_name + ' ',
				$scope.user.spouse.first_name + '  '
			],
			'Action': [
				'Caress',
				'Lick',
				'Blow On',
				'Kiss',
				'Massage',
				'Tease'
			],
			'Target': [
				'Genitals',
				'Chest',
				'Neck',
				'Face',
				'Bum',
				'Legs'
			]
		},
		'Date': {
			'Recreation': [
				'Hiking',
				'Gym',
				'Museum',
				'Swimming',
				'Sports',
				'Park'
			],
			'Dinner': [
				'American',
				'Mexican',
				'Italian',
				'Greek',
				'Oriental',
				'Tropical'
			],
			'Entertainment': [
				'Movie',
				'Performance',
				'Dance',
				'Board games',
				'Video games',
				'Carnival'
			]
		},
		'Lingerie': {
			'Top': [
				'Bra',
				'Camisole',
				'Bustier',
				'Chemise',
				'Corset',
				'Bodystocking'
			],
			'Bottom': [
				'Panties',
				'Thong',
				'Miniskirt',
				'Stockings',
				'Crotchless',
				'Skirt'
			],
			'Accessories': [
				'Gloves',
				'Nipple covers',
				'Jewelry',
				'High heels',
				'Robe',
				'Mask'
			]
		},
		'Role-play': {
			'He': [
				'Director',
				'Boss',
				'Masseuse',
				'Student',
				'Doctor',
				'Jailbird'
			],
			'She': [
				'Nun',
				'Stripper',
				'Teacher',
				'Secretary',
				'Officer',
				'Maid'
			],
			'Location': [
				'Jail',
				'Club',
				'Hotel',
				'Office',
				'Studio',
				'Hideout'
			]
		},
		// 'Custom': {
		// 	'Die 1': [
		// 		'',
		// 		' ',
		// 		'  ',
		// 		'   ',
		// 		'    ',
		// 		'     '
		// 	],
		// 	'Die 2': [
		// 		'',
		// 		' ',
		// 		'  ',
		// 		'   ',
		// 		'    ',
		// 		'     '
		// 	],
		// 	'Die 3': [
		// 		'',
		// 		' ',
		// 		'  ',
		// 		'   ',
		// 		'    ',
		// 		'     '
		// 	]
		// },
		'Numbers': {
			'Die 1': [
				1,
				2,
				3,
				4,
				5,
				6
			],
			'Die 2': [
				1,
				2,
				3,
				4,
				5,
				6
			]
		}
	};

	jQuery(document).ready(function() {

		jQuery("#rollDice, .cube-container").click(function() {
			if (!$scope.animating) {
				jQuery("#rollDice").attr("disabled", "disabled");
				jQuery(".wrap").each(function() {
					$scope.roll(jQuery(this).attr("id"), 1);
				});
				setTimeout(function() {
					jQuery("#rollDice").removeAttr("disabled");
				}, 1000);
			}
		});

		// roll
		$scope.roll = function(id, seconds) {
			$scope.animating = true;
			if (firstSpin < 2) {
				direction = 0;
				firstSpin ++;
			}
			else direction = Math.floor(Math.random() * 2);
			var toDegrees;
			function generateToDegrees() {
				toDegrees = (Math.floor(Math.random() * 4)) * 90;
				if (direction == 0) {
					if (toDegrees == jQuery(".cube", "#" + id).attr("data-y")) {
						generateToDegrees();
					}
				}
				if (direction == 1) {
					if (toDegrees == jQuery(".cube", "#" + id).attr("data-x")) {
						generateToDegrees();
					}
				}
			}
			generateToDegrees();
			if (direction == 0) { // horizontal spin
				jQuery(".cube", "#" + id).css({
					"margin": "0 auto",
					"position": "relative",
					"width": "200px",
					"transform-style": "preserve-3d",
					"-webkit-transform-style": "preserve-3d"
				});
				jQuery(".cube-back", "#" + id).css({
					"transform": "translateZ(-100px) rotateY(180deg)",
					"-webkit-transform": "translateZ(-100px) rotateY(180deg)"
				});
				jQuery(".cube-top", "#" + id).css({
					"transform": "rotateX(-90deg) translateY(-100px)",
					"-webkit-transform": "rotateX(-90deg) translateY(-100px)",
					"transform-origin": "top center",
					"-webkit-transform-origin": "top center"
				});
				jQuery(".cube-bottom", "#" + id).css({
					"transform": "rotateX(90deg) translateY(100px)",
					"transform-origin": "bottom center",
					"-webkit-transform": "rotateX(90deg) translateY(100px)",
					"-webkit-transform-origin": "bottom center"
				});
				var fromDegrees = jQuery(".cube", "#" + id).css("transform");
				if (fromDegrees == undefined) fromDegrees = jQuery(".cube", "#" + id).css("-webkit-transform");
				if (fromDegrees == undefined) fromDegrees = 0;
				jQuery(".animation." + id).html("" +
					"@keyframes spin {" +
						"from { transform: rotateY(" + fromDegrees + "); }" +
						"to { transform: rotateY(" + toDegrees + "deg); }" +
					"}" +
					"@-webkit-keyframes spin {" +
						"from { -webkit-transform: rotateY(" + fromDegrees + "); }" +
						"to { -webkit-transform: rotateY(" + toDegrees + "deg); }" +
					"}" +
					"#" + id + " .cube { animation: spin " + seconds + "s forwards; -webkit-animation: spin " + seconds + "s forwards; }" +
				"");
				setTimeout(function() {
					jQuery(".cube", "#" + id).css({
						"transform": "rotateY(" + toDegrees + "deg)",
						"-webkit-transform": "rotateY(" + toDegrees + "deg)"
					});				jQuery(".cube", "#" + id).attr("data-y", toDegrees);
					var cube = jQuery(".wrap#" + id).html();
					var transform = jQuery(".cube", "#" + id).css("transform");
					if (transform == undefined) transform = jQuery(".cube", "#" + id).css("-webkit-transform");
					jQuery(".wrap#" + id).html("");
					jQuery(".animation." + id).html("");
					jQuery(".wrap#" + id).html(cube);
					jQuery(".wrap#" + id + " .cube").css({
						"transform": transform,
						"-webkit-transform": transform
					});
					$scope.animating = false;
				}, 1000);
			} else { // vertical spin
				jQuery(".cube", "#" + id).css({
					"margin": "0 auto",
					"transform-origin": "0 100px"
				});
				jQuery(".cube-top", "#" + id).css({
					"transform": "rotateX(-270deg) translateY(-100px)",
					"-webkit-transform": "rotateX(-270deg) translateY(-100px)"
				});
				jQuery(".cube-back", "#" + id).css({
					"transform": "translateZ(-100px) rotateX(180deg)",
					"-webkit-transform": "translateZ(-100px) rotateX(180deg)"
				});
				jQuery(".cube-bottom", "#" + id).css({
					"transform": "rotateX(-90deg) translateY(100px)",
					"-webkit-transform": "rotateX(-90deg) translateY(100px)"
				});
				var fromDegrees = jQuery(".cube", "#" + id).css("transform");
				if (fromDegrees == undefined) fromDegrees = jQuery(".cube", "#" + id).css("-webkit-transform");
				if (fromDegrees == undefined) fromDegrees = 0;
				jQuery(".animation." + id).html("" +
					"@keyframes spin-vertical {" +
						"from { transform: rotateX(" + fromDegrees + "); }" +
						"to { transform: rotateX(" + toDegrees + "deg); }" +
					"}" +
					"@-webkit-keyframes spin-vertical {" +
						"from { -webkit-transform: rotateX(" + fromDegrees + "); }" +
						"to { -webkit-transform: rotateX(" + toDegrees + "deg); }" +
					"}" +
					"#" + id + " .cube {animation: spin-vertical " + seconds + "s forwards; -webkit-animation: spin-vertical " + seconds + "s forwards; }" +
				"");
				setTimeout(function() {
					jQuery(".cube", "#" + id).css({
						"transform": "rotateX(" + toDegrees + "deg)",
						"-webkit-transform": "rotateX(" + toDegrees + "deg)"
					});
					jQuery(".cube", "#" + id).attr("data-x", toDegrees);
					var cube = jQuery(".wrap#" + id).html();
					var transform = jQuery(".cube", "#" + id).css("transform");
					if (transform == undefined) transform = jQuery(".cube", "#" + id).css("-webkit-transform");
					jQuery(".wrap#" + id).html("");
					jQuery(".animation." + id).html("");
					jQuery(".wrap#" + id).html(cube);
					jQuery(".wrap#" + id + " .cube").css({
						"transform": transform,
						"-webkit-transform": transform
					});
					$scope.animating = false;
				}, 1000);
			}
		}

		// refresh dice (spinning them a few time clears out visual errors)
		$scope.refresh = function()
		{
			$scope.loading = true;
			for (x = 1; x <= 3; x ++) {
				jQuery(".wrap").each(function() {
					jQuery(".wrap").each(function() {
						$scope.roll(jQuery(this).attr("id"), 1);
					});
				});
			}
			$timeout(function() {
				$scope.loading = false;
			}, 1000);
		}
		$scope.refresh();

		$scope.reload = function()
		{
			$location.url("/game/dice/" + $scope.set);
		}

		// customize dice
		jQuery(".customize-dice input").keyup(function() {
			var content = jQuery(this).val();
			var cube = jQuery(this).parent().parent().attr("data-cube");
			var number = jQuery(this).parent().attr("data-number");
			jQuery("div[data-number='" + number + "']", "#" + cube).html(content);
		});
	});
});
