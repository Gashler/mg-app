angular.module('app')
.controller('ServicePurchasedController', function($scope, $http, $location, $routeParams)
{
	// define variables
	$scope.loading = false;
	$scope.loadingService = false;
	$scope.person = $routeParams.person

	// get data
	$scope.getData = function() {
		$scope.loading = true;
		$http.get('/services/purchases/' + $scope.person).then(function(response) {
			$scope.services = response.data;
			$scope.loading = false;
		});
	};
	$scope.getData();

	// complete service
	$scope.completeService = function(service, index)
	{
		$http.get('/services/complete/' + service.id).then(function(response) {
			if (response.data) {
				$scope.services.splice(index, 1);
			}
		});
	}
});
