angular.module('app')
.controller('ServiceIndexController', function($scope, $http, $location, $routeParams)
{
	// define variables
	$scope.loading = false;
	$scope.loadingService = false;
	$scope.purchasingService = false;
	$scope.person = $routeParams.person
	$scope.suggestion = "";

	// get data
	$scope.getData = function() {
		$scope.loading = true;
		$http.get('/services/' + $scope.person).then(function(response) {
			console.log(response);
			$scope.suggestions = response.data.suggestions;
			$scope.services = response.data.services;
			$scope.loading = false;
		});
	};
	$scope.getData();

	// create service
	$scope.createService = function()
	{
		$http.get('/services/create').then(function(response) {
			$scope.service = response.data;
			console.log('$scope.service = ', $scope.service);
			$location.url('/services/edit/' + $scope.service.id);
		});
	}

	// add service
	$scope.addService = function()
	{
		$scope.loadingService = true;
		$http.get('/services/add/' + $scope.suggestion.id).then(function(response) {
			$scope.loadingService = false;
			$scope.services.unshift(response.data);
		});
	}

	// add service
	$scope.purchaseService = function(service)
	{
		$scope.purchasingService = true;
		if ($scope.user.money >= service.price) {
			$http.get('/services/purchase/' + service.id).then(function(response) {
				$scope.purchasingService = false;
				if (!response.data.errors) {
					$scope.message(response.data.message.type, response.data.message.body, 10000);
					$scope.user.money = response.data.user_money;
					$scope.user.spouse.money = response.data.spouse_money;
					$scope.audio('money');
				}
			});
		} else {
			alert("You can't afford this service.");
		}
	}
});
