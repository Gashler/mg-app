angular.module('app')
.controller('ServiceShowController', function($scope, $http, $routeParams, $location)
{
	// define variables
	$scope.loading = true;

	// get data
	$http.get('/Service/' + $routeParams.id).then(function(response) {
		$scope.service = response.data;
		$scope.loading = false;
	});

	// delete service
	$scope.delete = function()
	{
		var c = confirm("Are you sure you want to delete this service? This cannot be undone.");
		if (c) {
			$http.delete('/Service/' + $scope.service.id).then(function(response) {
				if (!response.data.errors) {
					$scope.message(response.data.message.type, response.data.message.body, 5000);
					$location.url('/services/My');
				}
			})
		}
	}
});
