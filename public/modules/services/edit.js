angular.module('app')
.controller('ServiceEditController', function($scope, $http, $routeParams)
{
	// define variables
	$scope.loading = true;
	$scope.service = null;
	$scope.prices = [];
	for (var price = 0; price <= 500; price += 25) {
		$scope.prices.push(price);
	}

	// get data
	$http.get('/Service/' + $routeParams.id).then(function(response) {
		$scope.service = response.data;
		console.log('$scope.service = ', $scope.service);
		$scope.loading = false;
	});

	// get service
	$scope.getService = function()
	{
		$scope.loadingService = true;
		$http.get('/services/random?private=1&asker=' + $scope.user.spouse.gender).then(function(response) {
			$scope.loadingService = false;
			$scope.service = response.data.service;
		});
	}
});
