angular.module('app')
	.controller('LoversLane', function ($scope, $http, $timeout, $interval, $route) {
		/*****************
		* setup
		******************/

		// define variables
		var player;
		var opponent;
		if ($scope.user == undefined) {
			$scope.getVars();
		}

		// size board
		$scope.sizeBoard = function () {
			if ($('#content').width() <= $(window).height() - $('#header-menu').height()) {
				var maxSize = $('#content').width();
				var padding = 1;
			} else {
				var maxSize = $(window).height() - $('#header-menu').height();
				var padding = .85;
			}
			var ratio = maxSize / $('#board').height();
			ratio *= padding;
			$('#game').css({
				transform: 'scale(' + ratio + ')'
			});
		}
		$timeout(function () {
			$scope.sizeBoard();
		}, 500);
		$(window).resize(function () {
			$scope.sizeBoard();
		});
		$scope.sizeBoard();

		$scope.getGame = function () {
			$scope.loaded = false;
			$http.get('/lovers-lane/game').then(function (object) {
				$scope.unit = object.data;
				$scope.game = object.data.data;
				console.log('$scope.game = ', $scope.game);
				$scope.loaded = true;
				$scope.newGame = false;

				// size board
				// var height = $('#centerContentContainer').height() - $('.turn').height();
				// $('#showContent').height(height + 'px');

				// define variables
				user = $scope.user.gender + '_';
				spouse = $scope.user.spouse.gender + '_';
				$scope.getPlayerVariables();

				if (!$scope.game.hasOwnProperty('m_left')) {
					$scope.newGame = true;
					$scope.game[user + 'left'] = $('.player.' + $scope.user.gender).offset().left - $('#board').offset().left;
					$scope.game[user + 'top'] = $('.player.' + $scope.user.gender).offset().top - $('#board').offset().top;
					$scope.game[spouse + 'left'] = $('.player.' + $scope.user.spouse.gender).offset().left - $('#board').offset().left;
					$scope.game[spouse + 'top'] = $('.player.' + $scope.user.spouse.gender).offset().top - $('#board').offset().top;
				}

				// if new game
				if ($scope.newGame) {
					$scope.alternateTurn();
					$scope.landOnSpace();
				}
			});
		}
		$scope.getGame();

		$scope.ucfirst = function (string) {
			return ucfirst(string);
		}

		/**************************
		* player movement
		***************************/

		$scope.getPlayerVariables = function () {
			player = $scope.game.turn + '_';
			if ($scope.game.turn == 'm') {
				$scope.game.player = $scope.user;
				$scope.game.opponent = $scope.user.spouse;
				opponent = 'f_';
			} else {
				$scope.game.player = $scope.user.spouse;
				$scope.game.opponent = $scope.user;
				opponent = 'm_';
			}
		}

		// alternate turn
		$scope.alternateTurn = function () {

			// alternate player/opponent variables
			if ($scope.game.turn == 'm') {
				$scope.game.turn = 'f';
			} else {
				$scope.game.turn = 'm';
			}

			$scope.getPlayerVariables();

			// reset variables
			$scope.game.showContinueButton = false;
			$scope.game.showAlternateTurnButton = false;
			$scope.game.showNakedButton = false;
			$scope.game.showPayButton = false;
			$scope.game.showPerformButton = false;
			$scope.game.showRegenerateButton = false;
			$scope.game.showStartButton = false;
			$scope.game.showWaitButton = false;
			$scope.game.showStore = false;
			$scope.game.allow_rolling = true;
			$scope.game.disclaimerWarning = null;
			$scope.game.disclaimerDanger = null;
			$scope.game.showContent = false;

			// increment turn count
			$scope.game.turns++;

			// raise level
			if ($scope.game.turns % $scope.game.raiseLevel == 0) {
				$scope.raiseLevel();
			}
		}

		// roll
		$scope.roll = function () {

			// if in jail
			console.log("$scope.game[player + 'jail'] = ", $scope.game[player + 'jail']);
			if ($scope.game[player + 'jail']) {
				console.log('line 112');
				return $scope.inJail();
			}

			$scope.audio('dice');

			// reset variables
			$scope.game.showStore = false;
			$scope.game.content.header = null;
			$scope.game.content.body = null;
			$scope.game.disclaimerWarning = null;
			$scope.game.disclaimerDanger = null;
			$scope.game.showContinueButton = false;

			// define variables
			console.log(player);
			var starting_point = parseInt($scope.game[player + 'space']);
			var roll = Math.floor((Math.random() * 6) + 1);
			console.log('starting_point = ' + starting_point);
			var spaces = $('.space').length;
			$scope.game.currentSpace = starting_point + roll;

			// if roll exceeds available spaces
			if ($scope.game.currentSpace > spaces) {
				$scope.game.currentSpace -= spaces;
			}

			// get content
			$scope.getSpaceContent();

			/*****************
			 * move player
			 *****************/

			// first turn
			movePlayer();

			// other turns
			interval = $interval(function () {
				movePlayer();
			}, 1000);

			// destroy interval when leaving page
			$scope.$on("$destroy", function () {
				$interval.cancel(interval);
			});

			// move player
			function movePlayer() {
				$scope.game.allow_rolling = false;
				if ($scope.game[player + 'space'] !== $scope.game.currentSpace) {
					$scope.game[player + 'space']++;

					// if passing "payday" space
					if ($scope.game[player + 'space'] == 16) {
						$scope.payday();
					}

					// complete a round
					if ($scope.game[player + 'space'] == 17) {
						$scope.game[player + 'space'] = 1;
						$scope.game[player + 'left'] += $scope.game.spaceSize;

						// move right
					} else if ($scope.game[player + 'space'] <= 4) {
						$scope.game[player + 'left'] += $scope.game.spaceSize;
						css = { left: $scope.game[player + 'left'] + 'px' };

						// move down
					} else if ($scope.game[player + 'space'] > 4 && $scope.game[player + 'space'] <= 8) {
						$scope.game[player + 'top'] += $scope.game.spaceSize;
						css = { transform: 'translateY(' + ($scope.game[player + 'top'] - 25) + 'px)' };

						// move left
					} else if ($scope.game[player + 'space'] > 8 && $scope.game[player + 'space'] <= 12) {
						$scope.game[player + 'left'] -= $scope.game.spaceSize;
						css = { left: $scope.game[player + 'left'] + 'px' };

						// move up
					} else if ($scope.game[player + 'space'] > 12 && $scope.game[player + 'space'] <= 17) {
						$scope.game[player + 'top'] -= $scope.game.spaceSize;
						css = { transform: 'translateY(' + ($scope.game[player + 'top'] - 25) + 'px)' };
					}

					element = $('.player.' + $scope.game.turn);
					$(element).css(css).addClass('top');
					$('.piece', element).addClass('rise');
					$('.shadow', element).addClass('shrink');
					$timeout(function () {
						$('.piece', element).removeClass('rise');
						$('.shadow', element).removeClass('shrink');
						$scope.$apply();
					}, 500);
				} else {
					element = $('.player.' + $scope.game.turn);
					$(element).removeClass('top');
					$interval.cancel(interval);
					$scope.landOnSpace();
				}
			}
		};

		// raise level
		$scope.raiseLevel = function () {
			if ($scope.game.level < 5) {
				$scope.game.level++;
				var oldHeat = $scope.game.heat[$scope.game.heatIndex].name;
				$scope.game.heatIndex--;
				var newHeat = $scope.game.heat[$scope.game.heatIndex].name;
				$scope.setHeat($scope.game.level);
				$scope.message('success', "Heat level raised from \"" + oldHeat + "\" to \"" + newHeat + ".\"", 4000);
			}
		}

		/****************************
		* land on space
		*****************************/

		$scope.landOnSpace = function () {
			// $scope.game.disclaimerWarning = "";
			// $scope.game.spaces[$scope.game.currentSpace].type = 'jail';

			// if on empty lot
			if ($scope.game.spaces[$scope.game.currentSpace] == undefined) {
				$scope.game.currentSpace -= $('.space').length;
			}
			if ($scope.game.spaces[$scope.game.currentSpace].type == 'lot') {
				$scope.game.disclaimer = true;

				// if on an empty lot or on your own property
				if (!$scope.game.spaces[$scope.game.currentSpace].property) {
					$scope.game.disclaimerWarning = "You don't own a property here yet, so you'll have to work for a living.";
					$scope.game.content.body += ".";
					$scope.game.showStartButton = true;
					$scope.game.showRegenerateButton = true;

					// if on a property
				} else {

					// if player own's property
					if ($scope.game.spaces[$scope.game.currentSpace].property.owner == $scope.game.player.gender) {
						$scope.alternateTurn();

						// if opponent owns property
					} else {
						$scope.audio('dun_dun_dun');
						$scope.game.fee = parseInt($scope.game.spaces[$scope.game.currentSpace].price / 4);
						$scope.game.disclaimerDanger = "You landed on " + $scope.game.opponent.first_name + "'s " +
							$scope.game.spaces[$scope.game.currentSpace].property.category + ".";
						if ($scope.game.player.money > $scope.game.fee) {
							$scope.game.content.body = "You must either " + $scope.game.content.body + " or pay " +
								$scope.game.opponent.first_name + " $" + $scope.game.fee + ".";
							$scope.game.showPayButton = true;
						} else {
							$scope.game.content.body = "You can't afford the fee, so in payment, you must " + $scope.game.content.body + ".";
						}
						$scope.game.showPerformButton = true;
					}
				}

				// strip
			} else if ($scope.game.spaces[$scope.game.currentSpace].type == 'strip') {
				$scope.audio('cheer');
				$scope.nothingLeftToStrip = function () {
					$scope.game.content.header = "Nothing Left to Strip";
					var bodyParts = {
						m: [
							'chest',
							'bum',
							'penis'
						],
						f: [
							'breasts',
							'bum',
							'vagina'
						]
					};
					var rand = Math.floor((Math.random() * 3));
					$scope.game.content.body = "You'll have to make this up to " +
						$scope.game.opponent.first_name + " by massaging your " +
						bodyParts[$scope.game.player.gender][rand] +
						" for " + $scope.game.opponent.possessive + " viewing pleasure.";
					$scope.game.showStartButton = true;
					$scope.game.showNakedButton = false;
				}

				if (!$scope.game[$scope.game.player.gender + '_naked']) {
					$scope.game.content.header = 'Time to Strip';
					$scope.game.content.body = "For " + $scope.game.opponent.first_name +
						"'s viewing pleasure, remove one article of clothing as seductively as you can.";
					$scope.game.showNakedButton = true;
					$scope.game.showStartButton = true;

					// set player as naked
					$scope.setNaked = function () {
						$scope.game[$scope.game.player.gender + '_naked'] = true;
						$scope.nothingLeftToStrip();
					}
				} else {
					$scope.nothingLeftToStrip();
				}

				// jail
			} else if ($scope.game.spaces[$scope.game.currentSpace].type == 'jail') {

				// set free
				if ($scope.game[player + 'jail'] == 0) {
					$scope.game[player + 'jail'] = null;
					$scope.setFree();

					// put in jail
				} else {
					$scope.audio('fail');
					$scope.game[player + 'jail'] = 3;
					$scope.game.content.header = "You've been locked up!";
					$scope.game.content.body = "It's time to pay for your naughty deeds. Either you must spend the next " + $scope.game[player + 'jail'] + " turns in jail, or you must perform whatever task " + $scope.game.opponent.first_name + " asks for.";
					$scope.showStartButton = false;
					$scope.game.showWaitButton = true;
					$scope.game.showPerformButton = true;
				}

				// bankrupt
			} else if ($scope.game.spaces[$scope.game.currentSpace].type == 'bankrupt') {
				$scope.game.content.header = "You're bankrupt!";
				$scope.game.content.body = "";
				$scope.game.player.money = 0;
				$scope.audio('fail');
				$scope.game.showAlternateTurnButton = true;

				// payday
			} else if ($scope.game.spaces[$scope.game.currentSpace].type == 'payday') {
				if (!$scope.newGame) {
					$scope.game.content.header = "Payday!";
					$scope.game.content.body = "You've collected $" + $scope.game.paydayAmount + ".";
					$scope.game.showAlternateTurnButton = true;
				}
			}

			$scope.newGame = false;
			$scope.game.showContent = true;
			$('#showContent').hide().fadeIn(1000);
		}

		// payday
		$scope.payday = function () {
			$scope.audio('money');
			$scope.game.player.money += $scope.game.paydayAmount;
			$scope.message('success', $scope.game.player.first_name + " received $" + $scope.game.paydayAmount, 4000);
		}

		// get space content
		$scope.getSpaceContent = function () {
			$('#showContent').fadeOut(function () {
				$scope.game.showContent = false;
			});

			// if roll exceeds available spaces
			var spaces = $('.space').length;
			if ($scope.game.currentSpace > spaces) {
				$scope.game.currentSpace -= spaces;
			}

			// get category
			var category = '';
			console.log('$scope.game.currentSpace = ' + $scope.game.currentSpace);
			if ($scope.game.spaces[$scope.game.currentSpace] !== undefined && $scope.game.spaces[$scope.game.currentSpace].property) {
				var category = '&category_id=' + $scope.game.spaces[$scope.game.currentSpace].property.category_id;
			}

			$http.get('/Action/random?' +
				'rating_id=' + $scope.game.level +
				'&performer=' + $scope.game.player.gender +
				'&address_performer=false' +
				category
			).then(function (response) {
				if (response.data.success) {
					$scope.game.content.body = response.data.data.description;
				}
				if (response.data.message) {
					$scope.message(response.data.message.type, response.data.message.body);
				}
				if ($scope.game.regenerating) {
					$scope.game.regenerating = false;
					$scope.game.showContent = true;
					$('#showContent').hide().fadeIn(1000);
				}
			});
		}

		// regenerate content
		$scope.regenerate = function () {
			$scope.game.regenerating = true;
			$scope.getSpaceContent();
		}

		/******************************
		* jail functions
		*******************************/
		$scope.performTask = function () {
			$scope.game.content.header = "";
			$scope.game.content.body = "";
			$scope.game.showWaitButton = false;
			$scope.game.showPerformButton = false;
			$scope.game.showPayButton = false;
			if ($scope.game[player + 'jail'] > 0) {
				$scope.game[player + 'jail'] = 0;
				$scope.startTimer(null, '$scope.setFree()');
			} else {
				$scope.startTimer();
			}
		}

		$scope.wait = function () {
			$scope.game.showContent = false;
			$scope.game[player + 'jail']--;
			$scope.alternateTurn();
		}

		$scope.inJail = function () {
			$scope.game.content.header = "Still in jail";
			var s = "";
			if ($scope.game[player + 'jail'] > 1) {
				var s = 's';
			}
			$scope.game.content.body = "You've still got " + $scope.game[player + 'jail'] + " more turns to go. Are you sure you don't want to fulfill " + $scope.game.opponent.first_name + "'s wish?";
			$scope.game.showWaitButton = true;
			$scope.game.showPerformButton = true;
			$scope.game.showContent = true;
		}

		$scope.setFree = function (x) {
			$scope.game[player + 'jail'] = null;
			$scope.game.content.header = "You're free!";
			$scope.game.content.body = "";
			$scope.game.showContinueButton = true;
			$scope.game.showContent = true;
		}

		/*******************************
		* property functions
		********************************/

		$scope.buyProperty = function (property) {
			var price;
			angular.forEach($scope.game.spaces[$scope.game.currentSpace].prices, function (p) {
				if (p.key == property.key) {
					price = p;
				}
			});
			$scope.game.player.money -= price.price;
			$scope.game.spaces[$scope.game.currentSpace].owner = $scope.game.player.gender;
			$scope.game.spaces[$scope.game.currentSpace].property = {
				owner: $scope.game.player.gender,
				category: property.key,
				category_id: property.category_id
			};
			$scope.audio('add');
			$scope.alternateTurn();
		}

		$scope.pay = function () {
			if ($scope.game.player.money > $scope.game.fee) {
				$scope.message('success', $scope.game.player.first_name + " payed " + $scope.game.opponent.first_name + " $" + $scope.game.fee + ".", 4000);
				$scope.audio('money');
				$scope.game.player.money -= $scope.game.fee;
				$scope.game.opponent.money += $scope.game.fee;
				$scope.game.showPayButton = false;
				$scope.game.showPerformButton = false;
				$scope.game.showContent = false;
				$scope.alternateTurn();
			}
		}

		/*****************************
		* game controls
		******************************/
		$scope.resetGame = function () {
			$http.post('/Base/delete/' + $scope.unit.model, [
				$scope.unit.id
			]).then(function (response) {
				if (response.data.message) {
					$scope.message(response.data.message.type, response.data.message.body);
				} else {
					$scope.message('success', "Game reset", 2000);
					location.reload();
				}
			});
		}

		$scope.resetMoney = function () {
			$http.get('/User/reset-money').then(function (response) {
				if (!response.data.error) {
					$scope.user.money = response.data.data.user.money;
					$scope.user.spouse.money = response.data.data.user.spouse.money;
					$scope.message('success', "Money reset", 2000);
					$scope.audio('money');
					$scope.togglePopup('#options');
				}
			});
		}

		// start timer
		$scope.startTimer = function (time, callback) {
			// reset applicable variables
			$scope.game.showStartButton = false;
			$scope.game.showRegenerateButton = false;
			$scope.game.showNakedButton = false;
			$scope.game.showTime = true;
			$scope.game.disclaimer = false;
			if (time == null) {
				$scope.game.time = $scope.game.defaultTime;
			}
			var interval = $interval(function () {
				if ($scope.game.time == 0) {
					$interval.cancel(interval);
					$scope.audio('ding');
					$scope.game.showStartButton = false;
					$scope.game.showRegenerateButton = false;
					$scope.game.showTime = false;
					$scope.game.showContent = false;
					if (callback) {
						eval(callback);
					} else {
						$scope.showStore();
					}
				} else {
					$scope.game.time--;
				}
			}, 1000);

			// destroy interval when leaving page
			$scope.$on("$destroy", function () {
				$interval.cancel(interval);
			});
		}

		/*****************************
		* store
		******************************/

		// show store
		$scope.showStore = function () {
			if (!$scope.game.spaces[$scope.game.currentSpace].property) {
				$scope.game.showStore = true;
				if ($scope.game.spaces[$scope.game.currentSpace].type == 'lot'
					&& $scope.game.spaces[$scope.game.currentSpace].prices[0].price <= $scope.game.player.money
				) {
					$scope.game.canAffordProperties = true;
				} else {
					$scope.game.canAffordProperties = false;
					$scope.alternateTurn();
				}
			} else {
				$scope.alternateTurn();
			}
		}

		$scope.setHeat = function (newLevel) {
			$scope.audio('power_up');
			$scope.game.level = newLevel;
			angular.forEach($scope.game.heat, function (heat, index) {
				if (heat.level <= newLevel) {
					$scope.game.heat[index].active = true;
				} else {
					$scope.game.heat[index].active = false;
				}
			});
		}

		/*****************************
		* updates
		******************************/

		// update/synchronize game every few seconds
		$scope.updateInterval = $interval(function () {
			$scope.sizeBoard();
			$http.patch('/LoversLane', $scope.game).then(function (response) {
				if (response.data.message) {
					$scope.message(response.data.message.type, response.data.message.body);
				}
			});
		}, 5000);

		// destroy interval when leaving page
		$scope.$on("$destroy", function () {
			$interval.cancel($scope.updateInterval);
		});
	});
