angular.module('app')
.controller('SexPositionsController', function($scope, $http)
{
	// define variables
	$scope.loading = false;
	$scope.category = "";
	$scope.search = "";

	// get data
	$scope.getData = function() {
		$scope.loading = true;
		$http.get('/SexPosition').then(function(response) {
			$scope.sexPositions = response.data.sexPositions;
			$scope.categories = response.data.categories;

			// resize images
			angular.element(document).ready(function() {
				$('img.illustration').each(function(element) {
					console.log('element = ', element);
					console.log('whoop!');
					var width = $(this).width();
					console.log('width = ', width);
					var height = $(this).height();
					console.log('height = ', height);
					if (width >= height) {
						var css = {
							'max-width': '300px',
							'width': '100%'
						}
					} else {
						console.log('bleep!');
						var css = {
							'max-height': '300px',
							'height': '100%'
						}
					}
					$(this).css(css);
				});
			});
			$scope.loading = false;
		});
	};
	$scope.getData();

	// filter to random object
	$scope.random = function()
	{
		$scope.category = '';
		var length = $scope.sexPositions.length;
		var rand = Math.floor((Math.random() * length));
		$scope.search = $scope.sexPositions[rand].name;
		console.log('$scope.search = ', $scope.search);
	}

	// clear search
	$scope.clearSearch = function()
	{
		$scope.search = '';
	}
});
