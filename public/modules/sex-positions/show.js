angular.module('app')
.controller('SexPositionController', function($scope, $http, $routeParams)
{
	// define variables
	$scope.loading = true;

	// get data
	$http.get('/SexPosition/' + $routeParams.id).then(function(response) {
		$scope.sexPosition = response.data;
		$scope.loading = false;
	});
});
