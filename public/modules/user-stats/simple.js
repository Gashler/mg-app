angular.module('app')
.directive('mgUserStatsSimple', function() {
    return {
        templateUrl: '/modules/user-stats/simple.html',
        controller: function($scope, $http) {

            // count users
            $scope.getUserStats = function()
            {
                $scope.loadingStats = true;
                $http.get('/Stat/user-stats-and-calendar').then(function(response) {
                    $scope.loadingStats = false;
                    $scope.userStats = response.data.stats.results;
                    $scope.year = response.data.year;
                    console.log('$scope.userStats = ', $scope.userStats);
                    $scope.generateChart();
                });
            }
            $scope.getUserStats();

            $scope.generateChart = function()
            {
                $('#reportContainer').html('<canvas id="myChart" width="400" height="350"></canvas>');
                var ctx = $("#myChart");
                if ($scope.report == 'f_orgasms') {
                    var myChart = new Chart(ctx, {
                        options: {
                            scaleBeginAtZero: true
                        },
                        type: 'line',
                        data: {
                            labels: $scope.year.labels,
                            datasets: [
                                {
                                    label: $scope.year.datasets.f_orgasm.label,
                                    fill: true,
                                    lineTension: 0.1,
                                    backgroundColor: "rgba(255,64,128,0.4)",
                                    borderColor: "rgba(255,0,128, 1)",
                                    borderCapStyle: 'butt',
                                    borderDash: [],
                                    borderDashOffset: 0.0,
                                    borderJoinStyle: 'miter',
                                    pointBorderColor: 'black',
                                    pointBackgroundColor: 'black',
                                    pointBorderWidth: 1,
                                    pointHoverRadius: 5,
                                    pointHoverBackgroundColor: "rgba(255,64,128,0.4)",
                                    pointHoverBorderColor: "rgba(255,0,128, 1)",
                                    pointHoverBorderWidth: 2,
                                    pointRadius: 1,
                                    pointHitRadius: 10,
                                    data: $scope.year.datasets.f_orgasm.data,
                                    spanGaps: false,
                                }
                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    stacked: true
                                }]
                            }
                        }
                    });
                }
                if ($scope.report == 'm_orgasms') {
                    var myChart = new Chart(ctx, {
                        options: {
                            scaleBeginAtZero: true
                        },
                        type: 'line',
                        data: {
                            labels: $scope.year.labels,
                            datasets: [
                                {
                                    label: $scope.year.datasets.m_orgasm.label,
                                    fill: true,
                                    lineTension: 0.1,
                                    backgroundColor: "rgba(64,128,255,0.4)",
                                    borderColor: "rgba(0,128,255, 1)",
                                    borderCapStyle: 'butt',
                                    borderDash: [],
                                    borderDashOffset: 0.0,
                                    borderJoinStyle: 'miter',
                                    pointBorderColor: 'black',
                                    pointBackgroundColor: 'black',
                                    pointBorderWidth: 1,
                                    pointHoverRadius: 5,
                                    pointHoverBackgroundColor: "rgba(64,128,255,0.4)",
                                    pointHoverBorderColor: "rgba(0,128,255, 1)",
                                    pointHoverBorderWidth: 2,
                                    pointRadius: 1,
                                    pointHitRadius: 10,
                                    data: $scope.year.datasets.m_orgasm.data,
                                    spanGaps: false,
                                }
                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    stacked: true
                                }]
                            }
                        }
                    });
                }
                if ($scope.report == 'sex') {
                    var myChart = new Chart(ctx, {
                        type: 'line',
                        data: {
                            labels: $scope.year.labels,
                            datasets: [
                                {
                                    label: $scope.year.datasets.sex.label,
                                    fill: true,
                                    lineTension: 0.1,
                                    backgroundColor: "rgba(255,192,128,.4)",
                                    borderColor: "rgb(255,192,128)",
                                    borderCapStyle: 'butt',
                                    borderDash: [],
                                    borderDashOffset: 0.0,
                                    borderJoinStyle: 'miter',
                                    pointBorderColor: 'black',
                                    pointBackgroundColor: 'black',
                                    pointBorderWidth: 1,
                                    pointHoverRadius: 5,
                                    pointHoverBackgroundColor: "rgba(255,192,128,.4)",
                                    pointHoverBorderColor: "rgb(255,192,128)",
                                    pointHoverBorderWidth: 2,
                                    pointRadius: 1,
                                    pointHitRadius: 10,
                                    data: $scope.year.datasets.sex.data,
                                    spanGaps: false,
                                }
                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    stacked: true
                                }]
                            }
                        }
                    });
                }
                if ($scope.report == 'quality_time') {
                    var myChart = new Chart(ctx, {
                        type: 'line',
                        data: {
                            labels: $scope.year.labels,
                            datasets: [
                                {
                                    label: $scope.year.datasets.quality_time.label,
                                    fill: true,
                                    lineTension: 0.1,
                                    backgroundColor: "rgba(128,255,192,.4)",
                                    borderColor: "rgb(128,255,192)",
                                    borderCapStyle: 'butt',
                                    borderDash: [],
                                    borderDashOffset: 0.0,
                                    borderJoinStyle: 'miter',
                                    pointBorderColor: 'black',
                                    pointBackgroundColor: 'black',
                                    pointBorderWidth: 1,
                                    pointHoverRadius: 5,
                                    pointHoverBackgroundColor: "rgba(128,255,192,.4)",
                                    pointHoverBorderColor: "rgb(128,255,192)",
                                    pointHoverBorderWidth: 2,
                                    pointRadius: 1,
                                    pointHitRadius: 10,
                                    data: $scope.year.datasets.quality_time.data,
                                    spanGaps: false,
                                }
                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    stacked: true
                                }]
                            }
                        }
                    });
                }
                if ($scope.report == 'romantic_date') {
                    var myChart = new Chart(ctx, {
                        type: 'line',
                        data: {
                            labels: $scope.year.labels,
                            datasets: [
                                {
                                    label: $scope.year.datasets.romantic_date.label,
                                    fill: true,
                                    lineTension: 0.1,
                                    backgroundColor: "rgba(192,128,255,.4)",
                                    borderColor: "rgb(192,128,255)",
                                    borderCapStyle: 'butt',
                                    borderDash: [],
                                    borderDashOffset: 0.0,
                                    borderJoinStyle: 'miter',
                                    pointBorderColor: 'black',
                                    pointBackgroundColor: 'black',
                                    pointBorderWidth: 1,
                                    pointHoverRadius: 5,
                                    pointHoverBackgroundColor: "rgba(192,128,255,.4)",
                                    pointHoverBorderColor: "rgb(192,128,255)",
                                    pointHoverBorderWidth: 2,
                                    pointRadius: 1,
                                    pointHitRadius: 10,
                                    data: $scope.year.datasets.romantic_date.data,
                                    spanGaps: false,
                                }
                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    stacked: true
                                }]
                            }
                        }
                    });
                }
            }
        }
    }
});
