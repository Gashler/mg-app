angular.module('app')
.controller('UserStats', function($scope, $http) {

    // define variables
    $scope.loadingStats = true;

    // count users
    $scope.getUserStats = function(reload)
    {
        if (reload) {
            $scope.loadingStats = true;
        }
        $http.get('/Stat/user-stats').then(function(response) {
            $scope.loadingStats = false;
            $scope.userStats = response.data;
            console.log('response.data = ', response.data);
            $scope.unit = $scope.userStats.goals.user;
            console.log('$scope.unit = ', $scope.unit);
        });
    }
    $scope.getUserStats(true);

    $scope.updateGoal = function(key, object)
    {
        $http.patch('/Base', {
            meta: {
                model: $scope.unit.model,
                id: $scope.unit.id,
                key: key,
            },
            value: object[key]
        }).then(function(response) {
            $scope.getUserStats();
            if (response.data.errors) {
                $scope.message('danger', response.data.errors);
            }
        });
    }
});
