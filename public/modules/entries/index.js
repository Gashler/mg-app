angular.module('app')
.controller('EntryIndexController', function($scope, $http, $location)
{
	// define variables
	$scope.loading = false;

	// get data
	$scope.getData = function() {
		$scope.loading = true;
		$http.get('/Entry').then(function(response) {
			$scope.entries = response.data;
			$scope.loading = false;
		});
	};
	$scope.getData();

	// create entry
	$scope.createEntry = function()
	{
		$http.get('/entries/create').then(function(response) {
			$scope.entry = response.data;
			console.log('$scope.entry = ', $scope.entry);
			$location.url('/conversation/entries/edit/' + $scope.entry.id);
		});
	}
});
