angular.module('app')
.controller('EntryShowController', function($scope, $http, $routeParams, $location)
{
	// define variables
	$scope.loading = true;

	// get data
	$http.get('/Entry/' + $routeParams.id).then(function(response) {
		$scope.entry = response.data;
		$scope.loading = false;
	});

	// delete entry
	$scope.delete = function()
	{
		var c = confirm("Are you sure you want to delete this entry? This cannot be undone.");
		if (c) {
			$http.delete('/Entry/' + $scope.entry.id).then(function(response) {
				if (!response.data.errors) {
					$scope.message(response.data.message.type, response.data.message.body, 5000);
					$location.url('/conversation/entries');
				}
			})
		}
	}
});
