angular.module('app')
.controller('EntryEditController', function($scope, $http, $routeParams)
{
	// define variables
	$scope.loading = true;
	$scope.question = null;

	// get data
	$http.get('/Entry/' + $routeParams.id).then(function(response) {
		$scope.entry = response.data;
		$scope.loading = false;
	});

	// get question
	$scope.getQuestion = function()
	{
		$scope.loadingQuestion = true;
		$http.get('/questions/random?private=1&asker=' + $scope.user.spouse.gender).then(function(response) {
			$scope.loadingQuestion = false;
			$scope.question = response.data.question;
		});
	}
});
