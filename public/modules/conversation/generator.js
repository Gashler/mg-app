angular.module('app')
.controller('ConversationGeneratorController', function($scope, $http)
{
	// define variables
	$scope.loading = true;
	$scope.loadingQuestion = false;
	$scope.asker = 'alternate';

	// get data
	$http.get('/questions/random-with-categories').then(function(response) {
		console.log('response.data = ', response.data);
		$scope.question = response.data.question;
		console.log('$scope.question = ', $scope.question);
		// $scope.ratings = response.data.ratings;
		$scope.categories = response.data.categories;
		$scope.askerGender = $scope.user.gender;
		// $scope.rating = $scope.ratings[2];
		$scope.category = '';
		$scope.loading = false;
	});

	// get data
	$scope.getQuestion = function() {
		$scope.loadingQuestion = true;
		console.log('$scope.category = ', $scope.category);
		// determine asker
		if ($scope.asker == 'm' || $scope.asker == 'f') {
			$scope.askerGender = $scope.asker;
		} else if ($scope.asker == 'alternate') {
			if ($scope.askerGender == 'm') {
				$scope.askerGender = 'f';
			} else {
				$scope.askerGender = 'm';
			}
		} else if ($scope.asker == 'random') {
			$scope.askerGender = Math.floor((Math.random() * 2)) ? 'm' : 'f';
		}
		$http.get('/questions/random?'
			+ 'asker=' + $scope.askerGender
			// + '&rating_id=' + $scope.rating.id
			+ '&question_category_id=' + $scope.category
		).then(function(response) {
			$scope.question = response.data.question;
			$scope.loadingQuestion = false;
		});
	};
});
