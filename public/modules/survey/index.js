angular.module('app')
.controller('SurveyIndexController', function($scope, $http, $routeParams)
{
	// define variables
	$scope.person = $routeParams.person;
	$scope.loading = true;
	$scope.loadingQuestion = false;
	$scope.categories = [{
		name: 'Sex',
		id: 1
	}];
	$scope.category_id = $scope.categories[0].id;

	// get data
	$scope.getAnswers = function()
	{
		$http.get('/answers/index/' + $scope.person
			+ '?question_category_id=' + $scope.category_id
			+ '&asker=' + $scope.user.spouse.gender
			+ '&private=1'
		).then(function(response) {
			$scope.answers = response.data.answers;
			$scope.categories = response.data.categories;
			$scope.loading = false;
		});
	}
	$scope.getAnswers(1);

	// get data
	$scope.getQuestion = function(index) {
		$scope.loadingQuestion = true;
		if (index) {
			$scope.answers[index].loading = true;
		}
		$http.get('/answers/random?question_category_id=' + $scope.category_id
			+ '&asker=' + $scope.user.spouse.gender
			+ '&private=1'
		).then(function(response) {
			console.log('response.data = ', response.data);
			if (index) {
				$scope.answers[index] = response.data;
				$scope.answers[index].loading = false;
			} else {
				$scope.answers.push(response.data);
			}
			$scope.loadingQuestion = false;
		});
	};


	// store answer
	$scope.storeAnswer = function(answer, index)
	{
		if (answer.body) {
			$http.post('/answers', answer).then(function(response) {
				console.log('response = ', response);
				if (index && response.data) {
					$scope.answers[index].id = response.data.id;
				}
			});
		}
	}

	// delete answer
	$scope.deleteAnswer = function(id, index)
	{
		$scope.answers[index].loading = true;
		$http.delete('/answers/delete/' + id).then(function() {
			$scope.answers.splice(index, 1);
		})
	}
});
