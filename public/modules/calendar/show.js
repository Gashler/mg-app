app.directive('mgCalendar', function() {
    return {
        templateUrl: '/modules/calendar/show.html',
        controller: function($scope, $http, $timeout) {

            // initialize variables
            $scope.timeouts = 0;

            // get month
            $scope.getMonth = function(date) {
                $scope.loadingCalendar = true;
                $http.get('/Stat/month/' + date).then(function(result) {
                    $scope.loadingCalendar = false;
                    $scope.data = result.data;

                    // get current day
                    angular.forEach($scope.data.days, function(day, index) {
                        if (day.active) {
                            $scope.unit = day;
                        }
                    });

                    //// generate recurring appointments ////

                    // initialize variables
                    var oneDay = 24 * 60 * 60 * 1000; // hours* minutes * seconds * milliseconds
                    var matches = [];
                    angular.forEach($scope.data.recurring, function(appointment, appointment_index) {

                        // calculate number of days between current day in cycle and beginning day of recurring appointment
                        $scope.firstDate = new Date(appointment.date_start_js);

                        // for intervals based on days of week (only considered if the appointment's recurring interval is set to 0)
                        if (appointment.recurring_interval == 0) {
                            var days_of_week = [
                                {
                                    name: 'sun'
                                },
                                {
                                    name: 'mon'
                                },
                                {
                                    name: 'tue'
                                },
                                {
                                    name: 'wed'
                                },
                                {
                                    name: 'thu'
                                },
                                {
                                    name: 'fri'
                                },
                                {
                                    name: 'sat'
                                }
                            ];
                        }

                        // loop through dates in range and compare them to recurring appointments to see if appointments should occur on these days
                        for (var day_of_month = 1; day_of_month <= $scope.data.month.days_in_month; day_of_month ++) {
                            angular.forEach($scope.data.days, function(day) {
                                if (day.date_js !== undefined) {

                                    // define pre-intialized variables
                                    $scope.secondDate = new Date(day.date_js);
                                    $scope.diffDays = Math.round(Math.abs(($scope.firstDate.getTime() - $scope.secondDate.getTime())/(oneDay)));

                                    // if day matches current loop iteration
                                    if (day.day_of_month == day_of_month) {

                                        // if recurring appointment could logically occur on this day
                                        if (appointment.date_start !== day.date && day.date > appointment.date_start) {

                                            // for numeric intervals between recurring times (i.e. every 5 days)
                                            if (appointment.recurring_interval > 0) {

                                                // if the interval beteween the beginning date and the current date divides perfectly
                                                if ($scope.diffDays % appointment.recurring_interval == 0) {
                                                    matches.push(day_of_month);
                                                }

                                            }

                                            // for weekday intervals
                                            angular.forEach(days_of_week, function(day_of_week, index) {

                                                // if the appointment is set to occur on the current day of week in the loop
                                                if (appointment[day_of_week.name] && lcfirst(day.name) == day_of_week.name) {

                                                    // if this match hasn't already been added, add it
                                                    if (matches.indexOf(day_of_month) == -1) {
                                                        matches.push(day_of_month);
                                                    }
                                                }
                                            });
                                        }
                                    }
                                }
                            });
                        }

                        // replicate recurring appointments on the days just determined
                        angular.forEach(matches, function(match) {
                            angular.forEach($scope.data.days, function(day, day_index) {

                                $scope.secondDate = new Date(day.date_js);
                                $scope.diffDays = Math.round(Math.abs(($scope.firstDate.getTime() - $scope.secondDate.getTime())/(oneDay)));

                                // if there is a match on the current day in the loop
                                if (day.day_of_month == match) {

                                    //// alternate who's in charge when applicable ////
                                    if (appointment.hasOwnProperty('in_charge')) {
                                        var appointmentDuplicate = JSON.parse(JSON.stringify(appointment));

                                        // for user-defined recurring intervals
                                        if (appointmentDuplicate.recurring_interval > 0) {

                                            // if an even day
                                            if ($scope.diffDays % 2) {
                                                appointmentDuplicate.in_charge = 'm' ? 'f' : 'm';
                                            }
                                            $scope.data.days[day_index].appointments.push(appointmentDuplicate);

                                        }

                                        //// for user-defined weekday intervals ////

                                        // determine day of week of first occurrence of appointment
                                        var firstOccurrenceDayIndex = new Date(appointmentDuplicate.date_start_js).getDay();
                                        var firstOccurrenceDayName = days_of_week[firstOccurrenceDayIndex].name;
                                        var matchesPerWeek = 0;

                                        // determine if number of recurrences per week is an odd or even number
                                        angular.forEach(days_of_week, function(day_of_week, index) {

                                            // if appointment recurs on this day of week
                                            if (appointmentDuplicate[day_of_week.name]) {

                                                // copy who the original person in charge is if this is the first day
                                                if (day_of_week.name == firstOccurrenceDayName) {
                                                    days_of_week[index].in_charge = appointmentDuplicate.in_charge;

                                                // otherwise just note that someone needs to be set as in charge on this day
                                                } else {
                                                    days_of_week[index].in_charge = true;
                                                }
                                                matchesPerWeek ++;
                                            }
                                        });

                                        //// aleternate who's in charge throughout the first week ////
                                        var in_charge = appointmentDuplicate.in_charge;

                                        // for days after the first occurrence
                                        angular.forEach(days_of_week, function(day_of_week, index) {
                                            if (day_of_week.in_charge && index > firstOccurrenceDayIndex) {
                                                in_charge = 'm' ? 'f' : 'm';
                                                days_of_week[index].in_charge = in_charge;
                                            }
                                        });

                                        // for days before the first occurence
                                        angular.forEach(days_of_week, function(day_of_week, index) {
                                            if (day_of_week.in_charge && index < firstOccurrenceDayIndex) {
                                                in_charge = 'm' ? 'f' : 'm';
                                                days_of_week[index].in_charge = in_charge;
                                            }
                                        });

                                        //// alternate who's in charge for weeks after the first week ////
                                        angular.forEach(days_of_week, function(day_of_week, index) {
                                            if (appointmentDuplicate[day_of_week.name] && lcfirst(day.name) == day_of_week.name) {
                                                if (matches.indexOf(day_of_month) == -1) {

                                                    // if there's an even number of occurrences per week, then the schedule will never change
                                                    if (matchesPerWeek % 2 == 0) {
                                                        appointmentDuplicate.in_charge = day_of_week.in_charge;
                                                        $scope.data.days[day_index].appointments.push(appointmentDuplicate);
                                                    } else {

                                                        // if it's been an even number of weeks since first occurrence, copy the original week
                                                        if (Math.floor($scope.diffDays / 7) % 2 == 0) {
                                                            appointmentDuplicate.in_charge = day_of_week.in_charge;
                                                            $scope.data.days[day_index].appointments.push(appointmentDuplicate);

                                                        // if it's been an odd number of weeks since first occurrence, alternate the original week
                                                        } else {
                                                            if (day_of_week.in_charge == 'm') {
                                                                appointmentDuplicate.in_charge = 'f';
                                                            } else {
                                                                appointmentDuplicate.in_charge = 'm';
                                                            }
                                                            $scope.data.days[day_index].appointments.push(appointmentDuplicate);
                                                        }
                                                    }
                                                }
                                            }
                                        });
                                    }
                                }
                            });
                        });
                        matches = [];
                    });
                });
            }
            $scope.getMonth('');

            // set active day
            $scope.setActiveDay = function(day, index) {
                angular.forEach($scope.data.days, function(day, index) {
                    if (day.active) {
                        $scope.data.days[index].active = 0;
                    }
                });
                $scope.data.days[index].active = 1;
                $scope.unit = day;
            }

            // update stats
            $scope.updateStats = function(key) {
                if ($scope.unit.stats.date == undefined) {
                    $scope.unit.stats.date = $scope.unit.date;
                }
                $http.post('/Stat/update', $scope.unit.stats).then(function(response) {
                    $scope.unit.stats.id = response.data.id;
                    if ($scope.timeout) {
                        $timeout.cancel($scope.timeout);
                    }
                    $scope.timeout = $timeout(function() {
                        $scope.getUserStats();
                        $scope.timeouts = 0;
                    }, 2000);
                });
            }

            // change month
            $scope.changeMonth = function(change) {
                month = parseInt($scope.data.month.number) + change;
                year = $scope.data.year;
                if (month > 12) {
                    month = 1;
                    year ++;
                }
                if (month < 1) {
                    month = 12;
                    year --;
                }
                date = year + '-' + month;
                $scope.getMonth(date);
            }

            // update time
            $scope.updateTime = function(key)
            {
                var time_type = key.split("_");
                time_type = time_type[1];
                var hour = parseInt($scope.appointment[time_type].hour);
                if ($scope.appointment[time_type].am == 'pm') {
                    hour += 12;
                }
                if (hour < 10) {
                    hour = '0' + hour;
                }
                $scope.appointment[key] = hour + ':' + $scope.appointment[time_type].minute + ':00';
                $scope.patch(key);
            }
        }
    }
});
