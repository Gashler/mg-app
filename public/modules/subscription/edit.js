angular.module('app')
.controller('SubscriptionEdit', function($scope, $http, $location, $routeParams, $filter) {

    $scope.loading = true;
    $scope.loading_toggle = false;
    $scope.loading_payment_method = false;
    $scope.popup_alert = null;

    // initialize billing months array
    $scope.months = [];
    for (month = 1; month <= 12; month ++) {
        if (month < 10) {
            month = '0' + month;
        }
        $scope.months.push(month);
    }

    // initialize billing years array
    $scope.years = [];
    var year = parseInt(new Date().getFullYear());
    var max = year + 11;
    for (year; year < max; year ++) {
        $scope.years.push(year);
    }

    // get customer
    $scope.getCustomer = function() {
        $http.get('/Subscription/' + $scope.user.account.id).then(function(response) {
            $scope.loading = false;
            if (response.data.error) {
                $scope.message('danger', response.data.message, 5000);
            } else {
                $scope.customer = response.data.data;
            }
        });
    }
    $scope.getCustomer();

    // get plans
    $scope.getPlans = function() {
        $scope.loading = true;
        $http.get('/subscription-plans').then(function(response) {
            $scope.loading = false;
            $scope.plans = response.data;
        });
    }
    $scope.getPlans();

    // update customer
    $scope.updateObject = function(object, id, key, value)
    {
        $http.put('/Subscription/update/' + $scope.user.account_id, {
            object: object,
            object_id: id,
            key: key,
            value: value,
        }).then(function(response) {
            if (response.data.error) {
                $scope.message('danger', response.data.message, 5000);
            } else {
                $scope.message('success', response.data.message, 5000);
            }
        });
    }

    // cancel or reactivate subscription
    $scope.toggleSubscription = function(method)
    {
        $scope.loading_toggle = true;
        $http.post('/Subscription/toggle', {
            method: method
        }).then(function(response) {
            $scope.loading_toggle = false;
            if (response.data.error) {
                $scope.message('danger', response.data.message, 5000);
            } else {
                $scope.message('success', response.data.message, 5000);
                $scope.customer = response.data.customer;
            }
        });
    }

    // add payment method
    $scope.addPaymentMethod = function()
    {
        $scope.loading_payment_method = true;
        Stripe.card.createToken({
            number: $scope.number,
            cvc: $scope.cvc,
            exp_month: $scope.expMonth,
            exp_year: $scope.expYear
        }, paymentResponseHandler);
    }

    // Stripe Response Handler
    function paymentResponseHandler(status, response) {
        if (response.error) {
            $scope.loading_payment_method = false;
            $scope.message('danger', response.error.message, 5000);
        } else {
            $http.post('/Subscription/addPaymentMethod', {
                stripeToken: response.id
            }).then(function(response) {
                $scope.loading_payment_method = false;
                $scope.message('success', response.data.message, 5000);
                $scope.customer = response.data.data;
                $scope.togglePopup('#paymentMethodForm');
            });
        }
        // $scope.$apply();
    };

    // change plan
    $scope.changePlan = function(plan_id) {
        var plan;
        angular.forEach($scope.plans, function(available_plan) {
            if (plan_id == available_plan.id) {
                plan = available_plan;
            }
        });
        if (confirm(
            "This will cancel your existing billing plan and start a new plan. You will immediately be charged " +
            $filter('currency')(plan.amount / 100) +
            ". Are you sure you want to do this?"
        )) {
            $scope.loading = true;
            $http.post('/Subscription/changePlan', {
                plan: plan.id
            }).then(function(response) {
                $scope.loading = false;
                $scope.message('success', response.data.message, 5000);
                $scope.customer = response.data.data;
            });
            // $scope.$apply();
        } else {
            $scope.loading = true;
            $scope.getCustomer();
        }
    }

});
