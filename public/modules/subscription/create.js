angular.module('app')
.controller('SubscriptionCreate', function($scope, $http, $location, $routeParams, $filter, $timeout) {

    $scope.expired = $routeParams.expired;
    $scope.no_subscription = $routeParams.no_subscription;
    $scope.payment = {};

    // initialize billing months array
    $scope.months = [];
    for (month = 1; month <= 12; month ++) {
        if (month < 10) {
            month = '0' + month;
        }
        $scope.months.push(month);
    }

    // initialize billing years array
    $scope.years = [];
    var year = parseInt(new Date().getFullYear());
    var max = year + 11;
    for (year; year < max; year ++) {
        $scope.years.push(year);
    }

    // get plans
    $scope.getPlans = function()
    {
        $scope.loading = true;
        if (!$routeParams.id) {
            $routeParams.id = '';
        }
        $http.get('/subscription-plans/' + $routeParams.id).then(function(response) {
            console.log('response = ', response.data);
            $scope.loading = false;
            $scope.plans = response.data;
            $scope.plan_index = 0;
            if (!$scope.plans[$scope.plan_index]) {
                $scope.plan = $scope.plans;
            } else {
                $scope.plan = $scope.plans[$scope.plan_index];
            }
            $scope.plan_id = $scope.plan.id;
        });
    }
    $scope.getPlans();

    // get token
    $scope.getToken = function()
    {
        $scope.loading = true;
        Stripe.card.createToken($scope.payment, createSubscription);
    }

    // Stripe Response Handler
    function createSubscription(code, result)
    {
        if (result.error) {
            $scope.loading = false;
            $scope.message('danger', result.error.message, 10000);
        } else {
            $http.post('/Subscription', {
                object: $scope.plan.object,
                stripeToken: result.id,
                plan_id: $scope.plan.id
            }).then(function(response) {
                $scope.loading = false;
                if (!$scope.registering) {
                    $scope.message(response.data.message.type, response.data.message.body, null, 99);
                }
                if (response.data.message.type == 'success') {
                    $scope.user = response.data.user;
                    if (!$scope.registering || $scope.registering == undefined) {
                        $timeout(function() {
                            window.location = '/';
                        }, 1000);
                    } else {
                        $scope.step = 10;
                        $scope.checkInbox();
                        $('#loading').hide();
                    }
                }
            });
        }
        $scope.$apply();
    };
});
