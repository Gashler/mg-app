angular.module('app')
.controller('RoleplayingOutline', function($scope, $http, $routeParams)
{
	// define variables
	$scope.loading = true;

	// get data
	$http.get('/RoleplayOutline/' + $routeParams.id).then(function(data) {
		$scope.roleplayOutline = data.data;
		$scope.loading = false;
	});
});
