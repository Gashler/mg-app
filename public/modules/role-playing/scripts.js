angular.module('app')
.controller('RoleplayingScripts', function($scope, $http)
{
	// define variables
	$scope.loading = false;

	// get data
	$scope.getData = function() {
		$scope.loading = true;
		$http.get('/RoleplayScript').then(function(data) {
			$scope.roleplayScripts = data.data;
			$scope.loading = false;
		});
	};
	$scope.getData();
});
