angular.module('app')
.controller('RoleplayingScript', function($scope, $http, $routeParams)
{
	// define variables
	$scope.loading = true;

	// get data
	$http.get('/RoleplayScript/' + $routeParams.id).then(function(data) {
		$scope.roleplayScript = data.data;
		$scope.loading = false;
	});
});
