angular.module('app')
.controller('RoleplayingOutlines', function($scope, $http)
{
	// define variables
	$scope.loading = false;

	// get data
	$scope.getData = function() {
		$scope.loading = true;
		$http.get('/RoleplayOutline').then(function(data) {
			$scope.roleplayOutlines = data.data;
			$scope.loading = false;
		});
	};
	$scope.getData();


		// $scope.currentPage = 1;
		// $scope.pageSize = 3;
		//
		// $scope.pageChangeHandler = function(num) {
		//
		// };


	// function OtherController($scope) {
	// 	$scope.pageChangeHandler = function(num) {
	// 	};
	// }
});
