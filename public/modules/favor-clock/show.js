app.directive('mgFavorClock', function () {
    return {
        templateUrl: '/modules/favor-clock/show.html',
        controller: function ($scope, $http, $interval) {

            // initialize variables
            $scope.loadingFavorClock = 0;

            // get favor clock
            $scope.getFavorClock = function (loading) {
                if (loading) {
                    $scope.loadingFavorClock = true;
                }
                $http.get('/FavorClock').then(function (response) {
                    if (loading) {
                        $scope.favorClock = response.data;
                    } else {
                        // console.log('$scope.favorClock.data = ', $scope.favorClock.time);
                        // console.log('response.data.time = ', response.data.time);
                        // console.log(response.data.time - $scope.favorClock.time);
                        // console.log($scope.favorClock.time - response.data.time);

                        // if there's a major discrepency between server and client data, update client data
                        // if (
                        //     response.data.time - $scope.favorClock.time >= 5
                        //     || $scope.favorClock.time - response.data.time >= 5
                        // ) {
                        //     $scope.favorClock = response.data;
                        // }

                    }
                    // console.log('$scope.favorClock = ', $scope.favorClock);
                    if (!$scope.favorClock.server || !$scope.favorClock.ower) {
                        $scope.favorClock.server = $scope.user.gender;
                        $scope.favorClock.ower = $scope.user.spouse.gender;
                    }
                    $scope.formatTime();
                    if (loading) {
                        if ($scope.favorClock.active) {
                            $scope.activateTimer();
                        }
                        $scope.loadingFavorClock = false;
                    }
                });
            }
            $scope.getFavorClock(true);

            // update favor clock
            $scope.updateFavorClock = function () {
                $scope.favorClock.time_owed = $scope.favorClock.time;
                $http.put('/FavorClock', $scope.favorClock).then(function (response) {
                    $scope.favorClock = response.data;
                    $scope.formatTime();
                });
            }

            $scope.toggleServer = function () {
                $scope.favorClock.increasing = !$scope.favorClock.increasing;
                $scope.updateFavorClock();
                $interval.cancel($scope.timeInterval);
                $interval.cancel($scope.updateInterval);
                // $scope.activateTimer();
                $scope.favorClock.active = false;
            }

            // get favor clock
            $scope.toggleTimer = function () {
                $scope.favorClock.active = !$scope.favorClock.active;
                $scope.updateFavorClock();
                if ($scope.favorClock.active) {
                    $scope.activateTimer();
                } else {
                    $interval.cancel($scope.timeInterval);
                    $interval.cancel($scope.updateInterval);
                }
            }

            // activate timer
            $scope.activateTimer = function () {
                $scope.timeInterval = $interval(function () {
                    if ($scope.favorClock.increasing) {
                        $scope.favorClock.time++;
                    } else {
                        $scope.favorClock.time--;
                    }
                    if ($scope.favorClock.time <= 0) {
                        $scope.favorClock.increasing = true;
                        if ($scope.favorClock.server == 'm') {
                            $scope.favorClock.ower = 'f';
                        } else {
                            $scope.favorClock.ower = 'm';
                        }
                        $scope.updateFavorClock();
                    }
                    $scope.formatTime();
                }, 1000);
                $scope.updateInterval = $interval(function () {
                    $scope.getFavorClock();
                }, 2500);

                // destroy intervals when leaving page
                $scope.$on("$destroy", function () {
                    $interval.cancel($scope.timeInterval);
                    $interval.cancel($scope.updateInterval);
                });
            }

            // formate time
            $scope.formatTime = function () {
                $scope.favorClock.display_time = parseInt($scope.favorClock.time / 3600); // hours
                var minutes = parseInt(($scope.favorClock.time % 3600) / 60); // minutes
                if (minutes < 10) minutes = "0" + minutes;
                $scope.favorClock.display_time += ":" + minutes;
                seconds = ($scope.favorClock.time % 60); // seconds
                if (seconds < 10) seconds = "0" + seconds;
                $scope.favorClock.display_time += ":" + seconds;
            }


            // reset time
            $scope.reset = function () {
                $scope.favorClock.time = 0;
                $scope.formatTime();
            }
        }
    }
});
