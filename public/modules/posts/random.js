angular.module('app')
.directive('mgRandomPosts', function() {
    return {
        templateUrl: '/modules/posts/random.html',
        controller: function($scope, $http) {

            // define variables
            $scope.loagingPosts = true;

            // count s
            $scope.getData = function()
            {
                $scope.loadingPosts = true;
                $http.get('/posts/random-set').then(function(response) {
                    $scope.loadingPosts = false;
                    $scope.categories = response.data;
                    console.log('$scope.categories = ', $scope.categories);
                });
            }
            $scope.getData();
        }
    }
});
