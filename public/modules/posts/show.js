angular.module('app')
.controller('PostShowController', function($scope, $http, $routeParams)
{
	// define variables
	$scope.loading = true;

	// get data
	$http.get('/posts/show/' + $routeParams.id).then(function(response) {
		$scope.post = response.data;
		$scope.loading = false;
	});
});
