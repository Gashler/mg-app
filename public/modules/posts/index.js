angular.module('app')
.controller('PostIndexController', function($scope, $http, $routeParams)
{
	// define variables
	$scope.loading = false;
	$scope.search = "";

	// get data
	$scope.getData = function() {
		$scope.loading = true;
		if ($scope.category) {
			var id = $scope.category.id;
		} else {
			var id = $routeParams.category_id;
		}
		if (id == undefined) {
			id = 0;
		}
		$http.get('/posts/?category_id=' + id).then(function(response) {
			$scope.posts = response.data.posts;
			$scope.category = response.data.category;
			$scope.categories = response.data.categories;
			$scope.loading = false;
		});
	};
	$scope.getData();
});
