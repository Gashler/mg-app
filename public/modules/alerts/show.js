angular.module('app')
.directive('mgAlerts', function() {
    return {
        templateUrl: '/modules/alerts/show.html',
        controller: function($scope, $http, $interval) {

            // define variables
            $scope.loadingAlerts = true;

            // count users
            $scope.getAlerts = function()
            {
                $scope.loadingAlerts = true;
                $http.get('/Alert').then(function(response) {
                    $scope.alerts = response.data;
                    $scope.loadingAlerts = false;

                    angular.forEach($scope.alerts, function(a, index) {

                        // trigger popups
                        if (a.type == 'popup') {
                            $scope.alert = a;
                            $scope.togglePopup('#alertPopup');
                        }

                        // update countdown timer
                        if (a.countdown) {
                            $scope.formatTime(a);
                            $interval(function() {
                                if (a.countdown > 0) {
                                    a.countdown --;
                                    $scope.formatTime(a);
                                }
                            }, 1000);
                        }
                    });
                });
            }
            $scope.getAlerts();


            $scope.formatTime = function(a)
            {
                a.display_time = parseInt(a.countdown / 3600); // hours
                var minutes = parseInt((a.countdown % 3600) / 60); // minutes
                if (minutes < 10) minutes = "0" + minutes;
                a.display_time += ":" + minutes;
                seconds = (a.countdown % 60); // seconds
                if (seconds < 10) seconds = "0" + seconds;
                a.display_time += ":" + seconds;
            }
        }
    }
});
