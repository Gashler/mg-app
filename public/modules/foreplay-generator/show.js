angular.module('app')
.controller('ForeplayGeneratorController', function($scope, $http)
{
	// define variables
	$scope.loading = true;
	$scope.loadingAction = false;
	$scope.performer = 'alternate';
	$scope.performerGender = $scope.user.gender;

	// get data
	$http.get('/ForeplayGenerator').then(function(response) {
		$scope.action = response.data.action;
		$scope.ratings = response.data.ratings;
		$scope.categories = response.data.categories;
		$scope.rating = $scope.ratings[2];
		$scope.category = $scope.categories[3];
		$scope.loading = false;
	});

	// get data
	$scope.getAction = function() {
		$scope.loadingAction = true;

		// determine performer
		if ($scope.performer == 'm' || $scope.performer == 'f') {
			$scope.performerGender = $scope.performer;
		} else if ($scope.performer == 'alternate') {
			if ($scope.performerGender == 'm') {
				$scope.performerGender = 'f';
			} else {
				$scope.performerGender = 'm';
			}
		} else if ($scope.performer == 'random') {
			$scope.performerGender = Math.floor((Math.random() * 2)) ? 'm' : 'f';
		}
		$http.get('/Action/random?'
			+ 'performer=' + $scope.performerGender
			+ '&rating_id=' + $scope.rating.id
			+ '&category_id=' + $scope.category.id
		).then(function(response) {
			$scope.action = response.data.data;
			$scope.loadingAction = false;
		});
	};
});
