angular.module('app')
.directive('mgCompanyStats', function() {
    return {
        templateUrl: '/modules/company-stats/show.html',
        controller: function($scope, $http) {

            // count companys
            $scope.getCompanyStats = function()
            {
                $scope.loadingStats = true;
                $http.get('/CompanyStat/').then(function(response) {
                    $scope.loadingStats = false;
                    $scope.companyStats = response.data;
                    $scope.report = 'subscribers';
                    $scope.generateChart();
                });
            }
            $scope.getCompanyStats();

            $scope.generateChart = function()
            {
                $('#reportContainer').html('<canvas id="myChart" width="400" height="100"></canvas>');
                var ctx = $("#myChart");
                if ($scope.report == 'verified_users') {
                    var myChart = new Chart(ctx, {
                        options: {
                            scaleBeginAtZero: true
                        },
                        type: 'line',
                        data: {
                            labels: $scope.companyStats.labels,
                            datasets: [
                                {
                                    label: $scope.companyStats.datasets.verified_users.label,
                                    fill: true,
                                    lineTension: 0.1,
                                    backgroundColor: "rgba(255,64,128,0.4)",
                                    borderColor: "rgba(255,0,128, 1)",
                                    borderCapStyle: 'butt',
                                    borderDash: [],
                                    borderDashOffset: 0.0,
                                    borderJoinStyle: 'miter',
                                    pointBorderColor: 'black',
                                    pointBackgroundColor: 'black',
                                    pointBorderWidth: 1,
                                    pointHoverRadius: 5,
                                    pointHoverBackgroundColor: "rgba(255,64,128,0.4)",
                                    pointHoverBorderColor: "rgba(255,0,128, 1)",
                                    pointHoverBorderWidth: 2,
                                    pointRadius: 1,
                                    pointHitRadius: 10,
                                    data:  $scope.companyStats.datasets.verified_users.data,
                                    spanGaps: false,
                                }
                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    stacked: true
                                }]
                            }
                        }
                    });
                }
                if ($scope.report == 'subscribers') {
                    var myChart = new Chart(ctx, {
                        options: {
                            scaleBeginAtZero: true
                        },
                        type: 'line',
                        data: {
                            labels: $scope.companyStats.labels,
                            datasets: [
                                {
                                    label: $scope.companyStats.datasets.subscribers.label,
                                    fill: true,
                                    lineTension: 0.1,
                                    backgroundColor: "rgba(255,64,128,0.4)",
                                    borderColor: "rgba(255,0,128, 1)",
                                    borderCapStyle: 'butt',
                                    borderDash: [],
                                    borderDashOffset: 0.0,
                                    borderJoinStyle: 'miter',
                                    pointBorderColor: 'black',
                                    pointBackgroundColor: 'black',
                                    pointBorderWidth: 1,
                                    pointHoverRadius: 5,
                                    pointHoverBackgroundColor: "rgba(255,64,128,0.4)",
                                    pointHoverBorderColor: "rgba(255,0,128, 1)",
                                    pointHoverBorderWidth: 2,
                                    pointRadius: 1,
                                    pointHitRadius: 10,
                                    data:  $scope.companyStats.datasets.subscribers.data,
                                    spanGaps: false,
                                }
                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    stacked: true
                                }]
                            }
                        }
                    });
                }
                if ($scope.report == 'unverified_users') {
                    var myChart = new Chart(ctx, {
                        options: {
                            scaleBeginAtZero: true
                        },
                        type: 'line',
                        data: {
                            labels: $scope.companyStats.labels,
                            datasets: [
                                {
                                    label: $scope.companyStats.datasets.unverified_users.label,
                                    fill: true,
                                    lineTension: 0.1,
                                    backgroundColor: "rgba(255,64,128,0.4)",
                                    borderColor: "rgba(255,0,128, 1)",
                                    borderCapStyle: 'butt',
                                    borderDash: [],
                                    borderDashOffset: 0.0,
                                    borderJoinStyle: 'miter',
                                    pointBorderColor: 'black',
                                    pointBackgroundColor: 'black',
                                    pointBorderWidth: 1,
                                    pointHoverRadius: 5,
                                    pointHoverBackgroundColor: "rgba(255,64,128,0.4)",
                                    pointHoverBorderColor: "rgba(255,0,128, 1)",
                                    pointHoverBorderWidth: 2,
                                    pointRadius: 1,
                                    pointHitRadius: 10,
                                    data:  $scope.companyStats.datasets.unverified_users.data,
                                    spanGaps: false,
                                }
                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    stacked: true
                                }]
                            }
                        }
                    });
                }
                if ($scope.report == 'verification_rates') {
                    var myChart = new Chart(ctx, {
                        options: {
                            scaleBeginAtZero: true
                        },
                        type: 'line',
                        data: {
                            labels: $scope.companyStats.labels,
                            datasets: [
                                {
                                    label: $scope.companyStats.datasets.verification_rates.label,
                                    fill: true,
                                    lineTension: 0.1,
                                    backgroundColor: "rgba(255,64,128,0.4)",
                                    borderColor: "rgba(255,0,128, 1)",
                                    borderCapStyle: 'butt',
                                    borderDash: [],
                                    borderDashOffset: 0.0,
                                    borderJoinStyle: 'miter',
                                    pointBorderColor: 'black',
                                    pointBackgroundColor: 'black',
                                    pointBorderWidth: 1,
                                    pointHoverRadius: 5,
                                    pointHoverBackgroundColor: "rgba(255,64,128,0.4)",
                                    pointHoverBorderColor: "rgba(255,0,128, 1)",
                                    pointHoverBorderWidth: 2,
                                    pointRadius: 1,
                                    pointHitRadius: 10,
                                    data:  $scope.companyStats.datasets.verification_rates.data,
                                    spanGaps: false,
                                }
                            ]
                        },
                        options: {
                            scales: {
                                yAxes: [{
                                    stacked: true
                                }]
                            }
                        }
                    });
                }
            }
        }
    }
});
