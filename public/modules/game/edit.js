angular.module('app')
.controller('GameEdit', function($scope, $http, $timeout, $routeParams, vars) {
    if ($scope.user == undefined) {
    $scope.getVars();
}

    // initialize variables
    $scope.loaded = false;
    game_id = $routeParams.id;

    /***************
    * Game
    ***************/

    // get game
    $http.get('/Game/getedit/' + game_id).then(function(data) {
        angular.forEach(data, function(value, key) {
            $scope[key] = value;
        });
        $scope.loaded = true;
        // $scope.setObject($scope.game);
        $scope.unit = $scope.game;
        // configure variables
        // if($scope.unit.turn == 'm') $scope.unit.turn.spouse = 'f';
        // else $scope.unit.turn.spouse = 'm';

        // update game
        $scope.updateGame = function(data) {
            if(data == null) data = $scope.game;
            $http.put('/game/' + $scope.game.id, data);
        }

    });

});
