angular.module('app')
.controller('GameShow', function($scope, $http, $timeout, $routeParams, vars) {
	if ($scope.user == undefined) {
    $scope.getVars();
}

	/********************
	* Game
	*********************/

    // initialize variables
    game_id = $routeParams.id;

	// get game
	$http.get('/Game/show/' + game_id).then(function(data) {
		$scope.game = data;
    });

});
