angular.module('app')
.controller('GamePlay', function($scope, $http, $timeout, $routeParams, vars) {
    if ($scope.user == undefined) {
    $scope.getVars();
}

    // initialize variables
    $scope.loaded = false;
    game_id = $routeParams.id;

    /********************
    * Game
    *********************/

    // get game
    $http.get('/Game/play/' + game_id).then(function(data) {
        $scope.game = data;
        $scope.map = $scope.game.maps[0];
        $scope.loaded = true;

        // update game
        function updateGame(data) {
            $http.put('/game/' + $scope.game.id, data);
        }


        /**************************
        * Common Functions
        ***************************/

        $scope.create = function(data, model) {
            if (!$scope.hasOwnProperty('obj')) {
                $scope.unit = $scope.game;
            }
            eval('$scope.loading_' + model + ' = true;');
            $http.post('/' + ucfirst(model) + '/store', data).then(function(result) {
                result = $scope.setPaths(result, model);
                $scope.unit[model + 's'].push(result);
                eval('$scope.loading_' + model + ' = false;');
            });
        };

        // set paths
        $scope.setPaths = function(object, model, model_as, row_index, col_index) {

            if (model_as == null) {
                object.model = model;
            }
            else {
                object.model = model_as;
            }
            object.parent = $scope.unit.path;

            // if object's parent is array
            if (typeof eval(object.parent) === 'array') {
                index = eval(object.parent + '.length');
                object.path = object.parent + '[' + index + ']';
            }

            // if object's parent is part of array
            else if (typeof eval(object.parent + '.' + model + 's') === 'object') {
                if (model_as == null) {
                    index = eval(object.parent + '.' + model + 's' + '.length');
                    object.path = object.parent + '.' + model + 's' + '[' + index + ']';
                }
                else {
                    index = eval(object.parent + '.' + model_as + 's' + '.length');
                    object.path = object.parent + '.' + model_as + 's' + '[' + index + ']';
                }
            }

            // if object's parent is not an array or part of an array
            else {
                object.path = $scope.unit.path + '.';
                if (row_index !== undefined && col_index !== undefined) {
                    object.path += 'rows[' + row_index + '].cols[' + col_index + '].';
                }
                if (model_as == null) {
                    object.path += model;
                }
                else {
                    object.path += model_as;
                }
            }
            return object;
        }

        // update object
        $scope.update = function(obj) {
            $http.post('/' + ucfirst(unit.model) + '/update/' + unit.id, obj).then(function(result) {
                if (result !== undefined) {
                    angular.forEach(result, function(new_value, new_property) {
                        angular.forEach(obj, function(old_value, old_property) {
                            if (new_property == old_property) {
                                obj[old_property] = new_value;
                            }
                        });
                    });
                }
                eval(unit.path + ' = obj;');
            });
        };

        /******************
        * Player Buttons
        *******************/

        $scope.playerButton = function(type) {
            $('.player-buttons .btn').each(function() {
                $(this).removeClass('active');
            });
            $('.player-buttons .btn.' + type).addClass('active');
        }

        /*****************
        * Popups
        ******************/

        animating = false;
        $scope.togglePopup = function(popup) {

            // hide popup
            function hidePopup(popup) {
                $(popup).css('display', 'none !important');
                $(popup + ' .panel').parent().fadeOut(250);
                $(popup + ' .panel').css('margin-top', '-200px');
            }

            // show popup
            if (!animating) {
                animating = true;
                if ($(popup).css('display') == 'none') {
                    $('#container ' + popup).fadeIn(250);
                    $('#container ' + popup + ' .panel').css('margin-top', '20px');
                    setTimeout(function() {
                        animating = false;
                    }, 250);
                }
                else hidePopup(popup);
            }

            // clicking in black filter
            $('.popup').click(function(event) {
                event.stopPropagation();
                hidePopup('#' + $(this).attr('id'));
                $scope.$apply();
            });
            $('.popup .popup').click(function(event) {
                event.stopPropagation();
            });

            // prevent clicking on popup from hiding popup and prevent mulitple events triggered by other popups
            id = popup.replace('#', '');
            $(popup + ' .panel, .popup[id!="' + id + '"]').click(function(event) {
                event.stopPropagation();
            });

        }


        // get unit
        $scope.getUnit = function(model, id, object) {
            if (object == undefined) {
                object = $scope.game.maps;
            }
            angular.forEach(object, function(value, key) {
                if (value !== null) {
                    if (typeof value === 'object' && value.hasOwnProperty('model') && value.model == model && value.hasOwnProperty('mid') && value.mid == id) {
                        $scope.subject = value;
                    }
                    else if (typeof value === 'array' || typeof value === 'object') {
                        $scope.getUnit(model, id, value);
                    }
                }
            });
        }

        // get object
        $scope.getObject = function(model, id, parent) {
            if (parent == undefined) {
                parent = 'game';
            }
            if ($scope[parent][model + 's'].length > 0) {
                object_found = false;
                angular.forEach($scope[parent][model + 's'], function(object, key) {
                    if (!object_found) {
                        if (object.id == id) {
                            object_found = true;
                            if ($scope[parent][model + 's'][key] !== undefined) {
                                $scope[model] = $scope[parent][model + 's'][key];
                            }
                            else {
                                $scope.getObject(model, id, 'master');
                            }
                        }
                    }
                });
            }
            // else getObject(model, id, 'master');
        }

            /*************************
            * Player
            **************************/

            // get player
            if ($scope.game.players[0] !== null) {
                $scope.player = $scope.game.players[0];
            }
            else {
                $scope.player = $scope.master.players[0];
            }

            console.log($scope.player);

            // update player
            $scope.updatePlayer = function() {
                $scope.$apply();
                $http.put('/character', $scope.player.id, $scope.player);
            }

            /*************************
            * Uvents
            **************************/

            // get starting uvent
            if ($scope.game.uvent_current !== null) $scope.getObject('uvent', $scope.game.uvent_current);
            else $scope.uvent = $scope.game.uvents[0];

            // go to next uvent
            $scope.nextUvent = function(result_id) {
                changeUvent(result_id);
            };

            // change uvent
            function changeUvent(id) {
                $scope.getObject('uvent', id);
                $scope.game.uvent_current = $scope.uvent.id;
                updateGame([{
                    uvent_current : $scope.game.uvent_current
                }]);
            }

            /*************************
            * Options
            **************************/

            // select option
            $scope.selectOption = function(id) {
                changeUvent(id);
            };

            /*************************
            * Timers
            **************************/

            // countdown timer
            if ($scope.uvent !== undefined && $scope.uvent.timer !== null) {
                time = setInterval(function() { count() }, 1000);
                function count() {

                    // count down
                    if ($scope.uvent.timer !== null && $scope.uvent.timer.direction == 'down') {
                        if ($scope.uvent.timer.value > $scope.uvent.timer.min + 1) {
                            $scope.uvent.timer.value --;
                            $scope.$apply();
                        }
                        else timerResult();
                    }

                    // count up
                    if ($scope.uvent.timer !== null && $scope.uvent.timer.direction == 'up') {
                        if ($scope.uvent.timer.value < $scope.uvent.timer.max - 1) {
                            $scope.uvent.timer.value --;
                            $scope.$apply();
                        }
                        else timerResult();
                    }

                    // timer result
                    function timerResult() {
                        if ($scope.uvent.timer.result_type != null) {
                            record_found = false;
                            angular.forEach($scope.game[$scope.uvent.timer.result_type.toLowerCase() + 's'], function(object, index) {
                                if (!record_found) {
                                    if (object.id == $scope.uvent.timer.result_id) {
                                        record_found = true;
                                        clearInterval(time);
                                        $scope[$scope.uvent.timer.result_type.toLowerCase()] = $scope.game[$scope.uvent.timer.result_type.toLowerCase() + 's'][index];
                                    }
                                }
                            });
                        }
                    }
                };
            }

        });
    });
