angular.module('app')
.controller('GameCreate', function($http, $location) {
    if ($scope.user == undefined) {
        $scope.getVars();
    }

    // create game
    $http.get('/Game/create').then(function(result) {
        $location.path('/games/' + result.id + '/edit');
    })

});
