angular.module('app')
.controller('GameIndex', function($scope, $http, $timeout, $routeParams, vars) {
    if ($scope.user == undefined) {
    $scope.getVars();
}

    // initialize variables
    $scope.loading = true;

    $http.get('/Game/all').then(function(games) {

        // get data
        $scope.games = games;

        // bulk action checkboxes
        $scope.checkbox = function() {
            var checked = false;
            $('.bulk-check').each(function() {
                if ($(this).is(":checked")) checked = true;
            });
            if (checked == true) $('.applyAction').removeAttr('disabled');
            else $('.applyAction').attr('disabled', 'disabled');
        };

        // disable loading image
        $scope.loading = false;

    });

    $scope.currentPage = 1;
    $scope.pageSize = 15;

});
