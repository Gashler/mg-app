angular.module('app')
.controller('GameEnd', function($http, $location, $routeParams) {
    if ($scope.user == undefined) {
    $scope.getVars();
}

    // end game
    $http.get('/Game/end/' + $routeParams.id).then(function(result) {
        if(result.success) {
            $location.path('/games/' + result.data.master_id + '/' + $routeParams.redirect);
        }
        else {
            $scope.errors = result.message;
        }
    })

});
