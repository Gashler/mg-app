angular.module('app')
    .controller('ProfileEdit', function ($scope, $http, $location, $routeParams, $filter) {
        $scope.spouse = '';

        if ($scope.user == undefined) {
            $scope.getVars();
        }

        // initalize variables
        $scope.loading = true;
        if ($routeParams.person == 'user') {
            var person_id = $scope.user.id;
            $scope.person_possessive = 'My';
        } else if ($routeParams.person == 'spouse') {
            var person_id = $scope.user.spouse.id;
            $scope.spouse = 'spouse';
            $scope.person_possessive = $scope.user.spouse.first_name + "'s";
        }

        // get user
        $http.get('/users/show/' + person_id).then(function (response) {
            $scope.loading = false;
            $scope.person = response.data;
        });
    });
