angular.module('app')
	.controller('Poker', function ($scope, $http, $interval, $timeout, $route) {
		// define variables
		$scope.loading = true;
		$scope.stopUpdates = false;
		// $scope.changesOld = {};
		// $scope.changesOlder = {};
		$scope.cards = [1, 2, 3, 4, 5];
		$scope.amounts = [
			{
				amount: 5,
				lower: false,
				raise: true
			},
			{
				amount: 25,
				lower: false,
				raise: true
			},
			{
				amount: 100,
				lower: false,
				raise: true
			}
		];

		$http.get('/poker/game').then(function (response) {
			/*******************************
			* Get data
			*******************************/
			console.log('response.data.data = ', response.data.data);

			// define variables
			$scope.loading = false;
			$scope.addPlayerMoneyToObject(response.data.data);
			// $scope.olderGame = jQuery.extend(true, {}, response.data.data);
			$scope.oldGame = jQuery.extend(true, {}, response.data.data);
			$scope.changes = jQuery.extend(true, {}, response.data.data);
			$scope.game = jQuery.extend(true, {}, $scope.oldGame);
			$scope.game = response.data.data;

			// generate user and spouse gender prefixes
			$scope.player = $scope.user.gender + '_';
			$scope.opponent = $scope.user.spouse.gender + '_';

			// if starting a new game
			if ($scope.game.phase == 0 && !$scope.game[$scope.opponent + 'ready']) {
				$scope.message('warning', "Waiting for " + $scope.user.spouse.first_name + " to join.");
			}

			// if no ante
			if ($scope.game.phase == 0 && $scope.game.noAnte) {
				$scope.dismissMessages();
			}

			// if joining a new game
			if ($scope.game.firstGame && $scope.game.phase == 0 && !$scope.game[$scope.player + 'ready'] && !$scope.game.noAnte) {
				$scope.loading = true;
				$scope.message('success', "Connecting to " + $scope.user.spouse.first_name + " ...", 7000);
				$scope.game[$scope.player + 'ready'] = 1;
				$scope.game[$scope.player + 'status'] = 'join';
			}

			// resize cards for mobile displays
			$scope.resizeHands();
			$(window).resize(function () {
				$scope.resizeHands();
			});
			$scope.originalGameHeight = $("#gameContent").height();

			// cancel functionality that closes opening popup upon clicking on gray background
			$("body").on({
				click: function (e) {
					$(this).off("click");
				}
			}, ".dark");

			// disable buttons
			if ($scope.game.phase < 2) {
				if (!$scope.game.newGame && $scope.game[$scope.player + 'ready'] && !$scope.game[$scope.opponent + 'ready']) {
					if ($scope.game.phase == 0) {
						$scope.message('warning', 'Waiting on ' + $scope.user.spouse.first_name + ' to join.');
					} else {
						$scope.message('warning', 'Waiting on ' + $scope.user.spouse.first_name + ' to discard or stay.', 5000);
					}
				}
			}
			if ($scope.game.phase > 1) {

				$("#discard, #deal").attr("disabled", "disabled");
			}

			// if a new game, deal
			if ($scope.game.phase > 0 && !$scope.game.noAnte) {
				$scope.deal();
			}

			// if in betting rouond
			if ($scope.game.phase == 2) {
				$scope.betting();
			}

			// mark for discarding
			marked = 0;
			$("#userHand").on({
				click: function () {
					if ($scope.game.phase == 1 && !$scope.game[$scope.player + 'ready']) {


						if ($(".discard", this).length == 0 && marked < 4) {
							$(this).prepend("<div class='discard'><div class='discardX'>&times;</div></div>");
							marked++;
						}
						else if ($(".discard", this).length > 0) {
							$(".discard", this).remove();
							marked--;
						}
						if ($("#userHand .discard").length !== 0) $("#discard").removeAttr("disabled");
						else $("#discard").attr("disabled", "disabled");
					}
				}
			}, ".cardContainer");

			$scope.alternateTurn = function () {
				if ($scope.game.turn == 'm') {
					$scope.game.turn == 'f';
				} else {
					$scope.game.turn = 'm';
				}
			}

			/****************************************************************
			* send updates to database and check for updates from spouse
			****************************************************************/

			$scope.updateInterval = $interval(function () {
				$scope.addPlayerMoneyToObject($scope.game);
				$scope.constructUpdates();

				// get changed values since last update
				angular.forEach($scope.game, function (new_value, new_key) {
					angular.forEach($scope.oldGame, function (old_value, old_key) {
						// angular.forEach($scope.olderGame, function(older_value, older_key) {
						if (new_key == old_key) {
							var ignore = false;
							// // prevent overwriting opponent's data
							// if (new_key.startsWith($scope.opponent)) {
							// 	ignore = true;
							// };
							if ($scope.changes.hasOwnProperty(new_key) && $scope.changes[new_key] == new_value) {
								ignore = true;
							}

							if (
								!ignore && new_value !== old_value
								// && new_value !== older_value
								&& new_value !== ''
								&& new_key !== 'm_updated_at'
								&& new_key !== 'f_updated_at'
							) {
								$scope.updates.data[new_key] = new_value;
							}
						}
						// });
					});
				});

				// if (!$scope.stopUpdates) {
				$http.patch("/poker", $scope.updates).then(function (response) {
					if (response.data) {

						// update changes and game objects with new data
						$scope.changes = response.data;
						angular.forEach($scope.changes, function (value, key) {
							if (
								$scope.changesOld.hasOwnProperty(key) && $scope.changesOld[key] == value
								// || $scope.changesOlder.hasOwnProperty(key) && $scope.changesOlder[key] == value
							) {
								delete $scope.changes[key];
							} else {
								$scope.game[key] = value;
							}
						});
						console.log('$scope.changes = ', $scope.changes);

						// if money has changed
						if ($scope.changes.hasOwnProperty('m_money')) {
							if ($scope.user.gender == 'm') {
								$scope.user.money = $scope.changes['m_money'];
							} else {
								$scope.user.spouse.money = $scope.changes['m_money'];
							}
						}
						if ($scope.changes.hasOwnProperty('f_money')) {
							if ($scope.user.gender == 'f') {
								$scope.user.money = $scope.changes['f_money'];
							} else {
								$scope.user.spouse.money = $scope.changes['f_money'];
							}
						}

						// restart
						if ($scope.game[$scope.player + 'status'] == 'restart') {
							$scope.stopUpdates = true;
							$timeout(function () {
								$http.get('/poker/destroy').then(function () {
									$route.reload();
								});
							}, $scope.game.updateInterval);
						}

						// spouse joins
						if ($scope.game.firstGame && $scope.game.phase == 0 && $scope.changes[$scope.opponent + 'status'] == "join") {
							$scope.loading = true;
							$scope.dismissMessages();

							// $scope.game[$scope.opponent + 'status'] = '';
							$scope.game[$scope.player + 'status'] = 'deal';
							if ($scope.user.money >= $scope.game.ante && $scope.user.spouse.money >= $scope.game.ante) {
								$timeout(function () {
									$scope.dismissMessages();
									$scope.message('success', $scope.user.spouse.first_name + ' has joined the game.', 5000);
									$scope.game.phase = 1;
									$scope.deal(true);
								}, $scope.game.updateInterval * 3);
							} else {
								$scope.game.noAnte = true;
								$scope.dismissMessages();
							}
						}

						// if entering phase 1
						if (typeof $scope.changes['phase'] !== 'undefined' && $scope.changes['phase'] == 1) {
							$('#stay, #fold').removeAttr('disabled');
						}

						// if entering phase 2
						if (typeof $scope.changes['phase'] !== 'undefined' && $scope.changes['phase'] == 2) {
							$('#discard').attr('disabled', 'disabled');
						}

						// restart game
						if ($scope.changes[$scope.opponent + 'status'] == "restart") {
							$scope.dismissMessages();
							$scope.message('warning', $scope.user.spouse.first_name + " has restarted the game.", 5000);
							$timeout(function () {
								$route.reload();
							}, $scope.game.updateInterval);
						}

						// discard
						if ($scope.changes[$scope.opponent + 'status'] == "discard") {
							$scope.dismissMessages();
							if ($scope.game.amount > 1) {
								s = "s";
							} else {
								s = "";
							}
							$scope.message('warning', $scope.user.spouse.first_name + " discards " + $scope.game.amount + " card" + s + ".", 5000);
							if ($scope.game[$scope.player + 'ready']) {
								$scope.betting();
							}
						}

						// fold
						if ($scope.changes[$scope.opponent + 'status'] == "fold") {
							$('#options button').attr('disabled', 'disabled');
							popbox("<h2>" + $scope.user.spouse.first_name + " folds</h2><button class='btn btn-default popClose'>OK</button>");
							$(".popClose").click(function () {
								$scope.fold(true);
							});
						}

						// stay
						if ($scope.changes[$scope.opponent + 'status'] == "stay" || $scope.changes[$scope.opponent + 'status'] == "stayAgain") {
							$scope.dismissMessages();

							// phase 1 (discarding)
							if ($scope.game.phase == 1) {
								$scope.message('warning', $scope.user.spouse.first_name + ' stays.', 5000);
								if ($scope.game[$scope.player + 'ready']) {
									$scope.betting();
								}
							}

							// phase 2 (betting)
							if ($scope.game.phase == 2) {
								if ($scope.game.turn == $scope.user.gender && $scope.game[$scope.player + 'ready']) {
									$scope.message('warning', $scope.user.spouse.first_name + ' stays.', 5000);
									$scope.betting();
								} else if ($scope.game[$scope.player + 'ready']) {
									$("#raise, #call").attr("disabled", "disabled");
									if ($scope.game[$scope.player + 'ready']) {
										$scope.message('warning', $scope.user.spouse.first_name + " stays. Waiting on " + $scope.user.spouse.first_name + ' to raise or call.');
									}
								}
							}
						}

						// raise
						if ($scope.changes[$scope.opponent + 'status'] == "raise" || $scope.changes[$scope.opponent + 'status'] == "raiseAgain") {
							$scope.dismissMessages();
							$scope.game[$scope.opponent + 'strip_message'] = "";
							if ($scope.game[$scope.opponent + 'strip'] !== 0) {
								var s = "";
								if ($scope.game[$scope.opponent + 'strip'] !== 1) s = "s";
								$scope.game[$scope.opponent + 'strip_message'] = "<div class='alert alert-success'>And throws in " + $scope.game[$scope.opponent + 'strip'] + " article" + s + " of clothing.</div>";
							}
							$scope.determineClothingChanges();
							$scope.game[$scope.player + 'strip_message'] = "";
							var s = "";
							if ($scope.game[$scope.player + 'strip'] > 0) {
								if ($scope.game[$scope.player + 'strip'] !== 1) {
									s = "s";
								}
								$scope.game[$scope.player + 'strip_message'] = "<div class='alert alert-warning'>If you meet this amount, you must throw in " + $scope.game[$scope.player + 'strip'] + " article" + s + " of clothing.</div>";
							}
							popbox("<h2>" + $scope.user.spouse.first_name + " raises $" + $scope.game.amount + "</h2>" + $scope.game[$scope.opponent + 'strip_message'] + $scope.game[$scope.player + 'strip_message'] + "<div class='btn-group'><button class='btn btn-default popClose meet'>Meet $" + $scope.game.amount + "</button><button class='btn btn-default popClose fold'>Fold</button></div>");
							$(".meet").click(function () {
								$scope.user.money -= $scope.game.amount;
								$scope.game.pot += $scope.game.amount;
								$scope.game[$scope.player + 'status'] = "meet";
								$scope.game[$scope.player + 'ready'] = 2;
								$scope.betting();
							});
						}

						// meet
						if ($scope.changes[$scope.opponent + 'status'] == "meet") {
							$scope.game[$scope.opponent + 'strip_message'] = "";
							if ($scope.game[$scope.opponent + 'strip'] !== 0) {
								var s = "";
								if ($scope.game[$scope.opponent + 'strip'] !== 1) s = "s";
								$scope.game[$scope.opponent + 'strip_message'] = "<p class='alert alert-success'>And throws in " + $scope.game[$scope.opponent + 'strip'] + " article" + s + " of clothing.</p>";
							}
							popbox("<h2>" + $scope.user.spouse.first_name + " meets your $" + $scope.game.amount + "</h2>" + $scope.game[$scope.opponent + 'strip_message'] + "<button class='btn btn-default popClose meet'>OK</button>");
						}

						// call
						if ($scope.changes[$scope.opponent + 'status'] == "call") {
							if (!$scope.game[$scope.player + 'ready']) {
								popbox("<h2>" + $scope.user.spouse.first_name + " calls</h2><button class='btn btn-default popClose'>OK</button>");
								$(".popClose").click(function () {
									$scope.call();
								});
							}
						}

						// deal
						if ($scope.changes[$scope.opponent + 'status'] == "deal") {
							$scope.game.phase = 1;
							$scope.deal();
						}

						// deal when player is ready
						if ($scope.changes[$scope.opponent + 'status'] == "dealWhenReady") {
							$scope.game[$scope.player + 'status'] = 'deal';
							var promise = $interval(function () {
								if ($scope.game[$scope.player + 'ready']) {
									$scope.game[$scope.player + 'status'] = 'deal';
									$scope.message('success', "You've been dealt a new hand.", 5000);
									$interval.cancel(promise);
									$scope.game.phase = 1;
									$scope.deal(true);
								}
							}, 500);

							// destroy interval when leaving page
							$scope.$on("$destroy", function () {
								$interval.cancel(promise);
							});
						}

						// no ante
						if ($scope.changes.noAnte) {
							$scope.dismissMessages();
						}

						// reset money
						if ($scope.changes[$scope.opponent + 'status'] == "resetMoney") {
							$scope.dismissMessages();
							$scope.game.noAnte = false;
							$scope.message('warning', $scope.user.spouse.first_name + " reset the " + $scope.site.money_name_plural + " to $" + $scope.site.default_money + ".", 5000);
							$scope.game.phase = 1;
							$scope.deal(false);
						}

						// Timer
						if ($scope.changes[$scope.opponent + 'status'] == "Timer") {
							popbox("<h2>" + $scope.user.spouse.first_name + " has started the Timer</h2><p>Perform a striptease until you hear a bell.</p>");
						}

						// Timer done
						if ($scope.changes[$scope.opponent + 'status'] == "TimerDone") {
							console.log('390 - timer done!');
							$scope.audio('ding');
							closePopbox();
						}
					}
				});
				// }
				// $scope.olderGame = jQuery.extend(true, {}, $scope.oldGame);
				$scope.oldGame = jQuery.extend(true, {}, $scope.game);
				// $scope.changesOlder = jQuery.extend(true, {}, $scope.changesOld);
				$scope.changesOld = jQuery.extend(true, {}, $scope.changes);
			}, $scope.game.updateInterval);

			// destroy interval when leaving page
			$scope.$on("$destroy", function () {
				$interval.cancel($scope.updateInterval);
			});
		});

		/******************************************************
		* updates
		*******************************************************/

		$scope.addPlayerMoneyToObject = function (object) {
			if ($scope.user.gender == 'm') {
				object.m_money = $scope.user.money;
				object.f_money = $scope.user.spouse.money;
			} else {
				object.m_money = $scope.user.spouse.money;
				object.f_money = $scope.user.money;
			}
		}

		$scope.constructUpdates = function () {
			$scope.updates = {
				meta: {
					'spouse_updated_at': $scope.game[$scope.opponent + 'updated_at'],
				},
				data: {}
			};
		}

		/*******************************************************
		* Phase 2 - Betting
		*******************************************************/

		$scope.betting = function () {
			// update variables
			$scope.game.phase = 2;

			// disable buttons
			$("#fold, #stay, #raise, #call").attr("disabled", "disabled");

			// remove any discarded elements
			$('.discard').remove();

			// if both players can afford to bet
			if ($scope.user.money >= $scope.game.ante && $scope.user.spouse.money >= $scope.game.ante) {

				// if player has first bet
				if ($scope.game.turn == $scope.user.gender && $scope.game.firstBet) {
					$("#stay, #raise, #call").removeAttr("disabled");
					$timeout(function () { // this delay prevents this popup from rising above other sequential popups
						popbox("<h2>You start the betting</h2><button class='btn btn-default popClose'>OK</button>");
					}, 500);
					$scope.game[$scope.player + 'ready'] = false;

					// every other bet for player
				} else if ($scope.game.turn == $scope.user.gender && !$scope.game.firstBet) {
					$("#stay, #raise, #call").removeAttr("disabled");
					popbox("<h2>Your turn to bet</h2><button class='btn btn-default popClose'>OK</button>");
					$scope.game[$scope.player + 'ready'] = false;

					// if opponent has first bet
				} else if ($scope.game.turn == $scope.user.spouse.gender && $scope.game.firstBet) {
					$scope.message('warning', "Waiting on " + $scope.user.spouse.first_name + " to start the betting.");
					$scope.game[$scope.player + 'ready'] = true;

					// every other bet for opponent
				} else {
					if ($scope.user.money >= $scope.game.ante && $scope.user.money >= $scope.game.ante) {
						if ($scope.game.firstBet) {
							$scope.message('warning', "Waiting on " + $scope.user.spouse.first_name + " to raise or call.");
						}
						$scope.game[$scope.player + 'ready'] = true;
					}
				}

				// if one or both players can't afford to bet
			} else {
				$scope.call();
			}
		}

		/*************************
		* game actions
		**************************/

		// discard
		$scope.discard = function () {
			$('#stay').attr('disabled', 'disabled');
			$("#discard").attr("disabled", "disabled");
			if (!$scope.game[$scope.player + 'ready'] && $scope.game.phase == 1) {
				$scope.game[$scope.player + 'ready'] = 1;
				$scope.game.amount = 0;
				$("#userHand .cardContainer").each(function () {
					if ($(".discard", this).length > 0) {
						card = $("#userHand .cardContainer:last-child");
						$("#userHand .cardContainer:last-child").remove();
						$(this).after(card);
						$(this).remove();
						$scope.game.amount++;
						$timeout(function () {
							$("#userHand .card.new").addClass("flipped");
						}, 1);
					}
				});
				$scope.game[$scope.player + 'status'] = 'discard';
				$scope.game[$scope.player + 'hand'] = $('#userHand').html();
				if (!$scope.game[$scope.opponent + 'ready']) {
					$("#stay, #fold").attr("disabled", "disabled");
					$scope.message('warning', 'Waiting on ' + $scope.user.spouse.first_name + ' to discard or stay.', 5000);
				} else {
					$scope.betting();
				}
			}
		}

		// stay
		$scope.stay = function () {
			if ($scope.game.phase == 1) {
				$("#userHand .discard").remove();
				$scope.game[$scope.player + 'ready'] = 1;
			}
			if ($scope.game.phase == 2) {
				$scope.game.firstBet = false;
				$scope.game[$scope.player + 'ready'] = 3;
				$scope.game.turn = $scope.user.spouse.gender;
			}
			if ($scope.game[$scope.player + 'status'] !== 'stay') {
				$scope.game[$scope.player + 'status'] = 'stay';
			} else {
				$scope.game[$scope.player + 'status'] = 'stayAgain';
			}

			$("#discard, #stay, #raise, #call, #deal, #fold").attr("disabled", "disabled");
			if ($scope.game.phase == 1 && !$scope.game[$scope.opponent + 'ready']) {
				$scope.message('warning', 'Waiting on ' + $scope.user.spouse.first_name + ' to discard or stay.', 5000);
			} else if ($scope.game.phase == 1) {
				$scope.betting();
			}
		}

		// determine if player can afford to raise
		$scope.canAfford = function () {
			angular.forEach($scope.amounts, function (amount, index) {
				if ($scope.game.raiseTotal + amount.amount > $scope.user.money || $scope.game.raiseTotal + amount.amount > $scope.user.spouse.money) {
					$scope.amounts[index].raise = false;
				} else {
					$scope.amounts[index].raise = true;
				}
				if ($scope.game.raiseTotal - amount.amount < 0) {
					$scope.amounts[index].lower = false;
				} else {
					$scope.amounts[index].lower = true;
				}
			});
		}

		// raise
		$scope.raise = function () {
			$scope.togglePopup('#raisePopup');
			$scope.game.raiseTotal = 0;
			$scope.canAfford();
			$(".raise").each(function () {
				if (!(parseInt($(this).attr("data-raise")) >= $scope.user.money)) $(this).attr("disabled");
			});
		}

		// raise amount
		$scope.raiseAmountButton = function (amount) {
			$scope.game.raiseTotal += amount;
			$scope.determineClothingChanges();
			$scope.canAfford();
		}

		// apply raise amount
		$scope.confirmRaise = function () {
			$scope.togglePopup('#raisePopup');
			if ($scope.game.raiseTotal > 0) {
				// $scope.game[$scope.player + 'ready'] = 3;
				$scope.game.firstBet = false;
				$scope.game.amount = $scope.game.raiseTotal;
				$scope.game.pot += $scope.game.raiseTotal;
				$scope.user.money -= $scope.game.raiseTotal;
				$scope.game.turn = $scope.user.spouse.gender;
				if ($scope.game[$scope.player + 'status'] !== 'raise') {
					$scope.game[$scope.player + 'status'] = "raise";
				} else {
					$scope.game[$scope.player + 'status'] = "raiseAgain";
				}
				$("#stay, #raise, #call, #fold").attr("disabled", "disabled");
				$scope.message('warning', "Waiting on " + $scope.user.spouse.first_name + " to raise or call.", 5000);
			}
		}

		// fold
		$scope.fold = function (statusAlreadySet) {
			if (!statusAlreadySet) {
				$scope.game[$scope.player + 'status'] = "fold";
			}
			$scope.call(statusAlreadySet);
		}
		$('body').on('.fold', 'click', function () {
			$scope.fold();
		});

		// call
		$scope.call = function (statusAlreadySet) {
			$scope.dismissMessages();
			$scope.game.firstGame = false;
			$scope.game[$scope.player + 'ready'] = true;

			if (!statusAlreadySet) {
				if ($scope.game[$scope.player + 'status'] !== "fold") {
					$scope.game[$scope.player + 'status'] = "call";
				}
			}

			// disable buttons
			$("#fold, #raise, #call").attr("disabled", "disabled");

			// remove hidden cards (the extras drawn for when discarding)
			$("#userHand .cardContainer:nth-child(n+6)").remove();

			// get spouse hand
			$("#loadSpouseHand").html($scope.game[$scope.opponent + 'hand']);
			$("#loadSpouseHand figure").addClass("back");

			// insert spouse hand into page
			index = 1;
			$("#spouseHand .cardContainer").each(function () {
				var element = $(this);
				$(".card", element).prepend($("#loadSpouseHand .cardContainer:nth-of-type(" + index + ") .card").html());
				$("figure", element).addClass("front");
				index++;
			});

			// initialize comparison variables
			var userValues;
			var userPoints;
			var userHand;
			var userHighestSetValue;
			var spouseValues;
			var spousePoints;
			var spouseHand;
			var spouseHighestSetValue;

			// add up points of hands
			$(".hand").each(function () {
				var values = [];
				values[1] = 0;
				values[2] = 0;
				values[3] = 0;
				values[4] = 0;
				values[5] = 0;
				values[6] = 0;
				values[7] = 0;
				values[8] = 0;
				values[9] = 0;
				values[10] = 0;
				values[11] = 0;
				values[12] = 0;
				values[13] = 0;
				var cards = [];
				var suits = [];
				var x = 0;
				var points;
				var straightExists = false;
				var flushExists = false;
				var hand;
				var uniquePairs = 0;
				var largestMatch = 2;
				var highestSetValue = 0;
				$(".card .back", this).each(function () {

					// add to array
					cards.push(parseInt($(this).attr("data-value")));

					value = parseInt($(this).attr("data-value"));
					suit = parseInt($(this).attr("data-suit"));

					// determine matches
					values[value]++;
					if (values[value] >= 2) {
						if (values[value] == 2) {
							uniquePairs++;
						}
						if (values[value] >= 3) {
							largestMatch++;
						}
						if (values[value] > highestSetValue) {
							highestSetValue = values[value];
						}
						points += 8192;
					}

					x++;

					// add suits to list
					suits.push(suit);
				});

				// determine nothing
				if (uniquePairs == 0) {
					hand = "nothing";
					points = 0;
				}

				// determine 1 pair
				if (uniquePairs == 1) {
					hand = "1 pair";
					points = 2;
				}

				// determine 2 pairs
				if (uniquePairs == 2) {
					hand = "2 pairs";
					points = 3;
				}

				// determine 3 of a kind
				if (largestMatch == 3) {
					hand = "3 of a kind";
					points = 4;
				}

				// determine straights
				if (uniquePairs == 0) {
					cards.sort(function (a, b) {
						return a - b;
					});
					var straight = 1;
					for (x = 0; x <= 5; x++) {
						previousCard = x - 1;
						if (previousCard !== -1 && cards[x] == cards[previousCard] + 1) straight++;
					}
					if (straight == 5) {
						straightExists = true;
						hand = "a straight";
						points = 5;
					}
				}

				// determine flushes
				var startingSuit;
				var sameSuit = 1;
				for (x = 0; x <= suits.length; x++) {
					if (x == 0) startingSuit = suits[x];
					else if (suits[x] == startingSuit) sameSuit++;
				}
				if (sameSuit == suits.length) {
					flushExists = true;
					hand = "a flush";
					points = 6;
				}

				// determine full house
				if (uniquePairs == 2 && largestMatch == 3) {
					hand = "a full house";
					points = 7;
				}

				// determine 4 of a kind
				if (largestMatch == 4) {
					hand = "4 of a kind";
					points = 8;
				}

				// straight flush
				if (straightExists && flushExists == true) {
					hand = "a straight flush";
					points = 9;
				}

				// assign scores and hand descriptions to users
				cards.sort(function (a, b) {
					return b - a;
				});
				var id = $(this).attr("id");
				if (id == "userHand") {
					userCards = cards;
					userPoints = points;
					userHand = hand;
					userHighestSetValue = highestSetValue;
				}
				if (id == "spouseHand") {
					spouseCards = cards;
					spousePoints = points;
					spouseHand = hand;
					spouseHighestSetValue = highestSetValue;
				}

			});

			// determine winner
			var winner;
			var winnerMoney;
			if (!$scope.game.called) {
				$scope.game.potOld = angular.copy($scope.game.pot);
			}

			// settle ties
			if (userPoints == spousePoints) {
				for (x = 1; x <= 5; x++) {
					if (userCards[x] !== spouseCards[x]) {
						if (userCards[x] > spouseCards[x]) {
							winner = $scope.user.first_name;
							winnerMoney = $scope.user.money;
							if (userHand == "nothing" && spouseHand == "nothing") {
								userHand = "the high card";
								break;
							}
							if (userHand == spouseHand) {
								userHand += " with higher cards";
							}
						} else {
							winner = $scope.user.spouse.first_name;
							winnerMoney = $scope.game[$scope.player + 'money'];
							if (userHand == "nothing" && spouseHand == "nothing") {
								spouseHand = "the high card";
								break;
							}
							if (userHand == spouseHand) {
								spouseHand += " with higher cards";
							}
						}
					}
					if (winner !== undefined) {
						break;
					}
				}
			} else {
				if (userPoints > spousePoints) {
					winner = $scope.user.first_name;
				} else {
					winner = $scope.user.spouse.first_name;
				}
			}

			// reveal spouse's hand
			$("#spouseHand .cardContainer").each(function (e) {
				var element = $(this);
				$timeout(function () {
					$(".card", element).addClass("flipped");
				}, e * 1000);
			});

			// prevent updates during animation
			$timeout(function () {
				$scope.stopUpdates = false;
			}, $scope.game.updateInterval);

			// announce winner
			$timeout(function () {
				var person = winner;
				var s = "s";
				var exclamation = "";
				if (winner == $scope.user.first_name) {
					person = "You";
					s = "";
					exclamation = "!";
				} else {
					// $scope.game[$scope.opponent + 'strip'] = 0;
				}

				// if folding
				if ($scope.game[$scope.player + 'status'] == 'fold' || $scope.game[$scope.opponent + 'status'] == 'fold') {
					title = person + " would have won" + exclamation;
				}

				// if calling
				else {
					title = person + " win" + s + " $" + $scope.game.potOld + " " + $scope.site.money_name_plural + "" + exclamation;
				}

				// popbox
				popbox("<h2>" + title + "</h2><p>You have " + userHand + ".</p><p>" + $scope.user.spouse.first_name + " has " + spouseHand + ".</p><button id='acceptResult' class='btn btn-default popClose'>OK</button>");
				$("#acceptResult").click(function () {
					closePopbox();
					$("#discard, #stay, #raise, #call").attr("disabled", "disabled");
					if (!$scope.game[$scope.player + 'strip'] && !$scope.game[$scope.opponent + 'strip']) {
						if (winner == $scope.user.first_name) {
							$scope.loading = true;
							$scope.game[$scope.player + 'status'] = 'dealWhenReady';
							$scope.message('warning', "A new round will begin when " + $scope.user.spouse.first_name + " is ready.");
						} else {
							$scope.loading = true;
							$scope.message('warning', "Waiting on " + $scope.user.spouse.first_name + " to deal ...");
							$scope.game[$scope.player + 'ready'] = 1;
						}
					}
				});

				// update clothing
				$timeout(function () {
					if (
						(winner == $scope.user.first_name && $scope.game[$scope.opponent + 'strip'] > 0)
						|| (winner == $scope.user.spouse.first_name && $scope.game[$scope.player + 'strip'] > 0)
					) {
						$scope.applyClothingChanges(winner);
					}
				}, 1000);

				// transfer money
				if (userPoints > spousePoints) {
					$scope.user.money += $scope.game.pot;
				} else {
					$scope.user.spouse.money += $scope.game.pot;
				}
				$scope.game.pot = 0;

				$scope.stopUpdates = false;
			}, 5000);
			$scope.game.called = true;
		};

		/*************************
		* card functions
		**************************/

		// resize hands
		$scope.resizeHands = function () {
			var maxWidth = 835;
			if ($(window).width() < maxWidth) {
				width = $(window).width() / maxWidth;
				gameWidth = (maxWidth * width) * .9;
				marginBottom = (maxWidth - (width * maxWidth)) * .26;
				gameHeight = $scope.originalGameHeight - (marginBottom * 1.5);
				$("#userHandContainer").css("width", gameWidth + "px");
				$("#gameContent, #userHandContainer").css({
					"width": gameWidth + "px",
					//"height": gameHeight + "px"
				});
				$(".hand").css({
					"transform": "scale(" + width + ")",
					"transform-origin": "0 0",
					"-webkit-transform": "scale(" + width + ")",
					"-webkit-transform-origin": "0 0",
					"margin-bottom": "-" + marginBottom + "px"
				});
			}
		}

		// generate card
		$scope.generateCard = function () {
			x = 1 + Math.floor(Math.random() * 13);
			y = 1 + Math.floor(Math.random() * 4);
			col = (x * $scope.game.cardWidth) - $scope.game.cardWidth;
			row = (y * $scope.game.cardHeight) - $scope.game.cardHeight;

			// give "new" class to cards 5 - 10 (hidden cards used to replace cards that the user discards), to distinguish them from the other cards (so animation effects can be applied when they're added)
			$scope.newCard = "";
			if (z >= 6) {
				$scope.newCard = " new";
			}

			// prevent duplicate cards
			$scope.cardExists = false;
			if ($("#load").html() !== "") {
				$("#load figure.back").each(function () {
					if (parseInt($(this).attr("data-value")) == x && parseInt($(this).attr("data-suit")) == y) {
						$scope.cardExists = true;
					}
				});
			}
			if ($scope.cardExists == false) {
				if ($("#userHand .cardContainer").length > 0) {
					$("#userHand figure.back").each(function () {
						if (parseInt($(this).attr("data-value")) == x && parseInt($(this).attr("data-suit")) == y) {
							$scope.cardExists = true;
						}
					});
					if (!$scope.cardExists) {
						$scope.appendCard();
					} else {
						$scope.generateCard();
					}
				} else {
					$scope.appendCard();
				}
			} else {
				$scope.generateCard();
			}
		}

		// append card
		$scope.appendCard = function () {
			$("#userHand").append('<div class="cardContainer"><div class="card' + $scope.newCard + '"><figure class="front" style="background-position:-1950px -420px"></figure><figure class="back" data-value="' + x + '" data-suit="' + y + '" style="background-position:-' + col + 'px -' + row + 'px"></figure></div></div>');
		}

		// deal
		$scope.deal = function (dealer) {
			closePopbox();
			$scope.loading = false;
			$scope.game.pot = 0;
			$('#stay, #fold').removeAttr('disabled');

			if (!$scope.game.firsGame && !dealer && $scope.game.phase == 1) {
				$scope.message('success', "You've been dealt a new hand.", 5000);
			}

			// if joining new game
			if ($scope.game.phase == 0 && !dealer && $scope.user.money >= $scope.game.ante) {
				$scope.message('success', "You've joined the game.", 5000);;
			}

			// if both users can afford the ante
			if ($scope.user.money >= $scope.game.ante && $scope.user.money >= $scope.game.ante) {

				// ante up
				$scope.user.money -= $scope.game.ante;
				$scope.game.pot += $scope.game.ante;

				// reset round variables
				marked = 0;
				$scope.game.called = false;
				$scope.game[$scope.player + 'ready'] = 0;
				$scope.game[$scope.opponent + 'ready'] = 0;
				$scope.game[$scope.player + 'strip'] = 0;
				$scope.game[$scope.opponent + 'strip'] = 0;
				$("#waitingMessage").slideUp();
				var hand = "";

				for (z = 1; z <= 10; z++) {
					$scope.generateCard();
				}

				// flip cards
				$scope.flipCards();
				$scope.game[$scope.player + 'hand'] = $('#userHand').html();

				// create backsided cards for spouse's hand
				hand = "";
				for (x = 1; x <= 5; x++) {
					row = (2 * $scope.game.cardHeight);
					col = (13 * $scope.game.cardWidth);
					hand += '<div class="cardContainer"><div class="card"><figure style="background-position:-' + col + 'px -' + row + 'px"></figure></div></div>';
				}
				$("#spouseHand").html(hand);

				// disabled buttons
				if ($scope.game.phase == 2) {
					$("#raise, #call").attr("disabled", "disabled");
				}

			} else {
				$scope.game.noAnte = true;
				$scope.dismissMessages();
			}
		}

		// flip cards
		$scope.flipCards = function () {
			$scope.stopUpdates = true;
			$("#userHand .card").removeClass('flipped');
			$("#userHand .cardContainer:nth-child(-n+5)").each(function (e) {
				var element = $(this);
				$timeout(function () {
					$(".card", element).show().addClass("flipped");
				}, e * 250);
			});
			$timeout(function () {
				$scope.stopUpdates = false;
			}, 1250);
			$timeout(function () { // adding the front side is a hack to make the spin look right in IE
				$(".front").remove();
				$scope.game[$scope.player + 'hand'] = $('#userHand').html();
			}, 1250);
		}



		/************************
		* clothing functions
		*************************/

		// determine clothing changes
		$scope.determineClothingChanges = function () {
			$scope.userProjectedMoney = $scope.user.money - $scope.game.raiseTotal;
			$scope.game[$scope.player + 'strip'] = 0;
			for (x = $scope.game.articlesPerGame - 1; x >= 0; x--) {
				if ($scope.user.money > $scope.game.stripThreshold * x && ($scope.userProjectedMoney <= $scope.game.stripThreshold * x)) {
					$scope.game[$scope.player + 'strip']++;
				}
			}
			for (x = 0; x <= $scope.game.articlesPerGame - 1; x++) {
				if ($scope.user.money <= $scope.game.stripThreshold * x && ($scope.userProjectedMoney >= $scope.game.stripThreshold * x)) {
					$scope.game[$scope.player + 'strip']--;
				}
			}
			console.log('$scope.game.' + $scope.player + 'strip = ', $scope.game[$scope.player + 'strip']);
		};

		// apply clothing $scope.changes
		$scope.applyClothingChanges = function (winner) {
			if ($scope.game[$scope.opponent + 'strip'] !== 0 || $scope.game[$scope.player + 'strip'] !== 0) {
				var wins = "wins";
				if (winner == $scope.user.first_name) {
					loser = $scope.user.spouse.first_name;
					winner = "You";
					wins = "win";
				}
				else {
					winner = $scope.user.spouse.first_name;
					loser = "You";
				}
				var header = "";
				var dressMessage = "";
				var stripMessage = "";
				var s = "";
				var TimerOption = false;
				if (($scope.game[$scope.player + 'strip'] > 0 && $scope.game[$scope.opponent + 'strip'] < 0) || ($scope.game[$scope.player + 'strip'] < 0 && $scope.game[$scope.opponent + 'strip'] > 0)) {
					header = "Double Win for " + winner;
				} else if (winner == "You" && $scope.game[$scope.opponent + 'strip'] > 0 && $scope.game[$scope.player + 'strip'] >= 0) {
					header = "For Your Viewing Pleasure";
				} else if (loser == "You" && $scope.game[$scope.player + 'strip'] > 0 && $scope.game[$scope.opponent + 'strip'] >= 0) {
					header = "Time to Strip";
				} else if (!$scope.game[$scope.opponent + 'strip'] && $scope.game[$scope.player + 'strip'] < 0) {
					if (winner == "You") {
						header = "Lucky for You";
					} else {
						header = "How Disappointing";
					}
				}
				if ($scope.game[$scope.opponent + 'strip'] > 0 || $scope.game[$scope.player + 'strip'] > 0) {
					var clothesToStrip;
					if (loser == "You") {
						clothesToStrip = $scope.game[$scope.player + 'strip'];
					} else {
						clothesToStrip = $scope.game[$scope.opponent + 'strip'];
					}
					if (clothesToStrip !== 1) {
						s = "s";
					}
					stripMessage = "<p>" + loser + " must strip off " + clothesToStrip + " article" + s + " of clothing.</p>";
					TimerOption = true;
				}
				if ($scope.game[$scope.opponent + 'strip'] < 0 || $scope.game[$scope.player + 'strip'] < 0) {
					var clothesToDress;
					if (winner == user) {
						clothesToDress = $scope.game[$scope.player + 'strip'];
					}
					else {
						clothesToDress = $scope.game[$scope.opponent + 'strip'];
					}
					if (clothesToDress < -1) {
						s = "s";
					}
					dressMessage = "<p>" + winner + " " + wins + " back " + (clothesToDress * (-1)) + " article" + s + " of clothing.</p>";
				}
				if (stripMessage !== "" || dressMessage !== "") {
					if (TimerOption == false || loser == "You") {
						popbox("<h2>" + header + "</h2>" + stripMessage + dressMessage + "<button class='btn btn-default popClose'>OK</button>");
					} else {
						popbox("<h2>" + header + "</h2>" + stripMessage + dressMessage + "<h2 id='Timer'></h2><button class='popClose btn btn-default' id='done'>Done</button>&nbsp;<button id='useTimer' class='btn btn-primary'>Use Timer</button>");
					}
					var Timer;
					$("#useTimer").click(function () {
						$scope.game[$scope.player + "status"] = "Timer";
						$("#useTimer").remove();
						$("#Timer").show();
						var t = 60 * $scope.game[$scope.opponent + 'strip'];
						console.log('$scope.game["' + $scope.opponent + 'strip"] = ' + $scope.game[$scope.opponent + 'strip']);
						console.log('t = ', t);
						$scope.Timer = $interval(function () {
							t--;
							var minutes = parseInt(t / 60);
							var seconds = t % 60;
							if (seconds < 10) {
								seconds = "0" + seconds;
							}
							$scope.game[$scope.opponent + 'time'] = minutes + ":" + seconds;
							$("#Timer").html($scope.game[$scope.opponent + 'time']);
							if (t == 0) {
								$scope.timerDone();
							}
						}, 1000);

						// destroy interval when leaving page
						$scope.$on("$destroy", function () {
							$interval.cancel($scope.Timer);
						});
					});
					$("#done").click(function () {
						$scope.timerDone();
					});
				}
			}
		}

		// timer done
		$scope.timerDone = function () {
			$interval.cancel($scope.Timer);
			$scope.game[$scope.player + "status"] = "deal";
			$scope.message('success', "You've been dealt a new hand.", 5000);
			$scope.audio('ding');
			closePopbox();
			$scope.deal(true);
		}

		/*************************
		* game menu options
		**************************/

		// new game
		$scope.newGame = function () {
			$scope.game[$scope.player + 'status'] = 'restart';
			$scope.togglePopup('#optionsPopup');
			$scope.loading = true;
			$timeout(function () {
				$http.delete('/poker').then(function () {
					$route.reload();
				});
			}, $scope.game.updateInterval * 2);

			// if restart doesn't work due to problems syncing with spouse
			// $timeout(function() {
			// 	$scope.message('danger', 'Trouble syncing with ' + $scope.user.spouse.first_name + '. Please try again or have ' + $scope.user.spouse.first_name + ' restart instead.');
			// }, $scope.game.updateInterval * 4);

		}

		// reset money
		$scope.resetMoney = function () {
			$scope.hidePopups();
			$scope.user.money = $scope.site.default_money;
			$scope.user.spouse.money = $scope.site.default_money;
			$scope.game.pot = 0;
			$scope.game[$scope.player + 'status'] = "resetMoney";
			$scope.game.noAnte = false;
			$scope.message('success', "You've reset the " + $scope.site.money_name_plural + " to $" + $scope.site.default_money, 5000);;
			$scope.game.phase = 1;
			$scope.deal(true);
		}
	});
