angular.module('app')
.controller('Verify', function($scope, $http, $location) {

    // define variables
    $scope.resending = false;
    $scope.resent = false;

    if ($scope.user == undefined) {
        $scope.getVars();
    }

    if ($scope.user.verified) {
        $scope.loading = true;
        $location.path('/');
    }

    // send verification email again
    $scope.sendAgain = function()
    {
        $scope.resending = true;
        $http.get('register/send-again').then(function(response) {
            console.log('response.data = ', response.data);
            if (response.data.errors) {
                $scope.errors = response.data.errors;
            } else {
                $scope.resending = false;
                $scope.resent = true;
            }
        });
    }
});
