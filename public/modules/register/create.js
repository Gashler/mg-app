angular.module('app')
    .directive('mgRegister', function () {
        return {
            scope: {
                email: '='
            },
            templateUrl: '/modules/register/create.html',
            controller: function ($scope, $http, $location, vars) {
                vars.async().then(function (result) {
                    angular.forEach(result, function (value, key) {
                        $scope[key] = value;
                    });
                });

                // define variables
                $scope.registering = true;
                $scope.loading = true;
                $scope.noEmail = true;
                $scope.showEmailField = false;
                $scope.step = 3;
                $scope.displayStep = 1;
                $scope.displaySteps = 3;
                $scope.form = {
                    users: {
                        Husband: { first_name: null },
                        Wife: { first_name: null }
                    }
                };

                $http.get('/register/data').then(function (response) {
                    $scope.loading = false;
                    $scope.data = response.data;
                    if ($scope.data.oauth) {
                        $scope.data.email = $scope.data.oauth.email;
                    } else if ($scope.email) {
                        $scope.data.email = $scope.email;
                    }
                    if ($scope.data.email) {
                        $scope.noEmail = false;
                    }
                    angular.forEach($scope.data.users, function (person, key) {
                        if ($scope.form.users[key].hasOwnProperty('first_name')) {
                            $scope.form.users[key].first_name = person.first_name;
                        }
                    });
                });

                // get plans
                $scope.getPlans = function () {
                    $scope.loading = true;
                    $http.get('/subscription-plans/').then(function (response) {
                        $scope.loading = false;
                        $scope.plans = response.data;
                        if (!$scope.plans[0]) {
                            $scope.plan = $scope.plans;
                        } else {
                            $scope.plan = $scope.plans[0];
                        }
                    });
                }
                $scope.getPlans();

                $scope.continue = function () {
                    if ($scope.form.users['Husband'].first_name !== '' && $scope.form.users['Husband'].last_name !== '') {
                        if (!$scope.form.users['Husband'].email && !$scope.form.users['Wife'].email) {
                            $scope.step = 4;
                        } else {
                            $scope.start('free');
                        }
                    }
                }

                $scope.showPlans = function () {
                    $scope.step = 2;
                }

                $scope.selectPlan = function (plan) {
                    $scope.plan = plan;
                    $scope.displayStep = 2;
                    if ($scope.form.users['Husband'].first_name && $scope.form.users['Wife'].first_name) {
                        $scope.step = 4;
                    } else {
                        $scope.step = 3;
                    }
                }

                $scope.assignEmail = function (registrant_key) {
                    $scope.form.registrant_key = registrant_key;
                    $scope.form.users[registrant_key].email = $scope.data.email;
                    angular.forEach($scope.form.users, function (person, person_key) {
                        if (registrant_key !== person_key) {
                            $scope.spouse_key = person_key;
                        }
                    });
                    if (!$scope.data.oauth) {
                        $scope.step = 5;
                    } else {
                        $scope.step = 6;
                    }
                }

                $scope.addSpouse = function () {
                    $scope.step = 6;
                }

                $scope.proceedToPayment = function () {
                    $scope.step = 9;
                }

                $scope.submit = function () {
                    $scope.loading = true;
                    $http.post('Register', $scope.form).then(function (response) {
                        $scope.loading = false;
                        console.log('response = ', response);
                        if (!response.data.errors) {
                            $scope.step = 10;
                            $scope.displayStep = 3;
                        } else {
                            $scope.errors = response.data.errors;
                            $scope.step = 7;
                        }
                        console.log('$scope.step = ', $scope.step);
                    });
                }

                $scope.checkInbox = function () {
                    $scope.loading = false;
                }

                $scope.finish = function () {
                    console.log('click');
                    window.location = '/';
                }
            }
        }
    });
