angular.module('app')
    .controller('Password', function ($scope, $http, $location, $routeParams) {
        var user = $scope.user;

        if ($scope.user == undefined) {
            $scope.getVars();
        }
        if ($routeParams.spouse === 'spouse') {
            user = $scope.user.spouse;
        }

        // define variables
        $scope.loading = false;
        $scope.password = '';
        $scope.password_confirm = '';

        $scope.updatePassword = function () {
            $scope.loading = true;
            $http.patch('/Base', {
                meta: {
                    model: 'User',
                    id: $scope.user.id,
                    return: true
                },
                value: {
                    password: $scope.password,
                    need_password: false
                }
            }).then(function (response) {
                $scope.loading = false;
                if (!response.data.errors) {
                    $scope.message('success', 'Your password has been updated.', 5000);
                    $scope.user = response.data.unit;
                    // $location.url('/');
                    $scope.loading = true;
                    window.location = '/#/account';
                } else {
                    $scope.message('danger', response.data.errors);
                }
            });
        }
    });
