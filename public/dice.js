/*
Copyright (C) 2000 Free Software Foundation, Inc. See LICENSE.txt
*/var firstSpin = 0;
var direction;
jQuery(document).ready(function() {
	jQuery("#rollDice").click(function() {
		jQuery("#rollDice").attr("disabled", "disabled");
		jQuery(".wrap").each(function() {
			roll(jQuery(this).attr("id"), 1);
		});
		setTimeout(function() {
			jQuery("#rollDice").removeAttr("disabled");
		}, 1000);
	});
	function roll(id, seconds) {
		if (firstSpin < 2) {
			direction = 0;
			firstSpin ++;
		}
		else direction = Math.floor(Math.random() * 2);
		var toDegrees;
		function generateToDegrees() {
			toDegrees = (Math.floor(Math.random() * 4)) * 90;
			if (direction == 0) {
				if (toDegrees == jQuery(".cube", "#" + id).attr("data-y")) {
					generateToDegrees();
				}
			}
			if (direction == 1) {
				if (toDegrees == jQuery(".cube", "#" + id).attr("data-x")) {
					generateToDegrees();
				}
			}
		}
		generateToDegrees();
		if (direction == 0) { // horizontal spin
			jQuery(".cube", "#" + id).css({
				"margin": "0 auto",
				"position": "relative",
				"width": "200px",
				"transform-style": "preserve-3d",
				"-webkit-transform-style": "preserve-3d"
			});
			jQuery(".cube-back", "#" + id).css({
				"transform": "translateZ(-100px) rotateY(180deg)",
				"-webkit-transform": "translateZ(-100px) rotateY(180deg)"
			});
			jQuery(".cube-top", "#" + id).css({
				"transform": "rotateX(-90deg) translateY(-100px)",
				"-webkit-transform": "rotateX(-90deg) translateY(-100px)",
				"transform-origin": "top center",
				"-webkit-transform-origin": "top center"
			});
			jQuery(".cube-bottom", "#" + id).css({
				"transform": "rotateX(90deg) translateY(100px)",
				"transform-origin": "bottom center",
				"-webkit-transform": "rotateX(90deg) translateY(100px)",
				"-webkit-transform-origin": "bottom center"
			});
			var fromDegrees = jQuery(".cube", "#" + id).css("transform");
			if (fromDegrees == undefined) fromDegrees = jQuery(".cube", "#" + id).css("-webkit-transform");
			if (fromDegrees == undefined) fromDegrees = 0;
			jQuery(".animation." + id).html("" +
				"@keyframes spin {" +
					"from { transform: rotateY(" + fromDegrees + "); }" +
					"to { transform: rotateY(" + toDegrees + "deg); }" +
				"}" +
				"@-webkit-keyframes spin {" +
					"from { -webkit-transform: rotateY(" + fromDegrees + "); }" +
					"to { -webkit-transform: rotateY(" + toDegrees + "deg); }" +
				"}" +
				"#" + id + " .cube { animation: spin " + seconds + "s forwards; -webkit-animation: spin " + seconds + "s forwards; }" +
			"");
			setTimeout(function() {
				jQuery(".cube", "#" + id).css({
					"transform": "rotateY(" + toDegrees + "deg)",
					"-webkit-transform": "rotateY(" + toDegrees + "deg)"
				});				jQuery(".cube", "#" + id).attr("data-y", toDegrees);
				var cube = jQuery(".wrap#" + id).html();
				var transform = jQuery(".cube", "#" + id).css("transform");
				if (transform == undefined) transform = jQuery(".cube", "#" + id).css("-webkit-transform");
				jQuery(".wrap#" + id).html("");
				jQuery(".animation." + id).html("");
				jQuery(".wrap#" + id).html(cube);
				jQuery(".wrap#" + id + " .cube").css({
					"transform": transform,
					"-webkit-transform": transform
				});
			}, 1000);
		} else { // vertical spin
			jQuery(".cube", "#" + id).css({
				"margin": "0 auto",
				"transform-origin": "0 100px"
			});
			jQuery(".cube-top", "#" + id).css({
				"transform": "rotateX(-270deg) translateY(-100px)",
				"-webkit-transform": "rotateX(-270deg) translateY(-100px)"
			});
			jQuery(".cube-back", "#" + id).css({
				"transform": "translateZ(-100px) rotateX(180deg)",
				"-webkit-transform": "translateZ(-100px) rotateX(180deg)"
			});
			jQuery(".cube-bottom", "#" + id).css({
				"transform": "rotateX(-90deg) translateY(100px)",
				"-webkit-transform": "rotateX(-90deg) translateY(100px)"
			});
			var fromDegrees = jQuery(".cube", "#" + id).css("transform");
			if (fromDegrees == undefined) fromDegrees = jQuery(".cube", "#" + id).css("-webkit-transform");
			if (fromDegrees == undefined) fromDegrees = 0;
			jQuery(".animation." + id).html("" +
				"@keyframes spin-vertical {" +
					"from { transform: rotateX(" + fromDegrees + "); }" +
					"to { transform: rotateX(" + toDegrees + "deg); }" +
				"}" +
				"@-webkit-keyframes spin-vertical {" +
					"from { -webkit-transform: rotateX(" + fromDegrees + "); }" +
					"to { -webkit-transform: rotateX(" + toDegrees + "deg); }" +
				"}" +
				"#" + id + " .cube {animation: spin-vertical " + seconds + "s forwards; -webkit-animation: spin-vertical " + seconds + "s forwards; }" +
			"");
			setTimeout(function() {
				jQuery(".cube", "#" + id).css({
					"transform": "rotateX(" + toDegrees + "deg)",
					"-webkit-transform": "rotateX(" + toDegrees + "deg)"
				});
				jQuery(".cube", "#" + id).attr("data-x", toDegrees);
				var cube = jQuery(".wrap#" + id).html();
				var transform = jQuery(".cube", "#" + id).css("transform");
				if (transform == undefined) transform = jQuery(".cube", "#" + id).css("-webkit-transform");
				jQuery(".wrap#" + id).html("");
				jQuery(".animation." + id).html("");
				jQuery(".wrap#" + id).html(cube);
				jQuery(".wrap#" + id + " .cube").css({
					"transform": transform,
					"-webkit-transform": transform
				});
			}, 1000);
		}
	}
	jQuery(".wrap").each(function() {
		jQuery(".wrap").each(function() {
			roll(jQuery(this).attr("id"), 1);
		});
	});
	jQuery(".wrap").each(function() {
		jQuery(".wrap").each(function() {
			roll(jQuery(this).attr("id"), 1);
		});
	});
	jQuery(".wrap").each(function() {
		jQuery(".wrap").each(function() {
			roll(jQuery(this).attr("id"), 1);
		});
	});
	setTimeout(function() {
		jQuery(".wrap").each(function() {
			roll(jQuery(this).attr("id"), 1);
		});	
	}, 1000);
});
