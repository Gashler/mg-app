/****************************
* Common Functions
*****************************/

// disable enter key submit for certain forms
function disableEnterKey(e) {
    var key;
    if (window.event)
        key = window.event.keyCode;
    //IE
    else
        key = e.which;
    //firefox
    return (key != 13);
}

// capitalize first letter of string
function ucfirst(string) {
    if (typeof string === 'string') {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
}

// lower the case of the first letter of string
function lcfirst(string) {
    if (typeof string === 'string') {
        return string.charAt(0).toLowerCase() + string.slice(1);
    }
}

// in array
function inArray(needle, haystack) {
    var length = haystack.length;
    for (var i = 0; i < length; i++) {
        if (haystack[i] == needle) return true;
    }
    return false;
}

// initialize parsley
window.ParsleyConfig = {
    errorsWrapper: '<div></div>',
    errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>',
    errorClass: 'has-error',
    successClass: 'has-success'
};

// upload media
function uploadMedia(model, id, tags) {
    var form_data = new FormData();

    // prepare attachment data
    form_data.append('model', model);
    form_data.append('id', id);

    // prepare tags
    $(tags).each(function (key, tag) {
        form_data.append('tags[][name]', tag.name);
    });

    // prepare files
    files = $('#files')[0].files;
    $(files).each(function (key, file) {
        form_data.append('files[]', file);
    });

    // send data
    $.ajax({
        type: "post",
        url: "/media",
        data: form_data,
        cache: false,
        contentType: false,
        processData: false,
        success: function (media) {
            console.log(media);
            return media;
        },
        error: function (data) {
            console.log("error");
            console.log(data);
        }
    });
}

$(document).ready(function () {

    // initialize masonry
    // $('.grid').masonry({
    //     // options
    //     itemSelector: '.grid-item',
    //     columnWidth: 500
    // });

    // initialize bootstrap-select plugin
    $('.selectpicker').selectpicker();

    // check the checkboxes of all rows in a table and enable/disable action button
    $('body').on('click', "thead input[type='checkbox']", function () {
        if ($(this).prop("checked") == false) {
            $("tbody td:first-child input[type='checkbox']").each(function () {
                $(this).prop("checked", false);
            });
            $('.applyAction').attr('disabled', 'disabled');
        }
        else {
            $("tbody td:first-child input[type='checkbox']").each(function () {
                $(this).prop("checked", true);
            });
            $('.applyAction').removeAttr('disabled');
        }
    });
    $('body').on('click', ".bulk-check", function () {
        var checked = false;
        $(".bulk-check").each(function () {
            if ($(this).prop("checked") == true) checked = true;
        });
        if (checked == false) {
            $('.applyAction').attr('disabled', 'disabled');
        }
        else {
            $('.applyAction').removeAttr('disabled');
        }
    });

    // change method of index form
    $('select.actions').change(function () {
        $('form').attr('action', $(this).val());
    });

    // jQUery UI
    var today = new Date();
    var firstYear = today.getFullYear() - 18;
    $('.datepicker').datetimepicker({
        controlType: 'select',
        timeFormat: 'hh:mm tt'
    });
    $('.dateonlypicker').datepicker({
        controlType: 'select',
        changeMonth: true,
        changeYear: true,
        yearRange: '1900:' + firstYear,
        dateFormat: 'yy-mm-dd',
        //timeFormat: 'hh:mm tt'
    });

    // toggle main-menu items (main-menu-meta)
    $('body').on('click', '#main-menu-meta .list-group-item', function () {
        if ($(this).attr('data-category') == 'dashboard') {
            $(this).addClass('main-menu-header');
        } else if ($(this).attr('data-category') !== '') {
            $(this).blur();
            category = $(this).attr('data-category');
            $('#main-menu-meta .list-group-item[data-category!="' + category + '"], #main-menu-mobile-user').fadeOut(500);

            // get distance from top of screen
            distance = $(this).offset().top - ($('#header-menu').height() + $('#main-menu-back').height());

            $(this).addClass('main-menu-header').css('position', 'relative').animate({ 'bottom': distance + 'px' }, 500, function () {
                $(this).removeAttr('style');
                $('#main-menu-headers div[data-category="' + category + '"]').fadeIn();
                $('#main-menu .list-group-item').hide();
                $('#main-menu .list-group-item[data-category="' + category + '"]').fadeIn();
                top = $('#main-menu-meta > .main-menu-header').offset().top + $('#main-menu-meta > .main-menu-header').height();
                $('#main-menu').css({
                    'position': 'absolute',
                    'top': top,
                }).fadeIn();
                $('#main-menu-back').fadeIn();
            });
        }
    });

    // main menu back button
    $('body').on('click', '#main-menu-back', function () {
        $(this).fadeOut(250);
        $('#main-menu-meta, #main-menu').fadeOut(250, function () {
            if ($('#main-menu-meta .list-group-item.main-menu-header').css('display') == 'none') {
                $('#main-menu-meta .list-group-item.main-menu-header').fadeOut(250, function () {
                    $(this).removeClass('main-menu-header').removeAttr('style');
                    $('#main-menu-meta, #main-menu-meta .list-group-item, #main-menu-mobile-user').hide().fadeIn(250);
                });
            }
            else {
                $('.main-menu-header').removeClass('main-menu-header').css('position', 'static');
                $('#main-menu a, .main-menu-header').fadeOut(250, function () {
                    $('#main-menu-meta, #main-menu-meta .list-group-item, #main-menu-mobile-user').hide().fadeIn(250);
                });
            }
        });
    });

    /***********************************
     * activate submenu for current page
     ***********************************/
    path = path.split('/');
    menu_hidden = false;
    category_exists = false;

    // if page has category in path
    $('#main-menu a').each(function () {
        if (!category_exists) {
            if ($(this).attr('data-category') == path[0]) {
                category_exists = true;
                if (menu_hidden == false) {
                    $('#main-menu-meta > a, #main-menu > a').hide();
                    $('#main-menu-back').show();
                    $('#main-menu').show();
                    menu_hidden = true;
                }
                $(this).css('display', 'block');
            }
        }
    });

    // if menu item can be found for this page
    if (!category_exists) {
        menu_item_exists = false;
        $('#main-menu a').each(function () {
            if (!menu_item_exists) {
                if ($(this).attr('href') == '/' + path[0] || $(this).attr('data-href') == '/' + path[0]) {
                    menu_item_exists = true;
                    category = $(this).attr('data-category');
                    showCategory(category);
                }
            }
        });
    }
    $('#main-menu-meta a[data-category="' + path[0] + '"]').addClass('main-menu-header').show();

    // show category
    function showCategory(category) {
        if (menu_hidden == false) {
            $('#main-menu-meta > a, #main-menu > a').hide();
            $('#main-menu-back').show();
            $('#main-menu').show();
            menu_hidden = true;
        }
        $('#main-menu a').each(function () {
            if ($(this).attr('data-category') == category) {
                $(this).css('display', 'block');
            }
        });
    }

    // toggle main menu container (mobile displays)
    var menu_open = false;
    $('body').on('click', '#main-menu-toggle', function () {
        if (!menu_open) {
            $('#black-filter').fadeIn();
            $('#sidebar').css('opacity', '1 !important').fadeIn();
            menu_open = true;
        }
        else {
            hideMainMenu();
        }
    });

    // hide main menu
    function hideMainMenu() {
        $('#black-filter').fadeOut();
        $('#sidebar').fadeOut();
        menu_open = false;
    }

    // adjust main menu for window size
    function adjustMainMenuForDisplay() {
        if ($(window).width() > 991) {
            $('#black-filter').hide();
            $('#sidebar').css('opacity', '1 !important').show();
            $('#main-menu > a[data-toggle="popover"]').attr('data-placement', 'right');
        }
        else {
            menu_open = false;
            $('#sidebar').hide();
            $('#main-menu > a[data-toggle="popover"]').attr('data-placement', 'left');
        }
    }
    adjustMainMenuForDisplay();

    // adjust menu on window resize
    $(window).resize(function () {
        adjustMainMenuForDisplay();
    });

    // hide main menu when clicking on certain areas
    $('body').on('click', '#black-filter', function () {
        hideMainMenu();
    });
    $('body').on('click', '#main-menu a, #main-menu-mobile-user a, #dashboard, .popover-content', function () {
        if ($(window).width() < 1009 && $(this).attr('href') !== 'javascript:void(0)') {
            hideMainMenu();
        }
    });

    // initialize bootstrap popovers
    $("[data-toggle='popover']").popover({ html: true, trigger: 'click' });

    // delete label
    $('body').on('click', '.form-group .label .fa-times', function () {
        $(this).parent().parent().remove();
    });

    // initialize tinymce
    tinymce.init({
        selector: ".wysiwyg",
        theme: "modern",
        relative_urls: false,
        menubar: false,
        // statusbar: false,
        setup: function (ed) {
            ed.on('init', function () {
                this.getDoc().body.style.fontSize = '14px';
                this.getDoc().body.style.color = 'rgb(85, 85, 85)';
            });
        },
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table directionality",
            "template paste textcolor colorpicker textpattern"
        ],
        toolbar1: "insertfile styleselect | bullist numlist outdent indent | image media fullscreen",
    });

    tinymce.init({
        selector: ".wysiwyg-advanced",
        theme: "modern",
        relative_urls: false,
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "template paste textcolor colorpicker textpattern"
        ],
        style_formats: [
            { title: 'His Character', block: 'div', classes: 'character him' },
            { title: 'Her Character', block: 'div', classes: 'character her' },
            { title: 'His Dialog', block: 'p', classes: 'dialog him' },
            { title: 'Her Dialog', block: 'p', classes: 'dialog her' },
            { title: 'His Action', block: 'p', classes: 'action him' },
            { title: 'Her Action', block: 'p', classes: 'action her' },
            { title: 'Generic Action', block: 'p', classes: 'action' },
            { title: 'Scene Header', block: 'h2', classes: 'scene-header' },
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic forecolor backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
        image_advtab: true,
        templates: [
            { title: 'Test template 1', content: 'Test 1' },
            { title: 'Test template 2', content: 'Test 2' }
        ]
    });

    // add buttons to tinymce editor for inserting images
    function addMediaButtons() {
        $('.mce-combobox.mce-last.mce-abs-layout-item').before('' +
            '<div id="uploadImage" style="border-top-right-radius:0 !important; border-bottom-right-radius:0 !important; border-top-left-radius:4px !important; border-bottom-left-radius:4px !important;" title="Upload Image" role="button" class="mce-btn">' +
            '<button data-toggle="modal" data-target="#imageUpload" style="border-top-right-radius:0 !important; border-bottom-right-radius:0 !important; border-top-left-radius:4px !important; border-bottom-left-radius:4px !important;" role="presentation" type="button" tabindex="-1">' +
            '<img style="height:16px; width:16px;" src="/img/upload.svg">' +
            '</button>' +
            '</div>' +
            '<div data-toggle="modal" data-target="#mediaLibrary" style="border-top-right-radius:0 !important; border-bottom-right-radius:0 !important;" title="Media Library" role="button" class="mce-btn">' +
            '<button style="border-top-right-radius:0 !important; border-bottom-right-radius:0 !important;" role="presentation" type="button" tabindex="-1">' +
            '<img style="height:16px; width:16px;" src="/img/tiles.svg">' +
            '</button>' +
            '</div>' +
            '');
        $('input#mceu_44-inp').addClass("mceu_44-inp-hack");
        addedMediaButtons = true;
    }
    $('body').on('click', '#mceu_16', function () {
        addMediaButtons();
    });

    // load media modals if WYISYG editor exists
    // if ($('.wysiwyg').length > 0) {
    // if ($('#modals').html() == '') $('#modals').load('/helpers/media-modals');
    // }

    // close sidebar menu popovers when clicking outside
    $('[data-toggle="popover"]').popover();
    $('body').on('click', function (e) {
        $('[data-toggle="popover"]').each(function () {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });

    // add tag

    $(window).keydown(function (event) {
        if ($('#tagger').is(':focus')) {
            if (event.keyCode == 13 /* enter */ || event.keyCode == 9 /* tab */ || event.keyCode == 188 /* comma */) {
                event.preventDefault();
                addTag();
            }
        }
    });
    $('body').on('click', '#addTag', function () {
        addTag();
    });
    function addTag() {
        if ($('#tagger').val() != '') {
            var tag = $('#tagger').val();
            $('.tag-list').append('' +
                '<span class="label label-default">' +
                tag + '&nbsp;' +
                '<i class="flaticon-x simpleRemoveTag"></i>' +
                '<input type="hidden" name="tag_names[]" value="' + tag + '">' +
                '</span>' +
                '');
            $('#tagger').val('');
        }
    };

    // simple remove tag
    $('body').on('click', '.simpleRemoveTag', function () {
        $(this).parent().remove();
    });

    // remove tag
    $('body').on('click', '.removeTag', function () {
        var id = $(this).attr('data-tag-id');
        $('[data-tag-id="' + id + '"]').remove();
        $.ajax({
            type: "POST",
            url: "/service-tags/" + id,
            data: { '_method': 'delete' }
        });
    });

});

// clean URL
function cleanURL(text) {
    text = text.toLowerCase();
    text = text.toLowerCase();
    text = text.replace(/ a /g, "-");
    text = text.replace(/ an /g, "-");
    text = text.replace(/ it /g, "-");
    text = text.replace(/ the /g, "-");
    text = text.replace(/\ and /g, "-");
    text = text.replace(/\ /g, "-");
    text = text.replace(/\,/g, "-");
    text = text.replace(/\./g, "-");
    text = text.replace(/\&/g, "-");
    text = text.replace(/\?/g, "-");
    text = text.replace(/\!/g, "-");
    text = text.replace(/\@/g, "-");
    text = text.replace(/\#/g, "-");
    text = text.replace(/\$/g, "-");
    text = text.replace(/\%/g, "-");
    text = text.replace(/\^/g, "-");
    text = text.replace(/\*/g, "-");
    text = text.replace(/\(/g, "-");
    text = text.replace(/\)/g, "-");
    text = text.replace(/\+/g, "-");
    text = text.replace(/\=/g, "-");
    text = text.replace(/\~/g, "-");
    text = text.replace(/\`/g, "-");
    text = text.replace(/\:/g, "-");
    text = text.replace(/\;/g, "-");
    text = text.replace(/\'/g, "-");
    text = text.replace(/\"/g, "-");
    text = text.replace(/\[/g, "-");
    text = text.replace(/\{/g, "-");
    text = text.replace(/\]/g, "-");
    text = text.replace(/\}/g, "-");
    text = text.replace(/\\/g, "-");
    text = text.replace(/\|/g, "-");
    text = text.replace(/\</g, "-");
    text = text.replace(/\>/g, "-");
    text = text.replace(/\--/g, "");
    text = text.replace(/\__/g, "");
    text = text.replace(/\_-/g, "");
    text = text.replace(/\-_/g, "");
    return cleaned_text = text;
}


// popbox
function popbox(content) {
    var dark;
    if (jQuery(".dark").length == 0) dark = "<div class='dark'></div>";
    else dark = "";
    var popbox = dark + "<div class='popboxWrapperWrapper'><div class='popboxWrapper'><div class='popboxContainer'><div class='popbox'>" + content + "</div></div></div></div>";
    if (jQuery("popboxWrapperWrapper").length > 0) jQuery(".popboxWrapperWrapper").after(popbox);
    else jQuery("body").prepend(popbox);
    jQuery(".dark, .popboxContainer").fadeIn(function () {
        jQuery('body').on('click', ".popClose", function () {
            closePopbox();
        });
        jQuery('body').on('click', ".dark", function () {
            if (!(jQuery(this).hasClass("static"))) closePopbox();
        });
    });
}
function closePopbox() {
    if (jQuery(".popboxWrapperWrapper").length == 1) {
        jQuery(".dark").fadeOut(function () {
            jQuery(this).remove();
        });
    }
    if (jQuery(".popboxWrapperWrapper").length > 1) {
        jQuery(".popboxWrapperWrapper:last").fadeOut(function () {
            jQuery(this).remove();
        });
        if (jQuery(".popboxWrapperWrapper").length == 1) jQuery(".dark").fadeOut(function () {
            jQuery(this).remove();
        });
    } else {
        jQuery(".popboxWrapperWrapper").fadeOut(function () {
            jQuery(this).remove();
        });
    }
}
function closePopboxes() {
    jQuery(".dark").fadeOut(function () {
        jQuery(this).remove();
    });
}
