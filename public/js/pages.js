// register angular app module
var app = angular.module('app', [])

// get constants
.factory('vars', function($http) {
    var promise;
    var vars = {
        async: function() {
            if (!promise) {
                promise = $http.get('/constants').then(function (response) {
                    return response.data;
                });
            }
            return promise;
        }
    };
    return vars;
})
