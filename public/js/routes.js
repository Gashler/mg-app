// routes
angular.module('app').config(function ($routeProvider) {
    $routeProvider.

        // main
        when('/', { templateUrl: '/modules/dashboard/index.html' }).
        when('/switch-user', { templateUrl: '/modules/dashboard/switch_user.html', controller: 'SwitchUser' }).

        // registration
        when('/registration/verify', { templateUrl: '/modules/register/verify.html', controller: 'Verify' }).

        // account
        when('/account', { templateUrl: '/modules/account/show.html' }).
        when('/password', { templateUrl: '/modules/password/edit.html', controller: 'Password' }).
        when('/password/:spouse', { templateUrl: '/modules/password/edit.html', controller: 'Password' }).
        when('/profile/:person/edit', { templateUrl: '/modules/profile/edit.html', controller: 'ProfileEdit' }).

        // posts
        when('/posts/:category_id?', { templateUrl: '/modules/posts/index.html', controller: 'PostIndexController' }).
        when('/posts/show/:id', { templateUrl: '/modules/posts/show.html', controller: 'PostShowController' }).

        // subscriptions
        when('/subscriptions/create/', { templateUrl: '/modules/subscription/create.html', controller: 'SubscriptionCreate' }).
        when('/subscriptions/edit/', { templateUrl: '/modules/subscription/edit.html', controller: 'SubscriptionEdit' }).

        // stats
        when('/user-stats', { templateUrl: '/modules/user-stats/verbose.html', controller: 'UserStats' }).

        // games
        when('/games', { templateUrl: '/modules/game/index.html', controller: 'GameIndex' }).
        when('/games/create', { template: null, controller: 'GameCreate' }).
        when('/games/:id/edit', { templateUrl: '/modules/game/edit.html', controller: 'GameEdit' }).
        when('/games/:id/', { templateUrl: '/modules/game/show.html', controller: 'GameShow' }).
        when('/games/:id/play', { controller: 'GamePlay', templateUrl: '/modules/game/play.html' }).
        when('/games/:id/end/:redirect', { template: null, controller: 'GameEnd' }).

        // apps
        when('/game/lovers-lane', { templateUrl: '/modules/lovers-lane/play.html', controller: 'LoversLane' }).
        when('/game/strip-poker', { templateUrl: '/modules/poker/play.html', controller: 'Poker' }).
        when('/game/dice', { templateUrl: '/modules/dice/play.html', controller: 'Dice' }).
        when('/game/dice/:set', { templateUrl: '/modules/dice/play.html', controller: 'Dice' }).
        when('/role-playing/generator', { templateUrl: '/modules/role-playing/generator.html', controller: 'RoleplayingGenerator' }).
        when('/role-playing/outlines', { templateUrl: '/modules/role-playing/outlines.html', controller: 'RoleplayingOutlines' }).
        when('/role-playing/outlines/:id', { templateUrl: '/modules/role-playing/outline.html', controller: 'RoleplayingOutline' }).
        when('/role-playing/scripts', { templateUrl: '/modules/role-playing/scripts.html', controller: 'RoleplayingScripts' }).
        when('/role-playing/scripts/:id', { templateUrl: '/modules/role-playing/script.html', controller: 'RoleplayingScript' }).
        when('/ideas/foreplay-generator', { templateUrl: '/modules/foreplay-generator/show.html', controller: 'ForeplayGeneratorController' }).
        when('/ideas/sex-positions-generator', { templateUrl: '/modules/sex-positions/generator.html', controller: 'SexPositionGeneratorController' }).
        when('/ideas/sex-positions', { templateUrl: '/modules/sex-positions/index.html', controller: 'SexPositionsController' }).
        when('/ideas/sex-positions/:id', { templateUrl: '/modules/sex-positions/show.html', controller: 'SexPositionController' }).

        // questions
        when('/conversation/conversation-starters', { templateUrl: '/modules/conversation/generator.html', controller: 'ConversationGeneratorController' }).
        when('/surveys/:person', { templateUrl: '/modules/survey/index.html', controller: 'SurveyIndexController' }).


        when('/conversation/entries', { templateUrl: '/modules/entries/index.html', controller: 'EntryIndexController' }).
        when('/conversation/entries/edit/:id', { templateUrl: '/modules/entries/edit.html', controller: 'EntryEditController' }).
        when('/conversation/entries/:id', { templateUrl: '/modules/entries/show.html', controller: 'EntryShowController' }).
        when('/services/favor-clock', { templateUrl: '/modules/favor-clock/index.html' }).
        when('/services/:person', { templateUrl: '/modules/services/index.html', controller: 'ServiceIndexController' }).
        when('/services/edit/:id', { templateUrl: '/modules/services/edit.html', controller: 'ServiceEditController' }).
        when('/service/:id', { templateUrl: '/modules/services/show.html', controller: 'ServiceShowController' }).
        when('/services/purchased/:person', { templateUrl: '/modules/services/purchased.html', controller: 'ServicePurchasedController' }).

        // home
        otherwise({ redirectTo: '/' });
})
