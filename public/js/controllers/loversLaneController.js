function loversLaneController($scope, $http) {

	// variables
	spaceSize = 150;

	$http.get('/lovers-lane/game').then(function (result) {

		// get data
		$scope.game = result.data;

		// define variables
		user = $scope.game.user.gender + '_';
		spouse = $scope.game.user.spouse.gender + '_';
		$scope.allow_rolling = true;
		$scope.game[user + 'left'] = $('.player.' + $scope.game.user.gender).offset().left - $('#board').offset().left;
		$scope.game[user + 'top'] = $('.player.' + $scope.game.user.gender).offset().top - $('#board').offset().top;
		$scope.game[spouse + 'left'] = $('.player.' + $scope.game.user.spouse.gender).offset().left - $('#board').offset().left;
		$scope.game[spouse + 'top'] = $('.player.' + $scope.game.user.spouse.gender).offset().top - $('#board').offset().top;

		// alternate turn
		function alternateTurn() {
			if ($scope.game.turn == 'm') $scope.game.turn = 'f';
			else $scope.game.turn = 'm';
			if ($scope.game.turn == 'm') $scope.game.player = $scope.game.user;
			else $scope.game.player = $scope.game.user.spouse;
		}
		alternateTurn();

		// roll
		$scope.roll = function () {

			// define variables
			player = $scope.game.turn + '_';
			starting_point = parseInt($scope.game[player + 'space']);
			roll = Math.floor((Math.random() * 6) + 1);
			spaces = $('.spaceContainer').length;
			destination = starting_point + roll;
			if (destination > spaces) destination -= spaces;

			/*****************
			 * move player
			 *****************/

			// first turn
			movePlayer();

			// other turns
			interval = setInterval(function () {
				movePlayer();
			}, 1000);

			// move player
			function movePlayer() {
				$scope.allow_rolling = false;

				if ($scope.game[player + 'space'] !== destination) {
					// console.log('player_space', $scope.game[player + 'space']);

					$scope.game[player + 'space']++;
					if ($scope.game[player + 'space'] == 17) {
						$scope.game[player + 'space'] = 1;
					};
					if ($scope.game[player + 'space'] <= 5) {
						$scope.game[player + 'left'] += spaceSize;
						css = { left: $scope.game[player + 'left'] + 'px' };
					};
					if ($scope.game[player + 'space'] > 5 && $scope.game[player + 'space'] <= 9) {
						$scope.game[player + 'top'] += spaceSize;
						css = { transform: 'translateY(' + ($scope.game[player + 'top'] - 25) + 'px)' };
					};
					if ($scope.game[player + 'space'] > 9 && $scope.game[player + 'space'] <= 13) {
						$scope.game[player + 'left'] -= spaceSize;
						css = { left: $scope.game[player + 'left'] + 'px' };
					};
					if ($scope.game[player + 'space'] > 13 && $scope.game[player + 'space'] <= 18) {
						$scope.game[player + 'top'] -= spaceSize;
						css = { transform: 'translateY(' + ($scope.game[player + 'top'] - 25) + 'px)' };
					};
					element = $('.player.' + $scope.game.turn);
					$(element).css(css).addClass('top');
					$('.piece', element).addClass('rise');
					$('.shadow', element).addClass('shrink');
					setTimeout(function () {
						$('.piece', element).removeClass('rise');
						$('.shadow', element).removeClass('shrink');
					}, 500);
				}
				else {
					$(element).removeClass('top');
					$scope.allow_rolling = true;
					clearInterval(interval);
					alternateTurn();
					$scope.$apply();
				}
			}

		};

	});

}
