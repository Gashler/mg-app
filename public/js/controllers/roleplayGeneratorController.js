function RoleplayGeneratorController($scope, $http) {
	$scope.loaded = false;
	$scope.getData = function() {
		$http.get('/api/generate-roleplay').then(function(data) {
			$scope.roleplay = data;
			$scope.generateFirstLineSayer();
			$scope.loaded = true;
			$scope.loading = false;
		});
	};
	$scope.getDataButton = function() {
		$scope.loading = true;
		$scope.getData();
	};
	if (typeof generate !== 'undefined') {
		$scope.getData();
	}
	if (typeof populate !== 'undefined') {
		$http.get('/api/roleplay/' + roleplay_id).then(function(data) {
			$scope.roleplay = data;
			$scope.loaded = true;
		});
	}

	// generate first line sayer
	$scope.generateFirstLineSayer = function() {
		var rand = Math.floor((Math.random() * 2));
		if (rand == 0) return $scope.roleplay.first_line_sayer = $scope.roleplay.his_name;
		else $scope.roleplay.first_line_sayer = $scope.roleplay.her_name;
	};
	
	$scope.generate = function(object) {
		$('label[for="' + object + '"]').after('<img class="loading-inline" src="/img/loading.gif">');
		url = object.replace("_", "-");
		$http.get('/api/' + url).then(function(data) {
			$scope.roleplay[object] = data/*.substr(0, 1).toUpperCase() + data.substr(1)*/;
			$('.loading-inline').remove();
		});
	};
	
	// determine if 'a' or 'an' is the proper word
	$scope.an = function(object) {
		first_letter = object[0];
		var an_letters = [
			'a',
			'e',
			'i',
			'o',
			'u'
		];
		if (an_letters.indexOf(first_letter) > -1) return 'an';
		else return 'a';
	};
	
}