angular.module('app')
.controller('HomeController', function($scope) {

	// define variables
	$scope.tab = 'cancel';

	$scope.resizeHome1 = function()
	{
		if ($(window).width() > 1200) {
			var height = $(window).height() * .94;
			$('#home-1').css('height', height + 'px');
		}
	}
	$scope.resizeHome1();
	$(window).resize(function() {
		$scope.resizeHome1();
	});

});
