
angular.module('app')
    .controller('BaseController', function ($scope, $http, $location, $timeout, $interval, $attrs, vars, $window) {

        /****************************
        * Define Common Variables
        *****************************/
        // $scope.registering = false;
        $scope.baseReady = false;
        $scope.errors = [];

        // get user and config variables
        $scope.getVars = function () {
            vars.async().then(function (result) {
                angular.forEach(result, function (value, key) {
                    $scope[key] = value;
                });
                $scope.baseReady = true;
                $scope.authorize();
            });
        };
        if ($scope.user == undefined) {
            $scope.getVars();
        }

        /****************************
        * Authentication
        *****************************/
        $scope.authorize = function () {
            if ($scope.user !== null) {

                // redirect if subscription has expired
                if (
                    $scope.user.role_name == 'Member'
                    && !$scope.user.account.bypass_subscription
                    && (
                        !$scope.user.account.stripe_id
                        || !$scope.user.account.subscribed
                    )
                ) {
                    // if ($scope.redirect) {
                    //     alert(36);
                    //     window.location = $scope.redirect;
                    // } else {
                    // alert(39);
                    if ($scope.user.account.stripe_id) {
                        var parameter = 'expired=1';
                    } else {
                        var parameter = 'no_subscription=1';
                    }
                    // $location.url("/subscriptions/create?" + parameter);
                    // }
                    throw new Error;
                }

                // redirect if registration isn't complete
                if (!$scope.user.verified) {
                    $location.path("/registration/verify");
                    throw new Error;
                }

                // redirect if user needs password
                if ($scope.user.need_password) {
                    $location.path("/password");
                    throw new Error;
                }
            }
        }

        // pagination controller
        function PaginationController($scope) {
            $scope.pageChangeHandler = function (num) { };
        }

        /****************************
        * Common Functions
        *****************************/

        // play audio
        $scope.audio = function (file) {
            var audio = new Audio('/audio/' + file + '.mp3');
            audio.play();
        }

        // check if variable is object
        $scope.isObject = function (x) {
            if (typeof x === 'object') {
                return true;
            }
            return false;
        }

        // check if user is logged in
        $scope.auth = function () {
            if ($scope.user !== null) {
                return true;
            }
            return false;
        }

        // check if user has one of the specified roles
        $scope.hasRole = function (roles) {
            if ($scope.auth()) {
                if (inArray($scope.user.role.name, roles)) {
                    return true;
                }
                return false;
            }
            return false;
        }

        // switch
        $scope.switchUser = function (gender) {
            $http.get('/switch-user/' + gender).then(function (result) {
                $scope.user = result.data;
                $location.path('/dashboard');
            }).catch(function (result) {

            });
        }

        // set message
        $scope.message = function (type, message, timeout, conversion) {
            // make sure message isn't already being displayed
            angular.forEach($scope.messages, function (message, key) {
                if (message.data == message) {
                    return;
                }
            });

            if (!Array.isArray(message)) {
                message = [message];
            }
            $scope.messages.push({
                type: type,
                data: message,
                conversion: conversion
            });
            var length = $scope.messages.length;
            $timeout(function () {
                $('.alert-main-container > div:nth-child(' + length + ')').addClass('slideUp');
            }, 10);
            if (timeout) {
                $timeout(function () {
                    $('.alert-main-container > div:nth-child(' + length + ')').addClass('fadeOut', function () {
                        $timeout(function () {
                            $scope.messages.splice(length - 1, 1);
                        }, 500);
                    });
                }, timeout);
            }
        }

        // messages test
        // var x = 1;
        // $interval(function() {
        //     x ++;
        //     $scope.message('warning', "Bla, bla, bla " + x, 5000);
        // }, 4000);

        // set message
        $scope.dismissMessage = function (index) {
            $('.alert-main-container > div:nth-child(' + (index + 1) + ')').addClass('fadeOut', function () {
                $timeout(function () {
                    $scope.messages.splice(index, 1);
                }, 500);
            });
        }

        // dismiss all messages
        $scope.dismissMessages = function () {
            angular.forEach($scope.messages, function (value, index) {
                $scope.dismissMessage(index);
            });
        }

        // edit unit
        $scope.edit = function (object, model) {
            // change the current object
            if ($scope.parent !== undefined) {
                if ($scope.grandparent == undefined || $scope.grandparent !== undefined && $scope.grandparent.model !== $scope.parent.model) {
                    $scope.grandparent = $scope.parent;
                }
            }
            $scope.parent = $scope.unit;
            $scope.unit = object;
            $scope[object.model] = object;

            if ($scope.unit.model == 'effect') {
                $scope.unit.rand_options = [
                    {
                        name: 'The value',
                        value: 0
                    },
                    {
                        name: 'A random value',
                        value: 1
                    }
                ];
            }
            $scope.changed_unit = true;

            // mask the disappearing of background content when the $scope.unit variable changes by temporarily duplicating effected elements
            // $('[ng-repeat]').not('.calendar [ng-repeat]').each(function() {
            //     parent = $(this).parent();
            //     if ($(parent).attr('data-model') && !$(parent).hasClass('placeholder') && !$(parent).next().hasClass('placeholder') && !$(parent).is('tr')) {
            //         $(parent).addClass('placeholder').attr('data-model', $scope.parent.model);
            //         placeholder = $(parent)[0].outerHTML;
            //         $(parent).removeClass('placeholder');
            //         $(parent).after(placeholder);
            //     }
            // });

            // show popup
            if (model == null) {
                model = object.model;
            }
            $scope.togglePopup('#' + model + 'Form');
            console.log('$scope.unit = ', $scope.unit);
            $timeout(function () {
                console.log('$scope.unit = ', $scope.unit);
            }, 5000);
        };

        /*****************
        * Popups
        ******************/
        $scope.changed_unit = false;
        $scope.animating = false;
        $scope.togglePopup = function (popup, changed_unit) {
            console.log('popup = ', popup);
            // initialize variables
            $scope.animating = false;

            // indidcate whether the primary object has been changed
            if (changed_unit == undefined) {
                $scope.changed_unit = true;
            } else {
                $scope.changed_unit = false;
            }

            // set parent
            if ($scope.grandparent !== undefined && $('[data-model="' + $scope.unit.model + '"]').length > 0) {
                var parent = '[data-model="' + $scope.grandparent.model + '"]';
            } else {
                var parent = 'body';
            }

            // show popup
            if ($(popup).css('display') === 'none') {
                // remove placeholders
                $(popup + ' .placeholder').remove();
                // fade in popup
                $(popup).fadeIn(250);
                $(popup + ' .panel').css('margin-top', '20px');
            } else {
                $scope.hidePopup(popup);
            }

            // clicking in black filter
            $('.popup').click(function (event) {
                event.stopPropagation();
                if (!$scope.animating) {
                    $scope.animating = true;
                    $scope.hidePopup('#' + $(this).attr('id'));
                    $scope.$apply();
                }
            });
            $('.popup .popup').click(function (event) {
                event.stopPropagation();
            });

            // prevent clicking on popup from hiding popup and prevent mulitple events triggered by other popups
            id = popup.replace('#', '');
            $(popup + ' .panel, .popup[id!="' + id + '"]', parent).click(function (event) {
                event.stopPropagation();
            });

        }


        // hide popup
        $scope.hidePopup = function (popup) {
            $scope.animating = true;
            $(popup).css('display', 'none !important');
            $(popup + ' .panel').parent().fadeOut(250, function () {
                $timeout(function () {
                    $scope.animating = false;
                }, 250);
            });
            $(popup + ' .panel').css('margin-top', '-200px');

            // regress object
            if ($scope.changed_unit) {
                $scope.regressUnit();
            }
            $scope.changed_unit = true;

            // remove placeholders
            if ($scope.hasOwnProperty('unit')) {
                $('[data-model="' + $scope.unit.model + '"] .placeholder, [data-model="' + $scope.unit.model + '"].placeholder').remove();
            }
        }

        // hide all popups
        $scope.hidePopups = function () {
            $scope.hidePopup('.popup');
        }


        // get index
        $scope.getIndex = function (parent, model, id) {
            if (parent[model + 's'].length > 0) {
                angular.forEach(parent[model + 's'], function (object, key) {
                    if (object.id == id) $scope.index = key + 1;
                });
            }
        }

        // set paths
        $scope.setPaths = function (unit, model, model_as, row_index, col_index) {
            if (model_as == null) {
                unit.model = model;
            }
            else {
                unit.model = model_as;
            }
            unit.parent = $scope.unit.path;

            // if unit's parent is array
            if (typeof eval(unit.parent) === 'array') {
                unit.idx = eval(unit.parent + '.length');
                unit.path = unit.parent + '[' + unit.idx + ']';
            }

            // if unit's parent is part of array
            else if (typeof eval(unit.parent + '.' + model + 's') === 'object') {
                if (model_as == null) {
                    unit.idx = eval(unit.parent + '.' + model + 's' + '.length');
                    unit.path = unit.parent + '.' + model + 's' + '[' + unit.idx + ']';
                }
                else {
                    unit.idx = eval(unit.parent + '.' + model_as + 's' + '.length');
                    unit.path = unit.parent + '.' + model_as + 's' + '[' + unit.idx + ']';
                }
            }

            // if unit's parent is not an array or part of an array
            else {
                unit.path = $scope.unit.path + '.';
                if (row_index !== undefined && col_index !== undefined) {
                    unit.path += 'rows[' + row_index + '].cols[' + col_index + '].';
                }
                if (model_as == null) {
                    unit.path += model;
                } else {
                    unit.path += model_as;
                }
            }
            return unit;
        }

        $scope.create = function (data, model) {
            eval('$scope.loading_' + model + ' = true;');
            $http.post('/' + ucfirst(model) + '/store', data).then(function (result) {
                result = $scope.setPaths(result, model);
                $scope.unit[model + 's'].push(result);
                $scope.edit(result);
                eval('$scope.loading_' + model + ' = false;');
            });
        };

        // regress unit
        $scope.regressUnit = function () {
            if ($scope.hasOwnProperty('unit')) {
                if ($scope.parent !== undefined) {
                    if ($scope.unit.hasOwnProperty('parent')) {
                        eval('$scope.unit = ' + $scope.unit.parent + ';');
                    } else {
                        $scope.unit = $scope.parent;
                    }
                }

                // exceptions
                if ($scope.unit.hasOwnProperty('model') && $scope.unit !== undefined) {
                    if ($scope.unit.model == 'col') $scope.regressUnit();
                    if ($scope.unit.model == 'row') $scope.regressUnit();
                }

                $scope.changed_unit = true;
            }
        }

        // update object
        $scope.update = function (key, return_data) {
            if (return_data) {
                $scope.unit.return = true;
            }
            // var data = {
            //     'model': $scope.unit.model,
            //     [key]: $scope.unit[key],
            // };
            console.log('line 346, $scope.unit = ', $scope.unit);
            $http.patch('/Base', $scope.unit).then(function (result) {
                if (result.data !== undefined) {
                    angular.forEach(result.data, function (new_value, new_property) {
                        angular.forEach($scope.unit, function (old_value, old_property) {
                            if (new_property == old_property && old_value !== new_value) {
                                $scope.unit[old_property] = new_value;
                            }
                        });
                    });
                    eval($scope.unit.path + ' = $scope.unit;');
                }
            });
        };

        // patch object
        $scope.patch = function (key, object) {
            if (!object) {
                object = $scope.unit;
            }
            console.log('object = ', object);
            $http.patch('/Base', {
                meta: {
                    model: object.model,
                    id: object.id,
                    key: key
                },
                value: object[key]
            }).then(function (response) {
                if (response.data.errors) {
                    $scope.message('danger', response.data.errors);
                }
                if (callback) {
                    eval('$scope.' + callback + '();');
                }
            });
        };

        // delete object
        $scope.delete = function (unit, object, model, index) {
            $scope.unit = unit;
            if (model == undefined) {
                model = object.model;
            }
            $http.post('/Base/delete/' + model, [object.id]).then(function (result) {
                console.log(result);
            });
            if ($scope.unit.hasOwnProperty('path')) {
                eval($scope.unit.path + '.' + object.model + 's.splice(' + object.idx + ', 1)');
            } else {
                eval('$scope.unit.' + model + 's.splice(' + index + ', 1)');
            }
            $scope.updateIndexes(object);
        };

        // attach objects
        $scope.attach = function (model, popup) {
            eval('$scope.loading_' + model + ' = true');
            $http.post('/Base/attach', {
                'model_path': $scope.unit.model_path,
                'model_id': $scope.unit.id,
                'method': model + 's',
                'method_id': $scope.selected.id
            }).then(function () {
                eval('$scope.loading_' + model + ' = false');
                eval('$scope.unit.' + model + 's.push($scope.selected);');
                $scope.updateIndexes();
                if (popup == null) {
                    $scope.togglePopup('#' + model + 'Form');
                }
                else {
                    $scope.togglePopup(popup);
                }
            });
        }

        // detach objects
        $scope.detach = function (model, id, index, popup) {
            eval('$scope.loading_' + model + ' = true');
            $http.post('/Base/detach', {
                'model_path': $scope.unit.model_path,
                'model_id': $scope.unit.id,
                'method': model + 's',
                'method_id': id
            }).then(function () {
                eval('$scope.loading_' + model + ' = false');
                eval($scope.unit.path + '.' + model + 's.splice(index, 1)');
                $scope.updateIndexes();
            });
        }

        // update indexes
        $scope.updateIndexes = function (unit) {
            eval('var parent = ' + unit.parent + ';');
            angular.forEach(parent, function (variable, index) {
                eval(variable.parent + variable.model + 's' + '[' + index + '].idx = ' + index);
            });
        }

    });
