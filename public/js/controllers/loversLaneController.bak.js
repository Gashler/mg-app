function loversLaneController($scope, $http) {
	
	// variables
	spaceSize = 150;
	
	$http.get('/lovers-lane/game').then(function(result) {
		
		// get data
		$scope.game = result.data;
		
		// define variables
		user = $scope.game.user.gender + '_';
		spouse = $scope.game.user.spouse.gender + '_';
		$scope.game[user + 'left'] = $('.player.' + $scope.game.user.gender).offset().left - $('#board').offset().left;
		$scope.game[user + 'top'] = $('.player.' + $scope.game.user.gender).offset().top - $('#board').offset().top;
		$scope.game[spouse + 'left'] = $('.player.' + $scope.game.user.spouse.gender).offset().left - $('#board').offset().left;
		$scope.game[spouse + 'top'] = $('.player.' + $scope.game.user.spouse.gender).offset().top - $('#board').offset().top;

		// roll
		$scope.roll = function() {

			// define variables
			player = $scope.game.turn + '_';
			spaces = $scope.game[player + 'space'] + Math.floor((Math.random() * 6) + 1);
			
			// increment player's space
			if($scope.game[player + 'space'] < 17) {
				$scope.game[player + 'space'] ++;
			}
			else {
				$scope.game[player + 'space'] = 2;	
			}
			
			// move player
			interval = setInterval(function() {
				if($scope.game[player + 'space'] <= spaces) {

					if($scope.game[player + 'space'] <= 5) {
						$scope.game[player + 'left'] += spaceSize;
						css = {left: $scope.game[player + 'left'] + 'px'};
					};
					if($scope.game[player + 'space'] > 5 && $scope.game[player + 'space'] <= 9) {
						$scope.game[player + 'top'] += spaceSize;
						css = {transform:'translateY(' + ($scope.game[player + 'top'] - 25) + 'px)'};
					};
					if($scope.game[player + 'space'] > 9 && $scope.game[player + 'space'] <= 13) {
						$scope.game[player + 'left'] -= spaceSize;
						css = {left: $scope.game[player + 'left'] + 'px'};
					};
					if($scope.game[player + 'space'] > 13 && $scope.game[player + 'space'] !== 1) {
						$scope.game[player + 'top'] -= spaceSize;
						css = {transform:'translateY(' + ($scope.game[player + 'top'] - 25) + 'px)'};
					};
					element = $('.player.' + $scope.game.turn);
					$(element).css(css).addClass('top');
					$('.piece', element).addClass('rise');
					$('.shadow', element).addClass('shrink');
					setTimeout(function() {
						$('.piece', element).removeClass('rise');
						$('.shadow', element).removeClass('shrink');
					}, 500);
					setTimeout(function() {
						$(element).removeClass('top');
					}, 1000);
					
					$scope.game[player + 'space'] ++;
				}
				else clearInterval(interval);
			}, 1000);

			// alternate turn
			if($scope.game.turn == 'm') $scope.game.turn = 'f';
			else $scope.game.turn = 'm';
			
		};
		
	});
	
}
