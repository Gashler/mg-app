// register angular app module
var app = angular.module('app', ['ngRoute', 'ngSanitize', 'dndLists', 'angularUtils.directives.dirPagination'])

    // disable http caching
    .config(['$httpProvider', function ($httpProvider) {
        //initialize get if not there
        if (!$httpProvider.defaults.headers.get) {
            $httpProvider.defaults.headers.get = {};
        }

        // Answer edited to include suggestions from comments
        // because previous version of code introduced browser-related errors

        //disable IE ajax request caching
        $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
        // extra
        $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
        $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
    }])

    // Due to aa077e8, the default hash-prefix used for $location hash-bang URLs has changed from the empty string ('') to the bang ('!'). This is the fix:
    .config(['$locationProvider', function ($locationProvider) {
        $locationProvider.hashPrefix('');
    }])

    // disable annoying console errors
    .config(['$qProvider', function ($qProvider) {
        $qProvider.errorOnUnhandledRejections(false);
    }])

    .filter('cut', function () {
        return function (value, wordwise, max, tail) {
            if (!value) return '';

            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace !== -1) {
                    //Also remove . and , so its gives a cleaner result.
                    if (value.charAt(lastspace - 1) === '.' || value.charAt(lastspace - 1) === ',') {
                        lastspace = lastspace - 1;
                    }
                    value = value.substr(0, lastspace);
                }
            }

            return value + (tail || ' …');
        };
    })

// .config(['$routeProvider',
// function($routeProvider) {
//     $routeProvider.
//     when('games', {
//         templateUrl: '/apps/game/index.html',
//         controller: '/apps/game/controller.js'
//     }).
//     // when('/phones/:phoneId', {
//     //     templateUrl: 'partials/phone-detail.html',
//     //     controller: 'PhoneDetailCtrl'
//     // }).
//     otherwise({
//         redirectTo: '/phones'
//     });
// }])

// get constants
app.factory('vars', function ($http) {
    var promise;
    var vars = {
        async: function () {
            if (!promise) {
                promise = $http.get('/constants').then(function (response) {
                    return response.data;
                });
            }
            return promise;
        }
    };
    return vars;
})

    // .directive('media', function() {
    //     return {
    //         templateUrl : '/modules/media/mediaWidget.html'
    //     };
    // })

    .service('media', function () {
        this.toggleLibrary = function ($scope, $http) {
            $scope.loading_media = true;
            $http.post('/Media/all', {
                user_id: user_id,
                type: 'Image',
                take: 20
            }).then(function (result) {
                $scope.loading_media = false;
                $scope.medias = result;
            });
            $scope.togglePopup('#mediaLibrary');
        }
    })

angular.module('app').directive('validateForm', function () {
    return {
        restrict: 'A',
        controller: function ($scope, $element) {
            var $elem = $($element);
            if ($.fn.parsley)
                $elem.parsley();
        }
    };
});