angular.module('app')
.directive('mgaction', function() {
    return {
        templateUrl: '/components/action/edit.html',
        controller: function($scope, $http) {

            /***************
            * Actions
            ****************/

            // create action
            $scope.createAction = function() {
                $scope.action = {
                    'type' : 'select',
                    'performer' : '',
                    'ids' : []
                };
                $scope.togglePopup('#actionForm');
            };

            // get random action
            $scope.getRandomAction = function() {
                $scope.action.assign = {
                    'game_id' : game_id,
                    'model' : $scope.unit.model_path,
                    'model_id' : $scope.unit.id,
                    'gender' : $scope.action.performer
                };
                $scope.assignActions();
            }

            // browse Actions
            $scope.browseActions = function() {
                $http.get('/Action/all').then(function(actions) {
                    $scope.actions = actions;

                    /*****************************************
                    * Filter results based on action performer
                    ******************************************/

                    // filter to actions performed by player
                    if($scope.action.performer == $scope.unit.turn) {
                        angular.forEach($scope.actions, function(action, key) {
                            if(action.gender == $scope.uvent.turn.spouse) {
                                $scope.actions.splice(key, 1);
                            }
                        });
                    }

                    // filter for actions peformed by spouse
                    if($scope.action.performer == $scope.unit.turn.spouse) {
                        angular.forEach($scope.actions, function(action, key) {
                            if(action.gender == $scope.uvent.turn) {
                                $scope.actions.splice(key, 1);
                            }
                        });
                    }

                    $scope.togglePopup('#browseActions');
                });
            }

            // select actions
            $scope.selectActions = function() {

                // get checked actions
                action_ids = [];
                angular.forEach($scope.actions, function(action) {
                    if(action.checked) action_ids.push(action.id);
                });

                $scope.action.assign = {
                    'game_id' : game_id,
                    'model' : $scope.unit.model_path,
                    'model_id' : $scope.uvent.id,
                    'gender' : $scope.action.performer,
                    'action_ids' : action_ids
                };
                $scope.assignActions();
            };

            // assign actions
            $scope.assignActions = function() {
                $http.post('/Action/assign', $scope.action.assign).then(function(actions) {
                    console.log(actions);
                    angular.forEach(actions, function(action) {
                        $scope.uvent.actions.push(action);
                    });
                    $scope.togglePopup('#actionForm');
                });
            }

            // delete action
            $scope.deleteAction = function(id, index) {
                $http.delete('/action/' + id);
                $scope.uvent.actions.splice(index, 1);
            };

        }
    }
});
