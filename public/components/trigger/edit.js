angular.module('app')
.directive('mgtriggeredit', function() {
    return {
        templateUrl : '/components/trigger/edit.html',
        controller: function($scope, $http) {

            /***************
            * Triggers
            ***************/

            // create trigger
            $scope.createTrigger = function() {
                $scope.loading_trigger = true;
                data = {
                    logic_id : $scope.unit.id,
                    type: 'proximity',
                    distance: 1,
                    scope: 'any',
                    operator_1: '==',
                    operator_2: '==',
                    subject_type: 'player',
                    verb: 'Talk'
                };
                $http.post('/Trigger/store', data).then(function(result) {
                    result = $scope.setPaths(result, 'trigger');
                    $scope.unit.triggers.push(result);
                    $scope.loading_trigger = false;
                    $scope.edit(result);
                });
            };

        }
    }
});
