angular.module('app')
.directive('mgeffectedit', function() {
    return {
        templateUrl : '/components/effect/edit.html',
        controller: function($scope, $http) {

            /***************
            * Effects
            ***************/

            // create effect
            $scope.createEffect = function() {
                $scope.create({
                    logic_id: $scope.unit.id,
                    max: 10,
                    min: 1,
                    operator: '==',
                    property_id: $scope.game.defaults.property.id,
                    property_index: 0,
                    propertyOption_id: $scope.game.defaults.property.options[0].id,
                    qty: 1,
                    rand: 0,
                    scope: 'specific',
                    subject_col: 'hidden',
                    subject_id: 1,
                    subject_type: 'Character',
                    subject_val: 1,
                    type: 'change'
                }, 'effect');
            };

            // get subject ID
            $scope.getSubjectId = function() {
                $scope.unit.property_id = $scope.game.properties[$scope.unit.subject_type][$scope.unit.property_index].id;
                $scope.update(unit);
            }

        }
    }
});
