angular.module('app')
.directive('mapedit', function() {
    return {
        templateUrl : '/components/map/edit.html',
        controller: function($scope, $http) {

            /***************
            * Maps
            ****************/

            // create map
            $scope.createMap = function() {
                object = $scope[$scope.unit];
                $scope.loading_logic = true;
                $scope.map = {
                    morph_type: $scope.unit,
                    morph_id: object.id,
                };
                $http.post('/map', $scope.map).then(function(result) {
                    result = $scope.setPaths(result);
                    object.maps.push(result);
                    $scope.editMap(result);
                    $scope.loading_map = false;
                });
            };

        }
    }
});
