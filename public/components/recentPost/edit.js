angular.module('app')
.directive('mgrecentpost', function() {
    return {
        templateUrl: '/components/action/template.html',
        controller: function($scope, $http) {

            // get recent post
            $http.get('/Post/recent').then(function(result) {
                $scope.recentPosts = result;
            });

        }
    }
});
