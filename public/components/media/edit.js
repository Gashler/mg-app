angular.module('app')
.directive('mgmedia', function() {
    return {
        templateUrl : '/components/media/edit.html',
        controller: function($scope, $http) {

            /*******************
            * Media
            ********************/

            // toggle media form
            $scope.mediaForm = function() {
                $scope.togglePopup('#mediaForm');
            }

            // upload media
            $scope.uploadMedia = function() {
                $scope.loading_media = true;

                // prepare data container
                var form_data = new FormData();

                // prepare attachment data
                form_data.append('model', $scope.unit.model_path);
                form_data.append('id', $scope.unit.id);

                // prepare tags
                tags = [{'name' : 'Games'}];
                $(tags).each(function(key, tag) {
                    form_data.append('tags[][name]', tag.name);
                });

                // prepare files
                files = $('#files')[0].files;
                $(files).each(function(key, file) {
                    form_data.append('files[]', file);
                });

                // send data
                $.ajax({
                    type: "post",
                    url: "/media",
                    data: form_data,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function(files) {
                        angular.forEach(files, function(file) {
                            $scope.unit.medias.push(file);
                            $scope.$apply();
                        });
                        $scope.togglePopup('#mediaForm');
                        $scope.loading_media = false;
                    }
                });
            }

            // toggle media library
            $scope.mediaLibrary = function() {
                $scope.loading_media = true;
                $http.post('/Media/all', {
                    user_id: user_id,
                    type: 'Image',
                    take: 20
                }).then(function(result) {
                    $scope.loading_media = false;
                    $scope.medias = result;
                    console.log($scope.medias);
                });
                $scope.togglePopup('#mediaLibrary');
            }

            // insert media
            $scope.insertMedia = function() {

                // ensure that this media isn't already in list
                duplicate = false;
                angular.forEach($scope.unit.medias, function(media) {
                    if(media.id == selected.id) {
                        duplicate = true;
                    }
                });

                // attach media to object
                if(!duplicate) {
                    $scope.attach('media', '#mediaLibrary');
                }

                // close media library
                else {
                    $scope.togglePopup('#mediaLibrary');
                }
            }

        }
    }
});
