angular.module('app')
.directive('mgupcomingtuvents', function() {
    return {
        templateUrl: '/components/upcomingUvents/template.html',
        controller: function($scope, $http) {

            // get upcoming uvents
            $http.get('/Uvent/upcoming').then(function(result) {
                $scope.upcomingUvents = result;
            });

        }
    }
});
