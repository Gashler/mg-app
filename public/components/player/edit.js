angular.module('app')
.directive('playeredit', function() {
    return {
        templateUrl : '/components/player/edit.html',
        controller: function($scope, $http) {

            /***************
            * Players
            ***************/

            // create player
            $scope.createPlayer = function(place_id, gender, type) {
                $scope.loading_player = true;

                // determine type
                if(type == null) type = 'npc';

                // if gender is unspecified, randomize gender
                if((type == null || type == 'npc') && gender == null) {
                    rand = Math.floor((Math.random() * 1));
                    if(rand == 0) gender = 'm';
                    else gender = 'f';
                };

                // create player with spouse's gender
                if(type == 'player') {
                    if($scope.game.players[0].gender == 'm') gender = 'f';
                    else gender = 'm';
                }

                // assemble data
                player = {
                    'game_id' : game_id,
                    'place_id' : $scope.unit.id,
                    'gender' : gender,
                    'type' : type,
                    'first_name' : 'New Player'
                };

                // send data
                $http.post('/Player', player).then(function(result) {
                    result = $scope.setPaths(result, 'player');
                    $scope.loading_player = false;
                    if(type == 'player') $scope.unit.players.push(result);
                    else $scope.unit.players.push(result);
                    $scope.edit(result, 'player');
                });
            };

        }
    }
});
