angular.module('app')
.directive('logicedit', function() {
    return {
        templateUrl : '/components/logic/edit.html',
        controller: function($scope, $http) {

            /***************
            * Logics
            ***************/

            // create logic
            $scope.createLogic = function() {
                $scope.loading_logic = true;
                $scope.logic = {
                    morph_type : $scope.unit.model_path,
                    morph_id : $scope.unit.id,
                };
                $http.post('/Logic/store', $scope.logic).then(function(result) {
                    result = $scope.setPaths(result, 'logic');
                    $scope.unit.logics.push(result);
                    $scope.loading_logic = false;
                    $scope.edit(result, 'logic');
                });
            };

        }
    }
});
