angular.module('app')
.directive('logicplay', function() {
    return {
        controller: function($scope, $http) {

            /***************
            * Logics
            ***************/

            // run logic
            $scope.runLogic = function(object, subject) {
                if(object !== undefined && object !== null && object.hasOwnProperty('logics') && object.logics.length > 0) {
                    angular.forEach(object.logics, function(logic, logic_index) {

                        /*******************
                        * evaluate triggers
                        ********************/

                        triggered = false;
                        if(logic.triggers.length > 0) {
                            angular.forEach(logic.triggers, function(trigger, trigger_index) {

                                // act upon
                                if(trigger.type == 'act' && subject !== undefined) {
                                    triggered = false;
                                    eval('if(subject.model ' + trigger.operator_1 + ' "' + trigger.subject_type + '") triggered = true;');
                                    if(triggered) {
                                        triggered = false;
                                        if(trigger.scope == 'any') {
                                            triggered = true;
                                        }
                                        else if(trigger.scope == 'specific') {
                                            eval('if(subject.id ' + trigger.operator_2 + ' ' + trigger.subject_id + ') triggered = true;');
                                            triggered = true;
                                        }
                                    }

                                    // act upon result
                                    if(trigger.act_type == 'give') {
                                        console.log('give!');
                                    }
                                    else if(trigger.act_type == 'destroy') {

                                    }

                                }

                                // proximity
                                if(trigger.type == 'proximity' && object.place_id !== undefined) {
                                    if(object.place_id == $scope.place.id) {
                                        triggered = true;
                                    }
                                }

                                // view
                                if($scope.viewActive) {
                                    eval('$scope.' + object.model + 'Show(object);');
                                }

                                // verb
                                if(trigger.type == 'verb') {
                                    if(trigger.verb == 'Talk' && $scope.talkActive) {
                                        triggered = true;
                                    }
                                }

                            });
                        }

                        /*******************
                        * evaluate effects
                        ********************/

                        if(logic.triggers.length == 0 || triggered) {
                            angular.forEach(logic.effects, function(effect, effect_index) {

                                // change
                                if(effect.type == 'change') {
                                    $scope.getUnit(effect.subject_type, effect.subject_id, $scope.game.maps);
                                    eval($scope.subject.path + '.' + effect.property.key + ' = ' + effect.property_option.value);

                                    // create narration
                                    var body = '';
                                    if($scope.subject.model == 'place' || $scope.subject.model == 'object') {
                                        body += "The ";
                                    }
                                    body += $scope.subject.name + '\'s property "' + effect.property.name + '" is set to "' + effect.property_option.name + '"';
                                    $scope.create({
                                        body: body,
                                        game_id: $scope.game.id,
                                        morph_id: $scope.subject.id,
                                        morph_type: ucfirst($scope.subject.model),
                                    }, 'narration');
                                    $scope.updateDirections();
                                }

                                // trigger
                                if(effect.type == 'trigger') {

                                    // dialog
                                    if(effect.subject_type == 'dialog') {
                                        $scope.character = object;
                                        if(effect.dialogs.length > 0) {
                                            $scope.dialog = effect.dialogs[0];
                                            $scope.togglePopup('#dialogPlay');
                                        }
                                    }

                                }

                            });
                        }

                    });
                }
                angular.forEach(object, function(value, key) {
                    if(typeof value === 'array' || typeof value === 'object') {
                        $scope.runLogic(value);
                    }
                });
            }

        }
    }
});
