angular.module('app')
.directive('mgtimer', function() {
    return {
        templateUrl : '/components/timer/edit.html',
        controller: function($scope, $http) {

            /***************
            * Timers
            ***************/

            // create timer
            $scope.createTimer = function() {
                $scope.loading_timer = true;
                $scope.timer = {
                    game_id : game_id,
                    morph_type : 'Uvent',
                    morph_id : $scope.uvent.id,
                    result_id : 0,
                    result_type : 'Uvent',
                    direction : 'down',
                    value : 60,
                    min : 0,
                    max : 60
                };
                $http.post('/timer', $scope.timer).then(function(result) {
                    result = $scope.setPaths(result);
                    $scope.unit.timer = result;
                    $scope.editTimer();
                    $scope.loading_timer = false;
                });
            };

            // edit timer
            $scope.editTimer = function() {
                $scope.timer = $scope.uvent.timer;
                $scope.togglePopup('#timerForm');
            };

            // update timer
            $scope.updateTimer = function() {
                $http.put('/timer/' + $scope.timer.id, $scope.timer);
            };

            // delete timer
            $scope.deleteTimer = function() {
                console.log($scope.uvent.timer.id);
                $http.delete('/timer/' + $scope.uvent.timer.id);
                $scope.uvent.timer = null;
            };

        }
    }
});
