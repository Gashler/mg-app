angular.module('app')
.directive('dialogedit', function() {
    return {
        templateUrl : '/components/dialog/edit.html',
        controller: function($scope, $http) {

            /***************
            * Edit Dialog
            ***************/

            $scope.createDialog = function() {
                $scope.create({
                    effect_id: $scope.unit.id,
                    character_id: $scope.grandparent.id
                }, 'dialog');
            }

        }
    }
});
