angular.module('app')
.directive('narrationedit', function() {
    return {
        templateUrl : '/components/narration/edit.html',
        controller: function($scope, $http) {

            /***************
            * narrations
            ***************/

            $scope.createNarration = function() {
                $scope.create({
                    morph_id: $scope.unit.id,
                    morph_type: $scope.unit.model,
                    type: 'popup'
                }, 'narration');
            }

        }
    }
});
