angular.module('app')
.directive('placeplay', function() {
    return {
        templateUrl : '/components/place/play.html',
        controller: function($scope, $http, $timeout) {

            /*************************
            * Places
            **************************/

            // get coordinates for various locations
            $scope.getCoordinates = function(type) {
                angular.forEach($scope.map.rows, function(row, row_index) {
                    angular.forEach(row.cols, function(col, col_index) {
                        if(col.place !== null && (type == undefined || col.place[type])) {
                            $scope.place = col.place;
                        }
                    });
                });
            }

            // get starting place
            function findStartingPlace(type) {
                if(type == undefined) {
                    type = 'current';
                }
                $scope.getCoordinates(type);
                if($scope.place == null) {
                    findStartingPlace('start');
                }
            }
            findStartingPlace('current');
            console.log('place = ', $scope.place);

            // determine movement
            $scope.determineMovement = function(apply, direction) {

                // north
                if(direction == 'n') {
                    row_index = $scope.place.row_index - 1;
                    col_index = $scope.place.col_index;
                }

                // east
                if(direction == 'e') {
                    row_index = $scope.place.row_index;
                    col_index = $scope.place.col_index + 1;
                }

                // south
                if(direction == 's') {
                    // alert('direction: ' + direction + ', row_index: ' + row_index + ', col_index: ' + col_index);
                    row_index = $scope.place.row_index + 1;
                    col_index = $scope.place.col_index;
                }

                // west
                if(direction == 'w') {
                    row_index = $scope.place.row_index;
                    col_index = $scope.place.col_index - 1;
                }

                if($scope.map.rows[row_index] !== undefined && $scope.map.rows[row_index].cols[col_index] !== undefined && $scope.map.rows[row_index].cols[col_index].place !== null) {

                    target = $scope.map.rows[row_index].cols[col_index].place;

                    // make sure the place isn't blocked
                    blocked = false;

                    // north
                    if(direction == 'n' && (target.block_bottom || $scope.place.block_top)) {
                        blocked = true;
                    }

                    // east
                    if(direction == 'e' && (target.block_left || $scope.place.block_right)) {
                        blocked = true;
                    }

                    // south
                    if(direction == 's' && (target.block_top || $scope.place.block_bottom)) {
                        blocked = true;
                    }

                    // west
                    if(direction == 'w' && (target.block_right || $scope.place.block_left)) {
                        blocked = true;
                    }

                    // toggle direciton buttons
                    if(!blocked) {
                        $('#' + direction).removeAttr('disabled');
                    }
                    else {
                        $('#' + direction).attr('disabled', 'disabled');
                    }

                    // move to the place
                    if(apply && !blocked) {
                        $scope.selectPlace($scope.map.rows[row_index].cols[col_index].place);
                    }

                }
                else {
                    // alert(direction + ' is not defined');
                    $('#' + direction).attr('disabled', 'disabled');
                }
            }

            // change place
            $scope.changePlace = function(direction) {
                $scope.map.rows[$scope.place.row_index].cols[$scope.place.col_index].place.current = 0;
                $scope.determineMovement(true, direction);
                $scope.map.rows[$scope.place.row_index].cols[$scope.place.col_index].place.current = 1;
                $scope.updateDirections();
                $scope.runLogic($scope.place);
            }

            // keyboard controls
            $('body').keydown(function(event) {

                // up
                if(event.which == 38) {
                    $scope.determineMovement(true, 'n');
                }

                // right
                if(event.which == 39) {
                    $scope.determineMovement(true, 'e');
                }

                // south
                if(event.which == 40) {
                    $scope.determineMovement(true, 's');
                }

                // west
                if(event.which == 37) {
                    $scope.determineMovement(true, 'w');
                }

            });

            // blur button
            $scope.blurButton = function(name) {
                $timeout(function() {
                    eval('$scope.' + name + 'Active = false;');
                }, 100);
            }

            // disable directions
            $scope.updateDirections = function() {
                $('.direction').each(function() {
                    $scope.determineMovement(false, $(this).attr('id'));
                });
            }
            $scope.updateDirections();

            // select place
            $scope.selectPlace = function(place) {
                place.current = 1;
                $scope.place = place;
                $scope.update(place);
                $scope.updateDirections();
                $scope.runLogic($scope.place);
            }

            // interactions
            $scope.interactWithUnit = function(unit) {

                // handle unit
                if($scope.handleActive) {
                    console.log(unit);
                    if(unit.model == 'object' && unit.takeable) {
                        alert('takeable!');
                        $http.put('/Object/' + unit.id, {
                            morph_type : 'Character',
                            morph_id : $scope.player.character.id
                        });
                        $scope.place.objects.splice(unit.index, 1);
                        $scope.player.objects.push(unit);
                    }
                }

                // view unit
                if($scope.viewActive) {
                    if(unit.model == 'object') {
                        $scope.objectShow(unit);
                    }
                    else if(unit.model == 'character') {
                        $scope.characterShow(unit);
                    }
                }

                // run logic
                $scope.runLogic(unit);
            }

        }
    }
});
