angular.module('app')
.directive('placeedit', function() {
    return {
        templateUrl : '/components/place/edit.html',
        controller: function($scope, $http) {

            /***************
            * Places
            ***************/

            // create place
            $scope.createPlace = function(row_id, row_index, col_id, col_index) {
                $scope.loading_place = true;
                $http.post('/Place/store', {
                    col_id: col_id
                }).then(function(result) {
                    result = $scope.setPaths(result, 'place', null, row_index, col_index);
                    $scope.unit.rows[row_index].cols[col_index].place = result;
                    $scope.loading_place = false;

                    // if first place, add starting marker
                    // if($scope.game.place_start == null) {
                    // 	$scope.place.start = true;
                    // 	$scope.game.place_start = $scope.place.id;
                    // 	$scope.updatePlace();
                    // }

                });

                /**********************
                * Auto adjust map size
                ***********************/

                cols = $scope.unit.rows[0].cols.length;
                rows = $scope.unit.rows.length;

                // add row
                if(row_index == 0) addRow(true);
                else if(row_index == $scope.unit.rows.length - 1) addRow();
                function addRow(prepend) {
                    data = {
                        map_id : $scope.unit.id,
                        cols : cols
                    }
                    if(prepend !== undefined) {
                        data.prepend = true;
                        method = 'unshift';
                    }
                    else method = 'push';
                    $http.post('/Row', data).then(function(row) {
                        $scope.unit.rows[method](row);
                    });
                };

                // add column
                if(col_index == 0) addCol(true);
                else if(col_index == $scope.unit.rows[row_index].cols.length - 1) addCol();
                function addCol(prepend) {
                    data = {
                        map_id : $scope.unit.id,
                        row_id : row_id,
                        rows : rows,
                    };
                    if(prepend !== undefined) {
                        data.prepend = true;
                        method = 'unshift';
                    }
                    else method = 'push';
                    $http.post('/Row', data).then(function(cols) {
                        angular.forEach($scope.unit.rows, function(row, row_key) {
                            angular.forEach(cols, function(col) {
                                if(col.row_id == row.id) {
                                    $scope.unit.rows[row_key].cols[method](col);
                                }
                            });
                        });
                    });
                };

            };

            // set starting place
            $scope.clickPlace = function(place) {

                $scope.place = place;
                var btn = false;

                // set starting place
                if($scope.set_start) {
                    btn = true;

                    // remove old starting place
                    angular.forEach($scope.unit.rows, function(row, row_index) {
                        angular.forEach(row.cols, function(col, col_index) {
                            if(col.place !== null) {
                                if(col.place.start) {
                                    col.place.start = 0;
                                    $scope.place = col.place;
                                    $scope.updatePlace();
                                }
                            }
                        });
                    });
                    place.start = 1;
                    $scope.updatePlace();
                    $scope.set_start = false;
                }

                // top border
                if($scope.border_top) {
                    btn = true;
                    $scope.place.block_top = $scope.place.block_top ? 0 : 1;
                    $scope.updatePlace();
                }

                // right border
                if($scope.border_right) {
                    btn = true;
                    $scope.place.block_right = $scope.place.block_right ? 0 : 1;
                    $scope.updatePlace();
                }

                // bottom border
                if($scope.border_bottom) {
                    btn = true;
                    $scope.place.block_bottom = $scope.place.block_bottom ? 0 : 1;
                    $scope.updatePlace();
                }

                // left border
                if($scope.border_left) {
                    btn = true;
                    $scope.place.block_left = $scope.place.block_left ? 0 : 1;
                    $scope.updatePlace();
                }

                // edit place
                if(!btn) {
                    $scope.edit(place, 'place');
                }

            }

            // update place
            $scope.updatePlace = function() {
                $http.post('/Place/update/' + $scope.place.id, $scope.place);
            }

            // delete place
            $scope.deletePlace = function(id) {
                $http.post('/Base/delete/Place', [id]).then(function(result) {
                    console.log($scope.unit);
                    eval($scope.unit.path + ' = null;');
                    $scope.togglePopup('#placeForm');
                });
            };

        }
    }
});
