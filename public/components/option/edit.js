angular.module('app')
.directive('mgoption', function() {
    return {
        templateUrl : '/components/option/edit.html',
        controller: function($scope, $http) {

            /***************
            * Options
            ***************/

            // create option
            $scope.createObject = function() {
                $scope.loading_option = true;
                $scope.option = {
                    game_id : game_id,
                    morph_type : 'Uvent',
                    morph_id : $scope.uvent.id,
                    result_id : 0,
                    result_type : 'Uvent'
                };
                $http.post('/option', $scope.option).then(function(result) {
                    result = $scope.setPaths(result);
                    $scope.uvent.options.push(result);
                    $scope.editObject(result.id);
                    $scope.loading_option = false;
                });
            };

            // edit option
            $scope.editObject = function(id) {
                record_found = false;
                getObject($scope.uvent, 'option', id)
                $scope.togglePopup('#optionForm');
            };

            // update option
            $scope.updateObject = function() {
                $http.put('/option/' + $scope.option.id, $scope.option);
            };

            // delete option
            $scope.deleteOption = function(id, index) {
                $http.delete('/option/' + id);
                $scope.uvent.options.splice(index, 1);
            };

        }
    }
});
