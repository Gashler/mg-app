angular.module('app')
.directive('mgiconedit', function() {
    return {
        templateUrl : '/components/icon/edit.html',
        controller: function($scope, $http) {

            /***************
            * Icons
            ***************/

            // set ranges for "for" loops
            $scope.range = function(min, max, step) {
                step = step || 1;
                var input = [];
                for (var i = min; i <= max; i += step) {
                    input.push(i);
                }
                return input;
            };

            // assign icon
            $scope.assignIcon = function(str) {
                $scope.unit.icon = str;
                $scope.togglePopup('#iconForm', false);
                $scope.update(unit);
            }

        }
    }
});
