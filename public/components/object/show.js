angular.module('app')
.directive('objectshow', function() {
    return {
        templateUrl : '/components/object/show.html',
        controller: function($scope, $http) {

            /****************
            * Show Object
            *****************/

            $scope.objectShow = function(unit) {
                $scope.object = unit;
                $scope.togglePopup('#objectShow');
            }

            // drop on object
            $scope.dropOnObject = function(object_index, item) {
                object = $scope.place.objects[object_index --];
                $scope.runLogic(object, item);
            }

        }
    }
});
