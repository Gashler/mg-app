angular.module('app')
.directive('objectedit', function() {
    return {
        templateUrl : '/components/object/edit.html',
        controller: function($scope, $http) {

            /***************
            * Objects
            ***************/

            // create object
            $scope.createObject = function() {
                $scope.create({
                    name: 'Object ' + ($scope.unit.objects.length + 1),
                    game_id: game_id,
                    morph_type: ucfirst($scope.unit.model),
                    morph_id: $scope.unit.id
                }, 'object');
            };

        }
    }
});
