angular.module('app')
.directive('characteredit', function() {
    return {
        templateUrl : '/components/character/edit.html',
        controller: function($scope, $http) {

            /***************
            * Characters
            ***************/

            // create character
            $scope.createCharacter = function(place_id, gender, type) {
                $scope.loading_character = true;

                // determine type
                if(type == null) type = 'character';

                // if gender is unspecified, randomize gender
                if((type == null || type == 'character') && gender == null) {
                    rand = Math.floor((Math.random() * 1));
                    if(rand == 0) gender = 'm';
                    else gender = 'f';
                };

                // create player with spouse's gender
                if(type == 'player') {
                    if($scope.game.players[0].gender == 'm') gender = 'f';
                    else gender = 'm';
                }

                // determine player_id
                if($scope.unit.model == 'player') {
                    var player_id = $scope.unit.id;
                }
                else {
                    var player_id = null;
                }

                $scope.create({
                    'first_name': 'New Character',
                    'game_id': game_id,
                    'gender': gender,
                    'place_id': $scope.unit.id,
                    'player_id': player_id,
                    'type': type
                }, 'character');
            };
        }
    }
});
