angular.module('app')
.directive('charactershow', function() {
    return {
        templateUrl : '/components/character/show.html',
        controller: function($scope, $http) {

            /****************
            * Show Character
            *****************/

            $scope.characterShow = function(character) {
                $scope.character = character;
                $scope.togglePopup('#characterShow');
            }

            // drop on character
            $scope.dropOnCharacter = function(character_index, item) {
                character = $scope.place.characters[character_index --];
                $scope.runLogic(character, item);
            }

        }
    }
});
