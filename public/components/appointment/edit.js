(function(){
'use strict';
angular.module('app')
.directive('appointmentEdit', function() {
    return {
        restric: 'E',
        templateUrl: '/components/appointment/edit.html',
        controller: function($scope, $http) {

            /***************
            * Appointments
            ***************/

            // create appointment
            $scope.createAppointment = function() {
                console.log('$scope.unit = ', $scope.unit);
                $scope.loading_appointment = true;

                // determine default turn value
                if($scope.unit.appointments.length == 0) {
                    var turn = 'alternate';
                }

                // post data
                $http.post('/Appointment/store', {
                    account_id: $scope.user.account_id,
                    date_start: $scope.unit.date
                }).then(function(response) {
                    $scope.unit.appointments.push(response.data);
                    $scope.edit(response.data, 'appointment');
                    $scope.loading_appointment = false;
                });
            };
        }
    }
});
})();
