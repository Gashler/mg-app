angular.module('app')
.directive('uventplay', function() {
    return {
        templateUrl : '/components/uvent/play.html',
        controller: function($scope, $http) {

            /*************************
            * Uvents
            **************************/

            // get starting uvent
            if($scope.game.uvent_current !== null) $scope.getObject('uvent', $scope.game.uvent_current);
            else $scope.uvent = $scope.game.uvents[0];

            // go to next uvent
            $scope.nextUvent = function(result_id) {
                changeUvent(result_id);
            };

            // change uvent
            function changeUvent(id) {
                $scope.getObject('uvent', id);
                $scope.game.uvent_current = $scope.uvent.id;
                updateGame([{
                    uvent_current : $scope.game.uvent_current
                }]);
            }

        }
    }
});
