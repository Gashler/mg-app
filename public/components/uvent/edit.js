angular.module('app')
.directive('uventedit', function() {
    return {
        templateUrl : '/components/uvent/edit.html',
        controller: function($scope, $http) {

            /***************
            * Uvents
            ***************/

            // create uvent
            $scope.createUvent = function() {
                $scope.loading_uvent = true;

                // determine default turn value
                if($scope.unit.uvents.length == 0) turn = 'alternate';

                // prepare data
                $scope.uvent = {
                    morph_type : $scope.unit.model_path,
                    morph_id : $scope.unit.id,
                    result_id : 0,
                    result_type : 'Uvent',
                    turn : $scope.unit.turn
                };

                // post data
                $http.post('/Uvent', $scope.uvent).then(function(result) {
                    result = $scope.setPaths(result, 'uvent');
                    $scope.unit.uvents.push(result);
                    $scope.edit(result, 'uvent');
                    $scope.loading_uvent = false;
                });
            };
        }
    }
});
