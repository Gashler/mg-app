<?php namespace

use Auth;
use Config;
use DB;
use Input;
use Eloquent;
use Redirect;
use Validator;
use View;

class [Model]Controller extends BaseController {

    // all [models]
    public function getAll() {
        return [Model]::all();
    }

    // get [model]
    public function getGet($id) {
        return [Model]::find($id);
    }

    // store [model]
    public function postStore() {
        $validator = Validator::make($data = Input::all(), [Model]::$rules);
        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        return [Model]::create($data);
    }

    // update [model]
    public function postUpdate($id) {
        $[model] = [Model]::findOrFail($id);
        $validator = Validator::make($data = Input::all(), [Model]::$rules);
        if ($validator->fails())
        {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        $[model]->update($data);
        return Model::find($[model]->id);
    }

}
